//
//  PlaybackListener.cpp
//
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#include "PlaybackListener.h"

PlaybackListener::PlaybackListener(id<PlayerCallbackHandler> handler)
{
    final_number_of_clips = -1;
    current_clip_index = 0;
    mHandler = handler;
    mClips = NULL;
}

PlaybackListener::~PlaybackListener()
{
    
}

void PlaybackListener::updateClips(NSMutableArray *  newClips)
{
    mClips = newClips;
}

void PlaybackListener::updateFinalClipCount(int clip_count)
{
    DLog(@"set final number of clips: %d", clip_count);
    final_number_of_clips = clip_count;
}

void PlaybackListener::notify(int msg, int ext1, int ext2)
{
    if (mHandler != nil) {
        [mHandler handleMessage:msg
                          ext1:ext1
                          ext2:ext2];
    }
}

int PlaybackListener::getNextClip(char** url_cstr)
{
    DLog(@"%s: %d, %d", __FUNCTION__, final_number_of_clips, current_clip_index);
    if (final_number_of_clips == [mClips count] &&
        current_clip_index >= final_number_of_clips-1 )
    {
        return MEDIA_PLAYBACK_STATUS_COMPLETE;
    }
    
    
    if (current_clip_index >= [mClips count]) {
        return MEDIA_PLAYBACK_STATUS_IN_PROGRESS;
    }
    else {
        current_clip_index ++;

        NSString *current_clip = [mClips objectAtIndex:current_clip_index];
        
        *url_cstr = (char *) malloc( [current_clip length] * sizeof(char));
        strcpy(*url_cstr, (char *) [current_clip UTF8String]);
        
        return MEDIA_PLAYBACK_STATUS_STARTED;
    }
}
