//
//  CameraCommand.h
//  App
//
//  Created by Tran Kien Nghi on 4/29/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

typedef enum: NSInteger {
    DemoCommandTypeBitRate = 0,
    DemoCommandTypeBitRateStationary,
    DemoCommandTypeBitRateMoving,
    DemoCommandTypeFrameRate,
    DemoCommandTypeQP,
    DemoCommandTypeRC,
    DemoCommandTypeGOP,
    DemoCommandTypeResolution,
    DemoCommandTypeFlicker,
    DemoCommandTypeWifiChannel,
    DemoCommandTypeAdaptive,
    DemoCommandTypePlayMelody,
    DemoCommandTypeStopMelody,
    DemoCommandTypeBattery,
    
    DemoCommandTypeTalkback     = 20,
    DemoCommandTypeStartTalkback, //21
    DemoCommandTypeStopTalkback,  //22
    
    DemoCommandTypeHandsInit = 30, // Another hand cmd?
    
    DemoCommandTypeFindCamInLAN = 40, //
    DemoCommandTypeConnCamInLAN = 41, //
    
    DemoCommandTypeHand         = 100,
    DemoCommandTypeHandClaw     = 110,
    DemoCommandTypeHandClawClose, //111
    DemoCommandTypeHandClawOpen,  //112
    
    DemoCommandTypeHandS        = 120,
    DemoCommandTypeHandSUp,     //121
    DemoCommandTypeHandSDown,   //122
    
    DemoCommandTypeHandH        = 130,
    DemoCommandTypeHandHUp,     //131
    DemoCommandTypeHandHDown,   //132
    
    DemoCommandTypeHandW        = 140,
    DemoCommandTypeHandWLeft,   //141
    DemoCommandTypeHandWRight,  //142
    
    DemoCommandTypeHandABit     = 200,
    DemoCommandTypeHandClawABit = 210,
    DemoCommandTypeHandClawCloseABit, //21
    DemoCommandTypeHandClawOpenABit,  //22
    
    DemoCommandTypeHandSABit    = 220,
    DemoCommandTypeHandSUpABit,     //41
    DemoCommandTypeHandSDownABit,   //42
    
    DemoCommandTypeHandHABit    = 230,
    DemoCommandTypeHandHUpABit,     //51
    DemoCommandTypeHandHDownABit,   //52
    
    DemoCommandTypeHandWABit    = 240,
    DemoCommandTypeHandWLeftABit,   //61
    DemoCommandTypeHandWRightABit,  //62
    
    DemoCommandTypeMove         = 300,
    DemoCommandTypeMoveForward, //301
    DemoCommandTypeMoveBackward,//302
    DemoCommandTypeMoveLeft,    //303
    DemoCommandTypeMoveRight,   //304
    
    DemoCommandTypeMoveABit     = 400,
    DemoCommandTypeMoveForwardABit, //91
    DemoCommandTypeMoveBackwardABit,//92
    DemoCommandTypeMoveLeftABit,    //93
    DemoCommandTypeMoveRightABit,   //94
    
    DemoCommandTypeStop         = 500,
    DemoCommandTypeFBStop,       //= 500,//23
    DemoCommandTypeStopClaw,
    DemoCommandTypeStopS,
    DemoCommandTypeStopH,
    DemoCommandTypeStopW,
    
    DemoCommandTypeSPKVolume    = 600,
    DemoCommandTypeVideoConfig,
    
    DemoCommandTypeMotorNoise   = 610,
    
    DemoCommandTypeFWVersion     = 700,
    
    DemoCommandTypeRecording     = 800,
    DemoCommandTypeStartRecording = 801,
    DemoCommandTypeStopRecording = 802,
    
    DemoCommandTypeTakeSnapshot = 900,
    
    DemoCommandTypeBoundary             = 1000,
    DemoCommandTypeBoundaryPosition     = 1001,
    
    DemoCommandTypeAppDateTime     = 1100,
    
    DemoCommandTypeFlipup     = 1200,
    
    DemoCommandTypeFlightResponse     = 1300,
    
    DemoCommandTypeBroadcastPeriod = 1400,
    DemoCommandTypeScanTimer,
    
} DemoCommandType;

@interface CameraCommand : NSObject

@property (copy, nonatomic) NSString* setCommand;
@property (copy, nonatomic) NSString* getCommand;
@property (copy, nonatomic) NSString* commandTitle;
//@property (copy, nonatomic, setter=setCurrentValue:) NSString* currentValue;
@property (copy, nonatomic) NSString* currentValue;
@property (copy, nonatomic) NSString* errorValue;
@property (strong, nonatomic) NSArray* valueList;
@property (nonatomic) DemoCommandType commandType;

//for UI handling
@property (assign, nonatomic) BOOL isLoading;

+ (instancetype)commandWithGet:(NSString*)getCommand set:(NSString*)setCommand title:(NSString*)commandTitle currentValue:(NSString*)currentValue valueList:(NSArray*)valueList isLoading:(BOOL)loading commandType:(DemoCommandType)cmdType;

- (BOOL)isBooleanValues;
- (void)restoreCurrentValue;
//- (void)setCurrentValue:(NSString *)currentValue;

@end
