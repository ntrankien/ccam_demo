//
//  H264PlayerListener.h
//  App
//
//  Created by Developer on 18/9/13.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#ifndef __App__H264PlayerListener__
#define __App__H264PlayerListener__

#include <H264MediaPlayer/mediaplayer.h>
#import "PlayerCallbackHandler.h"

class H264PlayerListener: public MediaPlayerListener
{

public:
    ~H264PlayerListener();
    H264PlayerListener(id<PlayerCallbackHandler> handler);
    
    void notify(int msg, int ext1, int ext2);
    int getNextClip(char**);
    
private:
    id<PlayerCallbackHandler> mHandler;
    
};

#endif
