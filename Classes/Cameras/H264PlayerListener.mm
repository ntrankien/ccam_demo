//
//  H264PlayerListener.mm
//  App
//
//  Created by Developer on 18/9/13.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#include "H264PlayerListener.h"

H264PlayerListener::H264PlayerListener(id<PlayerCallbackHandler> handler)
{
    mHandler = handler;
}

H264PlayerListener::~H264PlayerListener()
{
   
}

void H264PlayerListener::notify(int msg, int ext1, int ext2)
{
    if (mHandler != nil) {
        [mHandler handleMessage:msg ext1:ext1 ext2:ext2];
    }
}

int H264PlayerListener::getNextClip(char** url)
{
	//TODO
	return MEDIA_PLAYBACK_STATUS_IN_PROGRESS;
}
