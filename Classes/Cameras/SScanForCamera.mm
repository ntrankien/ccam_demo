//
//  ScanForCamera.m
//  MBP_ios
//
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "SScanForCamera.h"

@interface SScanForCamera ()

@property (nonatomic, strong) AsyncUdpSocket *udpSock;
@property (nonatomic, strong) AsyncUdpSocket *udpSSock;
@property (nonatomic, copy) NSString *tmpMac;

@property (nonatomic, strong) NSTimer *timerTimeout;

@end

@implementation SScanForCamera

#define LUCY_QUERY_REQUEST_STRING   @"Lucy-CamQUERY   *               "

- (id)init
{
	self = [super init];
	self.next_profile_index = 0;
	self.bc_addr = @"";
	self.own_addr = @"";
	self.scan_results = [[NSMutableArray alloc] init];
    self.notifier = nil;
    
	return self;
}

- (id)initWithNotifier:(id<ScanForCameraNotifier>)caller
{
    self = [super init];
	self.next_profile_index = 0;
	self.bc_addr = @"";
	self.own_addr = @"";
	self.scan_results = [[NSMutableArray alloc] init];
    self.notifier = caller;

    return self;
}

- (void)dealloc
{
    _udpSock.delegate = nil;
    _udpSSock.delegate = nil;
}

// scan for a specific device
// @mac : mac address with colon eg: 11:22:33:44:55:66
- (void)scan_for_device:(NSString *)mac
{

	///GET IP here
	NSString *bc = @"";
	NSString *own = @"";
	[SScanForCamera getBroadcastAddress:&bc AndOwnIp:&own];
	self.bc_addr = [NSString stringWithString:bc];
	self.own_addr = [NSString stringWithString:own];
	
	NSString *str =  LUCY_QUERY_REQUEST_STRING;
	NSString *my_ip_str = self.own_addr;
	NSString *blank_str = @" ";
	int i = 0;
    NSInteger blank_chars = 16 - [my_ip_str length];
    
	if (blank_chars >0) {
		for (i = 0; i < blank_chars; i++){
			my_ip_str= [my_ip_str stringByAppendingString:blank_str];
		}
	}
	
	str = [str stringByAppendingString:my_ip_str];
	str = [str substringToIndex:47];
	
	// add mac address
	str = [str	stringByAppendingString:[mac lowercaseString]]; 
	
	DLog(@"scan 1 dev req: %@", str);
	NSData* bytes = [str dataUsingEncoding:NSUTF8StringEncoding];
	
	@synchronized (self)
	{
		self.deviceScanInProgress = YES;
	}
	
	self.next_profile_index = 0;
	[_scan_results removeAllObjects];
	
    // Receiving socket
	self.udpSock = [[AsyncUdpSocket alloc] initIPv4];
	[_udpSock setDelegate:self];
    [_udpSock bindToPort:10001 error:nil];
	[_udpSock receiveWithTimeout:4 tag:1];
	
	// Sending socket
	self.udpSSock = [[AsyncUdpSocket alloc] initWithDelegate:self];
	
	// Broadcast
	[_udpSSock enableBroadcast:YES error: nil];
	[_udpSSock sendData:bytes toHost:self.bc_addr port:10000 withTimeout:1 tag:1];
    
    self.timerTimeout = [NSTimer scheduledTimerWithTimeInterval:5
                                                         target:self
                                                       selector:@selector(scan_for_devices_done)
                                                       userInfo:nil
                                                        repeats:NO];
}

- (void)scan_for_device_in_background:(NSString *)mac
{
    [self scan_for_device:mac];
    [[NSRunLoop currentRunLoop] addTimer:_timerTimeout
                                 forMode:NSDefaultRunLoopMode];
    [[NSRunLoop currentRunLoop] run];
}

- (void)scan_for_devices_done
{

    //[NSTimer cancelPreviousPerformRequestsWithTarget:self selector:@selector(fireScanForDevicesDone:) object:nil];
    if (_timerTimeout) {
        [_timerTimeout invalidate];
        self.timerTimeout = nil;
    }
    
	@synchronized (self) {
		self.deviceScanInProgress = NO;
	}
    
    [self scan_done_notify];
}

- (void)fireScanForDevicesDone:(NSTimer *)timer
{
    DLog(@"\n\n\n\n%s mac:%@", __FUNCTION__, _tmpMac);
    [self scan_for_devices_done];
}

- (void)scan_done_notify
{
    NSArray *outArray = nil;
    
    if (_notifier != nil &&
        [_notifier isKindOfClass:[UIViewController class]] &&
        [_notifier respondsToSelector:@selector(scan_done:)])
    {
        if (_scan_results == nil) {
            outArray =[[NSArray alloc] init];
        }
        else {
            outArray = [[NSArray alloc] initWithArray:_scan_results copyItems:NO];
        }
        
        [_notifier scan_done:outArray];
    }
    else {
        DLog(@"Scan Done without notifier");
    }
}

- (BOOL)getResults:(NSArray **)out_Array
{
	if (_deviceScanInProgress == YES) {
		return NO;
	}
    
	if (_scan_results == nil) {
		*out_Array =[[NSArray alloc] init];
	}
	else {	
		*out_Array = [[NSArray alloc] initWithArray:_scan_results copyItems:NO];
	}
	
	return YES;
}

- (void)cancel
{
    self.notifier = nil; 
}

#pragma mark -- UDP delegate

// Called when the datagram with the given tag has been sent.
- (void)onUdpSocket:(AsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
	/* close socket */
	[sock close];
}

// Called if an error occurs while trying to send a datagram.
// This could be due to a timeout, or something more serious such as the data being too large to fit in a sigle packet.
- (void)onUdpSocket:(AsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error;
{
	DLog(@"UDP Socket error: %li  localhost:%@", (long)error.code, [sock localHost]);
}

// Called when the socket has received the requested datagram.
// Under normal circumstances, you simply return YES from this method.
- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock didReceiveData:(NSData *)data withTag:(long)tag fromHost:(NSString *)host port:(UInt16)port
{
	NSString *data_str = [NSString stringWithUTF8String:(char*)[data bytes]];
	DLog(@"000 rcv fr: %@ : msg: %@", host, data_str);
	
	/* verify signature */
	if ([data_str hasPrefix:@"Lucy-Cam"]) {
		CamProfile *newProfile = [CamProfile alloc];
		[newProfile initWithResponse:data_str andHost:host];
		
		BOOL isFound = NO;
		int i;
        
		for (i = 0; i < _next_profile_index; i++) {
			CamProfile *bb = (CamProfile *)[_scan_results objectAtIndex:i];
			
			if ( [bb.mac_address isEqualToString:newProfile.mac_address] ) {
				isFound = YES; 
				break;
			}
		}
		
        
		if ( !isFound ) {
            DLog(@"002 insert: %@",newProfile.mac_address);
			[_scan_results insertObject:newProfile atIndex:self.next_profile_index];
			self.next_profile_index++;
		}
	}
	
	if (self.next_profile_index <64) {
		/* try again until we failed */
		[sock receiveWithTimeout:2 tag:1];
	}
	
	return YES;
}

// Called if an error occurs while trying to receive a requested datagram.
// This is generally due to a timeout, but could potentially be something else if some kind of OS error occurred.
- (void)onUdpSocket:(AsyncUdpSocket *)sock didNotReceiveDataWithTag:(long)tag dueToError:(NSError *)error
{
	DLog(@"RCV data err: %li (%@)", (long)error.code, error.localizedDescription);
	
	/* close socket */
	[sock close];

    [self scan_for_devices_done];
}

// Called when the socket is closed.
// A socket is only closed if you explicitly call one of the close methods.
- (void)onUdpSocketDidClose:(AsyncUdpSocket *)sock
{
}

#pragma mark - Crazy

+ (void)getBroadcastAddress:(NSString **) bcast AndOwnIp:(NSString**) ownip
{
	// Free & re-init Addresses
	FreeAddresses();
    
	GetIPAddresses();
	GetHWAddresses();
	NSString *deviceBroadcastIP = nil;
	NSString *deviceIP = nil ;
    
	NSString *log = @"";
	int i;
    
	for (i = 0; i < MAXADDRS; ++i) {
		static unsigned long localHost = 0x7F000001;		// 127.0.0.1
		unsigned long theAddr;
        
		theAddr = ip_addrs[i];
        
		if (theAddr == INVALID_IP) {
			break;
		}
        
		if (theAddr == localHost) {
            continue;
        }
        
		if (strncmp(if_names[i], "en", strlen("en")) == 0) {
			deviceBroadcastIP =  [NSString stringWithFormat:@"%s", broadcast_addrs[i]];
			deviceIP = [NSString stringWithFormat:@"%s", ip_names[i]];
		}
        
		log = [log stringByAppendingFormat:@"%d %s %s %s %s\n", i, if_names[i], hw_addrs[i], ip_names[i],
               broadcast_addrs[i]];
	}
    
	if (deviceIP != nil) {
		*ownip = [NSString stringWithString:deviceIP];
	}
    
	if (deviceBroadcastIP != nil) {
		*bcast = [NSString stringWithString:deviceBroadcastIP];
	}
}

@end
