//
//  CameraCommandManager.m
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/14/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraCommandManager.h"

@interface CameraCommandManager()

@property (nonatomic, strong) NSArray* commandList;

@end

@implementation CameraCommandManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t p = 0;
    
    __strong static CameraCommandManager* _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
        
        _sharedObject.commandList = @[[CameraCommand commandWithGet:@"" set:@"move_forward" title:@"Move Forward" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeMoveForward],
                                      [CameraCommand commandWithGet:@"" set:@"move_backward" title:@"Move Backward" currentValue:@""valueList:@[] isLoading:YES commandType:DemoCommandTypeMoveBackward],
                                      [CameraCommand commandWithGet:@"" set:@"move_left" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeMoveLeft],
                                      [CameraCommand commandWithGet:@"" set:@"move_right" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeMoveRight],
                             
                                      [CameraCommand commandWithGet:@"" set:@"c_close" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandClawClose],
                                      [CameraCommand commandWithGet:@"" set:@"c_open" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandClawOpen],
                             
                                      [CameraCommand commandWithGet:@"" set:@"inch_c_close" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandClawCloseABit],
                                      [CameraCommand commandWithGet:@"" set:@"inch_c_open" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandClawOpenABit],
                             
                                      [CameraCommand commandWithGet:@"" set:@"s_up" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandSUp],
                                      [CameraCommand commandWithGet:@"" set:@"s_down" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandSDown],
                             
                             
                                      [CameraCommand commandWithGet:@"" set:@"inch_s_up" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandSUpABit],
                                      [CameraCommand commandWithGet:@"" set:@"inch_s_down" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandSDownABit],
                             
                                      [CameraCommand commandWithGet:@"" set:@"h_up" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandHUp],
                                      [CameraCommand commandWithGet:@"" set:@"h_down" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandHDown],
                             
                                      [CameraCommand commandWithGet:@"" set:@"inch_h_up" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandHUpABit],
                                      [CameraCommand commandWithGet:@"" set:@"inch_h_down" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandHDownABit],
                             
                                      [CameraCommand commandWithGet:@"" set:@"w_left" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandWLeft],
                                      [CameraCommand commandWithGet:@"" set:@"w_right" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandWRight],
                             
                                      [CameraCommand commandWithGet:@"" set:@"inch_w_left" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandWLeftABit],
                                      [CameraCommand commandWithGet:@"" set:@"inch_w_right" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandWRightABit],
                             
                                      [CameraCommand commandWithGet:@"" set:@"fb_stop" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeFBStop],
                                      [CameraCommand commandWithGet:@"" set:@"audio_out1" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeStartTalkback],
                                      [CameraCommand commandWithGet:@"" set:@"audio_out0" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeStopTalkback],
                             
                                      [CameraCommand commandWithGet:@"" set:@"inch_forward" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeMoveForwardABit],
                                      [CameraCommand commandWithGet:@"" set:@"inch_backward" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeMoveBackwardABit],
                                      [CameraCommand commandWithGet:@"" set:@"inch_left" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeMoveLeftABit],
                                      [CameraCommand commandWithGet:@"" set:@"inch_right" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeMoveRightABit],
                             
                                      [CameraCommand commandWithGet:@"get_boundary" set:@"set_boundary" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeBoundary],
                                      [CameraCommand commandWithGet:@"get_boundary_position" set:@"get_boundary_position" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeBoundaryPosition],
                             
                                      [CameraCommand commandWithGet:@"" set:@"c_stop" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeStopClaw],
                                      [CameraCommand commandWithGet:@"" set:@"s_stop" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeStopS],
                                      [CameraCommand commandWithGet:@"" set:@"h_stop" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeStopH],
                                      [CameraCommand commandWithGet:@"" set:@"w_stop" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeStopW],
                                      [CameraCommand commandWithGet:@"" set:@"hands_init" title:@"" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeHandsInit],
                             
                                      [CameraCommand commandWithGet:@"get_spk_volume" set:@"set_spk_volume" title:@"" currentValue:@"0" valueList:@[] isLoading:YES commandType:DemoCommandTypeSPKVolume],
                             
                                      [CameraCommand commandWithGet:@"" set:@"sd_start_recording" title:@"Start Recording" currentValue:@"2" valueList:@[@"1", @"2"] isLoading:YES commandType:DemoCommandTypeStartRecording],
                                      [CameraCommand commandWithGet:@"" set:@"sd_stop_recording" title:@"Stop Recording" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeStopRecording],
                             
                                      [CameraCommand commandWithGet:@"get_image_snapshot" set:@"" title:@"Take Snapshot" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeTakeSnapshot],
                                      
                                      [CameraCommand commandWithGet:@"value_flipup" set:@"set_flipup" title:@"Flip up" currentValue:@"" valueList:@[@"0", @"1"] isLoading:YES commandType:DemoCommandTypeFlipup],
                                      
                                      [CameraCommand commandWithGet:@"get_movement_noise" set:@"set_movement_noise" title:@"Motor Noise" currentValue:@"" valueList:@[@"0", @"1"] isLoading:YES commandType:DemoCommandTypeMotorNoise],
                                      
                                      [CameraCommand commandWithGet:@"get_flicker" set:@"set_flicker" title:@"Flicker" currentValue:@"" valueList:@[@"50", @"60"] isLoading:YES commandType:DemoCommandTypeFlicker],
                                      
                                      [CameraCommand commandWithGet:@"get_wifi_channel" set:@"set_wifi_channel" title:@"Wifi Channel" currentValue:@""
                                                          valueList:@[@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12"] isLoading:YES commandType:DemoCommandTypeWifiChannel],
                                      
                                      [CameraCommand commandWithGet:@"get_video_bitrate" set:@"set_video_bitrate" title:@"Video Bit Rate" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeBitRate],
                                      [CameraCommand commandWithGet:@"get_video_bitrate" set:@"set_video_bitrate" title:@"Stationary Video Bit Rate" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeBitRateStationary],
                                      [CameraCommand commandWithGet:@"get_video_bitrate" set:@"set_video_bitrate" title:@"Moving Video Bit Rate" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeBitRateMoving],
                                      
                                      [CameraCommand commandWithGet:@"get_adaptive" set:@"set_adaptive" title:@"Adaptive Video Quality" currentValue:@"" valueList:@[@"0", @"1"] isLoading:YES commandType:DemoCommandTypeAdaptive],
                                      
                                      [CameraCommand commandWithGet:@"get_video_framerate" set:@"set_video_framerate" title:@"Video Frame Rate" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeFrameRate],
                                      
                                      [CameraCommand commandWithGet:@"value_battery" set:@"value_battery" title:@"Battery" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeBattery],
                                      
                                      [CameraCommand commandWithGet:@"get_version" set:@"" title:@"Firmware version" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeFWVersion],
                                      [CameraCommand commandWithGet:@"" set:@"set_date" title:@"App date time" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeAppDateTime],
                                      
                                      [CameraCommand commandWithGet:@"get_video_qp" set:@"set_video_qp" title:@"Video QP" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeQP],
                                      
                                      [CameraCommand commandWithGet:@"get_video_rc" set:@"set_video_rc" title:@"Video RC" currentValue:@"" valueList:@[@"0", @"1"] isLoading:YES commandType:DemoCommandTypeRC],
                                      
                                      [CameraCommand commandWithGet:@"get_video_gop" set:@"set_video_gop" title:@"Video GOP" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeGOP],
                                      
                                      //[CameraCommand commandWithGet:@"get_resolution" set:@"set_resolution" title:@"Resolution" currentValue:@"" valueList:@[@"720p", @"480p", @"360p"] isLoading:YES commandType:DemoCommandTypeResolution],
                                      [CameraCommand commandWithGet:@"get_resolution" set:@"set_resolution" title:@"Resolution" currentValue:@"" valueList:@[@"720p", @"1080p"] isLoading:YES commandType:DemoCommandTypeResolution],
                                      
                                      [CameraCommand commandWithGet:@"" set:@"set_flight_response" title:@"Flight Response" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeFlightResponse],
                                      
                                      [CameraCommand commandWithGet:@"get_video_config" set:@"set_video_config" title:@"Video Config" currentValue:@"" valueList:@[@"low", @"medium", @"high"] isLoading:YES commandType:DemoCommandTypeVideoConfig],
                                      
                                      [CameraCommand commandWithGet:@"get_broadcast_period" set:@"set_broadcast_period" title:@"Broadcast Period" currentValue:@"100" valueList:@[] isLoading:YES commandType:DemoCommandTypeBroadcastPeriod],
                                      [CameraCommand commandWithGet:@"get_scan_timer" set:@"set_scan_timer" title:@"Router Scan Timer" currentValue:@"" valueList:@[] isLoading:YES commandType:DemoCommandTypeScanTimer],
                                      ];
    });
    
    return _sharedObject;
}

- (CameraCommand*)commandWithType:(DemoCommandType)cmdTypel{
    for (CameraCommand* cmd in _commandList) {
        if (cmd.commandType == cmdTypel) {
            return cmd;
        }
    }
    return nil;
}

@end
