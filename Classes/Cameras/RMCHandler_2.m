//
//  RMCHandler.m
//  LucyDemo
//
//  Created by Kronus on 9/30/16.
//  Copyright © 2016 LucyDemo. All rights reserved.
//

#import "RMCHandler_2.h"
#include <sys/socket.h>
#include <arpa/inet.h>

// Other
#include <ifaddrs.h>
// Other

// Handshake
#define MAX_HANDSHAKE_BUF_SIZE_2      1500
#define HANDSHAKE_BUF_SIZE_2          1500
#define MIN_HANDSHAKE_BUF_SIZE_2        80
#define MAX_HANDSHAKE_TIME_2       5

typedef NS_ENUM(NSInteger, PRHandshakeResult_2) {
    PRHandshakeFailure_2 = -1,
    PRHandshakeSuccess_2,
    PRHandshakeEPIPE_2,
};


@interface RMCHandler_2 ()

@property (nonatomic)       PConfigInfo *p2pInfo; // <--
@property (nonatomic, copy) NSString    *camMac; // <--

@property (nonatomic) NSInteger timeReceiveData;
@property (nonatomic) void      *p2pSess;

@property (nonatomic, strong) NSDate         *p2pTime;
@property (nonatomic)         NSTimeInterval tmp;
@property (nonatomic, strong) NSTimer        *timerRecvP2PData;
@property (nonatomic, strong) NSTimer        *timerBufferingTimeout;

@property (nonatomic, copy) NSString *streamName;
@property (nonatomic, copy) NSString *streamMode;

// Debug
@property (nonatomic, strong) NSMutableString *debugTbLog;
@property (nonatomic, strong) NSDate *debugTbDate;
// Debug <--

@property (nonatomic, copy) NSString* camUDID;
@property (nonatomic, strong) NSDictionary* p2pNotificationInfo;

@end

@implementation RMCHandler_2


#pragma mark -
#pragma mark RMC Callbacks
// Ouput -->
void writeFile_2(void* data, uint32_t size, eMediaSubType type, uint32_t ts, void* priv)
{
    RMCHandler_2 *tmpRMC = (__bridge RMCHandler_2 *)priv;
    tmpRMC.tmp = NSDate.timeIntervalSinceReferenceDate;
    
    
    id<RmcHanlderDataDelegate_2> tmpDelegate = tmpRMC.delegate2;
    
//    NSLog(@">>>>>  got data for : tmpRMC:%p mac:%@ mode:%d / %@,",tmpRMC, tmpRMC.camMac, tmpRMC.p2pMode, tmpRMC.streamMode);
    
    if (tmpDelegate) {
        if (type / 10 == eMediaTypeAudio) {
            [tmpDelegate didRecveAudioData:data withSize:size timestamp:ts];
        }
        else if ((eMediaSubTypeVideoJPEG > type) && (type / 10 == eMediaTypeVideo)) {
            [tmpDelegate didRecveVideoData:data withSize:size idr:(type == eMediaSubTypeVideoIFrame) timestamp:ts];
        }
        else {
            [tmpDelegate didRecveOtherData:data withSize:size type:type timestamp:ts];
        }
    }
}

void timeoutHandler(void* args)
{
}


// <-- output

// <-- Public
#pragma mark -
#pragma mark P2P Handshake

- (int)createRemoteSocket
{
    
    
    int localPort = 0, localPort1 =0 ;
    char publicAddress[32];
    int publicPort, publicPort1;
    int aSocketFd = -1, aSocketFd1=-1;
    int retries  = 3;
    
    do
    {
        //aSocketFd = -1;
        
        aSocketFd = RMCRemoteSocketCreate2(STUN_SERVER_URL,STUN_SERVER_PORT, &localPort, publicAddress, &publicPort);
        
        if (aSocketFd > 0 ) {
            int nosigpipe = 1;
            int status = setsockopt(aSocketFd, SOL_SOCKET, SO_NOSIGPIPE, &nosigpipe, sizeof(nosigpipe));
            
            if (status == -1) {
                NSLog(@"Error disabling sigpipe (setsockopt)");
                close(aSocketFd);
                aSocketFd = -1;
            }
            else {
                
                self.p2pInfo = [[PConfigInfo alloc] init];
                self.p2pInfo.sockFd = aSocketFd;
                self.p2pInfo.port   = publicPort;
                self.p2pInfo.ip     = [NSString stringWithFormat:@"%s", publicAddress];
                
                
            }
            
            aSocketFd1 = RMCRemoteSocketCreate2(STUN_SERVER_URL,STUN_SERVER_PORT,&localPort1, publicAddress, &publicPort1);
            
            if (aSocketFd1 <= 0)
            {
                NSLog(@"Fails to create socket2 - Something is wrong!!");
            }
            status = setsockopt(aSocketFd1, SOL_SOCKET, SO_NOSIGPIPE, &nosigpipe, sizeof(nosigpipe));
            
            if (status == -1) {
                NSLog(@"Error disabling sigpipe (setsockopt)");
                close(aSocketFd1);
                aSocketFd1 = -1;
            }
            else
            {
                
                self.p2pInfo.sockFd1 = aSocketFd1;
                //Dont care about port for now
            }
            
            //force it to break from while loop 
            retries = -1;
        }
        else
        {
            NSLog(@"Fails to create socket1 - Something is wrong!!");
            //200 ms from now
            [NSThread sleepForTimeInterval:1.0];
            
            
        }
        
        
    } while (retries -- > 0);
    
    
    
    return aSocketFd;
}

- (void)connectToCam:(NSString *) camUdid WithMode:(NSString *)aMode
{
    //    NSLog(@"%s, aMac:%@", __func__, aMac);

    if (camUdid.length < 19) {
        NSLog(@"\n\nUDID is invalid");
        return;
    }
    
    self.camUDID = camUdid;
    self.camMac     = [camUdid substringWithRange:NSMakeRange(6, 12)];
    
    NSLog(@"%s, aMac:%@ and >>>>>>>>>>>>>>>>>>>>>> MODE:%@", __func__, _camMac, aMode);
    
    
    self.streamMode = aMode;
    
    self.debugTbLog = [NSMutableString string];
    self.debugTbDate = nil;
    self.wantToCancel = NO;
    
    // Start...
    if ([aMode isEqualToString:P2P_MODE_LOCAL_2]) {
        [self setupP2PStream];
    }
    else if ([aMode isEqualToString:P2P_MODE_REMOTE_2]) {
        //[self setupP2PStreamRemote];
        NSLog(@" NEW FLOW ");
        [self setupP2PStreamRemote2];
    }
    
    
//    //HACK for DB
//    else {
//        if ( [CameraUtil isDoorBellWithUDID:_camUDID] == YES ){
//            self.p2pSess = NULL;
//            [self setupP2PStreamRelayWithConfig:nil];
//        }
//    }
}

- (void)connectToCam:(NSString *) camUdid notificationP2PInfo:(NSDictionary*)p2pInfo
{
    //    NSLog(@"%s, aMac:%@", __func__, aMac);
    if (camUdid.length < 19) {
        NSLog(@"\n\nUDID is invalid");
        return;
    }
    
    self.camUDID = camUdid;
    self.camMac     = [camUdid substringWithRange:NSMakeRange(6, 12)];
    self.p2pNotificationInfo = p2pInfo;
    
    NSLog(@"%s, aMac:%@ and >>>>>>>>>>>>>>>>>>>>>> MODE:%@", __func__, _camMac, P2P_MODE_RELAY_2);
    
    self.p2pSourceType = P2PStreamSourcePushNotification;
    self.streamMode = P2P_MODE_RELAY_2;
    
    self.debugTbLog = [NSMutableString string];
    self.debugTbDate = nil;
    self.wantToCancel = NO;
    self.p2pSess = NULL;
    
    /*
     {
     key = 5A6471473154656B4959563652336546;
     rn = 6571724A436F61416336684A;
     sip = "52.7.112.62";
     sp = 9090;
     }
     */
    
    NSString* key = [p2pInfo stringForKey:@"key"];
    NSString* rn = [p2pInfo stringForKey:@"rn"];
    NSString* sip = [p2pInfo stringForKey:@"sip"];
    NSString* sp = [p2pInfo stringForKey:@"sp"];
    
    NSLog(@"%s, aMac:%@ and >>>>>>>>>>>>>>>>>>>>>> MODE:%@", __func__, _camMac, P2P_MODE_RELAY_2);
    
    if (key.length && rn.length && sip.length && sp.length) {
        [self setupP2PStreamRelayWithSessionKey:key randomNumber:rn sIP:sip sPort:sp];
    }
}

- (void)reconnectToCamWithMode:(NSString *)aMode
{
    
    NSLog(@"%s, aMac:%@", __func__, _camMac);
    
    
    self.streamMode = aMode;
    
    self.debugTbLog = [NSMutableString string];
    self.debugTbDate = nil;
    self.wantToCancel = NO;
    
    // Start...
    if ([aMode isEqualToString:P2P_MODE_LOCAL_2]) {
        [self setupP2PStream];
    }
    else if ([aMode isEqualToString:P2P_MODE_REMOTE_2]) {
        NSLog(@" NEW FLOW 11 ");
        //[self setupP2PStreamRemote];
        [self setupP2PStreamRemote2];
    }
    
    
//    //HACK for DB
//    else {
//        if ( [CameraUtil isDoorBellWithUDID:_camUDID] == YES ){
//            self.p2pSess = NULL;
//            [self setupP2PStreamRelayWithConfig:nil];
//        }
//    }
}


- (void)setupP2PStream
{
    self.p2pMode = P2PModeLocal;
    int localPort = 0;
    int aSocketFd = -1;
    int triedTimes = 3;
    
    do {
        aSocketFd = RMCLocalSocketCreate(&localPort, NULL);
        
        if (aSocketFd >= 0) {
            break; // SS
        }
        
        [NSThread sleepForTimeInterval:1];
    }while(--triedTimes && !_wantToCancel);
    
    if ( aSocketFd >= 0) {
        self.p2pInfo = [[PConfigInfo alloc] init];
        self.p2pInfo.sockFd = aSocketFd;
        self.p2pInfo.port   = localPort;
        self.p2pInfo.ip     = [self getIPAddress];
        self.p2pInfo.commMode = P2P_MODE_LOCAL_2;
        
        NSLog(@"APP SIDE: socket: %d port:%d ip:%@ ", aSocketFd,self.p2pInfo.port, self.p2pInfo.ip);
        
        
        //Call func to establish connection with Camera
        P2PCommResult t = _outputCallback(_p2pInfo, _camUDID);
        
        if (t == P2PCommSuccess && !_wantToCancel) {
            
            self.streamName = self.p2pInfo.streamName;
            
            [self p2pSetupInputWithConfig:_p2pInfo];
            
            if (_p2pSess && !_wantToCancel) {
                [self p2pStartReceivingDataWithMode:P2P_MODE_LOCAL_2 andTriedTime:0];
                NSLog(@"%s End of MediaTransferSessionRecv loop. Cam:%@", __FUNCTION__, _camMac);
                
                return; // Finished.
            }
        }
    }
    
    if (!_wantToCancel && _delegate) {
        [_delegate rmcHandler:self didFailToConnectCam:_camMac mode:P2P_MODE_LOCAL_2];
        [self didFailToConnectCam];
    }
    
    NSLog(@"%s Cancel:%d", __func__, _wantToCancel);
}

// <-- Local

// --> Remote


//New flow
- (void)setupP2PStreamRemote2
{
    //Create Local socket to listen for P2P packets and alloc p2pInfo

    int sck = -1;
    @synchronized (self) {
        
        sck = [self createRemoteSocket];
    }

    
    if (sck >= 0) {
        self.p2pInfo.commMode = P2P_MODE_REMOTE_2;
        
        /* call to get_session_key &Parse Result  below */
        
        P2PCommResult t = _outputCallback(_p2pInfo, _camUDID);
       
        /*By now, we should have both Relay server IP and Direct camera IP to play with
         All stored in @_p2pInfo */
        
        if (t == P2PCommSuccess) {
            self.streamName = _p2pInfo.streamName;
            
            if ( _wantToCancel) {
                //TODO: Clean up ?
                return;// UGLY
            }
        
            /*Kick start P2P connection establishing here :RMCSetEncryptionKey, ..Set Threshold, Timeout..
              BLOCKING CALL HERE for the whole P2P Connection ****/
            [self p2pSetupInputWithConfig2:_p2pInfo];
            
            //after this means PR & PS both fails

        }
        
        //Call this to retry again
        [_delegate rmcHandler:self didFailToConnectCam:_camMac mode:P2P_MODE_REMOTE_2];
        [self didFailToConnectCam];
        
    }
    else
    {
        //Fails to create Socket
        NSLog(@"Fail to create SOCKET !!! will die");
        
    }
    
}

- (void)setupP2PStreamRelayWithSessionKey:(NSString*)sessionKey randomNumber:(NSString*)randomNumber sIP:(NSString*)sIP sPort:(NSString*)sPort{
    //Create Local socket to listen for P2P packets and alloc p2pInfo
    int sck = [self createRemoteSocket];
    
    if (sck >= 0) {
        self.p2pInfo.commMode = P2P_MODE_RELAY_2;
        
        //Assign PS stream info
        [_p2pInfo keyFromHexString:sessionKey];
        [_p2pInfo rnFromHexString:randomNumber];
        _p2pInfo.sip = sIP;
        _p2pInfo.sp = [sPort intValue];
        
        /*By now, we should have both Relay server IP and Direct camera IP to play with
         All stored in @_p2pInfo */
        
        self.streamName = _p2pInfo.streamName;
        
        if ( _wantToCancel) {
            //TODO: Clean up ?
            return;// UGLY
        }
        
        /*Kick start P2P connection establishing here :RMCSetEncryptionKey, ..Set Threshold, Timeout..
         BLOCKING CALL HERE for the whole P2P Connection ****/
        [self p2pSetupInputWithPushNotificationConfig:_p2pInfo];
        
        //after this means PR & PS both fails
        
        //Call this to retry again
        if (_delegate && [_delegate respondsToSelector:@selector(rmcHandler:didFailToConnectCam:)]) {
            [_delegate rmcHandler:self didFailToConnectCam:_camMac mode:P2P_MODE_RELAY_2];
        }
        
        //Rety to connect via push notification info again
        NSLog(@"Will retry to connect with session key: %@, sIP: %@, sp: %@", sessionKey, sIP, sPort);
        [self setupP2PStreamRelayWithSessionKey:sessionKey randomNumber:randomNumber sIP:sIP sPort:sPort];
        NSLog(@"Did retry to connect with session key: %@, sIP: %@, sp: %@", sessionKey, sIP, sPort);
    }
    else
    {
        //Fails to create Socket
        NSLog(@"Fail to create SOCKET !!! will die");
    }
}

- (void)switchCamToP2pSesMode:(int)mode // preview | full-mode
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        if (_streamName == nil) {
            _streamName = @"whatthehell";
        }
        
        NSString *command = [NSString stringWithFormat:@"p2p_ses_mode_set&value=%d&streamname=%@", mode, _streamName];
        NSString *response =  [self sendToCameraTheP2PCommand2:command];
        
        NSLog(@"1234 Switching p2P to mode: %d res: %@, streamname:%@ cam:%@",mode, response, _streamName , _camUDID);
    });
}
- (void)didFailToConnectCam
{
    NSLog(@"%s aMode:%@", __FUNCTION__, _streamMode);
    
    if ([_streamMode isEqualToString:P2P_MODE_LOCAL_2]) {
        double delayInSecs = 1;
        dispatch_time_t popTimes = dispatch_time(DISPATCH_TIME_NOW, delayInSecs * NSEC_PER_SEC);
        dispatch_after(popTimes, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            if (_wantToCancel) {
                NSLog(@"Cancel!");
                return;
            }
            
            self.p2pMode = P2PModeLocal;
            [self reconnectToCamWithMode:_streamMode];
        });
    }
    else if ([_streamMode isEqualToString:P2P_MODE_REMOTE_2] || [_streamMode isEqualToString:P2P_MODE_RELAY_2]) {
        
        double delayInSecs = 0.2;
        dispatch_time_t popTimes = dispatch_time(DISPATCH_TIME_NOW, delayInSecs * NSEC_PER_SEC);
        dispatch_after(popTimes, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            if (_wantToCancel) {
                NSLog(@"Cancel!");
                return;
            }
            
            self.p2pMode = P2PModeRelay;
            
            [self reconnectToCamWithMode:P2P_MODE_REMOTE_2];
        });
        
    }
    else {
        NSLog(@"\nOut of control");
    }
}
#pragma mark -
#pragma mark P2P Handshake- OLD FLOW - Deprecated
// Shared -->
- (void )p2pSetupInputWithConfig:(PConfigInfo *)p2pInfo
{
    NSLog(@"CAM SIDE: >>>>>>   %@, %d, %d", p2pInfo.ip, p2pInfo.port, p2pInfo.sockFd);
    
    self.p2pSess = RMCAllocUseExternalSocket(NULL, (char *)p2pInfo.ip.UTF8String,
                                             p2pInfo.port,
                                             p2pInfo.sockFd);
    
    if (_p2pSess) {
        NSString *aMode = p2pInfo.commMode;
        RMCNameSet(_p2pSess, p2pInfo.ip.UTF8String);
        
        RMCEncryptionSet(_p2pSess, 1, (uint8_t *)p2pInfo.key.UTF8String);
        
        RMCRXSideGoodFrameThresholdSet(_p2pSess, 100);
        
        if (!p2pInfo.rn) {
            p2pInfo.rn = [NSString stringWithFormat:@"%@%08X",@"ABCDEF", (unsigned int)[[NSDate date] timeIntervalSince1970]];
        }
        
        int t = 8;
        char *mac_wo_colon = (char *)_camMac.UTF8String;
        // 4.
        while (--t) {
            RMCRelayRequestSend(_p2pSess, eMediaSubTypeCommandAccessStream, mac_wo_colon, (void *)[p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]);
            if (t == 5) { // Log once
                NSLog(@"%s RMCRelKayRequestSend, _camMac:%@, mac_wo_colon:%s, p2pInfo.rn:%@, [p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]:%s", __func__, _camMac, mac_wo_colon, p2pInfo.rn, [p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]);
            }
        }
        
        //Default local
        uint32_t minTime = 500; // IA-1573
        uint32_t maxTime = 800;
        [self setP2pMode:P2PModeLocal];
        
        if ([aMode isEqualToString:P2P_MODE_REMOTE_2]) {
            minTime = 700;
            maxTime = 1000;
            [self setP2pMode:P2PModeRemote];
        }
        else if ([aMode isEqualToString:P2P_MODE_RELAY_2]) {
            
            minTime = 700;
            maxTime = 1000;
            [self setP2pMode:P2PModeRelay];
        }
        
        
        RMCRXSideTimeoutSet(_p2pSess, minTime, maxTime);
        RMCTimeOutHdlSet(_p2pSess, (RMCTimeOutHdl)timeoutHandler, NULL);
        
        RMCPrivateDataSet(_p2pSess, (__bridge void *)self);
    }
}



- (void)setupP2PStreamRemote
{
    self.p2pMode = P2PModeRemote;
    int sck = -1;
    @synchronized (self) {
    
        sck = [self createRemoteSocket];
    }
    
    if (sck >= 0) {
        self.p2pInfo.commMode = P2P_MODE_REMOTE_2; //P2P_MODE_COMBINE;
        P2PCommResult t = _outputCallback(_p2pInfo, _camUDID);
        
        if (t == P2PCommSuccess && !_wantToCancel) {
            self.streamName = _p2pInfo.streamName;
            PRHandshakeResult_2 handshakeResult = [self handshakeWithRemoteCamP2PConfigInfo:_p2pInfo];
            
            if (handshakeResult == PRHandshakeSuccess_2 && !_wantToCancel) {
                [self p2pSetupInputWithConfig:_p2pInfo];
                
                if (_p2pSess && !_wantToCancel) {
                    [self p2pStartReceivingDataWithMode:P2P_MODE_REMOTE_2 andTriedTime:0];
                    NSLog(@"%s End of MediaTransferSessionRecv loop. Cam:%@", __FUNCTION__, _camMac);
                    
                    return;
                }
            } else if ((handshakeResult != PRHandshakeSuccess_2) || _wantToCancel) {
                NSLog(@"%s handshakeResult:%ld", __FUNCTION__, (long)handshakeResult);
                
                if (handshakeResult == PRHandshakeEPIPE_2) {
                    //            shouldRetry = YES;
                }
            }
        }
    }
    
    
    if (!_wantToCancel && _delegate) {
        NSLog(@"%s Switch to relay mode", __FUNCTION__);
        [_delegate rmcHandler:self didFailToConnectCam:_camMac mode:P2P_MODE_REMOTE_2];
        [self didFailToConnectCam];
    }
}

// Relay -->
- (void)setupP2PStreamRelayWithConfig:(PConfigInfo *)config
{
    self.p2pMode = P2PModeRelay;
    P2PCommResult t = P2PCommFailure;
    
    if (!config || !config.sip) {
        int sck = -1;
        @synchronized (self) {
            
            sck = [self createRemoteSocket];
        }


        
        if (sck >= 0) {
            self.p2pInfo.commMode = P2P_MODE_RELAY_2;
            t = _outputCallback(_p2pInfo, _camUDID);
        }
    }
    else {
        t = P2PCommSuccess;
        self.p2pInfo = config;
    }
    
    if (t == P2PCommSuccess && !_wantToCancel) {
        self.streamName = _p2pInfo.streamName;
        self.p2pInfo.commMode = P2P_MODE_RELAY_2;
        
        self.p2pInfo.port = _p2pInfo.sp;
        self.p2pInfo.ip   = _p2pInfo.sip;
        
        [self p2pSetupInputWithConfig:_p2pInfo];
        
        if (_p2pSess && !_wantToCancel) {
            [self p2pStartReceivingDataWithMode:P2P_MODE_RELAY_2 andTriedTime:0];
            NSLog(@"%s End of MediaTransferSessionRecv loop. Cam:%@", __FUNCTION__, _camMac);
            
            return;
        }
    }
    
    NSLog(@"%s Cancel:%d", __func__, _wantToCancel);
    
    if (!_wantToCancel && _delegate) {
        NSLog(@"%s Failed. Start over", __FUNCTION__);
        [_delegate rmcHandler:self didFailToConnectCam:_camMac mode:P2P_MODE_RELAY_2];
        [self didFailToConnectCam];
    }
}
// Relay <--

- (PRHandshakeResult_2 )handshakeWithRemoteCamP2PConfigInfo:(PConfigInfo *)configInfo
{
    struct in_addr      destAddr;
    
    if ( inet_aton(configInfo.ip.UTF8String, &destAddr) == 0 ) {
        NSLog(@"%s Address %s is invalid\n", __FUNCTION__, configInfo.ip.UTF8String);
        return PRHandshakeFailure_2;
    }
    
    struct sockaddr_in  addr;
    addr.sin_family =  AF_INET;
    addr.sin_addr = destAddr;
    addr.sin_port = htons(configInfo.port);
    
    NSLog(@"%sdestAddr.s_addr:%u, addr.sin_port:%hu", __FUNCTION__, destAddr.s_addr, addr.sin_port);
    
    PRHandshakeResult_2 handshakeResult = PRHandshakeFailure_2;
    char                sendBuf[HANDSHAKE_BUF_SIZE_2];
    char                recvBuf[MAX_HANDSHAKE_BUF_SIZE_2];
    ssize_t size = -1;
    NSDate *startTime = [NSDate date];
    
    while ( !_wantToCancel &&
           ABS(startTime.timeIntervalSinceNow) < MAX_HANDSHAKE_TIME_2) {
        memset(sendBuf, 0, sizeof(sendBuf));
        memset(recvBuf, 0, sizeof(recvBuf));
        sprintf(sendBuf, "%s", "Hello P2PRemote");
        
        //DLog(@"Sending handshake info to fd %d\n", remote-Endpoint->socket-_fd);
        //        int ttl = 4; /* max = 255 */
        //        setsockopt(configInfo.sockFd, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl));
        int sSize = 50;
        
        for (int i = 0; i < 9; ++i) {
            size = sendto(configInfo.sockFd, (void *)sendBuf, sSize+=105, 0, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
            
            if (size == -1 && errno == EPIPE) {
                NSLog(@"\n\n\n\%s EPIPE. Suppose nerver occurs.", __FUNCTION__);
                return PRHandshakeEPIPE_2;
            }
        }
        
        //DLog(@"Sending handshake info %s from fd %d, ret %ld\n", sendBuf, configInfo.socketFd, size);
        
        //DLog(@"Receiving handshake info from fd %d\n", remote-Endpoint->socket-_fd);
        recv(configInfo.sockFd, recvBuf, sizeof(recvBuf), 0);
        //DLog(@"Receiving handshake info recvBuf:%s from fd socket-_fd:%d ret size:%ld\n", recvBuf, configInfo.socketFd, size);
        
        if (recvBuf[0] == 'V' && recvBuf[1] == 'L' &&
            recvBuf[2] == 'V' && recvBuf[3] == 'L') { //hand shake OK
            NSLog(@"\n---HANDSHAKE PORT OK---.");
            handshakeResult = PRHandshakeSuccess_2;
            break;
        }
        
        usleep(100000);
    }
    
    if (handshakeResult != PRHandshakeSuccess_2) {
        NSLog(@"%s Handshake failed.", __FUNCTION__);
    }
    
    //    int ttl = 64; /* max = 255 */
    //    setsockopt(configInfo.sockFd, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl));
    
    return handshakeResult;
}

#pragma mark -
#pragma mark P2P Data


///// MAIN P2P LOOP ///// 
- (void )p2pSetupInputWithConfig2:(PConfigInfo *)p2pInfo
{
    //Default local
    uint32_t minTime = 500; // IA-1573
    uint32_t maxTime = 800;
    
    
    
    NSLog(@"New flow 2 sess: CAM SIDE: >>>>  %@, %d, %d, relay: %@, %d  >>>>>> in Object: %@",
          p2pInfo.ip, p2pInfo.port, p2pInfo.sockFd,
          p2pInfo.sip, p2pInfo.sp, p2pInfo); //0x17428d070
    
    
    //Init to NULL first
    self.p2pSess = NULL;
    
    //Make 1st one: Direct p2p
    if (p2pInfo.ip.length) {
        self.p2pSess = RMCAllocUseExternalSocket(NULL, (char *)p2pInfo.ip.UTF8String,
                                                 p2pInfo.port,
                                                 p2pInfo.sockFd);
        
        if (!_p2pSess ) {
            
            //error handling
            NSLog(@"p2pSess is NULL - Jump to try PS Flow ");
        }
        else{
            RMCNameSet(_p2pSess, (char *)p2pInfo.ip.UTF8String);
            
            RMCEncryptionSet(_p2pSess, 1, (uint8_t *)p2pInfo.key.UTF8String); // 0x170287b20 # 0x17428d070 ==>>WTF

            
            
            RMCRXSideGoodFrameThresholdSet(_p2pSess, 100);
            
            if (!p2pInfo.rn) {
                p2pInfo.rn = [NSString stringWithFormat:@"%@%08X",@"ABCDEF", (unsigned int)[[NSDate date] timeIntervalSince1970]];
            }
            
            minTime = 700;
            maxTime = 1000;
            
            self.p2pMode = P2PModeRemote;
            
            [self setStreamMode:P2P_MODE_REMOTE_2];
            
            RMCRXSideTimeoutSet(_p2pSess, minTime, maxTime);
            RMCPrivateDataSet(_p2pSess, (__bridge void *)self);
            
            //RMCTimeOutHdlSet(_p2pSess, (RMCTimeOutHdl)timeoutHandler, NULL);
            
            // We must spread the access command out- during the timeout
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                int t = 8;
                char *mac_wo_colon = (char *)_camMac.UTF8String;
                //Send AccessStream
                while (--t && !_wantToCancel )  {
                    
                    RMCRelayRequestSend(_p2pSess, eMediaSubTypeCommandAccessStream, mac_wo_colon, (void *)[p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]);
                    if (t == 5) { // Log once
                        NSLog(@"%s RMCRelKayRequestSend , _camMac:%@, mac_wo_colon:%s, p2pInfo.rn:%@, [p2pInfo.rn UTF8 Enc]:%s", __func__,
                              _camMac, mac_wo_colon, p2pInfo.rn, [p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]);
                    }
                    
                    //1sec
                    [NSThread sleepForTimeInterval:1.0];
                }
                
            });
            
            
            //Start receving data on PR Mode
            [self p2pStartReceivingDataWithMode:P2P_MODE_REMOTE_2 andTriedTime:0];
            
            NSLog(@"%s End of P2p Remote For Cam:%@", __FUNCTION__, _camMac);
        }
    }
    //TODO: How to know if we fail here because user want to cancel ?
    //      or because PR has started for a while and timeout , in this case should try PR again
    //      or because PR fails first time, in this case what follow is correct, try PS
    if (_wantToCancel == NO)
    {
        
        //Make 2nd one: relay p2p
        if (p2pInfo.sip.length) {
            self.p2pSess = RMCAllocUseExternalSocket(NULL, (char *)p2pInfo.sip.UTF8String,
                                                     p2pInfo.sp,
                                                     p2pInfo.sockFd1);//!!?!!
            if (!_p2pSess ) {
                
                //TODO: error handling
                NSLog(@"p2pRelaySess is NULL - Will die");
            }
            else{
                //IF There is only 1 SOCKFD- do it here will overwrite the call in PR MODE ?
                RMCNameSet(_p2pSess, (char *)p2pInfo.sip.UTF8String);
                
                RMCEncryptionSet(_p2pSess, 1, (uint8_t *)p2pInfo.key.UTF8String);
                
                RMCRXSideGoodFrameThresholdSet(_p2pSess, 100);
                minTime = 700;
                maxTime = 1000;
                
                self.p2pMode = P2PModeRelay;
                NSLog(@"%s P2p relay For Cam:%@ mode: %d ", __FUNCTION__, _camMac, (int)self.p2pMode);
                
                [self setStreamMode:P2P_MODE_RELAY_2];
                
                RMCRXSideTimeoutSet(_p2pSess, minTime, maxTime);
                RMCPrivateDataSet(_p2pSess, (__bridge void *)self);
                
                //RMCTimeOutHdlSet(_p2pSess, (RMCTimeOutHdl)timeoutHandler, NULL);
                
                // We must spread the access command out- during the timeout
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    int t = 8;
                    char *mac_wo_colon = (char *)_camMac.UTF8String;
                    //Send AccessStream
                    while (--t && !_wantToCancel )  {
                        
                        RMCRelayRequestSend(_p2pSess, eMediaSubTypeCommandAccessStream, mac_wo_colon, (void *)[p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]);
                        if (t == 5) { // Log once
                            NSLog(@"%s RMCRelKayRequestSend, _camMac:%@, mac_wo_colon:%s, p2pInfo.rn:%@, [p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]:%s", __func__, _camMac, mac_wo_colon, p2pInfo.rn, [p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]);
                        }
                        
                        //1sec
                        [NSThread sleepForTimeInterval:1.0];
                    }
                    
                });
                
                //Start receving data on PS Mode
                [self p2pStartReceivingDataWithMode:P2P_MODE_REMOTE_2 andTriedTime:0];
                
                NSLog(@"%s End of P2p RELAY For Cam:%@", __FUNCTION__, _camMac);
            }
        }
    }
    
    self.p2pMode = P2PModeNone;
    //NSLog(@" CLosing socket ");
    //Whatever we do, must release the socket even it 's used or not
    //close(p2pInfo.sockFd1);
    //close(p2pInfo.sockFd);
    
}

- (void )p2pSetupInputWithPushNotificationConfig:(PConfigInfo *)p2pInfo
{
    //Default local
    uint32_t minTime = 500; // IA-1573
    uint32_t maxTime = 800;

    NSLog(@"p2pSetupInputWithPushNotificationConfig - New flow 2 sess: CAM SIDE: >>>>  %@, %d, %d, relay: %@, %d",
          p2pInfo.ip, p2pInfo.port, p2pInfo.sockFd,
          p2pInfo.sip, p2pInfo.sp);
    
    //Init to NULL first
    self.p2pSess = NULL;
    
    //TODO: How to know if we fail here because user want to cancel ?
    //      or because PR has started for a while and timeout , in this case should try PR again
    //      or because PR fails first time, in this case what follow is correct, try PS
    if (_wantToCancel == NO)
    {
        
        //Make 2nd one: relay p2p
        if (p2pInfo.sip.length) {
            self.p2pSess = RMCAllocUseExternalSocket(NULL, (char *)p2pInfo.sip.UTF8String,
                                                     p2pInfo.sp,
                                                     p2pInfo.sockFd1);//!!?!!
            if (!_p2pSess ) {
                
                //TODO: error handling
                NSLog(@"p2pRelaySess is NULL - Will die");
            }
            else{
                //IF There is only 1 SOCKFD- do it here will overwrite the call in PR MODE ?
                RMCNameSet(_p2pSess, (char *)p2pInfo.sip.UTF8String);
                
                RMCEncryptionSet(_p2pSess, 1, (uint8_t *)p2pInfo.key.UTF8String);
                
                RMCRXSideGoodFrameThresholdSet(_p2pSess, 100);
                minTime = 700;
                maxTime = 1000;
                
                self.p2pMode = P2PModeRelay;
                NSLog(@"%s P2p relay For Cam:%@ mode: %d ", __FUNCTION__, _camMac, (int)self.p2pMode);
                
                [self setStreamMode:P2P_MODE_RELAY_2];
                
                RMCRXSideTimeoutSet(_p2pSess, minTime, maxTime);
                RMCPrivateDataSet(_p2pSess, (__bridge void *)self);
                
                //RMCTimeOutHdlSet(_p2pSess, (RMCTimeOutHdl)timeoutHandler, NULL);
                
                // We must spread the access command out- during the timeout
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    
                    int t = 8;
                    char *mac_wo_colon = (char *)_camMac.UTF8String;
                    //Send AccessStream
                    while (--t && !_wantToCancel )  {
                        
                        RMCRelayRequestSend(_p2pSess, eMediaSubTypeCommandAccessStream, mac_wo_colon, (void *)[p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]);
                        if (t == 7) { // Log once
                            NSLog(@"%s RMCRelKayRequestSend, _camMac:%@, mac_wo_colon:%s, p2pInfo.rn:%@, [p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]:%s", __func__, _camMac, mac_wo_colon, p2pInfo.rn, [p2pInfo.rn cStringUsingEncoding:NSUTF8StringEncoding]);
                        }
                        
                        //1sec
                        [NSThread sleepForTimeInterval:0.3];
                    }
                });
                
                //Start receving data on PS Mode
                [self p2pStartReceivingDataWithMode:P2P_MODE_REMOTE_2 andTriedTime:0];
                
                NSLog(@"%s End of P2p RELAY For Cam:%@", __FUNCTION__, _camMac);
            }
        }
    }
    
    self.p2pMode = P2PModeNone;
    //NSLog(@" CLosing socket ");
    //Whatever we do, must release the socket even it 's used or not
    //close(p2pInfo.sockFd1);
    //close(p2pInfo.sockFd);
}

- (void)p2pStartReceivingDataWithMode:(NSString *)aMode andTriedTime:(NSInteger)triedTimes
{
    if (_delegate) {
        [_delegate rmcHandler:self didConnectSuccessToDeviceID:_camUDID];
    }
    
    self.tmp = NSDate.timeIntervalSinceReferenceDate;
    
    [self performSelectorOnMainThread:@selector(initTimerP2POnMainThread:) withObject:nil waitUntilDone:NO];
    
    if (_p2pMode != P2PModeNone) {
        //double delayInSeconds = 0.1;
        //dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        //dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            if (self.delegate2 && [self.delegate2 isKindOfClass:NSClassFromString(@"CameraPlayerViewController")]) {
                //Nghi - Cam is in view, switch to FULL MODE
                [self switchCamToP2pSesMode:3];
            }
            else{
                //Nghi - Cam is not in view, switch to PREVIEW MODE
                if ([FIRMWARE_VERSION_FOR_PREVIEW_MODE compare:self.fwVersion] <= NSOrderedSame) {
                    [self switchCamToP2pSesMode:1];
                }
            }
        });
        
        NSLog(@">>>>>>>>>>>>>> START P2P Main Receiving function for:%@", _camUDID);
        RMCRXSideRecv(_p2pSess, writeFile_2); // Blocking call
    }
    
    [self setP2pMode:P2PModeNone];
}

- (void)closeSession
{
    NSLog(@"%s for :%@", __FUNCTION__,_camUDID);
    
    if (_p2pSess && _p2pMode > P2PModeNone) {
        if ( _p2pMode == P2PModeRelay &&
            (_p2pInfo && _p2pInfo.rn ) &&
            _camMac) {
            
            int t = 8;
            char *mac_wo_colon = (char *)_camMac.UTF8String;
            
            while ( --t ) {
                RMCRelayRequestSend(_p2pSess, eMediaSubTypeCommandCloseStream, mac_wo_colon, (void *)_p2pInfo.rn.UTF8String);
            }
        }
        
       
        RMCCleanup(_p2pSess);
        
         NSLog(@"%s after RMCCleanup : %@", __FUNCTION__, _camUDID);
    }
    
}
// Clean -->
- (void)stop
{
    [self setDelegate2:nil];
    [self setDelegate:nil];
    
    [self setWantToCancel:YES];
    [self invalidateTimerRecvData];
    [self invalidateTimerBuffering];
    
    [self closeSession];
    
    self.p2pMode = P2PModeNone;
}




#pragma mark -
#pragma mark P2P Timer


- (void)initTimerP2POnMainThread:(id)sender
{
    if ( _timerRecvP2PData ) {
        [_timerRecvP2PData invalidate];
    }
    
    self.timerRecvP2PData = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                           selector:@selector(updateP2PDataTime:)
                                                           userInfo:nil repeats:NO];
}

- (void)invalidateTimerRecvData
{
    if (_timerRecvP2PData) {
        [_timerRecvP2PData invalidate];
        [self setTimerRecvP2PData:nil];
    }
}

- (void)invalidateTimerBuffering
{
    if (_timerBufferingTimeout) {
        [_timerBufferingTimeout invalidate];
        [self setTimerBufferingTimeout:nil];
    }
}

- (void)createTimerBuffering
{
    if (_timerBufferingTimeout) {
        [_timerBufferingTimeout invalidate];
    }
    
    self.timerBufferingTimeout = [NSTimer scheduledTimerWithTimeInterval:15 target:self
                                                                selector:@selector(timeoutBuffering:)
                                                                userInfo:nil repeats:NO];
}


- (void)updateP2PDataTime:(NSTimer *)timer
{
    
    
    [self invalidateTimerRecvData];
    
    if ( NSDate.timeIntervalSinceReferenceDate - _tmp > 10 ) {
        
        
        NSLog(@"%s  timeout  for Cam:%@ ", __FUNCTION__, self.camMac);
        
        [self timeoutWhenReceivingData];
    }
    else {
        self.timerRecvP2PData = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                               selector:@selector(updateP2PDataTime:)
                                                               userInfo:nil repeats:NO];
    }
}

- (void)timeoutBuffering:(NSTimer *)timer
{
    [self invalidateTimerRecvData];
    
    NSLog(@"%s P2P stream timed out.", __FUNCTION__);
    [self timeoutWhenReceivingData];
}

- (void)timeoutWhenReceivingData
{
    NSLog(@"%s 2213 _delegate:%p, _p2pMode:%d cam: %@", __func__, _delegate, (int)_p2pMode, _camUDID);
    [self invalidateTimerBuffering];
    
   if (_p2pSess && _p2pMode > P2PModeNone) {
       [self closeSession];
    }
    
    //self.p2pMode = P2PModeNone;
    
    if (_delegate) {
        [_delegate rmcHandler:self timeoutWhileReceivingDataWithDeviceID:_camUDID];
    }
    
}
// <---

// Clean <--
#pragma mark -
#pragma mark P2P Cmd & Audio

- (NSString *)sendCommand:(NSString *)cmd andFlag:(BOOL)isReq
{
    NSLog(@"%s cmd:%@  for cam: %@", __func__, cmd, _camUDID);
    
    NSString *t = @"";
    
    if ( !_p2pSess || _p2pMode == P2PModeNone ||
        (_wantToCancel && [cmd rangeOfString:@"p2p_ses_mode_set"].location == NSNotFound)) {
        NSLog(@"%s userWantToCancel:%d, p2pMode:%d", __FUNCTION__, _wantToCancel, (int)_p2pMode);
        return t;
    }
    
    NSString *actCmd = @"req=";
    
//    if (!isReq) {
//        actCmd = @"action=command&command=";
//    }
    
    NSRange range = [cmd rangeOfString:actCmd];
    
    if ( range.location == NSNotFound ) {
        cmd = [NSString stringWithFormat:@"%@%@", actCmd, cmd];
    }
    
    tCmdResponseBuffer req;
    tCmdResponse *res = NULL;
    
    const char *request = cmd.UTF8String;
    req.data = (void *)request;
    req.size = strlen(request);
    
    int commandID = RMCRxSideCmdReqSend(_p2pSess, &req);
    
    req.data = NULL;
    req.size = 0;
    
    if ( commandID >= 0 ) {
        int retryTime = 0;
        
        while ( !_wantToCancel && _p2pSess && _p2pMode > P2PModeNone && retryTime < 500 ) {
            res = RMCRxSideCmdResGet(_p2pSess, commandID);
            
            if ( res != NULL && res->status == eMediaErrorOK ) {
                break;
            }
            else {
                usleep(10000);
                retryTime++;
                continue;
            }
        }
        
        if ( !_wantToCancel && _p2pSess && _p2pMode > P2PModeNone && retryTime < 500 && res ) {
            // if movement control, need not to get the respose data
            BOOL isPreset = [cmd rangeOfString:@"move_preset_point"].location != NSNotFound;
            
            if (!isPreset && ([cmd rangeOfString:@"move"].location != NSNotFound || [cmd rangeOfString:@"_stop"].location != NSNotFound)) {
                NSLog(@"%s cmd:%@", __FUNCTION__, cmd);
                res->CmdResponse.data = NULL;
                res->CmdResponse.size = 0;
                res = NULL;
            }
            else {
                t = [[NSString alloc] initWithBytesNoCopy:res->CmdResponse.data
                                                   length:res->CmdResponse.size
                                                 encoding:NSUTF8StringEncoding
                                             freeWhenDone:NO];
            }
        }
        else {
            NSLog(@"%s Response is NULL or Canceled:%d, p2pMode:%d, cmd:%@", __FUNCTION__, _wantToCancel, (int)_p2pMode, cmd);
        }
    }
    
    return t;
}


- (NSString *)sendToCameraTheP2PCommand2:(NSString *)cmd
{
    tCmdResponseBuffer req;
    tCmdResponse *res = NULL;
    int commandID ;
    int retrySndTime = 0; //Don't retry to decrease time to start streaming
     NSString *actCmd = @"req=";
    
    NSString *t = @"";
    
    if ( !_p2pSess || _p2pMode == P2PModeNone ||
        (_wantToCancel && [cmd rangeOfString:@"p2p_ses_mode_set"].location == NSNotFound)) {
        DLog(@"%s userWantToCancel:%d, p2pMode:%d", __FUNCTION__, _wantToCancel, (int)_p2pMode);
        return t;
    }
    
   
    NSRange range = [cmd rangeOfString:actCmd];
    
    if ( range.location == NSNotFound ) {
        
        cmd = [NSString stringWithFormat:@"%@%@", actCmd, cmd];
    }
    
    
    
    const char *request = cmd.UTF8String;
    
    
    
    do
    {
        t = @"";
        req.data = (void *)request;
        req.size = strlen(request);
        
        commandID = RMCRxSideCmdReqSend(_p2pSess, &req);
        
        req.data = NULL;
        req.size = 0;
        
        if ( commandID >= 0 ) {
            int retryRcvTime = 0;
            
            
            //Polling for response for 500msec
            while ( !_wantToCancel && _p2pSess && _p2pMode > P2PModeNone && retryRcvTime < 50 ) {
                res = RMCRxSideCmdResGet(_p2pSess, commandID);
                
                
                if ( res != NULL && res->status == eMediaErrorOK ) {
                    t = [[NSString alloc] initWithBytesNoCopy:res->CmdResponse.data
                                                       length:res->CmdResponse.size
                                                     encoding:NSUTF8StringEncoding
                                                 freeWhenDone:NO];
                    break;
                }
                else {
                    usleep(10000);
                    retryRcvTime++;
                    continue;
                }
            }
            
            if (res && res->status != eMediaErrorOK  ) {
                DLog(@"%s %@ res status: %d , retryTime:%d, retrySnd:%d", __FUNCTION__, cmd,  res->status, retryRcvTime, retrySndTime);
                DLog(@"%s cam: %@ send: %d Response is NULL or Canceled:%d, p2pMode:%d", __FUNCTION__,
                     _camUDID,
                     retrySndTime, _wantToCancel,(int) _p2pMode);
            }
        }
        
        
        //Try again if t is still nil
    } while ( (retrySndTime-- > 0) &&
              (res && res->status != eMediaErrorOK)  );
    
    return t;
}

- (void)sendAudioData:(uint8_t *)outBuf length:(int)outSize ts:(int)ts
{
    if (!_debugTbDate) {
        self.debugTbDate = [NSDate date];
    }
    
    if ( ABS(_debugTbDate.timeIntervalSinceNow) > 5) {
        NSLog(@"%s Success Sizes:%@", __FUNCTION__, _debugTbLog);
        self.debugTbLog  = [NSMutableString stringWithFormat:@""];
        self.debugTbDate = nil;
    }
    
    if ( !_p2pSess || _p2pMode == P2PModeNone || _wantToCancel) {
        NSLog(@"%s userWantToCancel:%d, p2pMode:%d", __FUNCTION__, _wantToCancel, (int)_p2pMode);
        NSLog(@"%s Success Sizes:%@", __FUNCTION__, _debugTbLog);
        self.debugTbLog = [NSMutableString stringWithFormat:@""];
        return;
    }
    
    if ( RMCRxSideAudioSend(_p2pSess, outBuf, outSize, ts) < 0 ) {
        NSLog(@"%s Failed, log:%@", __FUNCTION__, _debugTbLog);
    }
    else {
        [_debugTbLog appendFormat:@"%d ", outSize];
    }
}

// Cmd & Audio <--

#pragma mark -
#pragma mark Other -->
- (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}
// <-- Other



@end
