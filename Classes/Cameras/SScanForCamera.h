//
//  ScanForCamera.h
//  MBP_ios
//
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <CameraScanner/CameraScanner.h>

@interface SScanForCamera : NSObject

@property (nonatomic, copy) NSString *bc_addr;
@property (nonatomic, copy) NSString *own_addr;
@property (nonatomic, strong) NSMutableArray *scan_results;

@property (nonatomic, weak) id<ScanForCameraNotifier> notifier;

@property (nonatomic) int next_profile_index;
@property (nonatomic) BOOL deviceScanInProgress; 

- (id)initWithNotifier:(id<ScanForCameraNotifier>)caller;

- (void)scan_for_device:(NSString *)mac;
- (void)scan_for_device_in_background:(NSString *)mac;
- (void)scan_done_notify;
- (BOOL)getResults:(NSArray **)out_Array;
- (void)cancel;

@end
