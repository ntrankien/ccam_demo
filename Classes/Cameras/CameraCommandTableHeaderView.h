//
//  CameraCommandTableHeaderView.h
//  App
//
//  Created by Tran Kien Nghi on 5/12/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <UIKit/UIKit.h>
#import "CameraCommandManager.h"
#import "TalkbackManager.h"

@class CameraCommand;
@class CameraCommandTableHeaderView;
@class RMCP2PHandler;

@protocol CameraCommandTableHeaderViewDelegate <NSObject>

@optional
- (void)cameraCommandTableHeaderView:(CameraCommandTableHeaderView*)cameraHeaderView didTapTalkBack:(BOOL)isStartTalkingBack;


@end

@interface CameraCommandTableHeaderView : UIView <TalkbackManagerDelegate>

@property (strong, nonatomic) CameraCommand* cmdStartRecodring;

//@property (nonatomic) BOOL shouldSendMoveCommands;
@property (nonatomic) BOOL isSendingMoveCommands;

// @property (nonatomic) BOOL shouldSendClawCommands;
@property (nonatomic) BOOL isSendingClawCommands;

//@property (nonatomic) BOOL shouldSendSCommands;
@property (nonatomic) BOOL isSendingSCommands;

//@property (nonatomic) BOOL shouldSendHCommands;
@property (nonatomic) BOOL isSendingHCommands;

//@property (nonatomic) BOOL shouldSendWCommands;
@property (nonatomic) BOOL isSendingWCommands;

@property (nonatomic) NSInteger moveSpeedValue;

@property (nonatomic, weak) id<CameraCommandTableHeaderViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imvMoveLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imvMoveRight;
@property (weak, nonatomic) IBOutlet UIImageView *imvMoveForward;
@property (weak, nonatomic) IBOutlet UIImageView *imvMoveBackward;

@property (weak, nonatomic) IBOutlet UIButton *btnClawClose;
@property (weak, nonatomic) IBOutlet UIButton *btnClawOpen;
@property (weak, nonatomic) IBOutlet UIButton *btnMoveForward;
@property (weak, nonatomic) IBOutlet UIButton *btnMoveBackward;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayMelody;
@property (weak, nonatomic) IBOutlet UIButton *btnStopMelodyTapped;
@property (weak, nonatomic) IBOutlet UITextField *txtMoveSpeed;

@property (weak, nonatomic) IBOutlet UIButton *btnDecreaseMoveSpeed;
@property (weak, nonatomic) IBOutlet UIButton *btnIncreaseMoveSpeed;
@property (weak, nonatomic) IBOutlet UIButton *btnTalkback;
@property (weak, nonatomic) IBOutlet UISlider *sliderVolume;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblVolumeValue;
@property (weak, nonatomic) IBOutlet UIButton *btnTakeSnapshot;

@property (weak, nonatomic) IBOutlet UIButton *btnStartRecording;
@property (weak, nonatomic) IBOutlet UIButton *btnEndRecording;
@property (weak, nonatomic) IBOutlet UITextField *tfRecordingDuration;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityRecording;

@property (weak, nonatomic) IBOutlet UITextField *tfCPosition;
@property (weak, nonatomic) IBOutlet UITextField *tfSPosition;
@property (weak, nonatomic) IBOutlet UITextField *tfHPosition;
@property (weak, nonatomic) IBOutlet UITextField *tfWPosition;

@property (weak, nonatomic) IBOutlet UITextField *tfMoveDistance;

@property (weak, nonatomic) IBOutlet UISwitch *switchInchCommand;

@property (nonatomic, assign) RMCP2PHandler *rmcHandler;
@property (nonatomic) BOOL isP2PMode;

- (instancetype)initWithNibNamed:(NSString*)nibName camCh:(CamChannel *)ch;
- (CameraCommand*)commandWithType:(DemoCommandType)cmdTypel;

//Global action
- (IBAction)buttonTouchedUp:(id)sender;
- (IBAction)buttonTouchedDown:(id)sender;

//Custom action
- (IBAction)decreaseSpeedTapped:(id)sender;
- (IBAction)increaseSpeedTapped:(id)sender;
- (IBAction)talkbackButtonTapped:(id)sender;
- (IBAction)sliderVolumeValueChanged:(id)sender;

- (IBAction)butonStartRecordingTapped:(id)sender;
- (IBAction)buttonEndResordingTapped:(id)sender;


- (void)stopTalkback;
- (void)reloadVolumeControl;

@end
