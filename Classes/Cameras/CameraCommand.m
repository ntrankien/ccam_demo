//
//  CameraCommand.m
//  App
//
//  Created by Tran Kien Nghi on 4/29/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraCommand.h"

@interface CameraCommand()

//@property (nonatomic, copy) NSString* previousValue;

@end

@implementation CameraCommand

+ (instancetype)commandWithGet:(NSString*)getCommand set:(NSString*)setCommand title:(NSString*)commandTitle currentValue:(NSString*)currentValue valueList:(NSArray*)valueList isLoading:(BOOL)loading commandType:(DemoCommandType)cmdType{
    CameraCommand* cmd = [CameraCommand new];
    
    cmd.getCommand = getCommand;
    cmd.setCommand = setCommand;
    cmd.commandTitle = commandTitle;
    cmd.currentValue = currentValue;
    //cmd.previousValue = currentValue;
    cmd.valueList = valueList;
    cmd.isLoading = loading;
    cmd.commandType = cmdType;
    
    return cmd;
}

//- (void)setCurrentValue:(NSString *)currentValue{
//    //self.previousValue = _currentValue;
//    _currentValue = currentValue;
//}

- (BOOL)isBooleanValues{
    return (_valueList.count == 2
            && (([@"0" isEqualToString:_valueList[0]] && [@"1" isEqualToString:_valueList[1]])
                || ([@"1" isEqualToString:_valueList[0]] && [@"0" isEqualToString:_valueList[1]]))
            );
}

- (void)restoreCurrentValue{
    //self.currentValue = _previousValue;
}

@end
