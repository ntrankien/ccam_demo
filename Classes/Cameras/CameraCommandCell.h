//
//  CameraCommandCell.h
//  App
//
//  Created by Tran Kien Nghi on 4/29/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <UIKit/UIKit.h>

@class CameraCommand;
@class CameraCommandCell;

@protocol CameraCommandCellDelegate <NSObject>

- (void)cameraCommandCell:(CameraCommandCell*)cmdCell didTapSendCommandValue:(NSString*)cmdValue;
- (void)cameraCommandCell:(CameraCommandCell*)cmdCell didTapChangeValue:(NSString*)cmdValue;

@end

@interface CameraCommandCell : UITableViewCell

@property (weak, nonatomic) id<CameraCommandCellDelegate> delegate;
@property (strong, nonatomic) CameraCommand* camCommand;
@property (weak, nonatomic) IBOutlet UITextField *txtCommandValue;
@property (weak, nonatomic) IBOutlet UISwitch *switchChangeValue;

- (void)displayCommand:(CameraCommand*)cmd;
- (void)indicateProcess;

@end
