//
//  CameraCommandTableHeaderView.m
//  App
//
//  Created by Tran Kien Nghi on 5/12/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraCommandTableHeaderView.h"
#import "PublicDefine.h"
#import "HttpCom.h"
#import "CameraCommandManager.h"
#import "MBProgressHUD.h"
#import "P2PConfigInfo.h"

#import <ReliableChannel/ReliableChannel.h>

#define kCommandType @"kCommandType"
#define kStopCommandRetryTime @"kStopCommandRetryTime"
#define kStopCommandRunLoopID @"kStopCommandRunLoopID"

@interface CameraCommandTableHeaderView()<TalkbackManagerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) CameraCommand* volumeCommand;

@property (nonatomic) CGFloat commandFrequency;
@property (nonatomic) CGFloat commandDuration;
@property (nonatomic) CGFloat p2pCommandDuration;

@property (nonatomic) BOOL isWaitForCMDResponse;
@property (nonatomic) BOOL isWaitForP2PCMDResponse;
@property (nonatomic) BOOL shouldWaitForCMDResponse;

@property (nonatomic) BOOL isUsingAbitCommands;

@property (nonatomic) NSTimeInterval currentMoveRunLoopID;
@property (nonatomic) NSTimeInterval currentStopMoveRunLoopID;

@property (nonatomic) NSTimeInterval currentClawRunLoopID;
@property (nonatomic) NSTimeInterval currentStopClawRunLoopID;

@property (nonatomic) NSTimeInterval currentSRunLoopID;
@property (nonatomic) NSTimeInterval currentStopSRunLoopID;

@property (nonatomic) NSTimeInterval currentHRunLoopID;
@property (nonatomic) NSTimeInterval currentStopHRunLoopID;

@property (nonatomic) NSTimeInterval currentWRunLoopID;
@property (nonatomic) NSTimeInterval currentStopWRunLoopID;

@property (nonatomic) CGFloat handFrequency;
@property (nonatomic) CGFloat handDuration;
@property (nonatomic) CGFloat p2pHandDuration;
@property (nonatomic, copy) NSString* recordingDuration;

@property (nonatomic) BOOL isSendingCmdHandsInit;

@property (nonatomic, strong) NSArray* recordingDurationList;
@property (nonatomic, strong) NSArray* recordingDurationTitleList;
@property (nonatomic, strong) UIPickerView* recordingPickerView;
//@property (nonatomic, copy) NSString* recordingValue;

- (IBAction)sliderVolumeTouchedUp:(id)sender;
- (IBAction)takeSnapshot:(id)sender;
- (IBAction)takeOffButtonTapped:(id)sender;
- (IBAction)landButtonTapped:(id)sender;

- (IBAction)switchInchCommandValueChanged:(id)sender;

@end

@implementation CameraCommandTableHeaderView

- (void)awakeFromNib{
    _btnClawClose.layer.cornerRadius = 3.0;
    _btnClawClose.clipsToBounds = YES;
    
    _btnClawOpen.layer.cornerRadius = 3.0;
    _btnClawOpen.clipsToBounds = YES;
    
    _btnStartRecording.layer.cornerRadius = 3.0;
    _btnStartRecording.clipsToBounds = YES;
    
    _btnEndRecording.layer.cornerRadius = 3.0;
    _btnEndRecording.clipsToBounds = YES;
    
    _btnMoveForward.layer.cornerRadius = 3.0;
    _btnMoveForward.clipsToBounds = YES;
    
    _btnMoveBackward.layer.cornerRadius = 3.0;
    _btnMoveBackward.clipsToBounds = YES;
    
    _btnPlayMelody.layer.cornerRadius = 3.0;
    _btnPlayMelody.clipsToBounds = YES;
    
    _btnStopMelodyTapped.layer.cornerRadius = 3.0;
    _btnStopMelodyTapped.clipsToBounds = YES;
    
    _btnDecreaseMoveSpeed.layer.cornerRadius = 3.0;
    _btnDecreaseMoveSpeed.clipsToBounds = YES;
    
    _btnIncreaseMoveSpeed.layer.cornerRadius = 3.0;
    _btnIncreaseMoveSpeed.clipsToBounds = YES;
}

- (NSInteger)moveSpeedValue{
    NSInteger moveSpeed = [[NSUserDefaults standardUserDefaults] integerForKey:MOVE_SPEED_VALUE];
    if (moveSpeed < 5 || moveSpeed > 255) {
        moveSpeed = DEFAULT_MOVE_SPEED_VALUE;
    }
    _moveSpeedValue = moveSpeed;
    return _moveSpeedValue;
}

- (BOOL)isUsingAbitCommands{
    _isUsingAbitCommands = [[NSUserDefaults standardUserDefaults] integerForKey:IS_USING_MOVE_HAND_ABIT];
    return _isUsingAbitCommands;
}

- (CGFloat)commandFrequency{
    _commandFrequency = CMD_SEND_FREQUENCY / 1000;
    return _commandFrequency;
}

- (CGFloat)commandDuration{
    _commandDuration = CMD_DURATION;
    return _commandDuration;
}

- (CGFloat)p2pCommandDuration{
    _p2pCommandDuration = [kUserDefault floatForKey:kP2PMoveDuration];;
    return _p2pCommandDuration;
}

- (CGFloat)handFrequency{
    self.handFrequency = [kUserDefault floatForKey:kHandRequency] / 1000;
    return _handFrequency;
}

- (CGFloat)handDuration{
    self.handDuration = [kUserDefault floatForKey:kHandDuration];
    return _handDuration;
}

- (CGFloat)p2pHandDuration{
    _p2pHandDuration = [kUserDefault floatForKey:kP2PHandDuration];
    return _p2pHandDuration;
}

- (NSInteger)positionForCommand:(CameraCommand*)cmd{
    NSInteger pos = 0;
    switch (cmd.commandType) {
        case DemoCommandTypeHandClawClose:
        case DemoCommandTypeHandClawOpen:{
            pos = [_tfCPosition.text integerValue];
        }
            break;
        
        case DemoCommandTypeHandHUp:
        case DemoCommandTypeHandHDown:{
            pos = [_tfHPosition.text integerValue];
        }
            break;
            
        case DemoCommandTypeHandSUp:
        case DemoCommandTypeHandSDown:{
            pos = [_tfSPosition.text integerValue];
        }
            break;
            
        case DemoCommandTypeHandWRight:
        case DemoCommandTypeHandWLeft:{
            pos = [_tfWPosition.text integerValue];
        }
            break;
        default:
            break;
    }
    
    return pos;
}

//- (NSString*)recordingDuration{
//    NSString* rcDuration = [_tfRecordingDuration.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    if (rcDuration.integerValue == 0) {
//        rcDuration = @"0";
//    }
//    _recordingDuration = rcDuration;
//    return _recordingDuration;
//}

- (BOOL)isWaitForP2PCMDResponse{
    _isWaitForP2PCMDResponse = [kUserDefault floatForKey:kWaitForP2PCommand];
    return _isWaitForP2PCMDResponse;
}

- (BOOL)isWaitForCMDResponse{
    _isWaitForCMDResponse = [kUserDefault floatForKey:kWaitForCommand];
    return _isWaitForCMDResponse;
}

- (BOOL)shouldWaitForCMDResponse{
    return _isP2PMode ? self.isWaitForP2PCMDResponse : self.isWaitForCMDResponse;
}

- (instancetype)initWithNibNamed:(NSString*)nibName camCh:(CamChannel *)ach {
//- (instancetype)initWithNibNamed:(NSString*)nibName{
    self = [super init];
    if (self) {
        NSArray* arr = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        self = [arr firstObject];
        
        self.volumeCommand = [self commandWithType:DemoCommandTypeSPKVolume];
        self.recordingDurationList = @[@"30", @"60", @"120", @"180", @"240", @"300", @"600", @"0"];
        self.recordingDurationTitleList = @[@"30 seconds", @"1 minute", @"2 minutes", @"3 minutes", @"4 minutes", @"5 minutes", @"10 minutes", @"Unlimited"];
        
        //Uncomment to use picker view
        self.recordingPickerView = [UIPickerView new];
        self.recordingPickerView.delegate = self;
        self.tfRecordingDuration.inputView = _recordingPickerView;
        
        self.recordingDuration = _recordingDurationList[7];
        _tfRecordingDuration.text = _recordingDurationTitleList[7];
        
        [self updateMoveSpeedValueUI];
        
        //Talkback
        _btnTalkback.selected = [TalkbackManager sharedInstance].walkieTalkieEnabled;
        
        //Inch command
        [_switchInchCommand setOn:[[NSUserDefaults standardUserDefaults] boolForKey:IS_USING_MOVE_HAND_ABIT]];
        
        NSLog(@"%s", __func__);
    }
    
    return self;
}

- (CameraCommand*)commandWithType:(DemoCommandType)cmdType{
    return [[CameraCommandManager sharedInstance] commandWithType:cmdType];
}

- (CameraCommand*)inchCommandForCommand:(CameraCommand*)cmd{
    CameraCommand* inchCommand = nil;
    
    switch (cmd.commandType) {
        case DemoCommandTypeMoveForwardABit:
        case DemoCommandTypeMoveBackwardABit:
        case DemoCommandTypeMoveRightABit:
        case DemoCommandTypeMoveLeftABit:
        case DemoCommandTypeHandClawCloseABit:
        case DemoCommandTypeHandClawOpenABit:
        case DemoCommandTypeHandSUpABit:
        case DemoCommandTypeHandSDownABit:
        case DemoCommandTypeHandHUpABit:
        case DemoCommandTypeHandHDownABit:
        case DemoCommandTypeHandWLeftABit:
        case DemoCommandTypeHandWRightABit:{
            inchCommand = cmd;
        }
            break;
        case DemoCommandTypeMoveForward:{
            inchCommand = [self commandWithType:DemoCommandTypeMoveForwardABit];
        }
            break;
        case DemoCommandTypeMoveBackward:{
            inchCommand = [self commandWithType:DemoCommandTypeMoveBackwardABit];
        }
            break;
        case DemoCommandTypeMoveRight:{
            inchCommand = [self commandWithType:DemoCommandTypeMoveRightABit];
        }
            break;
        case DemoCommandTypeMoveLeft:{
            inchCommand = [self commandWithType:DemoCommandTypeMoveLeftABit];
        }
            break;
        case DemoCommandTypeHandClawClose:{
            inchCommand = [self commandWithType:DemoCommandTypeHandClawCloseABit];
        }
            break;
        case DemoCommandTypeHandClawOpen:{
            inchCommand = [self commandWithType:DemoCommandTypeHandClawOpenABit];
        }
            break;
        case DemoCommandTypeHandSUp:{
            inchCommand = [self commandWithType:DemoCommandTypeHandSUpABit];
        }
            break;
        case DemoCommandTypeHandSDown:{
            inchCommand = [self commandWithType:DemoCommandTypeHandSDownABit];
        }
            break;
        case DemoCommandTypeHandHUp:{
            inchCommand = [self commandWithType:DemoCommandTypeHandHUpABit];
        }
            break;
        case DemoCommandTypeHandHDown:{
            inchCommand = [self commandWithType:DemoCommandTypeHandHDownABit];
        }
            break;
        case DemoCommandTypeHandWLeft:{
            inchCommand = [self commandWithType:DemoCommandTypeHandWLeftABit];
        }
            break;
        case DemoCommandTypeHandWRight:{
            inchCommand = [self commandWithType:DemoCommandTypeHandWRightABit];
        }
            break;
        default:
            break;
    }
    
    return inchCommand;
}

-(void) didMoveToWindow {
    [super didMoveToWindow]; // (does nothing by default)
    if (self.window == nil) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
}

- (void)sendCommand:(CameraCommand*)cmd withGet:(BOOL)isGet completion:(void(^) (CameraCommand* command, BOOL success))completion{
//    DLog(@"PRE-WILL send command: %@, ts: %f", cmd.setCommand, NSDate.timeIntervalSinceReferenceDate);
    if (isGet) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            __block NSString *bodyKey = nil;
            
            if (_isP2PMode) {
                BOOL isReq = YES;
#if kIsFakedToTest
                isReq = NO;
#endif
                bodyKey = [[ReliableChannel sharedInstance] sendCommand:cmd.getCommand andFlag:isReq];
            }
            else {
                bodyKey = [[HttpCom instance].comWithDevice sendCommandAndBlock:cmd.getCommand];
            }
            
            DLog(@"Command:%@, p2p:%d, response:%@", cmd.getCommand, _isP2PMode, bodyKey);
            cmd.isLoading = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (bodyKey.length) {
                    NSArray *tokens = [bodyKey componentsSeparatedByString:@": "];
                    
                    if ([tokens count] >= 2 ) {
                        NSString *bodyPrefix = tokens[0];
                        
                        if ([bodyPrefix hasPrefix:cmd.getCommand]) { // ok
                            cmd.currentValue = tokens[1];
                            if (completion) {
                                completion(cmd, YES);
                            }
                            return;
                        }
                        else{
                            cmd.errorValue = bodyKey;
                        }
                    }
                }
                
                if (completion) {
                    completion(cmd, NO);
                }
            });
        });
    }
    else{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            __block NSString *bodyKey = nil;
            
            NSString* setCommand = cmd.setCommand; //[cmd.setCommand stringByAppendingFormat:@"&value=%@", cmd.currentValue];
            if (cmd.currentValue.length) {
                setCommand = [setCommand stringByAppendingFormat:@"&value=%@", cmd.currentValue];
            }
#if 1
            NSInteger t = cmd.commandType - DemoCommandTypeMove;
            BOOL isMoveCmd = (0 < t && t < 100);
            
            t = cmd.commandType - DemoCommandTypeHand;
            BOOL isHandCmd = (0 < t && t < 100);
            
            if ( isMoveCmd || isHandCmd) {
                NSInteger position = [self positionForCommand:cmd];
                if (isHandCmd && position > 0) {
                    //Use position option for Hand command
                    setCommand = [setCommand stringByAppendingFormat:@"&pos=%ld", (long)position];
                }
                else if (isMoveCmd && _tfMoveDistance.text.intValue > 0){
                    //Use distance option for Move command
                    setCommand = [setCommand stringByAppendingFormat:@"&distance=%ld", (long)_tfMoveDistance.text.integerValue];
                }
                else{
                    //Use duration option - default
                    //Move command duration uses milisecond
                    float duration = 0;
                    if (_isP2PMode) {
                        duration = isMoveCmd ? self.p2pCommandDuration : self.p2pHandDuration;
                    }
                    else{
                        duration = isMoveCmd ? self.commandDuration : self.handDuration;
                    }
                    
                    setCommand = [setCommand stringByAppendingFormat:@"&dur=%.0f", duration];
                }
            }
            
            if(cmd.commandType == DemoCommandTypeStartRecording){
                setCommand = [setCommand stringByAppendingFormat:@"&dur=%@",self.recordingDuration];
            }
#else
            if (cmd.commandType == DemoCommandTypeMoveForward
                || cmd.commandType == DemoCommandTypeMoveBackward
                || cmd.commandType == DemoCommandTypeMoveLeft
                || cmd.commandType == DemoCommandTypeMoveRight) {
                //Move command duration uses milisecond
                setCommand = [setCommand stringByAppendingFormat:@"&dur=%.0f", self.commandDuration];
                
                shouldInform = NO;
            }
#endif
            
            //Send time stamp for Wheel+Hand command, FW version >= 30
            if (cmd.commandType >= DemoCommandTypeHand && cmd.commandType <= DemoCommandTypeMoveRightABit) {
                NSDictionary *ssidInfo = [P2PConfigInfo fetchSSIDInfo];
                NSString *ssid = ssidInfo ? [ssidInfo objectForKey:@"SSID"] : nil;
                BOOL allowAttachTimestampOnP2P = [[NSUserDefaults standardUserDefaults] boolForKey:@"attach_timestamp_on_p2p"];
                BOOL allowSendingTimeStamp = NO;
                if (_isP2PMode) {
                    allowSendingTimeStamp = allowAttachTimestampOnP2P;
                }
                else{
                    allowSendingTimeStamp = YES;
                }
                
                if (allowSendingTimeStamp) {
                    CameraCommand* cmd = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeFWVersion];
                    NSArray* fwVersionComponents = [cmd.currentValue componentsSeparatedByString:@"."];
                    if (fwVersionComponents.count == 3) {
                        NSString* firstComponent = [fwVersionComponents firstObject];
                        NSString* lastComponent = [fwVersionComponents lastObject];
                        
                        if (firstComponent.integerValue >= 3 && lastComponent.integerValue >= 30) {
                            NSDate* currentDate = [NSDate date];
                            NSDateFormatter *df = [[NSDateFormatter alloc] init];
                            [df setDateFormat:@"HHmmss"];
                            NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithName:@"UTC"];
                            [df setTimeZone:inputTimeZone];
                            NSString* currentDateString = [df stringFromDate:currentDate];
                            setCommand = [setCommand stringByAppendingFormat:@"&ts=%@", currentDateString];
                        }
                    }
                }
            }
            
            if (_isP2PMode) {
                BOOL isReq = YES;
#if kIsFakedToTest
                isReq = NO;
#endif
                bodyKey = [[ReliableChannel sharedInstance] sendCommand:setCommand andFlag:isReq];
            }
            else {
                //BUG FIXING: Make sure no motor command send after hadling stop commands
                BOOL allowToHandleMotorCommand = NO;
                if ( isMoveCmd || isHandCmd ){
                    if (isMoveCmd
                        && _currentMoveRunLoopID < _currentStopMoveRunLoopID) {
                        allowToHandleMotorCommand = YES;
                    }
                    
                    if ((cmd.commandType == DemoCommandTypeHandClawClose || cmd.commandType == DemoCommandTypeHandClawOpen)
                        && _currentClawRunLoopID < _currentStopClawRunLoopID) {
                        allowToHandleMotorCommand = YES;
                    }
                    
                    if ((cmd.commandType == DemoCommandTypeHandHUp || cmd.commandType == DemoCommandTypeHandHDown)
                        && _currentHRunLoopID < _currentStopHRunLoopID) {
                        allowToHandleMotorCommand = YES;
                    }
                    
                    if ((cmd.commandType == DemoCommandTypeHandSUp || cmd.commandType == DemoCommandTypeHandSDown)
                        && _currentSRunLoopID < _currentStopSRunLoopID) {
                        allowToHandleMotorCommand = YES;
                    }
                    
                    if ((cmd.commandType == DemoCommandTypeHandWLeft || cmd.commandType == DemoCommandTypeHandWRight)
                        && _currentWRunLoopID < _currentStopWRunLoopID) {
                        allowToHandleMotorCommand = YES;
                    }
                    
                    if (allowToHandleMotorCommand) {
                        if (completion) {
                            completion(cmd, NO);
                        }
                        return ;
                    }
                }
                
//                DLog(@"WILL send command: %@, ts: %f", setCommand, NSDate.timeIntervalSinceReferenceDate);
                
                bodyKey = [[HttpCom instance].comWithDevice sendCommandAndBlock:setCommand withTimeout:5.0];
            }
            
            DLog(@"Sent command: %@, ts: %f, p2p: %d, response: %@", setCommand, NSDate.timeIntervalSinceReferenceDate, _isP2PMode, bodyKey);
            //dispatch_async(dispatch_get_main_queue(), ^{
                if (bodyKey.length) {
                    NSArray *tokens = [bodyKey componentsSeparatedByString:@": "];
                    
                    if ([tokens count] >= 2 ) {
                        NSString *bodyPrefix = tokens[0];
                        NSString *bodyValue = [tokens[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        if ([bodyPrefix hasPrefix:cmd.setCommand] && [@"0" isEqualToString:bodyValue]) { // ok
                            if (completion) {
                                completion(cmd, YES);
                            }
                            return;
                        }
                        else{
                            cmd.errorValue = bodyValue;
                        }
                    }
                    else{
                        cmd.errorValue = bodyKey;
                    }
                }
            
                if (completion) {
                    completion(cmd, NO);
                }
            //});
        });
    }
}

#pragma mark - UI ACTIONS

- (IBAction)buttonTouchedUp:(id)sender {
    CameraCommand* cmd = [self commandWithType:((UIButton*)sender).tag];
    DLog(@"************************************* Touched UP with command: %@ *************************************", cmd.setCommand);
    switch (cmd.commandType) {
        case DemoCommandTypeMoveForward:
        case DemoCommandTypeMoveBackward:
        case DemoCommandTypeMoveLeft:
        case DemoCommandTypeMoveRight:{
            self.currentMoveRunLoopID = [NSDate timeIntervalSinceReferenceDate];
            
            if (_isSendingMoveCommands) {
                //Send stop command
                [self handleSendStopCmd:cmd];
            }
            else {
                //New request: Do not send short command for wheel
                //[self sendMoveCMD:cmd isABit:YES];
                //DLog(@"------------ Sent short command: %@ ------------", cmd.setCommand);
            }
            
            [self updateMovePadUIWithCommand:cmd hightLighted:NO];
        }
            break;
        case DemoCommandTypeHandClawOpen:
        case DemoCommandTypeHandClawClose:{
            self.currentClawRunLoopID = [NSDate timeIntervalSinceReferenceDate];
            
            if (_isSendingClawCommands) {
                //Send stop command
                [self handleSendStopCmd:cmd];
            }
            else {
                //self.isSendingClawCommands = YES;
                //[self sendClawCMD:cmd isABit:YES];
                //DLog(@"------------ Sent short command: %@ ------------", cmd.setCommand);
            }
        }
            break;
        case DemoCommandTypeHandSDown:
        case DemoCommandTypeHandSUp:{
            self.currentSRunLoopID = [NSDate timeIntervalSinceReferenceDate];
            
            if (_isSendingSCommands) {
                //Send stop command
                [self handleSendStopCmd:cmd];
            }
            else {
                //self.isSendingSCommands = YES;
                //[self sendSCMD:cmd isABit:YES];
                //DLog(@"------------ Sent short command: %@ ------------", cmd.setCommand);
            }
        }
            break;
        case DemoCommandTypeHandHDown:
        case DemoCommandTypeHandHUp:{
            self.currentHRunLoopID = [NSDate timeIntervalSinceReferenceDate];
            
            if (_isSendingHCommands) {
                //Send stop command
                [self handleSendStopCmd:cmd];
            }
            else {
                //self.isSendingHCommands = YES;
                //[self sendHCMD:cmd isABit:YES];
                //DLog(@"------------ Sent short command: %@ ------------", cmd.setCommand);
            }
        }
            break;
        case DemoCommandTypeHandWRight:
        case DemoCommandTypeHandWLeft:{
            self.currentWRunLoopID = [NSDate timeIntervalSinceReferenceDate];
            
            if (_isSendingWCommands) {
                //Send stop command
                [self handleSendStopCmd:cmd];
            }
            else {
                //self.isSendingWCommands = YES;
                //[self sendWCMD:cmd isABit:YES];
                //DLog(@"------------ Sent short command: %@ ------------", cmd.setCommand);
            }
        }
            break;
       
        default:
            break;
    }
}

- (IBAction)buttonTouchedDown:(id)sender {
    CameraCommand* cmd = [self commandWithType:((UIButton*)sender).tag];
    DLog(@"************************* Touched DOWN with command: %@ *************************", cmd.setCommand);
    
    [self handleSendCmd:cmd];
}

- (IBAction)decreaseSpeedTapped:(id)sender {
    if ([self decreaseMoveSpeed]) {
        [self updateMoveSpeedValueUI];
    }
}

- (IBAction)increaseSpeedTapped:(id)sender {
    if([self increaseMoveSpeed]){
        [self updateMoveSpeedValueUI];
    }
}

- (IBAction)talkbackButtonTapped:(id)sender {
    if (_btnTalkback.selected) {
        //Stop talkback
        [[TalkbackManager sharedInstance] disableTalkbackWithCamCh:nil mode:_isP2PMode completion:^(BOOL success) {
            if (success) {
            }
            else{
            }
            _btnTalkback.selected = [TalkbackManager sharedInstance].walkieTalkieEnabled;
        }];
    }
    else{
        //Start talkback
        [[TalkbackManager sharedInstance] enableTalkbackWithCamCh:nil mode:_isP2PMode completion:^(BOOL success) {
            if (success) {
            }
            else{
            }
            _btnTalkback.selected = [TalkbackManager sharedInstance].walkieTalkieEnabled;
        }];
    }
}

- (void)stopTalkback
{
    DLog(@"%s selected:%d", __func__, _btnTalkback.selected);
    
    if (_btnTalkback.selected) {
        //Stop talkback
        [[TalkbackManager sharedInstance] disableTalkbackWithCamCh:nil mode:_isP2PMode completion:^(BOOL success) {
            DLog(@"%s success:%d", __func__, success);
            _btnTalkback.selected = [TalkbackManager sharedInstance].walkieTalkieEnabled;
        }];
    }
}

- (IBAction)sliderVolumeValueChanged:(id)sender {
    UISlider* vlSlider = (UISlider*)sender;
    CGFloat newVolumeValue = lroundf(vlSlider.value);
    NSString* newVLStr = [NSString stringWithFormat:@"%.0f", newVolumeValue];
    _lblVolumeValue.text = newVLStr;
}

- (void)enableStartingRecording{
    dispatch_async(dispatch_get_main_queue(), ^{
        _btnStartRecording.enabled = YES;
        _btnStartRecording.userInteractionEnabled = YES;
        
        _tfRecordingDuration.enabled = YES;
        _tfRecordingDuration.userInteractionEnabled = YES;
        
        [_activityRecording stopAnimating];
        _activityRecording.hidden = YES;
    });
}

- (void)disableStartingRecording{
    dispatch_async(dispatch_get_main_queue(), ^{
        _btnStartRecording.enabled = NO;
        _btnStartRecording.userInteractionEnabled = NO;
        
        _tfRecordingDuration.enabled = NO;
        _tfRecordingDuration.userInteractionEnabled = NO;
        
        _activityRecording.hidden = NO;
        [_activityRecording startAnimating];
    });
}

- (IBAction)butonStartRecordingTapped:(id)sender {
    [MBProgressHUD hideAllHUDsForView:self animated:NO];
    
    CameraCommand* cmd = [self commandWithType:DemoCommandTypeStartRecording];
    
    NSString* duration = [_tfRecordingDuration.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (duration.length) {
        _activityRecording.hidden = NO;
        [_activityRecording startAnimating];
        __weak CameraCommandTableHeaderView* weakSelf = self;
        [self sendCommand:cmd withGet:NO completion:^(CameraCommand *command, BOOL success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_activityRecording stopAnimating];
            });
            if (success) {
                [weakSelf disableStartingRecording];
                dispatch_async(dispatch_get_main_queue(), ^{
                    MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
                    [hub setLabelText:@"Success"];
                    [hub hide:NO afterDelay:2.0];
                });
            }
            else{
                //TODO: show error message if needed
                [weakSelf enableStartingRecording];
                
                NSString* errmessage = @"";
                
                if ([@"-2" isEqualToString:command.errorValue]) {
                    errmessage = @"SD card is not inserted";
                }
                else if ([@"-3" isEqualToString:command.errorValue]) {
                    errmessage = @"SD card is full";
                }
                else if ([@"-4" isEqualToString:command.errorValue]) {
                    errmessage = @"Recording is running";
                }
                else {
                    errmessage = @"Unknown error!";
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
                    [hub setLabelText:errmessage];
                    [hub hide:NO afterDelay:2.0];
                });
            }
         }];
    }
    else{
        [_tfRecordingDuration becomeFirstResponder];
    }
}

- (IBAction)buttonEndResordingTapped:(id)sender {
    [MBProgressHUD hideAllHUDsForView:self animated:NO];
    
    CameraCommand* cmd = [self commandWithType:DemoCommandTypeStopRecording];
    __weak CameraCommandTableHeaderView* weakSelf = self;
    [self sendCommand:cmd withGet:NO completion:^(CameraCommand *command, BOOL success) {
        if (success) {
            [weakSelf enableStartingRecording];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
                [hub setLabelText:@"Success"];
                [hub hide:NO afterDelay:2.0];
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
                [hub setLabelText:@"Failed"];
                [hub hide:NO afterDelay:2.0];
            });
        }
    }];
}


- (IBAction)sliderVolumeTouchedUp:(id)sender {
    CGFloat newVolumeValue = lroundf(_sliderVolume.value);
    NSString* newVLStr = [NSString stringWithFormat:@"%.0f", newVolumeValue];
    if (![newVLStr isEqualToString:_volumeCommand.currentValue]) {
        _volumeCommand.currentValue = newVLStr;
        [self performSelector:@selector(sendVolumeCMD:) withObject:_volumeCommand afterDelay:CMD_VOLUME_DURATION];
    }
}

- (IBAction)takeSnapshot:(id)sender {
    [MBProgressHUD hideAllHUDsForView:self animated:NO];
    _btnTakeSnapshot.enabled = NO;
    _btnTakeSnapshot.userInteractionEnabled = NO;
    
    CameraCommand* cmd = [self commandWithType:DemoCommandTypeTakeSnapshot];
    [self sendCommand:cmd withGet:YES completion:^(CameraCommand *command, BOOL success) {
        _btnTakeSnapshot.enabled = YES;
        _btnTakeSnapshot.userInteractionEnabled = YES;
        if (success) {
            if ([@"-2" isEqualToString:command.currentValue]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
                    [hub setLabelText:@"SD card is not inserted"];
                    [hub hide:NO afterDelay:2.0];
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
                    [hub setLabelText:[NSString stringWithFormat:@"Success: %@", command.currentValue]];
                    [hub hide:NO afterDelay:1.0];
                });
            }
        }
    }];
}

- (IBAction)takeOffButtonTapped:(id)sender {
    [MBProgressHUD hideAllHUDsForView:self animated:NO];
    
    CameraCommand* cmd = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeFlightResponse];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        __block NSString *bodyKey = nil;
        
        NSString* setCommand = [NSString stringWithFormat:@"%@&index=3&value=1", cmd.setCommand];
        
        if (_isP2PMode) {
            BOOL isReq = YES;
#if kIsFakedToTest
            isReq = NO;
#endif
            NSString* t = setCommand;
            
            NSLog(@"%s t:%@", __func__, t);
            bodyKey = [[ReliableChannel sharedInstance] sendCommand:setCommand andFlag:isReq];

        }
        else {
            bodyKey = [[HttpCom instance].comWithDevice sendCommandAndBlock:setCommand];
        }
        
        DLog(@"Sent command: %@, ts:%f, p2p:%d, response:%@", setCommand, NSDate.timeIntervalSinceReferenceDate, _isP2PMode, bodyKey);
        if (bodyKey.length) {
            NSArray *tokens = [bodyKey componentsSeparatedByString:@": "];
            
            if ([tokens count] >= 2 ) {
                NSString *bodyPrefix = tokens[0];
                NSString *bodyValue = [tokens[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                if ([bodyPrefix hasPrefix:cmd.setCommand] && [@"0" isEqualToString:bodyValue]) { // ok
                    dispatch_async(dispatch_get_main_queue(), ^{
                        MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
                        [hub setLabelText:@"Success"];
                        [hub hide:NO afterDelay:2.0];
                    });

                    DLog(@"Send flight response command successfully");
                    return;
                }
                else{
                    cmd.errorValue = bodyValue;
                }
            }
            else{
                cmd.errorValue = bodyKey;
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
            [hub setLabelText:@"Failed"];
            [hub hide:NO afterDelay:2.0];
        });
        
        DLog(@"Send flight response command failed");
    });
}

- (IBAction)landButtonTapped:(id)sender {
    [MBProgressHUD hideAllHUDsForView:self animated:NO];
    
    CameraCommand* cmd = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeFlightResponse];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        __block NSString *bodyKey = nil;
        
        NSString* setCommand = [NSString stringWithFormat:@"%@&index=4&value=1", cmd.setCommand];
        
        if (_isP2PMode) {
            BOOL isReq = YES;
#if kIsFakedToTest
            isReq = NO;
#endif
            NSString* t = setCommand;
            
            NSLog(@"%s t:%@", __func__, t);
            bodyKey = [[ReliableChannel sharedInstance] sendCommand:setCommand andFlag:isReq];

        }
        else {
            bodyKey = [[HttpCom instance].comWithDevice sendCommandAndBlock:setCommand];
        }
        
        DLog(@"Sent command:%@, ts:%f, p2p:%d, response:%@", setCommand, NSDate.timeIntervalSinceReferenceDate, _isP2PMode, bodyKey);
        if (bodyKey.length) {
            NSArray *tokens = [bodyKey componentsSeparatedByString:@": "];
            
            if ([tokens count] >= 2 ) {
                NSString *bodyPrefix = tokens[0];
                NSString *bodyValue = [tokens[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                if ([bodyPrefix hasPrefix:cmd.setCommand] && [@"0" isEqualToString:bodyValue]) { // ok
                    dispatch_async(dispatch_get_main_queue(), ^{
                        MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
                        [hub setLabelText:@"Success"];
                        [hub hide:NO afterDelay:2.0];
                    });
                    
                    DLog(@"Send flight response command successfully");
                    return;
                }
                else{
                    cmd.errorValue = bodyValue;
                }
            }
            else{
                cmd.errorValue = bodyKey;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self animated:NO];
            [hub setLabelText:@"Failed"];
            [hub hide:NO afterDelay:2.0];
        });
        
        DLog(@"Send flight response command failed");
    });
}

- (IBAction)switchInchCommandValueChanged:(id)sender {
    BOOL isUsingInchCommand = _switchInchCommand.isOn;
    [[NSUserDefaults standardUserDefaults] setBool:isUsingInchCommand forKey:IS_USING_MOVE_HAND_ABIT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)btnHandInitSeqTapped:(id)sender {
    CameraCommand *cmd = [self commandWithType:DemoCommandTypeHandsInit];
    self.isSendingCmdHandsInit = YES;
    [self sendHandsInitCmd:cmd];
}

- (void)updateMoveSpeedValueUI{
    _txtMoveSpeed.text = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
}

- (void)updateMovePadUIWithCommand:(CameraCommand*)cmd hightLighted:(BOOL)hightLighted{
    NSString* imageSuffix = hightLighted ? @"_hl" : @"";
    switch (cmd.commandType) {
        case DemoCommandTypeMoveForward:
            _imvMoveForward.image = [UIImage imageNamed:[@"pan_up" stringByAppendingString:imageSuffix]];
            break;
        case DemoCommandTypeMoveBackward:
            _imvMoveBackward.image = [UIImage imageNamed:[@"pan_down" stringByAppendingString:imageSuffix]];
            break;
        case DemoCommandTypeMoveLeft:
            _imvMoveLeft.image = [UIImage imageNamed:[@"pan_left" stringByAppendingString:imageSuffix]];
            break;
        case DemoCommandTypeMoveRight:
            _imvMoveRight.image = [UIImage imageNamed:[@"pan_right" stringByAppendingString:imageSuffix]];
            break;
        default:
            break;
    }
}

#pragma mark - SEND CUSTOM COMMANDS
- (void)handleSendCmd:(CameraCommand *)cmd
{
    //CGFloat firstDelayTime = self.commandFrequency;
    //CGFloat firstDelayTime = 0;
    switch (cmd.commandType) {
        case DemoCommandTypeMoveForward:
        case DemoCommandTypeMoveBackward:
        case DemoCommandTypeMoveLeft:
        case DemoCommandTypeMoveRight:{
            self.currentMoveRunLoopID = [NSDate timeIntervalSinceReferenceDate];
            
            [self sendMoveCMD:cmd withRunLoopID:_currentMoveRunLoopID];
            
            [self updateMovePadUIWithCommand:cmd hightLighted:YES];
            break;
        }
        case DemoCommandTypeHandClawOpen:
        case DemoCommandTypeHandClawClose:{
            //Ensure new loop id is distinct
            NSTimeInterval currentTimestamp = [NSDate timeIntervalSinceReferenceDate];
            if (currentTimestamp == self.currentClawRunLoopID) {
                --currentTimestamp;
            }
            self.currentClawRunLoopID = currentTimestamp;
            [self sendClawCMD:cmd withRunLoopID:_currentClawRunLoopID];
            break;
        }
        case DemoCommandTypeHandSDown:
        case DemoCommandTypeHandSUp:{
            //Ensure new loop id is distinct
            NSTimeInterval currentTimestamp = [NSDate timeIntervalSinceReferenceDate];
            if (currentTimestamp == self.currentSRunLoopID) {
                --currentTimestamp;
            }
            self.currentSRunLoopID = currentTimestamp;
            [self sendSCMD:cmd withRunLoopID:_currentSRunLoopID];
            break;
        }
        case DemoCommandTypeHandHDown:
        case DemoCommandTypeHandHUp:{
            //Ensure new loop id is distinct
            NSTimeInterval currentTimestamp = [NSDate timeIntervalSinceReferenceDate];
            if (currentTimestamp == self.currentHRunLoopID) {
                --currentTimestamp;
            }
            self.currentHRunLoopID = currentTimestamp;
            [self sendHCMD:cmd withRunLoopID:_currentHRunLoopID];
            break;
        }
        case DemoCommandTypeHandWRight:
        case DemoCommandTypeHandWLeft:{
            //Ensure new loop id is distinct
            NSTimeInterval currentTimestamp = [NSDate timeIntervalSinceReferenceDate];
            if (currentTimestamp == self.currentWRunLoopID) {
                --currentTimestamp;
            }
            self.currentWRunLoopID = currentTimestamp;
            [self sendWCMD:cmd withRunLoopID:_currentWRunLoopID];
            break;
        }
            
        default:
            break;
    }
}

- (void)handleSendStopCmd:(CameraCommand *)cmd
{
    //Don't send stop command if it's using move inch commands
    if (self.isUsingAbitCommands) {
        self.isSendingMoveCommands = NO;
        self.isSendingClawCommands = NO;
        self.isSendingSCommands = NO;
        self.isSendingHCommands = NO;
        self.isSendingWCommands = NO;
        return;
    }
    
    switch (cmd.commandType) {
        case DemoCommandTypeMoveForward:
        case DemoCommandTypeMoveBackward:
        case DemoCommandTypeMoveLeft:
        case DemoCommandTypeMoveRight:{
            self.isSendingMoveCommands = NO;
            
            if (_tfMoveDistance.text.intValue == 0) {
                //New solution for PTZ: Cancel all previous stop move run loop
                self.currentStopMoveRunLoopID = [NSDate timeIntervalSinceReferenceDate];
                [self sendStopCommandWithDict:@{kCommandType: @(DemoCommandTypeFBStop),
                                                kStopCommandRetryTime: @(RETRY_STOP_CMD_TIMES),
                                                kStopCommandRunLoopID: @(_currentStopMoveRunLoopID)}];
                DLog(@"------------ Sent stop with command: %@ ------------", cmd.setCommand);
            }
        }
            break;
        case DemoCommandTypeHandClawOpen:
        case DemoCommandTypeHandClawClose:{
            self.isSendingClawCommands = NO;
            
            if (_tfCPosition.text.intValue == 0) {
                //New solution for Hands: Cancel all previous stop move run loop
                self.currentStopClawRunLoopID = [NSDate timeIntervalSinceReferenceDate];
                [self sendStopCommandWithDict:@{kCommandType: @(DemoCommandTypeStopClaw),
                                                kStopCommandRetryTime: @(RETRY_STOP_CMD_TIMES),
                                                kStopCommandRunLoopID: @(_currentStopClawRunLoopID)}];
                DLog(@"------------ Sent stop with command: %@ ------------", cmd.setCommand);
            }
        }
            break;
        case DemoCommandTypeHandSDown:
        case DemoCommandTypeHandSUp:{
            self.isSendingSCommands = NO;
            
            if (_tfSPosition.text.intValue == 0) {
                //New solution for Hands: Cancel all previous stop move run loop
                self.currentStopSRunLoopID = [NSDate timeIntervalSinceReferenceDate];
                [self sendStopCommandWithDict:@{kCommandType: @(DemoCommandTypeStopS),
                                                kStopCommandRetryTime: @(RETRY_STOP_CMD_TIMES),
                                                kStopCommandRunLoopID: @(_currentStopSRunLoopID)}];
                DLog(@"------------ Sent stop with command: %@ ------------", cmd.setCommand);
            }
        }
            break;
        case DemoCommandTypeHandHDown:
        case DemoCommandTypeHandHUp:{
            self.isSendingHCommands = NO;
            
            if (_tfHPosition.text.intValue == 0) {
                //New solution for Hands: Cancel all previous stop move run loop
                self.currentStopHRunLoopID = [NSDate timeIntervalSinceReferenceDate];
                [self sendStopCommandWithDict:@{kCommandType: @(DemoCommandTypeStopH),
                                                kStopCommandRetryTime: @(RETRY_STOP_CMD_TIMES),
                                                kStopCommandRunLoopID: @(_currentStopHRunLoopID)}];
                DLog(@"------------ Sent stop with command: %@ ------------", cmd.setCommand);
            }
        }
            break;
        case DemoCommandTypeHandWRight:
        case DemoCommandTypeHandWLeft:{
            self.isSendingWCommands = NO;
            
            if (_tfWPosition.text.intValue == 0) {
                //New solution for Hands: Cancel all previous stop move run loop
                self.currentStopWRunLoopID = [NSDate timeIntervalSinceReferenceDate];
                [self sendStopCommandWithDict:@{kCommandType: @(DemoCommandTypeStopW),
                                                kStopCommandRetryTime: @(RETRY_STOP_CMD_TIMES),
                                                kStopCommandRunLoopID: @(_currentStopWRunLoopID)}];
                DLog(@"------------ Sent stop with command: %@ ------------", cmd.setCommand);
            }
        }
            break;
        default:
            break;
    }
}

- (void)sendHandsInitCmd:(CameraCommand*)cmd {
    if (!_isSendingCmdHandsInit) {
        return;
    }
    
    DLog(@"%s: %@", __FUNCTION__, cmd.setCommand);
    
    [self sendCommand:cmd withGet:NO completion:^(CameraCommand *command, BOOL success) {
        self.isSendingCmdHandsInit = NO;
        DLog(@"%s cmd:%@, success:%d", __func__, cmd.getCommand, success);
    }];
}

- (void)sendStopWithCmdType:(DemoCommandType)cmdType retriedTimes:(NSInteger)retriedTimes{
    CameraCommand* cmd = [self commandWithType:cmdType];
    
    if (_isP2PMode) {
        [self sendCommand:cmd withGet:NO completion:^(CameraCommand *command, BOOL success) {
            DLog(@"%s cmd:%@, success:%d, retries:%ld", __func__, cmd.setCommand, success, (long)retriedTimes);
            //Retry sending on P2P mode when get failure
            if (!success && retriedTimes > 0) {
                
                [NSThread sleepForTimeInterval:RETRY_STOP_CMD_INETRVAL];
                
                switch (cmd.commandType) {
                    case DemoCommandTypeFBStop:{
                        //Only send when there is no command sending
                        if(!self.isSendingMoveCommands){
                            [self sendStopWithCmdType:DemoCommandTypeFBStop retriedTimes:retriedTimes - 1];
                        }
                    }
                        break;
                    case DemoCommandTypeStopClaw:{
                        if(!self.isSendingClawCommands){
                            [self sendStopWithCmdType:DemoCommandTypeStopClaw retriedTimes:retriedTimes - 1];
                        }
                    }
                        break;
                    case DemoCommandTypeStopS:{
                        if(!self.isSendingSCommands){
                            [self sendStopWithCmdType:DemoCommandTypeStopS retriedTimes:retriedTimes - 1];
                        }
                    }
                        break;
                    case DemoCommandTypeStopH:{
                        if(!self.isSendingHCommands){
                            [self sendStopWithCmdType:DemoCommandTypeStopH retriedTimes:retriedTimes - 1];
                        }
                    }
                        break;
                    case DemoCommandTypeStopW:{
                        if(!self.isSendingWCommands){
                            [self sendStopWithCmdType:DemoCommandTypeStopW retriedTimes:retriedTimes - 1];
                        }
                    }
                        break;
                    default:
                        break;
                }
            }
        }];
    }
    else{
        [self sendCommand:cmd withGet:NO completion:^(CameraCommand *command, BOOL success) {
            DLog(@"%s cmd:%@, success:%d", __func__, cmd.setCommand, success);
        }];
    }
}

- (void)sendVolumeCMD:(CameraCommand*)cmd{
    _sliderVolume.userInteractionEnabled = NO;
    _activityVolume.hidden = NO;
    [_activityVolume startAnimating];
    [self sendCommand:cmd withGet:NO completion:^(CameraCommand *command, BOOL success) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _sliderVolume.userInteractionEnabled = YES;
            [_activityVolume stopAnimating];
            _activityVolume.hidden = YES;
        });
    }];
}

- (void)getVolumeValueWithCompletion:(void(^)(CameraCommand* commmand, BOOL success))completion{
    [self sendCommand:_volumeCommand withGet:YES completion:^(CameraCommand *command, BOOL success) {
        completion(command, success);
    }];
}

- (void)reloadVolumeControl{
    if (_sliderVolume) {
        _sliderVolume.userInteractionEnabled = NO;
        [self getVolumeValueWithCompletion:^(CameraCommand *command, BOOL success) {
            if (success) {
                _sliderVolume.value = [command.currentValue floatValue];
                _lblVolumeValue.text = command.currentValue;
            }
            else{
                command.currentValue = @"0";
                _sliderVolume.value = 0;
            }
            _sliderVolume.userInteractionEnabled = YES;
            [_activityVolume stopAnimating];
        }];
    }
}

- (void)sendStopCommandWithDict:(NSDictionary*)moveCommandDict{
    DemoCommandType cmdType = [[moveCommandDict objectForKey:kCommandType] integerValue];
    int retriedTimes = [[moveCommandDict objectForKey:kStopCommandRetryTime] intValue];
    NSTimeInterval runLoopID = [[moveCommandDict objectForKey:kStopCommandRunLoopID] doubleValue];
    
    CameraCommand* cmd = [self commandWithType:cmdType];
    
    if (_isP2PMode) {
        [self sendCommand:cmd withGet:NO completion:^(CameraCommand *command, BOOL success) {
            DLog(@"%s cmd:%@, success:%d, retries:%ld", __func__, cmd.setCommand, success, (long)retriedTimes);
            //Retry sending on P2P mode when get failure
            if (!success && retriedTimes > 0) {
                
                [NSThread sleepForTimeInterval:RETRY_STOP_CMD_INETRVAL];
                switch (cmd.commandType) {
                    case DemoCommandTypeFBStop:{
                        if (!self.isSendingMoveCommands && runLoopID == _currentStopMoveRunLoopID) {
                            [self performSelector:@selector(sendStopCommandWithDict:) withObject:@{kCommandType: @(cmdType),
                                                                                                   kStopCommandRetryTime: @(retriedTimes - 1),
                                                                                                   kStopCommandRunLoopID: @(runLoopID)
                                                                                                   }];
                        }
                    }
                        break;
                    case DemoCommandTypeStopClaw:{
                        if (!self.isSendingClawCommands && runLoopID == _currentStopClawRunLoopID) {
                            [self performSelector:@selector(sendStopCommandWithDict:) withObject:@{kCommandType: @(cmdType),
                                                                                                   kStopCommandRetryTime: @(retriedTimes - 1),
                                                                                                   kStopCommandRunLoopID: @(runLoopID)
                                                                                                   }];
                        }
                    }
                        break;
                    case DemoCommandTypeStopS:{
                        if (!self.isSendingSCommands && runLoopID == _currentStopSRunLoopID) {
                            [self performSelector:@selector(sendStopCommandWithDict:) withObject:@{kCommandType: @(cmdType),
                                                                                                   kStopCommandRetryTime: @(retriedTimes - 1),
                                                                                                   kStopCommandRunLoopID: @(runLoopID)
                                                                                                   }];
                        }
                    }
                        break;
                    case DemoCommandTypeStopH:{
                        if (!self.isSendingHCommands && runLoopID == _currentStopHRunLoopID) {
                            [self performSelector:@selector(sendStopCommandWithDict:) withObject:@{kCommandType: @(cmdType),
                                                                                                   kStopCommandRetryTime: @(retriedTimes - 1),
                                                                                                   kStopCommandRunLoopID: @(runLoopID)
                                                                                                   }];
                        }
                    }
                        break;
                    case DemoCommandTypeStopW:{
                        if (!self.isSendingWCommands && runLoopID == _currentStopWRunLoopID) {
                            [self performSelector:@selector(sendStopCommandWithDict:) withObject:@{kCommandType: @(cmdType),
                                                                                                   kStopCommandRetryTime: @(retriedTimes - 1),
                                                                                                   kStopCommandRunLoopID: @(runLoopID)
                                                                                                   }];
                        }
                    }
                        break;
                    default:
                        break;
                }
                
            }
        }];
    }
    else{
        [self sendCommand:cmd withGet:NO completion:^(CameraCommand *command, BOOL success) {
        }];
    }
}

#pragma mark - Move

- (void)sendMoveCMD:(CameraCommand*)cmd withRunLoopID:(NSTimeInterval)runLoopID{
    if (runLoopID != self.currentMoveRunLoopID) {
        //don't execute old command loops
        //DLog(@"--------- ignore command: %@", cmd.setCommand);
        return;
    }
    
    CameraCommand* moveCommand = nil;
    
    if (self.isUsingAbitCommands) {
        moveCommand = [self inchCommandForCommand:cmd];
        if (!moveCommand) {
            return;
        }
    }
    else{
        cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
        moveCommand = cmd;
    }

    self.isSendingMoveCommands = YES;
    [self sendCommand:moveCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
        if (self.isSendingMoveCommands && self.shouldWaitForCMDResponse && (runLoopID == self.currentMoveRunLoopID)) {
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendMoveCMD:cmd withRunLoopID:runLoopID];
        }
    }];
    
    //for test: Send get_boundary_position comment to check
    NSLog(@"Sent command: %@", moveCommand.setCommand);
    CameraCommand* boundaryCMD = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeBoundaryPosition];
    [self sendCommand:boundaryCMD withGet:YES completion:^(CameraCommand *command, BOOL success) {
        NSLog(@"get_boundary: %@", command.currentValue);
    }];
    //
    
    if(_isSendingMoveCommands&& !self.shouldWaitForCMDResponse){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendMoveCMD:cmd withRunLoopID:runLoopID];
        });
    }
}

/*
 Send move a bit command, this is not currently used. Just this keep to reuse if needed
 */
- (void)sendMoveCMD:(CameraCommand*)cmd  isABit:(BOOL)isABit{
    cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
    CameraCommand* moveCommand = cmd;
    
    if (isABit) {
        moveCommand = [self inchCommandForCommand:cmd];
    }
    
    [self sendCommand:moveCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
    }];
}

#pragma mark - Claw

- (void)sendClawCMD:(CameraCommand*)cmd withRunLoopID:(NSTimeInterval)runLoopID{
    
    if (runLoopID != _currentClawRunLoopID) {
        //don't execute old command loops
        //DLog(@"--------- ignore command: %@", cmd.setCommand);
        return;
    }
    
    CameraCommand* handCommand = nil;
    
    if (self.isUsingAbitCommands) {
        handCommand = [self inchCommandForCommand:cmd];
        if (!handCommand) {
            return;
        }
    }
    else{
        cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
        handCommand = cmd;
    }
    
    self.isSendingClawCommands = YES;
    [self sendCommand:handCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
        if (self.isSendingClawCommands && self.shouldWaitForCMDResponse) {
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendClawCMD:cmd withRunLoopID:runLoopID];
        }
    }];
    
    //for test: Send get_boundary_position comment to check
    NSLog(@"Sent command: %@", handCommand.setCommand);
    CameraCommand* boundaryCMD = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeBoundaryPosition];
    [self sendCommand:boundaryCMD withGet:YES completion:^(CameraCommand *command, BOOL success) {
        NSLog(@"get_boundary: %@", command.currentValue);
    }];
    //
    
    if(_isSendingClawCommands&& !self.shouldWaitForCMDResponse){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendClawCMD:cmd withRunLoopID:runLoopID];
        });
    }
}

- (void)sendClawCMD:(CameraCommand*)cmd isABit:(BOOL)isABit {

    cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
    CameraCommand* handCommand = cmd;
    
    if (isABit) {
        handCommand = [self inchCommandForCommand:cmd];
    }
    
    [self sendCommand:handCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
    }];
}

#pragma mark - S

- (void)sendSCMD:(CameraCommand*)cmd withRunLoopID:(NSTimeInterval)runLoopID{
    
    if (runLoopID != _currentSRunLoopID) {
        //don't execute old command loops
        //DLog(@"--------- ignore command: %@", cmd.setCommand);
        return;
    }
    
    CameraCommand* sCommand = nil;
    
    if (self.isUsingAbitCommands) {
        sCommand = [self inchCommandForCommand:cmd];
        if (!sCommand) {
            return;
        }
    }
    else{
        cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
        sCommand = cmd;
    }
    
    self.isSendingSCommands = YES;
    [self sendCommand:sCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
        if (self.isSendingSCommands && self.shouldWaitForCMDResponse) {
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendSCMD:cmd withRunLoopID:runLoopID];
        }
    }];
    
    //for test: Send get_boundary_position comment to check
    NSLog(@"Sent command: %@", sCommand.setCommand);
    CameraCommand* boundaryCMD = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeBoundaryPosition];
    [self sendCommand:boundaryCMD withGet:YES completion:^(CameraCommand *command, BOOL success) {
        NSLog(@"get_boundary: %@", command.currentValue);
    }];
    //
    
    if(_isSendingSCommands&& !self.shouldWaitForCMDResponse){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendSCMD:cmd withRunLoopID:runLoopID];
        });
    }
}

- (void)sendSCMD:(CameraCommand*)cmd isABit:(BOOL)isABit {
    cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
    CameraCommand* handCommand = cmd;
    
    if (isABit) {
        handCommand = [self inchCommandForCommand:cmd];
    }
    
    [self sendCommand:handCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
    }];
}

#pragma mark - H

- (void)sendHCMD:(CameraCommand*)cmd withRunLoopID:(NSTimeInterval)runLoopID{
    
    if (runLoopID != _currentHRunLoopID) {
        //don't execute old command loops
        //DLog(@"--------- ignore command: %@", cmd.setCommand);
        return;
    }
    
    CameraCommand* hCommand = nil;
    
    if (self.isUsingAbitCommands) {
        hCommand = [self inchCommandForCommand:cmd];
        if (!hCommand) {
            return;
        }
    }
    else{
        cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
        hCommand = cmd;
    }
    
    self.isSendingHCommands = YES;
    [self sendCommand:hCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
        if (self.isSendingHCommands && self.shouldWaitForCMDResponse) {
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendHCMD:cmd withRunLoopID:runLoopID];
        }
    }];
    
    //for test: Send get_boundary_position comment to check
    NSLog(@"Sent command: %@", hCommand.setCommand);
    CameraCommand* boundaryCMD = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeBoundaryPosition];
    [self sendCommand:boundaryCMD withGet:YES completion:^(CameraCommand *command, BOOL success) {
        NSLog(@"get_boundary: %@", command.currentValue);
    }];
    //
    
    if(_isSendingHCommands&& !self.shouldWaitForCMDResponse){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendHCMD:cmd withRunLoopID:runLoopID];
        });
    }
}

- (void)sendHCMD:(CameraCommand*)cmd isABit:(BOOL)isABit {
    cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
    CameraCommand* handCommand = cmd;
    
    if (isABit) {
        handCommand = [self inchCommandForCommand:cmd];
    }
    
    [self sendCommand:handCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
    }];
}

#pragma mark - W

- (void)sendWCMD:(CameraCommand*)cmd withRunLoopID:(NSTimeInterval)runLoopID{
    
    if (runLoopID != _currentWRunLoopID) {
        //don't execute old command loops
        //DLog(@"--------- ignore command: %@", cmd.setCommand);
        return;
    }

    CameraCommand* wCommand = nil;
    
    if (self.isUsingAbitCommands) {
        wCommand = [self inchCommandForCommand:cmd];
        if (!wCommand) {
            return;
        }
    }
    else{
        cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
        wCommand = cmd;
    }
    
    self.isSendingWCommands = YES;
    [self sendCommand:wCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
        if (self.isSendingWCommands && self.shouldWaitForCMDResponse) {
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendWCMD:cmd withRunLoopID:runLoopID];
        }
    }];
    
    //for test: Send get_boundary_position comment to check
    NSLog(@"Sent command: %@", wCommand.setCommand);
    CameraCommand* boundaryCMD = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeBoundaryPosition];
    [self sendCommand:boundaryCMD withGet:YES completion:^(CameraCommand *command, BOOL success) {
        NSLog(@"get_boundary: %@", command.currentValue);
    }];
    //
    
    if(_isSendingWCommands&& !self.shouldWaitForCMDResponse){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [NSThread sleepForTimeInterval:self.commandFrequency];
            [self sendWCMD:cmd withRunLoopID:runLoopID];
        });
    }
}

- (void)sendWCMD:(CameraCommand*)cmd isABit:(BOOL)isABit {
    cmd.currentValue = [NSString stringWithFormat:@"%ld", (long)self.moveSpeedValue];
    CameraCommand* handCommand = cmd;
    
    if (isABit) {
        handCommand = [self inchCommandForCommand:cmd];
    }
    
    [self sendCommand:handCommand withGet:NO completion:^(CameraCommand *command, BOOL success) {
    }];
}

#pragma mark - Increase/Decrease move speed

- (BOOL)decreaseMoveSpeed{
    int currentSpeedLevel = (int)(self.moveSpeedValue - MIN_OF_MOVE_SPEED) / MOVE_SPEED_LEVEL_DURATION;
    int currentRedundantSpeed = (self.moveSpeedValue - MIN_OF_MOVE_SPEED) % MOVE_SPEED_LEVEL_DURATION;
    if (currentSpeedLevel > 0) {
        if (currentRedundantSpeed > 0) {
            self.moveSpeedValue = (currentSpeedLevel) * MOVE_SPEED_LEVEL_DURATION + MIN_OF_MOVE_SPEED;
        }else{
            self.moveSpeedValue = (currentSpeedLevel - 1) * MOVE_SPEED_LEVEL_DURATION + MIN_OF_MOVE_SPEED;
        }
        [[NSUserDefaults standardUserDefaults] setInteger:self.moveSpeedValue forKey:MOVE_SPEED_VALUE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return YES;
    }else if(currentRedundantSpeed > 0){
        self.moveSpeedValue = DEFAULT_MOVE_SPEED_VALUE;
        [[NSUserDefaults standardUserDefaults] setInteger:self.moveSpeedValue forKey:MOVE_SPEED_VALUE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return YES;
    }
    
    return NO;
}

- (BOOL)increaseMoveSpeed{
    int currentSpeedLevel = (int)(self.moveSpeedValue - MIN_OF_MOVE_SPEED) / MOVE_SPEED_LEVEL_DURATION;
    if (currentSpeedLevel < NUM_OF_LEVEL) {
        self.moveSpeedValue = (currentSpeedLevel + 1) * MOVE_SPEED_LEVEL_DURATION + MIN_OF_MOVE_SPEED;
        [[NSUserDefaults standardUserDefaults] setInteger:self.moveSpeedValue forKey:MOVE_SPEED_VALUE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return YES;
    }
    
    return NO;
}

#pragma mark - TalkbackManagerDelegate

- (void)talkbackFailedWithError:(NSError *)error{
    _btnTalkback.selected = [TalkbackManager sharedInstance].walkieTalkieEnabled;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
#if 0
    NSString* newString = [[textField.text stringByReplacingCharactersInRange:range withString:string]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *regex = @"[0-9]*";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isValidateNumber = [predicate evaluateWithObject:newString];
    
    if (isValidateNumber) {
        self.recordingDuration = newString;
        return YES;
    }
    
    return NO;
#else
    return YES;
#endif
}

#pragma mark - UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return _recordingDurationList.count;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return _recordingDurationTitleList[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.recordingDuration = _recordingDurationList[row];
    _tfRecordingDuration.text = _recordingDurationTitleList[row];
}

@end
