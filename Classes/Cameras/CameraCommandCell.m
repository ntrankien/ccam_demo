//
//  CameraCommandCell.m
//  App
//
//  Created by Tran Kien Nghi on 4/29/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraCommandCell.h"
#import "CameraCommand.h"

@interface CameraCommandCell()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblCommandTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSendCommand;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)btnSendCommandTapped:(id)sender;
- (IBAction)switchChangeValueChanged:(id)sender;

@end

@implementation CameraCommandCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _btnSendCommand.layer.cornerRadius = 3;
    _btnSendCommand.clipsToBounds = YES;;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)displayCommand:(CameraCommand*)cmd{
    self.camCommand = cmd;
    _lblCommandTitle.text = cmd.commandTitle;
    _txtCommandValue.text = cmd.currentValue;
    
    if ([cmd isBooleanValues]) {
        //Display switch
        _switchChangeValue.hidden = NO;
        _txtCommandValue.hidden = YES;
        _btnSendCommand.hidden = YES;
        
        if (cmd.currentValue.length >= 2) {
            cmd.currentValue = [cmd.currentValue substringToIndex:1];
        }
        BOOL isOn = [@"1" isEqualToString:cmd.currentValue];
        [_switchChangeValue setOn:isOn];
    }
    else{
        //Display text field
        _switchChangeValue.hidden = YES;
        _txtCommandValue.hidden = NO;
        _btnSendCommand.hidden = NO;
    }
    
    [self indicateProcess];
}

- (void)indicateProcess
{
    if (_camCommand.isLoading) {
        [_indicator startAnimating];
        [_txtCommandValue setUserInteractionEnabled:NO];
        [_switchChangeValue setUserInteractionEnabled:NO];
        [_btnSendCommand setUserInteractionEnabled:NO];
        [_btnSendCommand setEnabled:NO];
    }
    else{
        [_indicator stopAnimating];
        [_txtCommandValue setUserInteractionEnabled:YES];
        [_switchChangeValue setUserInteractionEnabled:YES];
        [_btnSendCommand setUserInteractionEnabled:YES];
        [_btnSendCommand setEnabled:YES];
    }
}

- (IBAction)btnSendCommandTapped:(id)sender {
    if (_camCommand.commandType == DemoCommandTypeVideoConfig) {
        NSString *currValue = _camCommand.currentValue;
        if ( !([currValue isEqualToString:@"low"] || [currValue isEqualToString:@"medium"] || [currValue isEqualToString:@"high"])) {
            NSLog(@"%s Not support value:%@", __func__, currValue);
            return;
        }
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(cameraCommandCell:didTapSendCommandValue:)]) {
        self.camCommand.isLoading = YES;
        [self indicateProcess];
        [_delegate cameraCommandCell:self didTapSendCommandValue:[_txtCommandValue.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
}

- (IBAction)switchChangeValueChanged:(id)sender {
    NSString* newCmdValue = self.switchChangeValue.isOn ? @"1" : @"0";
    _camCommand.currentValue = newCmdValue;
    if (_delegate && [_delegate respondsToSelector:@selector(cameraCommandCell:didTapChangeValue:)]) {
        self.camCommand.isLoading = YES;
        [self indicateProcess];
        [_delegate cameraCommandCell:self didTapChangeValue:_camCommand.currentValue];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (_delegate && [_delegate respondsToSelector:@selector(cameraCommandCell:didTapChangeValue:)]) {
        [_delegate cameraCommandCell:self didTapChangeValue:_camCommand.currentValue];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!_camCommand.valueList.count) {
        _camCommand.currentValue = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (!_camCommand.valueList.count) {
        _camCommand.currentValue = [[textField.text stringByReplacingCharactersInRange:range withString:string]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    return YES;
}

@end
