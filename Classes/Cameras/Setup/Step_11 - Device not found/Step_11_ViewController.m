//
//  Step_11_ViewController.m
//  Cinatic
//
//  Created by Cinatic on 2/26/13.
//  Copyright (c) 2013 Cinatic Connected Ltd. All rights reserved.
//

#import "MBProgressHUD.h"
#import <CameraScanner/CameraScanner.h>
//#import <MonitorCommunication/MonitorCommunication.h>
#import "Step_11_ViewController.h"
#import "HttpCom.h"
#import "SetupFailIndicator.h"
#import "BaseWebViewController.h"
#import "FirmwareUpgradeManager.h"
#import <CameraScanner/CameraScanner.h>
#import "FirmwareUpgradeManager.h"
#import "SetupViewController.h"

#define SEND_CAMERA_LOG_ALERT_TAG 601
#define SETUP_HELP_LINK @"http://ota.cinatic.com/ota/error_code.htm"

typedef enum: NSInteger {
    AlertViewTagFwUpgradeConfirm = 516,
    AlertViewTagFwUpgradeDone,
    AlertViewTagFwUpgradeFailed,
    AlertViewTagFwUpgradeUploadFailed,
} AlertViewTagFwUpgrade;

@interface Step_11_ViewController () <NSURLConnectionDataDelegate>

@property (nonatomic, weak) IBOutlet UILabel *errorLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *helpLabel;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *step1Label;
@property (nonatomic, weak) IBOutlet UILabel *step2Label;
@property (nonatomic, weak) IBOutlet UILabel *step3Label;
@property (nonatomic, weak) IBOutlet UIButton *tryAgainButton;
@property (nonatomic, weak) IBOutlet UIButton *helpButton;

@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) FirmwareUpgradeManager* firmwareUpgradeManager;

@end

@implementation Step_11_ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Disable user interaction
    [self.navigationController.navigationBar setUserInteractionEnabled:YES];

    UIBarButtonItem *cancelSetup = [[UIBarButtonItem alloc] initWithTitle:LocStr(@"Cancel") style:UIBarButtonSystemItemCancel target:self action:@selector(doCancelCameraSetup:)];
    self.navigationItem.leftBarButtonItem = cancelSetup;
    
    _titleLabel.text = [NSString stringWithFormat:@"%@. %@: %@ - FW: %@", LocStr(@"Camera not found"), LocStr(@"Error code"), [SetupFailIndicator sharedInstance].errorCode, [SetupFailIndicator sharedInstance].firmwareVerison] ;

    _helpLabel.text = LocStr(@"Click Help for a troubleshooting guide and tips on camera audio prompts (available only on certain models)");
    _subtitleLabel.text = LocStr(@"Follow these steps to restore your camera");
    _step1Label.text = LocStr(@"1. Keep your device and camera on the same Wi-Fi network.");
    _step2Label.text = LocStr(@"2. Confirm the Wi-Fi password is correct.");
    _step3Label.text = LocStr(@"3. Check if you are connected to the Internet.");
    
    [self.tryAgainButton setTitle:LocStr(@"Try again") forState:UIControlStateNormal];
    [self.tryAgainButton setBackgroundImage:[UIImage imageNamed:@"green_btn"] forState:UIControlStateNormal];
    [self.tryAgainButton setBackgroundImage:[UIImage imageNamed:@"green_btn_pressed"] forState:UIControlEventTouchDown];
    self.tryAgainButton.layer.cornerRadius = 3.0;
    [self.tryAgainButton clipsToBounds];
    
    [self.helpButton setTitle:LocStr(@"Help") forState:UIControlStateNormal];
    [self.helpButton setBackgroundImage:[UIImage imageNamed:@"green_btn"] forState:UIControlStateNormal];
    [self.helpButton setBackgroundImage:[UIImage imageNamed:@"green_btn_pressed"] forState:UIControlEventTouchDown];
    self.helpButton.layer.cornerRadius = 3.0;

    if ( _errorText ) {
        NSString *uuid = [[NSUserDefaults standardUserDefaults] stringForKey:CAMERA_UDID];
        NSString *hostSSID = [[NSUserDefaults standardUserDefaults] objectForKey:HOST_SSID];
        [_titleLabel setText: [NSString stringWithFormat: @"UDID: %@. %@: %@ - FW: %@ - SSID: %@", uuid, LocStr(@"Error code"), [SetupFailIndicator sharedInstance].errorCode, [SetupFailIndicator sharedInstance].firmwareVerison, hostSSID]];

    }
}

#pragma mark - Custom action methods

- (void)doCancelCameraSetup:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)handleHelpButtpn:(id)sender
{
    //Use interal webview
    BaseWebViewController* helpWebVC = [BaseWebViewController webViewURlString:SETUP_HELP_LINK];
    [self.navigationController pushViewController:helpWebVC animated:YES];
}

- (IBAction)tryAddButtonAction:(id)sender
{
    SetupViewController *camSetupVC = [SetupViewController findInNavigationController:self.navigationController];
    NSString *camModelId = [[NSUserDefaults standardUserDefaults] objectForKey:CAMERA_MODEL_ID];
    if (camSetupVC.setupCompletion) {
        camSetupVC.setupCompletion(NO, camModelId, nil);
    }
    camSetupVC.skipToWifiSetup = NO;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)doSkipToWifiSetup:(id)sender
{
    // Disable Keep screen on
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end
