//
//  Step_11_ViewController.h
//  Cinatic
//
//  Created by Cinatic on 2/26/13.
//  Copyright (c) 2013 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Step_11_ViewController : UIViewController

@property (nonatomic, copy) NSString *errorText;
@property (nonatomic, copy) NSString *upgradeFirmwareVersion;

@end
