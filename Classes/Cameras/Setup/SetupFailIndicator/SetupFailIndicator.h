//
//  SetupFailIndicator.h
//  Cinatic
//
//  Created by Tran Kien Nghi on 10/13/15.
//  Copyright © 2015 Cinatic Connected Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CAM_ERR_CONNECTION_TYPE_WIFI @"Wifi"
#define CAM_ERR_CONNECTION_TYPE_BLE @"BLE"
#define SET_FLICKER @"set_flicker"

#pragma mark - Wifi defines

#define CAM_ERR_CONNECT_VIA_WIFI @"1001"

#define CAM_ERR_GET_VERSION @"1100"
#define CAM_ERR_GET_MODEL @"1101"
#define CAM_ERR_GET_UDID @"1102"
#define CAM_ERR_RESTART_SYSTEM @"1103"
#define CAM_ERR_GET_ROUTERS @"1104"
#define CAM_ERR_SET_SERVER_AUTH @"1105"
#define CAM_ERR_VOX_ENABLE @"1106"
#define CAM_ERR_SET_TEMP_LO_ENABLE @"1107"
#define CAM_ERR_SET_TEMP_HI_ENABLE @"1108"
#define CAM_ERR_SET_FLICKER @"1109"
#define CAM_ERR_RECORDING_COOLOFF_DURATION @"1110"
#define CAM_ERR_RECORDING_ACTIVE_DURATION @"1111"
#define CAM_ERR_SETUP_WIRELESS_SAVE @"1112"
#define CAM_ERR_GET_WIFI_CONNECTION_STATE @"1113"
#define CAM_ERR_SET_MASTER_KEY @"1114"

#define CAM_ERR_GET_BACK_HOME_WIFI @"1200"
#define CAM_ERR_UDID_CHECK @"1201"
#define CAM_ERR_CHECK_AVAILABILITY_TIMEOUT @"1202"

#pragma mark - BLE defines

#define CAM_ERR_CONNECT_VIA_BLE @"1000"


@interface SetupFailIndicator : NSObject

//The first occurred error code
@property (nonatomic, copy) NSString* errorCode;
@property (nonatomic, copy, readonly) NSString* currentErrorCode;

//The first occurred step on failure
@property (nonatomic, copy) NSString* step;
@property (nonatomic, copy) NSString* currentStep;

@property (nonatomic, copy) NSString* connectionType; // Wifi/BLE
@property (nonatomic, copy) NSString* firmwareVerison;

+ (instancetype)sharedInstance;
+ (BOOL)isDefaultSuccessPattern:(NSString*)response command:(NSString*)command;

- (void)reset;

+ (NSString*)errorWithCommand:(NSString*)command;

+ (NSString*)errorConnectCameraViaWifi;
+ (NSString*)errorConnectCameraViaBLE;

+ (NSString*)errorFirmwareVersionRespone:(NSString*)fwResponse;
+ (NSString*)errorGetModelRespone:(NSString*)response;
+ (NSString*)errorGetUDIDRespone:(NSString*)response;
+ (NSString*)errorSystemRestartRespone:(NSString*)response;
+ (NSString*)errorGetCameraRouterRespone:(NSString*)response;
+ (NSString*)errorServerAuthenticateRespone:(NSString*)response;
+ (NSString*)errorSetFlickerRespone:(NSString*)response;
+ (NSString*)errorVOXEnableRespone:(NSString*)response;
+ (NSString*)errorSetTempLORespone:(NSString*)response;
+ (NSString*)errorSetTempHIRespone:(NSString*)response;
+ (NSString*)errorSetFlickerRespone:(NSString*)response;
+ (NSString*)errorRecordingCollOffRespone:(NSString*)response;
+ (NSString*)errorRecordingActiveRespone:(NSString*)response;
+ (NSString*)errorSetupWirelessSaveRespone:(NSString*)response;
+ (NSString*)errorGetWifiConnectionStateRespone:(NSString*)response;
+ (NSString*)errorSetMasterKeyRespone:(NSString*)response;

+ (NSString*)errorGetBackToHomeWifi;
+ (NSString*)errorUDIDCheck;
+ (NSString*)errorCheckAvailabilityTimeout;

@end
