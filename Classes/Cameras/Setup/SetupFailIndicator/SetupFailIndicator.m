//
//  SetupFailIndicator.m
//  Cinatic
//
//  Created by Tran Kien Nghi on 10/13/15.
//  Copyright © 2015 Cinatic Connected Ltd. All rights reserved.
//

#import "SetupFailIndicator.h"
#import "HttpCom.h"

@implementation SetupFailIndicator

+ (instancetype)sharedInstance
{
    static dispatch_once_t p = 0;

    __strong static id _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

#pragma mark - Private methods

-(void)setErrorCode:(NSString *)errorCode{
    if (!_errorCode.length) {
        _errorCode = errorCode;
    }
    if (errorCode.length) {
        _currentErrorCode = errorCode;
        DLog(@"Camera setup error code: %@", errorCode);
        
    }
}

- (void)logCameraSetupFailureEvent{
}

+ (BOOL)isDefaultSuccessPattern:(NSString*)response command:(NSString*)command{
    return ( response && [response hasPrefix:command] && response.length > command.length + 2);
}

+(NSString*)errorCodeForCommand:(NSString*)command{
    if ([@"get_version" isEqualToString:command]) {
        return CAM_ERR_GET_VERSION;
    }
    if ([@"get_model" isEqualToString:command]) {
        return CAM_ERR_GET_MODEL;
    }
    if ([@"get_udid" isEqualToString:command]) {
        return CAM_ERR_GET_UDID;
    }
    if ([@"restart_system" isEqualToString:command]) {
        return CAM_ERR_RESTART_SYSTEM;
    }
    if ([@"get_rt_list" isEqualToString:command]) {
        return CAM_ERR_GET_ROUTERS;
    }
    if ([@"set_server_auth" isEqualToString:command]) {
        return CAM_ERR_SET_SERVER_AUTH;
    }
    if ([@"vox_enable" isEqualToString:command]) {
        return CAM_ERR_VOX_ENABLE;
    }
    if ([@"set_temp_lo_enable" isEqualToString:command]) {
        return CAM_ERR_SET_TEMP_LO_ENABLE;
    }
    if ([@"set_temp_hi_enable" isEqualToString:command]) {
        return CAM_ERR_SET_TEMP_HI_ENABLE;
    }
    if ([@"set_flicker" isEqualToString:command]) {
        return CAM_ERR_SET_FLICKER;
    }
    if ([@"recording_cooloff_duration" isEqualToString:command]) {
        return CAM_ERR_RECORDING_COOLOFF_DURATION;
    }
    if ([@"recording_active_duration" isEqualToString:command]) {
        return CAM_ERR_RECORDING_ACTIVE_DURATION;
    }
    if ([@"setup_wireless_save" isEqualToString:command]) {
        return CAM_ERR_SETUP_WIRELESS_SAVE;
    }
    if ([@"get_wifi_connection_state" isEqualToString:command]) {
        return CAM_ERR_GET_WIFI_CONNECTION_STATE;
    }
    if ([@"set_master_key" isEqualToString:command]) {
        return CAM_ERR_SET_MASTER_KEY;
    }
    return nil;
}

#pragma mark - Public methods

- (void)reset{
    _errorCode = nil;
    _currentErrorCode = nil;
    _step = nil;
    _currentStep = nil;
    _firmwareVerison = nil;
    _connectionType = nil;
}

+ (NSString*)errorWithCommand:(NSString*)command{
    NSString* errorCode = [SetupFailIndicator errorCodeForCommand:command];
    [SetupFailIndicator sharedInstance].errorCode = errorCode;
    return errorCode;
}

+ (NSString*)errorConnectCameraViaWifi{
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_CONNECT_VIA_WIFI;
    
    return CAM_ERR_CONNECT_VIA_WIFI;
}

+ (NSString*)errorConnectCameraViaBLE{
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_CONNECT_VIA_BLE;
    
    return CAM_ERR_CONNECT_VIA_BLE;
}

+ (NSString*)errorFirmwareVersionRespone:(NSString*)fwResponse{
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"^%@: \\d{2}.\\d{2}.\\d{2}$", GET_VERSION] options:NSRegularExpressionAnchorsMatchLines error:&error];

    NSUInteger numberOfMatches = 0;
    //https://app.crittercism.com/developers/crash-details/53d6a3761787842dba000001/92382cf49352b5c103441c1416906a6f0b9a16d764de6c239889f96e
    if (fwResponse != nil)
    {
        numberOfMatches = [regex numberOfMatchesInString:fwResponse options:0 range:NSMakeRange(0, [fwResponse length])];
    }
    
    if(numberOfMatches != 1){
        [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_GET_VERSION;
    }
    
    return numberOfMatches == 1 ? nil : CAM_ERR_GET_VERSION;
}

+ (NSString*)errorGetModelRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:GET_MODEL] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_GET_MODEL;
    return CAM_ERR_GET_MODEL;
}

+ (NSString*)errorGetUDIDRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:GET_UDID] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_GET_UDID;
    return CAM_ERR_GET_UDID;
}

+ (NSString*)errorSystemRestartRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:RESTART_HTTP_CMD] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_RESTART_SYSTEM;
    return CAM_ERR_RESTART_SYSTEM;
}

+ (NSString*)errorGetCameraRouterRespone:(NSString*)response{
    if (response.length) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_GET_ROUTERS;
    return CAM_ERR_GET_ROUTERS;
}

+ (NSString*)errorServerAuthenticateRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:SET_SERVER_AUTH] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_SET_SERVER_AUTH;
    return CAM_ERR_SET_SERVER_AUTH;
}

+ (NSString*)errorSetFlickerRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:SET_FLICKER] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_SET_FLICKER;
    return CAM_ERR_SET_FLICKER;
}

+ (NSString*)errorVOXEnableRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:@"vox_enable"] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_VOX_ENABLE;
    return CAM_ERR_VOX_ENABLE;
}

+ (NSString*)errorSetTempLORespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:@"set_temp_lo_enable"] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_SET_TEMP_LO_ENABLE;
    return CAM_ERR_SET_TEMP_LO_ENABLE;
}

+ (NSString*)errorSetTempHIRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:@"set_temp_hi_enable"] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_SET_TEMP_HI_ENABLE;
    return CAM_ERR_SET_TEMP_HI_ENABLE;
}

+ (NSString*)errorRecordingCollOffRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:@"recording_cooloff_duration"] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_RECORDING_COOLOFF_DURATION;
    return CAM_ERR_RECORDING_COOLOFF_DURATION;
}

+ (NSString*)errorRecordingActiveRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:@"recording_active_duration"] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_RECORDING_ACTIVE_DURATION;
    return CAM_ERR_RECORDING_ACTIVE_DURATION;
}

+ (NSString*)errorSetupWirelessSaveRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:@"setup_wireless_save"] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_SETUP_WIRELESS_SAVE;
    return CAM_ERR_SETUP_WIRELESS_SAVE;
}

+ (NSString*)errorGetWifiConnectionStateRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:GET_STATE_NETWORK_CAMERA] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_GET_WIFI_CONNECTION_STATE;
    return CAM_ERR_GET_WIFI_CONNECTION_STATE;
}

+ (NSString*)errorSetMasterKeyRespone:(NSString*)response{
    if ( [self isDefaultSuccessPattern:response command:SET_MASTER_KEY] ) {
        return nil;
    }
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_SET_MASTER_KEY;
    return CAM_ERR_SET_MASTER_KEY;
}

+ (NSString*)errorGetBackToHomeWifi{
    [SetupFailIndicator sharedInstance].errorCode =CAM_ERR_GET_BACK_HOME_WIFI;
    return CAM_ERR_GET_BACK_HOME_WIFI;
}

+ (NSString*)errorUDIDCheck{
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_UDID_CHECK;
    return CAM_ERR_UDID_CHECK;
}

+ (NSString*)errorCheckAvailabilityTimeout{
    [SetupFailIndicator sharedInstance].errorCode = CAM_ERR_CHECK_AVAILABILITY_TIMEOUT;
    return CAM_ERR_CHECK_AVAILABILITY_TIMEOUT;
}

@end
