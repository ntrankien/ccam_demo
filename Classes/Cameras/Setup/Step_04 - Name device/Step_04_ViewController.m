//
//  Setup_04_ViewController.m
//  Cinatic
//
//  Created by Cinatic on 7/24/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import "Step_04_ViewController.h"
#import "HttpCom.h"
#import "SetupFailIndicator.h"

@interface Step_04_ViewController () <UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *progressView;
@property (nonatomic, weak) IBOutlet UILabel *pleaseWaitLabel;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *instructionLabel;
@property (nonatomic, weak) IBOutlet UITextField *cameraNameTextField;
@property (nonatomic) BOOL isCameraNameEdited;
@property (nonatomic, weak) IBOutlet UIButton *btnContinue;

@end

@implementation Step_04_ViewController

#pragma mark - UIViewController methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SetupFailIndicator sharedInstance].step = @"4";
    
    self.title = LocStr(@"Camera detected");

    _titleLabel.text = self.title;
    _instructionLabel.text = LocStr(@"Please name the location of your camera. This will help make your notifications more relevant.");
    _cameraNameTextField.placeholder = LocStr(@"Living Room, Nursery, etc., ...");
    _pleaseWaitLabel.text = LocStr(@"This may take a couple seconds to complete. Please wait ...");
    
    [_btnContinue setTitle:LocStr(@"Continue") forState:UIControlStateNormal];
    [_btnContinue setBackgroundImage:[UIImage imageNamed:@"green_btn"] forState:UIControlStateNormal];
    [_btnContinue setBackgroundImage:[UIImage imageNamed:@"green_btn_pressed"] forState:UIControlEventTouchDown];
    _btnContinue.enabled = NO;
    
    _cameraNameTextField.delegate = self;
    if (!_cameraName.length) {
        _cameraName = _camProfile.name;
    }
    if (_cameraName.length) {
        NSMutableAttributedString * cameraName = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", _cameraName]];
        NSRange cameraNameRange = NSMakeRange(0, _cameraName.length);
        [cameraName addAttribute:NSBackgroundColorAttributeName value:[UIColor colorWithRed:0.08 green:0.08 blue:0.08 alpha:0.06] range:cameraNameRange];
        [cameraName addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:cameraNameRange];
        [cameraName addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:17.0] range:cameraNameRange];
        self.cameraNameTextField.attributedText = cameraName;
        self.btnContinue.enabled = YES;
    }

    // only for Focus73
    if (_camProfile) {
        self.cameraName = _camProfile.name;
        [[HttpCom instance].comWithDevice setDevice_ip:_camProfile.ip_address];
        [[HttpCom instance].comWithDevice setDevice_port:_camProfile.port];
    }
    // Custom initialization
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_progressView removeFromSuperview];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom action methods

- (BOOL)isCameraNameValid:(NSString *)cameraName
{
    if ( cameraName.length < MIN_LENGTH_CAMERA_NAME || MAX_LENGTH_CAMERA_NAME < cameraName.length ) {
        return NO;
    }
    
    NSString *regex = @"[a-zA-Z'0-9 ._-]+";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isValid = [predicate evaluateWithObject:cameraName];
    return isValid;
}

- (IBAction)continueButtonAction:(id)sender
{
    NSInteger tag = ((UIButton*)sender).tag;
    NSString *cameraName = _cameraNameTextField.text;
    [_cameraNameTextField resignFirstResponder];
    
    if ( [cameraName length] < MIN_LENGTH_CAMERA_NAME || [cameraName length] > MAX_LENGTH_CAMERA_NAME ) {
        NSString *msg = LocStr(@"Camera name must be 5 to 30 characters");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Invalid camera name")
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:LocStr(@"Ok"), nil];
        [alert show];
    }
    else if ( ![self isCameraNameValid:cameraName] ) {
        NSString *msg = LocStr(@"Camera name not valid. Name must be 5-30 characters long and may only contain letters, numbers, spaces, dots/periods, hyphens, underscores or quotation marks.");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Invalid camera name")
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:LocStr(@"Ok"), nil];
        [alert show];
    }
    else if ( tag == CONF_CAM_BTN_TAG ) {
        // Show progress view
        [_progressView setHidden:NO];
        [self.view addSubview:_progressView];
        [self.view bringSubviewToFront:_progressView];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:cameraName forKey:CAMERA_NAME];
        [userDefaults synchronize];
        
        _btnContinue.enabled = NO;
        
        // This is a Focus73 model
        if (_camProfile) {
            NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:GET_VERSION];
            DLog(@"%s response: %@", __FUNCTION__, response);
            [SetupFailIndicator errorFirmwareVersionRespone:response];
            
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^get_version: \\d{2}.\\d{2}.\\d{2}$" options:NSRegularExpressionAnchorsMatchLines error:&error];
            if (!regex) {
                DLog(@"%s error:%@", __FUNCTION__, error.description);
            }
            else {
                if (response) {
                     NSUInteger numberOfMatches = [regex numberOfMatchesInString:response options:0 range:NSMakeRange(0, [response length])];
                    
                    DLog(@"%s numberOfMatches:%lu", __FUNCTION__, (unsigned long)numberOfMatches);
                    
                    if (numberOfMatches == 1) {
                        NSString *fwVersion = [[response componentsSeparatedByString:@": "] objectAtIndex:1];
                        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                        [userDefaults setObject:fwVersion forKey:FW_VERSION];
                        [userDefaults synchronize];
                    }
                }
            }
        }
        [self moveToNextStep];
    }
}

#pragma mark - Text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
    if (NSEqualRanges(range, textFieldRange) && [string length] == 0) {
        // Camera name textfield is empty
        _btnContinue.enabled = NO;
    }
    else {
        _btnContinue.enabled = YES;
    }
    
    if ([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - Private methods

- (void)moveToNextStep
{
    [_progressView removeFromSuperview];
    Step_05_ViewController *step05ViewController =  [[Step_05_ViewController alloc] initWithNibName:@"Step_05_ViewController" bundle:nil];
    step05ViewController.camProfile = _camProfile;
    [self.navigationController pushViewController:step05ViewController animated:NO];
    _btnContinue.enabled = YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

-(void) keyBoardWillShowNotification:(NSNotification*)notification
{
    NSDictionary* dictionary = [notification userInfo];
    NSValue* value = [dictionary valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [value CGRectValue];
    keyboardFrameBeginRect = [self.view convertRect:keyboardFrameBeginRect fromView:nil];
    NSInteger movementDistance = keyboardFrameBeginRect.size.height;

    [UIView animateWithDuration:0.3 animations: ^ {
        self.view.frame = CGRectMake(0, -movementDistance, SCREEN_WIDTH, SCREEN_HEIGHT);
    } completion:nil];
}

- (void)keyBoardWillHideNotification:(NSNotification*)notification
{
    [UIView animateWithDuration:0.3 animations: ^ {
        self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    } completion:nil];
}

@end
