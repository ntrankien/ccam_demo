//
//  SetupGuideTableViewCell.h
//  CCam
//
//  Created by Tran Kien Nghi on 3/23/17.
//  Copyright © 2017 Cinatic Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetupGuideTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *guideLbl;

@end
