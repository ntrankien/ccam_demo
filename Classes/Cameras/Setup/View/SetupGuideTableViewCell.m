//
//  SetupGuideTableViewCell.m
//  CCam
//
//  Created by Tran Kien Nghi on 3/23/17.
//  Copyright © 2017 Cinatic Technology. All rights reserved.
//

#import "SetupGuideTableViewCell.h"

@implementation SetupGuideTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
