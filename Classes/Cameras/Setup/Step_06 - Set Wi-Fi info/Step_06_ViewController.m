//
//  Step_06_ViewController.m
//  Cinatic
//
//  Created by Cinatic on 7/26/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import "Step_06_ViewController.h"
#import "Step_10_ViewController.h"
#import "HttpCom.h"
#import "SetupFailIndicator.h"
#import "NSString+AES.h"
#import "SetupViewController.h"
#import <HUBComm/HUBComm.h>

@interface Step_06_ViewController () <UIAlertViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UIView *selectCameraView;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UITableViewCell *ssidCell;
@property (nonatomic, strong) IBOutlet UITableViewCell *securityCell;
@property (nonatomic, strong) IBOutlet UITableViewCell *passwordCell;

@property (nonatomic, strong) IBOutlet UIView *otaDummyProgress;
@property (nonatomic, weak) IBOutlet UIProgressView *otaDummyProgressBar;
@property (nonatomic, weak) IBOutlet UILabel *instructionLabel;
@property (nonatomic, weak) IBOutlet UILabel *mayTakeTimeLabel;

@property (nonatomic, strong) IBOutlet UIView *progressView;
@property (nonatomic, weak) IBOutlet UILabel *checkingConnectionLabel;
@property (nonatomic, weak) IBOutlet UILabel *mayTakeMinuteLabel;

@property (nonatomic, strong) UITextField *tfSSID;
@property (nonatomic, strong) UITextField *tfPassword;

// timeout input password
@property (nonatomic, strong) NSTimer *inputPasswordTimer;
@property (nonatomic, strong) NSTimer *timeOut;
@property (nonatomic, strong) NSDate *timeoutCheckPassword;

@property (nonatomic, strong) DeviceConfiguration * deviceConf;
@property (nonatomic, copy) NSString *currentStateCamera;
@property (nonatomic, copy) NSString *password;

// current state of camera
@property (nonatomic, assign) BOOL isUserMakeConnect;
@property (nonatomic, assign) BOOL task_cancelled;

@end

@implementation Step_06_ViewController

#define TIME_INPUT_PASSWORD_AGAIN 60.0
#define RETRY_SETUP_WIFI_TIMES 5

#define SSID_SECTION 0
#define SEC_SECTION 1
#define SSID_INDEX 0
#define SEC_INDEX 0
#define PASSWORD_INDEX 1
#define CONFPASSWORD_INDEX 2

#define SHOW_PASSWORD_LABEL_TAG     113
#define PASSWORD_LABEL_TAG          100
#define PASSWORD_TEXTFIELD_TAG      200
#define NETWORK_NAME_LABEL_TAG      102
#define NETWORK_NAME_TEXTFIELD_TAG  202
#define SECURITY_LABEL_TAG          103
#define SEC_VALUE_LABEL_TAG         203
#define CAMERA_IMAGEVIEW_TAG        595

#define PASSWORD_RETRY_TAG              102
#define PASSWORD_WARNING_TAG            103
#define INVAID_UDID_TAG                 104
#define RETRY_GET_WIFI_CONNECTION_STATE      120

#pragma mark - IBAction

- (IBAction)showHidePasswordTicked:(UIButton*)sender {
    [sender setSelected:![sender isSelected]];
    
    [self.tfPassword setSecureTextEntry:!sender.selected];
}

#pragma mark - UIViewController methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = LocStr(@"Set Wi-Fi info");
  
    [SetupFailIndicator sharedInstance].step = @"6";
    
    [_progressView setHidden:YES];
    
    self.tfSSID = (UITextField *)[_ssidCell.contentView viewWithTag:NETWORK_NAME_TEXTFIELD_TAG];
    self.tfPassword = (UITextField *)[_passwordCell.contentView viewWithTag:PASSWORD_TEXTFIELD_TAG];
    UILabel *securityValueLabel = (UILabel *)[_securityCell.contentView viewWithTag:SEC_VALUE_LABEL_TAG];

    if ( securityValueLabel ) {
        securityValueLabel.text = _security;
    }
    
    _tfSSID.placeholder = LocStr(@"Enter Wi-Fi name");
    _tfPassword.placeholder = LocStr(@"Enter Wi-Fi password");
    
    ((UILabel *)[_ssidCell viewWithTag:NETWORK_NAME_LABEL_TAG]).text = LocStr(@"Name");
    ((UILabel *)[_securityCell viewWithTag:SECURITY_LABEL_TAG]).text = LocStr(@"Security");
    ((UILabel *)[_passwordCell viewWithTag:PASSWORD_LABEL_TAG]).text = LocStr(@"Password");
    ((UILabel *)[_passwordCell viewWithTag:SHOW_PASSWORD_LABEL_TAG]).text = LocStr(@"Show Password");
    
    _checkingConnectionLabel.text = [NSString stringWithFormat:@"%@ - SSID: %@", LocStr(@"Checking connection to camera"), _ssid];
    _instructionLabel.text = LocStr(@"Firmware upgrade in process. Do not unplug your camera.");
    _mayTakeMinuteLabel.text = LocStr(@"This may take a minute");
    _mayTakeTimeLabel.text = LocStr(@"This may take up 3 minutes");
    
    UIBarButtonItem *nextButton =
    [[UIBarButtonItem alloc] initWithTitle:LocStr(@"Next")
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(handleNextButton:)];
    
    self.navigationItem.rightBarButtonItem = nextButton;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    UIImageView *imageView = (UIImageView *)[_progressView viewWithTag:CAMERA_IMAGEVIEW_TAG];
    imageView.animationImages = @[
                                  [UIImage imageNamed:@"setup_camera_c1"],
                                  [UIImage imageNamed:@"setup_camera_c2"],
                                  [UIImage imageNamed:@"setup_camera_c3"],
                                  [UIImage imageNamed:@"setup_camera_c4"]
                                  ];
    
    imageView.animationDuration = 1.5;
    imageView.animationRepeatCount = 0;
    [imageView startAnimating];
    
    if ( !_ssid ) {
        DLog(@"empty SSID ");
    }
    
    if ( !_security ) {
        DLog(@"empty security ");
    }
    
    if ( _tfSSID && !_isOtherNetwork ) {
        _tfSSID.text = _ssid;
    }
    
    // Initialize transient object here
	self.deviceConf = [[DeviceConfiguration alloc] init];
	
    if ( ![self restoreDataIfPossible] ) {
		// Try to read the ssid from preference:
        self.deviceConf.ssid = _ssid;
    }
    else {
        /*
         * 1. Check deviceConf.ssid vs self.ssid
         * 2. check sec type : OPEN , WeP, wpa
         * 3. If ( =)  -> prefill pass- deviceConf.key  to  password/conf password text field
         */
        DLog(@"Step_06_ViewController - viewDidLoad - deviceConf.ssid: %@, - self.ssid: %@, - self.security: %@", _deviceConf.ssid, _ssid, _security);
        
        if ( [_deviceConf.ssid isEqualToString:_ssid] &&
            ([_security isEqualToString:@"wep"] || [_security isEqualToString:@"wpa"]) )
        {
            self.tfPassword.text = _deviceConf.key;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DLog(@"update security type");
    UILabel *sec = (UILabel *)[_securityCell viewWithTag:SEC_VALUE_LABEL_TAG];
    
    if ( sec ) {
        sec.text = _security;
    }
    
    _isUserMakeConnect = NO;
    [_tableView reloadData];
    
    [self updateNextNavBarButtonWithInput:nil withString:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // do it to fix UI iPhone 6, 6+
    [self.progressView setFrame:self.view.frame];
    [self.otaDummyProgress setFrame:self.view.frame];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _task_cancelled = YES;
    [self resetAllTimer];
    [self.navigationItem setHidesBackButton:NO];
}

- (void)dealloc{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self resetAllTimer];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    [self updateNextNavBarButtonWithInput:textField withString:resultString];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

- (void)animateTextField:(UITextField *)textField up:(BOOL)up
{
    int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    if ( textField.tag ==201 && UIInterfaceOrientationIsLandscape(interfaceOrientation) ) {
        //Confirm Password cell
        movementDistance+= 40;
    }
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations:@"anim" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ( textField.tag == PASSWORD_TEXTFIELD_TAG ) {
        self.password = textField.text;
        [textField resignFirstResponder];
        return NO;
    }

    [textField resignFirstResponder];
    return NO;
}

#pragma  mark -  Table View delegate & datasource

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 44.0f;
    
    if ( indexPath.section == 0) {
        height = 45.0f;
    }
    else if ( indexPath.section == SEC_SECTION && indexPath.row == PASSWORD_INDEX ) {
        height = 70.0f;
    }
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger tag = tableView.tag;
    
    if ( tag == 13 ) {
        if ( indexPath.section == SSID_SECTION ) {
            //only one cell in this section
            [_tfSSID setUserInteractionEnabled:_isOtherNetwork];
            
            return _ssidCell;
        }
        else if ( indexPath.section == SEC_SECTION ) {
            if ( indexPath.row == SEC_INDEX ) {
                [self.securityCell setAccessoryType:UITableViewCellAccessoryNone];
                if ( _isOtherNetwork ) {
                    [self.securityCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                }
                return _securityCell;
            }
            if ( indexPath.row == PASSWORD_INDEX ) {
                return _passwordCell;
            }
        }
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger tag = tableView.tag;
    
    if ( tag == 13 ) {
        if ( section == SSID_SECTION ) {
            return 1;
        }
        else if ( section == SEC_SECTION ) {
            if ( [self isSecurityOpenOrNone] ) {
                return 1;
            }
            else {
                return 2;
            }
        }
    }
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger tag = tableView.tag;
    if ( tag == 13 ) {
        return 2;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
    
    if ( indexPath.section == SSID_SECTION ) {
        //only one cell in this section
        if ( _isOtherNetwork ) {
            [_tfSSID setUserInteractionEnabled:YES];
            [_tfSSID becomeFirstResponder];
        }
    }
    else if ( indexPath.section == SEC_SECTION ) {
        if ( indexPath.row == PASSWORD_INDEX ) {
            [_tfPassword becomeFirstResponder];
        }
        else if ( indexPath.row == SEC_INDEX && _isOtherNetwork ) {
            [self changeSecurityType];
        }
    }
}

#pragma mark - Private methods

- (void)updateNextNavBarButtonWithInput:(UITextField*)textField withString:(NSString*)text {
    if (textField == nil || text == nil) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        if (_tfSSID.text.length > 0) {
            if ([self isSecurityOpenOrNone]) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
            else if (_tfPassword.text.length > 0) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
        }
    }
    else if (textField == _tfPassword) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        if (_tfSSID.text.length > 0) {
            if ([self isSecurityOpenOrNone]) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
            else if (text.length > 0) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
        }
    }
    else if (textField == _tfSSID) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        if (text.length > 0) {
            if ([self isSecurityOpenOrNone]) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
            else if (_tfPassword.text.length > 0) {
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }
        }
    }
}

- (BOOL)isSecurityOpenOrNone {
    return [[_security lowercaseString] isEqualToString:@"open"] || [[_security lowercaseString] isEqualToString:@"none"];
}

- (void)changeSecurityType
{
    //load step 07
    DLog(@"Load step 7");
    
    //Load the next xib
    Step_07_ViewController *step07ViewController = [[Step_07_ViewController alloc] initWithNibName:@"Step_07_ViewController" bundle:nil];
    step07ViewController.lastStep = self;
    [self.navigationController pushViewController:step07ViewController animated:NO];
}

- (void)handleNextButton:(id)sender
{
    NSString *ssidName = _tfSSID.text; // IA-1551
    
    NSString *logSSIDName = [ssidName stringByReplacingOccurrencesOfString:@"\n" withString:@"<NewLine>"];
    logSSIDName = [logSSIDName stringByReplacingOccurrencesOfString:@" " withString:@"<Space>"];
    DLog(@"Step_06_ViewController handleNextButton logSSIDName: %@", logSSIDName);
    
    if ( ssidName.length == 0 ) {
        NSString *msg = LocStr(@"Enter a network name and try again");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Network name")
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:LocStr(@"Ok"), nil];
        [alert show];
        [self updateNextNavBarButtonWithInput:nil withString:nil];
        return;
    }
    
    if ([_security.lowercaseString isEqualToString:@"open"] || [_security.lowercaseString isEqualToString:@"none"]) {
        NSString *title = LocStr(@"For your security, we do not support connecting to a Wi-Fi network that is not password protected. Please secure your network and try again.");
        [[[UIAlertView alloc] initWithTitle:title
                                    message:nil
                                   delegate:nil
                          cancelButtonTitle:nil
                          otherButtonTitles:LocStr(@"Ok"), nil] show];
        return;
    }
    
    NSString *ssidPassword = _tfPassword.text;
    if ( ssidPassword.length == 0 ) {
        NSString *msg = LocStr(@"Enter a password");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Password")
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:LocStr(@"Ok"), nil];
        [alert show];
        return;
    }
    
    //Check password length and non-ascii characters
    if (![_tfPassword.text canBeConvertedToEncoding:NSASCIIStringEncoding] || [_tfPassword.text hasPrefix:@" "] || [_tfPassword.text hasSuffix:@" "]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Warning")
                                                        message:LocStr(@"We detected space/non-printable character in your password. This may cause setup failure. You can still continue the setup to try.")
                                                       delegate:self
                                              cancelButtonTitle:LocStr(@"Cancel")
                                              otherButtonTitles:LocStr(@"Continue"), nil];
        alert.tag = PASSWORD_WARNING_TAG;
        [alert show];
        return;
    }
    
    [self continueHandleNextButton];
}

-(void)continueHandleNextButton{
    if(_tfPassword.text.length < WIFI_PASSWORD_MIN_LENGTH || _tfPassword.text.length > WIFI_PASSWORD_MAX_LENGTH){
        NSString *msg = LocStr(@"The password must be %ld to %ld characters.");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Warning")
                                                        message:[NSString stringWithFormat:msg, WIFI_PASSWORD_MIN_LENGTH, WIFI_PASSWORD_MAX_LENGTH]
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:LocStr(@"Ok"), nil];
        [alert show];
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.navigationItem setHidesBackButton:YES];
            self.navigationItem.rightBarButtonItem.enabled = NO;
            [self.view addSubview:_progressView];
            _progressView.hidden = NO;
            [self.view endEditing:YES];
        });
        [self checkWifiPassword];
    });
}

- (void)checkWifiPassword
{
    // Create progressView for process verify network
    DLog(@"%s other: %d, security: %@", __FUNCTION__, _isOtherNetwork, _security);
    self.ssid = _tfSSID.text;
    self.password = _tfPassword.text;
    
    [[NSUserDefaults standardUserDefaults] setObject:_ssid forKey:HOST_SSID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self sendWifiInfoToCamera];
}

- (void)prepareWifiInfo
{
    // NOTE: we can do this because we are connecting to camera now
    NSString *cameraMac = nil;
    NSString *stringUDID = [[HttpCom instance].comWithDevice sendCommandAndBlock:GET_UDID withTimeout:5.0];
    [SetupFailIndicator errorGetUDIDRespone:stringUDID];
    NSRange range = [stringUDID rangeOfString:@": "]; // get_udid: 01008344334C32B0A0VFFRBSVA
    
    if ( range.location != NSNotFound ) { //01008344334C32B0A0VFFRBSVA
        stringUDID = [stringUDID substringFromIndex:range.location + 2];
        if (stringUDID.length == 26 || stringUDID.length == 24) {
            cameraMac = [stringUDID substringWithRange:NSMakeRange(6, 12)];
            cameraMac = [Util add_colon_to_mac:cameraMac];
        }
        else {
            DLog(@"Error - Wrong UDID length: %@", stringUDID);
           
            [self invalidUdidDialog];
            return;
        }
    }
    else {
        DLog(@"Error - Received UDID wrong format - UDID: %@", stringUDID);
    }
    
    self.deviceConf.ssid = _ssid;
    
    // Save mac address for used later
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:cameraMac forKey:@"CameraMacWithQuote"];
    [userDefaults setObject:stringUDID forKey:CAMERA_UDID];
    [userDefaults synchronize];
    
    self.deviceConf.addressMode = @"DHCP";
    
    if ( [_security isEqualToString:@"wep"] ) { //@"Open",@"WEP", @"WPA-PSK/WPA2-PSK"
        _deviceConf.securityMode = @"WEP";
        _deviceConf.wepType = @"OPEN"; //default
        _deviceConf.keyIndex = @"1"; //default;
    }
    else if ( [_security isEqualToString:@"wpa"] ) {
        _deviceConf.securityMode = @"WPA-PSK/WPA2-PSK";
    }
    else if ([_security isEqualToString:@"shared"]) {
        _deviceConf.securityMode = @"SHARED";
    }
    else {
        _deviceConf.securityMode= @"OPEN";
    }
    
    DLog(@"Log - udid: %@, %@", stringUDID, cameraMac);
    _deviceConf.key = _password;
    _deviceConf.usrName = BASIC_AUTH_DEFAULT_USER;
    
    NSString *camPass = [CameraPassword getPasswordForCam:cameraMac];
    DLog(@"Log - 02 cam password is : %@", camPass);
    
    if ( !camPass ) { // default pass
        camPass = @"00000000";
        DLog(@"Log - 02 cam password is default: %@", camPass);
    }
    
    _deviceConf.passWd = camPass;
}

-(void)sendWifiInfoToCamera
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *apiKey = [[HUBCommCore sharedInstance].dataSource apiKeyForHUBCommCore];
    //NSString *fwVersion = [userDefaults stringForKey:FW_VERSION];

    NSDate *now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ZZZ"];
    
    NSMutableString *stringFromDate = [NSMutableString stringWithString:[formatter stringFromDate:now]];
    [stringFromDate insertString:@"." atIndex:3];
    
    DLog(@"VTECH OFF PN");
    [self defaultOffAllPNToCamera];
    
    float timeZoneInNumber = [[NSTimeZone localTimeZone] secondsFromGMT] / 3600.f; //-12 --> 12
    NSString *cmdFlicker = @"set_flicker&value=60";
    
    if ( timeZoneInNumber > -4 && timeZoneInNumber < 9 ) {
        cmdFlicker = @"set_flicker&value=50";
    }
    
    NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:cmdFlicker];
    [SetupFailIndicator errorSetFlickerRespone:response];
    DLog(@"%s set_flicker %@", __FUNCTION__, response);
    
    //Nghi: Support new Query City_Timezone for DST
    NSTimeZone *localTimezone = [NSTimeZone localTimeZone];
    NSString *localTimezoneName = [localTimezone name];
    NSString *cmdTimezone = [@"set_city_timezone&value=" stringByAppendingString:localTimezoneName];
    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:cmdTimezone withTimeout:5.0];
    DLog(@"%s set_city_timezone %@", __FUNCTION__, response);
    
    NSString *set_auth_cmd = [NSString stringWithFormat:@"%@%@%@%@%@",
                              SET_SERVER_AUTH,
                              SET_SERVER_AUTH_PARAM1, apiKey,
                              SET_SERVER_AUTH_PARAM2, stringFromDate];
    
    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:set_auth_cmd withTimeout:10.0];
    [SetupFailIndicator errorServerAuthenticateRespone:response];
    DLog(@"%@, -response: %@ ", set_auth_cmd, response);
    
    [self prepareWifiInfo];
    
    // Save and send
    if ( [_deviceConf isDataReadyForStoring] ) {
        NSDictionary *dict = [self getWritableConfiguration];
        [Util writeDeviceConfigurationData:dict];
    }
    
    NSDictionary *dict = [self readDeviceConfiguration];
    [_deviceConf restoreConfigurationData:dict];
    NSString *deviceConfiguration = [_deviceConf getDeviceEncodedConfString];
    NSString *setupCmd = [NSString stringWithFormat:@"%@%@", SETUP_HTTP_CMD, deviceConfiguration];
    setupCmd = [setupCmd stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    
    [self loopSetupWifiSending:setupCmd retryTimes:0];
    
    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:RESTART_HTTP_CMD];
    [SetupFailIndicator errorSystemRestartRespone:response];
    DLog(@"%s RESTART_HTTP_CMD: %@", __FUNCTION__, response);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self moveOnToCheckCameraOnlineStatus];
    });
}

- (BOOL)loopSetupWifiSending:(NSString *)setupCmd retryTimes:(NSInteger)times
{
    if ( times < RETRY_SETUP_WIFI_TIMES ) {
        NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:setupCmd];
        [SetupFailIndicator errorSetupWirelessSaveRespone:response];
        if ( [response isEqualToString:@"setup_wireless_save: 0"] ) {
            return YES;
        }
        
        DLog(@"%s send cmd  %@ - response is: %@", __FUNCTION__, setupCmd, response); //setup_wireless_save: 0
        times++;
        
        return [self loopSetupWifiSending:setupCmd retryTimes:times];
    }
    else{
        return NO;
    }
}

- (BOOL)loopGetWifiConnectionState:(NSString *)connect_state_cmd
{
    BOOL connected = FALSE;
    NSDate *now = [NSDate date];
    while ( [now compare:_timeoutCheckPassword] == NSOrderedAscending ) {
        NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:connect_state_cmd];
        [SetupFailIndicator errorGetWifiConnectionStateRespone:response];
        DLog(@"%s send cmd  %@ - response is: %@", __FUNCTION__, connect_state_cmd, response);
        if ( response != nil && [response isEqualToString:[NSString stringWithFormat:@"%@: CONNECTED", GET_STATE_NETWORK_CAMERA]] ) {
            connected = TRUE;
            break;
        } else if ( response != nil && [response isEqualToString:[NSString stringWithFormat:@"%@: DISCONNECTED", GET_STATE_NETWORK_CAMERA]] ) {
            connected = FALSE;
            break;
        }
        now = [NSDate dateWithTimeInterval:2.0 sinceDate:[NSDate date]];
        [[NSRunLoop currentRunLoop] runUntilDate:now];
    }
    return connected;
}

- (void)defaultOffAllPNToCamera //VTech only
{
    NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:@"vox_disable"];
    [SetupFailIndicator errorVOXEnableRespone:response];

    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:@"set_cam_park&value=0"];
    [SetupFailIndicator errorSetTempHIRespone:response];
}

- (void)analyticWithCMDResponse:(NSString*)response command:(NSString*)cmd{
}

- (NSString *)sendCommand:(NSString*)command baseCommand:(NSString*)baseCmd retryOnFailure:(NSInteger)retryTime{
    if( retryTime < 0 ) {
        DLog(@"%s, command: %@ all retries failed", __FUNCTION__, command);
        return nil;
    }
    else{
        NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:command];
        if ([SetupFailIndicator isDefaultSuccessPattern:response command:baseCmd]) {
            return response;
        }
        else {
            return [self sendCommand:command baseCommand:baseCmd retryOnFailure:retryTime - 1];
        }
    }
}

-(void)moveOnToCheckCameraOnlineStatus
{
    [self resetAllTimer];
    [self nextStepVerifyPassword];
    [_progressView removeFromSuperview];
    [_selectCameraView removeFromSuperview];
    _progressView.hidden = YES;
}

- (void)checkAppConnectToCameraAtStep06
{
    [self.view bringSubviewToFront:_progressView];
    _progressView.hidden = NO;
    
    NSString *currentSSID = [CameraPassword fetchSSIDInfo];
    DLog(@"check App Connect To Camera At Step03 after sending wifi info");
    if ( !currentSSID ) {
        // check again
        if ( _task_cancelled ) {
            // handle when user press back
        }
        else {
            [NSTimer scheduledTimerWithTimeInterval:3
                                             target:self
                                           selector:@selector(checkAppConnectToCameraAtStep06)
                                           userInfo:nil
                                            repeats:NO];
            
            DLog(@"Continue to check ssid after 3sec");
        }
    }
    else if ( [self isAppConnectedToCamera] ) {
        DLog(@"App connected with camera, start to get WIFI conn status...");
        [self.view bringSubviewToFront:_progressView];
        [self getStatusOfCameraToWifi:nil];
    }
    else {
        // currentSSID is not camera : this means app is kicked out
        DLog(@"App connected with %@ not camera, ask user to switch manually. " ,currentSSID );

        // show prompt to user select network camera again and handle next
        _progressView.hidden = YES;
        _selectCameraView.hidden = NO;
        [self.view bringSubviewToFront:_selectCameraView];
    }
}

-(void)becomeActive
{
    DLog(@"getstatusOfCamera again");
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(checkAppConnectToCameraAtStep06)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)nextStepVerifyPassword
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    DLog(@"Add cam... ");
    DLog(@"Load Step 10");
    
    if ( _deviceConf.ssid ) {
        [PrivateDataUtils saveHomeSSID:_deviceConf.ssid];
    }
    
    Step_10_ViewController *step10ViewController = [[Step_10_ViewController alloc] initWithNibName:@"Step_10_ViewController" bundle:nil];
    [self.navigationController pushViewController:step10ViewController animated:NO];
}

- (void)getStatusOfCameraToWifi:(NSTimer *)info
{
    NSString *commandGetState = GET_STATE_NETWORK_CAMERA;
    NSString *state = [[HttpCom instance].comWithDevice sendCommandAndBlock:commandGetState withTimeout:20.0];
    [SetupFailIndicator errorGetWifiConnectionStateRespone:state];
    DLog(@"getStatusOfCameraToWifi - command %@  response:%@", commandGetState,state);

    if ( state.length > 0 ) {
        _currentStateCamera = [[state componentsSeparatedByString:@": "] objectAtIndex:1];
    }
    else {
        _currentStateCamera = @"";
    }
    
    if ( [_currentStateCamera isEqualToString:@"CONNECTED"] ) {
        [self resetAllTimer];
        [self nextStepVerifyPassword];
        [_progressView removeFromSuperview];
        [_selectCameraView removeFromSuperview];
        _progressView.hidden = YES;
    }
    else {
        // Need to checkout current ssid here!
        if ( ![self isAppConnectedToCamera] ) {
            DLog(@"Step_06VC - current ssid is not a camera ssid !!!!! This check passed before coming here..");
            _selectCameraView.hidden = NO;
            [self.view bringSubviewToFront:_selectCameraView];
        }
        else {
            // get state network of camera after 4s
            _inputPasswordTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                                   target:self
                                                                 selector:@selector(getStatusOfCameraToWifi:)
                                                                 userInfo:nil
                                                                  repeats:NO];
        }
    }
}

- (BOOL)isAppConnectedToCamera
{
    NSString *currentSSID = [CameraPassword fetchSSIDInfo];
    NSString *cameraSSID = [[NSUserDefaults standardUserDefaults] stringForKey:CAMERA_SSID]; //CameraHD-00667fa037
    DLog(@"Step_06_VC - currentSSID: %@, - cameraWiFi: %@", currentSSID, cameraSSID);
    
    if ([currentSSID isEqualToString:cameraSSID]) {
        return YES;
    }
    
    return NO;
}

- (NSDictionary*)getWritableConfiguration {
    NSDictionary *dict = [_deviceConf getWritableConfiguration];
    NSDictionary *dictEncrypt = [AppDelegate encryptDeviceConfiguration:dict];
#ifndef RELEASE
    DLog(@"%s %@", __FUNCTION__, dictEncrypt);
#endif
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:YES forKey:USE_ENCRYPT_DATA_KEY];
    [userDefault synchronize];
    
    return dictEncrypt;
}

- (NSDictionary*)readDeviceConfiguration {
    NSDictionary *dict = [Util readDeviceConfiguration];
    NSDictionary *dictDecrypt = nil;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    BOOL isEncrypt = [userDefault boolForKey:USE_ENCRYPT_DATA_KEY];
    if (isEncrypt) {
#ifndef RELEASE
        DLog(@"%s %@", __FUNCTION__, dict);
#endif
        dictDecrypt = [AppDelegate decryptDeviceConfiguration:dict];
    }
    else {
        dictDecrypt = dict;
    }
#ifndef RELEASE
    DLog(@"%s data after decrypt: %@", __FUNCTION__, dictDecrypt);
#endif
    return dictDecrypt.count ? dictDecrypt : nil;
}

- (BOOL)restoreDataIfPossible
{
	NSDictionary *savedData = [self readDeviceConfiguration];
	if ( savedData ) {
		[_deviceConf restoreConfigurationData:savedData];
        
		// Populate the fields with stored data
		return YES;
	}
    
	return NO;
}

- (void)invalidUdidDialog
{
    _timeOut = nil;
    [self resetAllTimer];
    _progressView.hidden = YES;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Invalid UDID length")
                                                    message:LocStr(@"Please try pairing the camera again")
                                                   delegate:self
                                          cancelButtonTitle:LocStr(@"Ok")
                                          otherButtonTitles:nil];
    alert.tag = INVAID_UDID_TAG;
    [alert show];
}


- (void)showFailPasswordDialog
{
    DLog(@"pass is wrong: %@ ", _password);
    _timeOut = nil;
    [self resetAllTimer];
    _progressView.hidden = YES;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Can't connect to camera")
                                                    message:LocStr(@"Please check password again")
                                                   delegate:self
                                          cancelButtonTitle:LocStr(@"Cancel")
                                          otherButtonTitles:LocStr(@"Ok"), nil];
    alert.tag = PASSWORD_RETRY_TAG;
    [alert show];
}

- (void)resetAllTimer
{
    if ( _timeOut ) {
        [_timeOut invalidate];
        _timeOut = nil;
    }
    
    if ( _inputPasswordTimer ) {
        [_inputPasswordTimer invalidate];
        _inputPasswordTimer = nil;
    }
}

#pragma mark - UIAlertViewDelegate protocol methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSInteger tag = alertView.tag;
    
    if ( tag == PASSWORD_RETRY_TAG ) {
        if ( buttonIndex == 0) {
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            NSInteger setupType = [[NSUserDefaults standardUserDefaults] integerForKey:SET_UP_CAMERA];
            if ( setupType != WIFI_SETUP ) {
                SetupViewController *camSetupVC = [SetupViewController findInNavigationController:self.navigationController];
                camSetupVC.skipToWifiSetup = NO;
            }
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        else {
            DLog(@"Continue to check password");
        }
    }
    
    if ( tag == PASSWORD_WARNING_TAG) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            [self continueHandleNextButton];
        }
    }
    
    //Nghi: Check logic here please : see if it works all the time
    if (tag == INVAID_UDID_TAG) {
        if ( buttonIndex == 0 ) {
            [UIApplication sharedApplication].idleTimerDisabled = NO;
            NSInteger setupType = [[NSUserDefaults standardUserDefaults] integerForKey:SET_UP_CAMERA];
            
            if ( setupType != WIFI_SETUP ) {
                SetupViewController *camSetupVC = [SetupViewController findInNavigationController:self.navigationController];
                camSetupVC.skipToWifiSetup = NO;
            }
            
            [self.navigationController popToRootViewControllerAnimated:NO];
            
        }
    }
    //Nghi
    
    [self.navigationItem setHidesBackButton:NO];
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

@end
