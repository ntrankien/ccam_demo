//
//  Step_06_ViewController.h
//  Cinatic
//
//  Created by Cinatic on 7/26/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CameraScanner/CameraScanner.h>

#import "Util.h"
#import "Step_07_ViewController.h"

@interface Step_06_ViewController : UIViewController

@property (nonatomic, copy) NSString *security;
@property (nonatomic, copy) NSString *ssid;
@property (nonatomic, assign) BOOL isOtherNetwork;
@property (nonatomic, assign) BOOL isSetupViaLan;

- (void)handleNextButton:(id)sender;
- (void)sendWifiInfoToCamera;
- (BOOL)restoreDataIfPossible;
- (void)prepareWifiInfo;

@end

