//
//  Step_10_ViewController.m
//  Cinatic
//
//  Created by Cinatic on 7/27/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import "Step_10_ViewController.h"
#import "Step_11_ViewController.h"
#import "Step_12_ViewController.h"
#import "HttpCom.h"
#import <HUBComm/HUBComm.h>
#import "SetupFailIndicator.h"
#import "CameraListManager.h"

@interface Step_10_ViewController () <UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel *checkConnectionLabel;
@property (nonatomic, weak) IBOutlet UILabel *mayTakeMinuteLabel;
@property (nonatomic, weak) IBOutlet UILabel *setFailedHintLabel;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic, strong) UIView *instructionsView;

@property (nonatomic, strong) IBOutlet UIView *fwOtaUpgradingView;
@property (nonatomic, weak) IBOutlet UILabel *otaTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *otaSubtitleLabel;

@property (nonatomic, strong) UIProgressView *otaDummyProgressBar;
@property (nonatomic, strong) NSTimer *timeOut;

@property (nonatomic, copy) NSString *statusMessage;
@property (nonatomic, copy) NSString * errorCode;

@property (nonatomic) BOOL forceSetupFailed;
@property (nonatomic) BOOL shouldSendMasterKeyAgain;
@property (nonatomic) BOOL shouldStopScanning;

@property (nonatomic, strong) CamProfile *camProfile; // Created camera profile

@end

@implementation Step_10_ViewController

#define SEND_CONF_SUCCESS 1
#define SEND_CONF_ERROR 2

#define SETUP_CAMERAS_UNCOMPLETE 0
#define SETUP_CAMERAS_COMPLETE 1
#define SETUP_CAMERAS_FAIL 2

//Master_key=BC0B87B2832B67FF58F11749F19C4915D4B876C2505D9CC7D0D06F79653C8B11
#define MASTER_KEY @"Master_key="

#define TAG_IMAGE_VIEW_ANIMATION 595
#define PROXY_HOST @"192.168.193.1"
#define PROXY_PORT 8888

#define ALERT_ADD_CAM_FAILED    500
#define ALERT_ADD_CAM_UNREACH   501
#define ALERT_CHECK_STATUS      502
#define ALERT_SELECTED_OPTION   503

#define TIMEOUT_PROCESS         180.0f // 70 seconds

#pragma mark - UIViewController methods

- (void)viewDidLoad
{
    [super viewDidLoad];

    [SetupFailIndicator sharedInstance].step = @"10";
    
    NSString *hostSSID = [[NSUserDefaults standardUserDefaults] objectForKey:HOST_SSID];
    _checkConnectionLabel.text = [NSString stringWithFormat:@"%@ - SSID: %@", LocStr(@"Checking connection to camera"), hostSSID];
    
    NSString *message = LocStr(@"This may take a minute");
    NSString *fwVersion = [[NSUserDefaults standardUserDefaults] stringForKey:FW_VERSION];
    
    _mayTakeMinuteLabel.text = message;
    _setFailedHintLabel.text = nil; // LocStr(@"If setup fails, press below to restart");
    [_cancelButton setTitle:LocStr(@"Cancel") forState:UIControlStateNormal];
    _otaTitleLabel.text = LocStr(@"Updating ... Do not unplug the camera");
    _otaSubtitleLabel.text = LocStr(@"This may take up 3 minutes");
    
    // Keep screen on
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    // Do any additional setup after loading the view.
    // Note: handle notification center must be registed in viewDidLoad
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(becomeActive)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.cameraMac = (NSString *) [userDefaults objectForKey:@"CameraMacWithQuote"];
    self.stringUDID = [userDefaults stringForKey:CAMERA_UDID];
    
    UIImageView *imageView = (UIImageView *)[self.view viewWithTag:595];
    imageView.animationImages =[NSArray arrayWithObjects:
                                [UIImage imageNamed:@"setup_camera_c1"],
                                [UIImage imageNamed:@"setup_camera_c2"],
                                [UIImage imageNamed:@"setup_camera_c3"],
                                [UIImage imageNamed:@"setup_camera_c4"],
                                nil];
    imageView.animationDuration = 1.5;
    imageView.animationRepeatCount = 0;
    
    [imageView startAnimating];
    
    [_setFailedHintLabel performSelector:@selector(setHidden:) withObject:NO afterDelay:57]; //1 * 60 - 3
    [_cancelButton performSelector:@selector(setHidden:) withObject:NO afterDelay:57]; //1 * 60 - 3
    
    self.otaDummyProgressBar = (UIProgressView *)[_fwOtaUpgradingView viewWithTag:5990];
    
    [self waitingCameraRebootAndForceToWifiHome];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationItem setHidesBackButton:YES];
    self.fwOtaUpgradingView.frame = self.view.frame;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:NO];
    [super viewWillDisappear:animated];
}

- (void)dealloc{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)waitForCameraToReboot:(NSTimer *)exp
{
    if ( _shouldStopScanning ) {
        DLog(@"Step_10VC - stop scanning now.");

        self.errorText = LocStr(@"Camera cannot reboot.");
        [self setupFailed];
    }
    else {
        DLog(@"Step_10VC - Continue scan...");
        [self checkCameraAvailableAndFWUpgrading];
    }
}

- (NSInteger)checkCameraAvailableAndFWUpgrading
{
    if ( _shouldStopScanning ) {
        DLog(@"Step_10VC - stop scanning now.");
        self.errorText = LocStr(@"Camera is unavailable.");
        [self setupFailed];
        return CAMERA_STATE_UNKNOWN;
    }
    
    [self checkItOnline:^(BOOL success) {
        if ( success ) {
            
            DLog(@"Found it online");
            
            // Ensure camera name is set.
            [self updateBasicInfoForCamera];
            
            // Let user know we are done now.
            [self setupCompleted];
            
            
        }
        else {
            [NSTimer scheduledTimerWithTimeInterval:2.0
                                             target:self
                                           selector:@selector(waitForCameraToReboot:)
                                           userInfo:nil
                                            repeats:NO];
        }
    }];
    
    return 0;
}

- (void)setupCompleted
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // cancel timeout
    if ( _timeOut ) {
        [_timeOut invalidate];
        self.timeOut = nil;
    }
    
    [self.view setHidden:YES];
    
    [self.navigationItem setHidesBackButton:NO];
    
    // Load step 12
    DLog(@"Load step 12");
    Step_12_ViewController *step12ViewController = [[Step_12_ViewController alloc] initWithNibName:@"Step_12_ViewController" bundle:nil];
    step12ViewController.camProfile = _camProfile;
    [self.navigationController pushViewController:step12ViewController animated:NO];
}

- (void)setupFailed
{
    [self.navigationItem setHidesBackButton:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    DLog(@"Setup has failed - remove cam on server");
    
    if ( _forceSetupFailed ) {
        DLog(@"%s restarting setup immediately", __FUNCTION__);
        
        // Disable Keep screen on
        [UIApplication sharedApplication].idleTimerDisabled=  NO;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else {
        //Load step 11
        DLog(@"Load step 11");

        //Load the next xib
        Step_11_ViewController *step11ViewController = [[Step_11_ViewController alloc] initWithNibName:@"Step_11_ViewController" bundle:nil];
        step11ViewController.upgradeFirmwareVersion = nil;

        
        [self.navigationController pushViewController:step11ViewController animated:NO];
    }
}


- (void)sendCommandRebootCamera
{
    DLog(@"Send command reset camera");
    NSData* responseData = [[HttpCom instance].comWithDevice sendCommandAndBlock_raw:RESTART_HTTP_CMD];
    [SetupFailIndicator errorSystemRestartRespone:[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]];
}

#pragma mark - Actions

- (IBAction)cancelButtonAction:(id)sender
{
    if ( _timeOut ) {
        [_timeOut invalidate];
        self.timeOut = nil;
    }
    
    [self setStopScanning:nil];
    
    [_cancelButton setHidden:YES];
    [_setFailedHintLabel setHidden:YES];
    
    self.forceSetupFailed = TRUE;
}

- (void)checkCameraStatus
{
    if ( _shouldStopScanning ) {
        return;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *udid = [userDefaults objectForKey:CAMERA_UDID];
    
    if ( !udid.length ) {
        DLog(@"%s udid:%@", __FUNCTION__, udid);
        return;
    }
    
    
    HUBCommCore *mgr = [HUBCommCore sharedInstance];
    
    [mgr checkStatusWithRegistrationId:udid
                               success:^(NSDictionary *responseDict) {
                                   if ( responseDict ) {
                                       if ( [[responseDict objectForKey:@"status"] integerValue] == 200 ) {
                                           
                                           NSInteger deviceStatus = [[[responseDict objectForKey:@"data"] objectForKey:@"device_status"] integerValue];
                                           
                                           switch (deviceStatus)
                                           {
                                               case DEV_STATUS_UNKOWN:
                                               case DEV_STATUS_NOT_REGISTERED:
                                               case DEV_STATUS_DELETED:
                                                   // Check again
                                                   break;
                                                   
                                               case DEV_STATUS_NOT_IN_MASTER:
                                               {
                                                   if ( deviceStatus == DEV_STATUS_NOT_IN_MASTER ) {
                                                       [SetupFailIndicator errorUDIDCheck];
                                                       self.statusMessage = [NSString stringWithFormat:LocStr(@"Device (%@) is not present in device master"), _cameraMac];
                                                   }
                                                   else {
                                                       self.statusMessage = LocStr(@"Device is registered with another user.");
                                                   }
                                                   
                                                   
                                                   self.errorText = _statusMessage;
                                                   
                                                   if ( _timeOut ) {
                                                       [self.timeOut invalidate];
                                                       self.timeOut = nil;
                                                   }
                                                   
                                                   [self setStopScanning:nil];
                                                   break;
                                               }
                                                   
                                               case DEV_STATUS_REGISTERED_OTHER_USER:
                                               case DEV_STATUS_REGISTERED_LOGGED_USER:
                                               {
                                                   DLog(@"Step_10_VC register successfully. Move on");
                                                   
                                                   break;
                                               }
                                                   
                                               default:
                                                   break;
                                           }
                                           
                                           
                                           
                                           
                                       }
                                   }
                                   
                               }
                               failure:^(NSError *error) {
                                   //
                                   DLog(@"%s checking udid:%@ err: %@", __FUNCTION__,udid,  error);
                               }
     ];
}

- (void)updateBasicInfoForCamera
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *udid = [userDefaults objectForKey:CAMERA_UDID];
    NSString *hostSSID = [userDefaults objectForKey:HOST_SSID];
    NSString *cameraName = [userDefaults objectForKey:CAMERA_NAME];
    
    NSString *logSSIDName = [hostSSID stringByReplacingOccurrencesOfString:@"\n" withString:@"<NewLine>"];
    logSSIDName = [logSSIDName stringByReplacingOccurrencesOfString:@" " withString:@"<Space>"];
    
    DLog(@"Step_10VC - updatesBasicInfoForCamera udid: %@, name: %@ hostssid: %@ - logSSIDName: %@", udid, cameraName, hostSSID, logSSIDName);
    
    HUBCommCore *mgr = [HUBCommCore sharedInstance];
    
    [mgr updateDeviceBasicInfoWithRegistrationId:udid
                                      deviceName:cameraName
                                        timeZone:nil
                                            mode:nil
                                 firmwareVersion:nil
                                        hostSSID:hostSSID
                                      hostRouter:nil
                                         success:^(NSDictionary *responseDict)
     {
         if ( [responseDict[@"status"] integerValue] == 200 ) {
             DLog(@"Step_10VC - updatesBasicInfoForCamera successfully!\n%@", responseDict);
         }
         else {
             DLog(@"Step_10VC - updatesBasicInfoForCamera did not succeed!\n%@", responseDict);
         }
         
     }
                                         failure:
     ^(NSError *error) {
         
         DLog(@"%s error: %@", __FUNCTION__, error.localizedDescription);
         
     }];
}

#pragma mark - Timer callbacks

- (void)timeOutSetupProcess:(NSTimer *)expired
{
    self.timeOut = nil;
    NSString *homeSsid = [PrivateDataUtils homeSSID];
    
    NSString *currentSSID = [CameraPassword fetchSSIDInfo];
    if (!currentSSID || [self isAppConnectedToCamera]) {
        [SetupFailIndicator errorGetBackToHomeWifi];
    }
    
    DLog(@"Timeout while trying to search for Home Wifi: %@", homeSsid);
    
    [self setStopScanning:nil];
    
    if ( !_errorText ) {
        [SetupFailIndicator errorCheckAvailabilityTimeout];
        
        NSString* fwVersion = [[NSUserDefaults standardUserDefaults] stringForKey:GET_VERSION];
        NSString* errorMessageString = @"";
        if (fwVersion.length) {
            errorMessageString = [NSString stringWithFormat:@"%@ %@: %@. FW: %@", LocStr(@"Could not detect camera. Press and hold the Pair/Reset button on the bottom of your camera for 3 seconds, then try again."), LocStr(@"Error code"), [SetupFailIndicator sharedInstance].errorCode, fwVersion];
        }
        else {
            errorMessageString = [NSString stringWithFormat:@"%@ %@: %@", LocStr(@"Could not detect camera. Press and hold the Pair/Reset button on the bottom of your camera for 3 seconds, then try again."), LocStr(@"Error code"), [SetupFailIndicator sharedInstance].errorCode];
        }
        self.errorText = errorMessageString;
    }
}

- (BOOL)isAppConnectedToCamera
{
    NSString *currentSSID = [CameraPassword fetchSSIDInfo];
    NSString *cameraSSID = [[NSUserDefaults standardUserDefaults] stringForKey:CAMERA_SSID];
    return [currentSSID isEqualToString:cameraSSID];
}

- (void)step10CheckConnectionToHomeWifi:(NSTimer *)expired
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *homeSsid = [PrivateDataUtils homeSSID];
    NSString *currentSSID = [CameraPassword fetchSSIDInfo];
    NSString *wifiCameraSetup = [userDefaults stringForKey:CAMERA_SSID];
    
    if ( !currentSSID || [currentSSID isEqualToString:wifiCameraSetup] ) {
        DLog(@"Step-10 Now, still connected to wifiOf Camera, continue check | currentSSID = nil");
        
        // Phung: we are still connecting to wifi ... how about a restart_system to switch?
        [self sendCommandRebootCamera];
        
        [self performSelector:@selector(step10CheckConnectionToHomeWifi:) withObject:nil afterDelay:3.0];
    }
    else {
        DLog(@"Yeah, already connected to another wifi: %@ system-version:%@", currentSSID, [UIDevice currentDevice].systemVersion);
        if ( [currentSSID isEqualToString:homeSsid] ) {
            DLog(@"It is wifi home");
        }
        else {
            DLog(@"It is NOT wifi home");
        }
        
        // What if this wifi does not have internet connect OR
        // the wifi selected for camera does not have internet connection ????
        
        _instructionsView.hidden = YES;
        
        NSString *bc = @"";
        NSString *own = @"";
        [AppDelegate getBroadcastAddress:&bc AndOwnIp:&own];
        
        if ( ![own isEqualToString:@""] ) {
            if ( _timeOut ) {
                [_timeOut invalidate];
                self.timeOut = nil;
            }
            
            // Timer  ~1min - for camera reboot and add itself to server
            self.timeOut = [NSTimer scheduledTimerWithTimeInterval:TIMEOUT_PROCESS
                                                            target:self
                                                          selector:@selector(timeOutSetupProcess:)
                                                          userInfo:nil
                                                           repeats:NO];
            
            [self waitForCameraToReboot:nil];
            [self checkCameraStatus];
        }
        else {
            DLog(@"Dont get IP from wifi home");
            [self performSelector:@selector(step10CheckConnectionToHomeWifi:) withObject:nil afterDelay:3.0];
        }
    }
}

- (void)becomeActive
{
    _instructionsView.hidden = YES;
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                     target:self
                                   selector:@selector(step10CheckConnectionToHomeWifi:)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)sendMasterKeyToDevice
{
    NSString *set_mkey =[SET_MASTER_KEY stringByAppendingString:_stringAuth_token];
    NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:set_mkey];
    [SetupFailIndicator errorSetMasterKeyRespone:response];
    DLog(@"response: %@", response);
    
    if ( [response hasPrefix:@"set_master_key: 0"] ) {
        // done
        DLog(@"sending master key done");
        [self sendCommandRebootCamera];
        [self performSelectorOnMainThread:@selector(waitingCameraRebootAndForceToWifiHome) withObject:nil waitUntilDone:NO];
    }
    else if ( [response isEqualToString:@"set_master_key: -1"] || (!response && _shouldSendMasterKeyAgain) ) {
        /*
         * Bug from Focus66 FW: version 01.12.68. Fixed on the newer version
         * - Set master key failed at the 1st time.
         * - Set again is ok, so try to set it one more time
         */
        if ( _shouldSendMasterKeyAgain ) {
            self.shouldSendMasterKeyAgain = NO;
            [self sendMasterKeyToDevice];
        }
    }
    else {
        // HACK for VTech where set_master_key does not return a response
        DLog(@"??? sending master key done");
        [self sendCommandRebootCamera];
        [self performSelectorOnMainThread:@selector(waitingCameraRebootAndForceToWifiHome) withObject:nil waitUntilDone:NO];
    }
}

- (void)waitingCameraRebootAndForceToWifiHome
{
    // After sending reboot camera commmand check connection to wifi home after 3 seconds
    [self performSelector:@selector(step10CheckConnectionToHomeWifi:) withObject:nil afterDelay:3.0];
}

- (void)setStopScanning:(NSTimer *)exp
{
    self.shouldStopScanning = YES;
}

- (void)checkItOnline:(void (^)(BOOL success))returnAvailable
{
    DLog(@"--> Try to search IP online...");
    [[CameraListManager sharedInstance] checkCameraIsAvailable:^(BOOL success) {
        if ( success ) {
            self.errorCode = @"NoErr";
            returnAvailable(YES);
        }
        else {
            self.errorCode = @"NotAvail";
            returnAvailable(NO);
        }
    } withMac:_cameraMac];
}



- (void)showDialogWithTag:(NSInteger)tag message:(NSString *)msg
{
    switch (tag)
    {
        case ALERT_ADD_CAM_FAILED:
        case ALERT_CHECK_STATUS:
        {
            NSString *title = LocStr(@"Add camera error");
            title = LocStr(@"Camera installation error");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                            message:msg
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:LocStr(@"Ok"), nil];
            alert.tag = tag;
            [alert show];
            
            break;
        }
            
        case ALERT_ADD_CAM_UNREACH:
        {
            if ( _shouldStopScanning ) {
                // Need not to popup anymore
                return;
            }
            
            NSString *title = LocStr(@"Add camera error");
            title = LocStr(@"Camera installation error");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                            message:LocStr(@"The device is not able to connect to the server. Go to device settings to confirm device is connected to the Internet.")
                                                           delegate:self
                                                  cancelButtonTitle:LocStr(@"Cancel")
                                                  otherButtonTitles:LocStr(@"Retry"), nil];
            alert.tag = ALERT_ADD_CAM_UNREACH;
            [alert show];
            
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - Callbacks

- (void)addCamSuccessWithResponse:(NSDictionary *)responseData
{
    DLog(@"Do for concurent modep - addcam response");
    self.stringAuth_token = [[responseData objectForKey:@"data"] objectForKey:@"auth_token"];
    
    //send master key to device
    self.shouldSendMasterKeyAgain = YES;
    [self sendMasterKeyToDevice];
}

- (void)addCamFailedWithError:(NSDictionary *)errorResponse
{
    if ( !errorResponse ) {
        DLog(@"Error - error_response = nil");
        return;
    }
    
    DLog(@"addcam failed with error code:%d", [errorResponse[@"status"] intValue]);
    
    self.errorText = LocStr(@"Camera not found");
    
    [self setupFailed];
}

- (void)addCamFailedServerUnreachable
{
    DLog(@"addcam failed : server unreachable");
    
     [self showDialogWithTag:ALERT_ADD_CAM_UNREACH message:nil];
}

- (void)removeCamSuccessWithResponse:(NSDictionary *)responseData
{
    DLog(@"Log - removeCam success");
}

- (void)removeCamFailedWithError:(NSDictionary *)errorResponse
{
    DLog(@"Log - removeCam failed Server error: %@", errorResponse[@"message"]);
}

- (void)removeCamFailedServerUnreachable
{
    DLog(@"Log - server unreachable");
}

#pragma mark - AlertView delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSInteger tag = alertView.tag;
    
    if ( tag == ALERT_ADD_CAM_UNREACH ) {
        switch(buttonIndex) {
            case 0: // Cancel
            {
                self.errorText = LocStr(@"Camera not found");
                [self setupFailed];
                break;
            }
            case 1: // Retry
//                [self registerCamera];
                break;
            default:
                break;
        }
    }
    
}

- (void)updateFirmwareProgress:(NSNumber *)number
{
    float value = [number intValue]/100.0f;
    if ( value >= 0 ) {
        _otaDummyProgressBar.progress = value;
    }
}

@end
