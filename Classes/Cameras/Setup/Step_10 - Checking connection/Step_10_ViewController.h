//
//  Step_10_ViewController.h
//  Cinatic
//
//  Created by Cinatic on 7/27/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MonitorCommunication/MonitorCommunication.h>

@class ScanForCamera;

@interface Step_10_ViewController : UIViewController

@property (nonatomic, copy) NSString *cameraMac;
@property (nonatomic, copy) NSString *master_key;
@property (nonatomic, copy) NSString *errorText;
@property (nonatomic, copy) NSString *stringUDID;
@property (nonatomic, copy) NSString *stringAuth_token;

@end
