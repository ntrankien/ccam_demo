//
//  Step_05_ViewController.m
//  Cinatic
//
//  Created on 7/25/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import "Step_05_ViewController.h"
#import "Step_04_ViewController.h"
#import "Step_10_ViewController.h"
#import "Step05Cell.h"
#import "HttpCom.h"
#import "SetupFailIndicator.h"
#import "MBProgressHUD.h"
#import <HUBComm/HUBComm.h>

@interface Step_05_ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *selectNetworkLabel;
@property (nonatomic, weak) IBOutlet UILabel *instruction1Label;
@property (nonatomic, weak) IBOutlet UILabel *instruction2Label;
@property (nonatomic, weak) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UIButton *continueButton;
@property (nonatomic, weak) IBOutlet UIButton *skipButton;

@property (nonatomic, strong) IBOutlet UITableViewCell *cellView;
@property (nonatomic, strong) IBOutlet UITableViewCell *cellOtherNetwork;
@property (nonatomic, strong) IBOutlet UITableViewCell *cellRefresh;

@property (nonatomic, strong) IBOutlet UIView *viewProgress;
@property (nonatomic, weak) IBOutlet UILabel *searchingLabel;
@property (nonatomic, weak) IBOutlet UILabel *waitLabel;
@property (nonatomic, weak) IBOutlet UILabel *setupErrorLabel;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicatorView;

@property (nonatomic, strong) WifiEntry *selectedWifiEntry;
@property (nonatomic, strong) WifiEntry *otherWiFi;

@property int retryWifiCount;

@end

@implementation Step_05_ViewController

#define ALERT_CONFIRM_TAG              555
#define ALERT_RETRY_WIFI_TAG           559
#define ALERT_CANNOT_RETRY_WIFI_TAG    558
#define ALERT_SSID_NAME_WARNING        560

#define CELL_LABEL_TAG                 111
#define MAX_RETRY_WIFI_COUNT           5

#pragma mark - UIViewController methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SetupFailIndicator sharedInstance].step = @"5";
    
    self.title = LocStr(@"Select network");

    [_continueButton setBackgroundImage:[UIImage imageNamed:@"green_btn"] forState:UIControlStateNormal];
    [_continueButton setBackgroundImage:[UIImage imageNamed:@"green_btn_pressed"] forState:UIControlEventTouchDown];
    _continueButton.enabled = NO;
    
    _instruction1Label.text = LocStr(@"Select a trusted network");
    _selectNetworkLabel.text = LocStr(@"Select the Wi-Fi network you wish to connect your camera to");
    _instruction2Label.text = LocStr(@"(Network must be password protected)");
    _subtitleLabel.text = LocStr(@"Detected Wi-Fi network");
    _searchingLabel.text = LocStr(@"Searching for Wi-Fi networks");
    _waitLabel.text = LocStr(@"Please wait");
    
    [_skipButton setTitle:LocStr(@"Skip Wifi Setup") forState:UIControlStateNormal];
    
    [_continueButton setTitle:LocStr(@"Continue") forState:UIControlStateNormal];

    [(UILabel *)[_cellRefresh.contentView viewWithTag:CELL_LABEL_TAG] setText:LocStr(@"Refresh")];
    [(UILabel *)[_cellOtherNetwork.contentView viewWithTag:CELL_LABEL_TAG] setText:LocStr(@"Other network")];
    
    UIImageView *imageView = (UIImageView *)[_viewProgress viewWithTag:585];
    imageView.hidden = YES;
    
    // Create an entry for "Other.."
    self.otherWiFi = [[WifiEntry alloc] initWithSSID:@"\"Other Network\""];
    _otherWiFi.bssid = @"Other";
    _otherWiFi.authMode = @"None";
    _otherWiFi.signalLevel = 0;
    _otherWiFi.noiseLevel = 0;
    _otherWiFi.quality = nil;
    _otherWiFi.encryptType = @"None";
    
    [self.view addSubview:_viewProgress];
    [self.view bringSubviewToFront:_viewProgress];
    [_indicatorView startAnimating];
    
    [self performSelector:@selector(queryWifiList) withObject:nil afterDelay:0.001];
    self.retryWifiCount = 0;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.viewProgress.frame = self.view.frame;

    if ( _camProfile ) {
        self.skipButton.hidden = NO;
        [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, _skipButton.frame.origin.y - _tableView.frame.origin.y - 5)];
    }
}

- (void)dealloc{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (NSString*)errorCodeAndFW{
    return [NSString stringWithFormat:@""];
}

#pragma mark - Actions

- (IBAction)continueButtonAction:(id)sender
{
    // Stopped setup proccess if selected wifi is open. DO NOT support anymore!
    // The selected is HOME or not doesn't mater, just check to confirm.
    if ( [_selectedWifiEntry.authMode isEqualToString:@"open"] ) {
        NSString *title = LocStr(@"For your security, we do not support connecting to a Wi-Fi network that is not password protected. Please secure your network and try again.");
        [[[UIAlertView alloc] initWithTitle:title
                                   message:nil
                                  delegate:nil
                         cancelButtonTitle:nil
                           otherButtonTitles:LocStr(@"Ok"), nil] show];
    }
    else {
        NSRange noQoute = NSMakeRange(1, _selectedWifiEntry.ssidWithQuotes.length - 2);
        NSString *wifiName = [_selectedWifiEntry.ssidWithQuotes substringWithRange:noQoute];
        NSString *homeWifi = [PrivateDataUtils homeSSID];

        if([wifiName hasPrefix:@" "] || [wifiName hasSuffix:@" "]){
            NSString *msg = LocStr(@"We've detected a non-printable character in the Wi-Fi network name that may cause setup failure. Continue?");
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Warning") message:msg delegate:self cancelButtonTitle:LocStr(@"Cancel") otherButtonTitles:LocStr(@"Continue"), nil];
            alert.tag = ALERT_SSID_NAME_WARNING;
            [alert show];
        }else{
            [self continueSetupWithWifiName:wifiName andHomeWifiName:homeWifi];
        }
    }
}

- (void)continueSetupWithWifiName:(NSString*)wifiName andHomeWifiName:(NSString*)homeWifi{
    if ( [wifiName isEqualToString:homeWifi] ) {
        [self moveToNextStep];
    }
    else {
        [self showDialogToConfirm:homeWifi selectedWifi:wifiName];
    }
}

- (IBAction)skipButtonAction:(id)sender
{
    MBProgressHUD* hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hub setLabelText:LocStr(@"Waiting for camera configuration ...")];
    
    [self performSelectorInBackground:@selector(configureCameraAndMoveToFinalStep) withObject:NO];
}

#pragma mark - config camera

- (void)configureCameraAndMoveToFinalStep
{
    [self configureCamera];
    
    [[NSUserDefaults standardUserDefaults] setObject:[CameraPassword fetchSSIDInfo] forKey:HOST_SSID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSelectorOnMainThread:@selector(moveToFinalStep) withObject:nil waitUntilDone:NO];
}

- (void)defaultOffAllPNToCamera //VTech only
{        
    NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:@"vox_disable"];
    [SetupFailIndicator errorVOXEnableRespone:response];

    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:@"set_cam_park&value=0"];
    [SetupFailIndicator errorSetTempHIRespone:response];
}

- (void)configureCamera
{
    /*
     * 1. Set Auth.
     * 2. Default on all of PN.
     * 3. set flicker
     * 4. Get UDID
     * 5. Restart systems.
     */
    
    // 1.
    NSDate *now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ZZZ"];
    
    NSMutableString *stringFromDate = [NSMutableString stringWithString:[formatter stringFromDate:now]];
    [stringFromDate insertString:@"." atIndex:3];
    
    NSString *set_auth_cmd = [NSString stringWithFormat:@"%@%@%@%@%@",
                               SET_SERVER_AUTH,
                               SET_SERVER_AUTH_PARAM1, [[HUBCommCore sharedInstance].dataSource apiKeyForHUBCommCore],
                               SET_SERVER_AUTH_PARAM2, stringFromDate];
    
    NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:set_auth_cmd
                                                                   withTimeout:10.0];
    [SetupFailIndicator errorServerAuthenticateRespone:response];
    DLog(@"set auth -set_auth_cmd: %@, -response: %@ ", set_auth_cmd, response);
    
    DLog(@"VTech -- setup don't turn on Notification");
    [self defaultOffAllPNToCamera];
    
    // 3.
    float timeZoneInNumber = [[NSTimeZone localTimeZone] secondsFromGMT] / 3600.f; //-12 --> 12
    NSString *cmdFlicker = @"set_flicker&value=60";
    
    if ( timeZoneInNumber > -4 && timeZoneInNumber < 9 ) {
        cmdFlicker = @"set_flicker&value=50";
    }
    
    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:cmdFlicker];
    [SetupFailIndicator errorSetFlickerRespone:response];
    DLog(@"%s set_flicker %@", __FUNCTION__, response);
    
    //Nghi: Support new Query City_Timezone for DST
    NSTimeZone *localTimezone = [NSTimeZone localTimeZone];
    NSString *localTimezoneName = [localTimezone name];
    NSString *cmdTimezone = [@"set_city_timezone&value=" stringByAppendingString:localTimezoneName];
    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:cmdTimezone withTimeout:5.0];
    DLog(@"%s set_city_timezone %@", __FUNCTION__, response);

    // 4.
    NSString *stringUDID = @"";
    NSString *stringMac = @"00:00:00:00:00";
    
    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:GET_UDID
                                                         withTimeout:5.0];
    [SetupFailIndicator errorGetUDIDRespone:response];
    
    NSString *pattern = [NSString stringWithFormat:@"^%@: [0-9A-Z]{26}$", GET_UDID];
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:NSRegularExpressionAnchorsMatchLines
                                                                             error:&error];
    if ( !regex ) {
        DLog(@"%s error:%@", __FUNCTION__, error.description);
    }
    else {
        DLog(@"%s respone:%@", __FUNCTION__, response);
        
        if ( response ) {
            NSUInteger numberOfMatches = [regex numberOfMatchesInString:response //get_udid: 01008344334C32B0A0VFFRBSVA
                                                                options:0
                                                                  range:NSMakeRange(0, [response length])];
            DLog(@"%s numberOfMatches:%lu", __FUNCTION__, (unsigned long)numberOfMatches);
            
            if ( numberOfMatches == 1 ) {
                stringUDID = [response substringFromIndex:GET_UDID.length + 2];
                stringMac = [Util add_colon_to_mac:[stringUDID substringWithRange:NSMakeRange(6, 12)]];
            }
        }
    }
    
    // Save mac address for using later
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:stringMac forKey:@"CameraMacWithQuote"];
    [userDefaults setObject:stringUDID forKey:CAMERA_UDID];
    [userDefaults synchronize];
    
    // 5.
    response = [[HttpCom instance].comWithDevice sendCommandAndBlock:RESTART_HTTP_CMD];
    [SetupFailIndicator errorSystemRestartRespone:response];
    DLog(@"%s RESTART_HTTP_CMD: %@", __FUNCTION__, response);
}

- (void)defaultOnAllPNToCamera
{
    NSString *result = @"";
    
    //if ( [[NSUserDefaults standardUserDefaults] integerForKey:SET_UP_CAMERA] != FOCUS73_SETUP )
    {
        NSString *response = [self sendCommand:@"vox_disable" baseCommand:@"vox_disable" retryOnFailure:3];
        [self analyticWithCMDResponse:response command:@"vox_disable"];
        [SetupFailIndicator errorVOXEnableRespone:response];
        result = [result stringByAppendingFormat:@", %@", response];
        
        response = [self sendCommand:@"set_temp_lo_enable&value=0" baseCommand:@"set_temp_lo_enable" retryOnFailure:3];
        [self analyticWithCMDResponse:response command:@"set_temp_lo_enable"];
        [SetupFailIndicator errorSetTempLORespone:response];
        result = [result stringByAppendingFormat:@", %@", response];
        
        response = [self sendCommand:@"set_temp_hi_enable&value=0" baseCommand:@"set_temp_hi_enable" retryOnFailure:3];
        [self analyticWithCMDResponse:response command:@"set_temp_hi_enable"];
        [SetupFailIndicator errorSetTempHIRespone:response];
        result = [result stringByAppendingFormat:@", %@", response];
    }
    
    DLog(@"%s respnse:%@", __FUNCTION__, result);
    NSString *fwVersion = [[NSUserDefaults standardUserDefaults] stringForKey:FW_VERSION];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:CAMERA_MODEL_ID] isEqualToString:CP_MODEL_CONCURRENT] && [fwVersion compare:FW_VERSION_NOT_SET_RECORD_DURATION] == NSOrderedDescending) {
        //NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:@"recording_cooloff_duration&value=120"];
        NSString *response = [self sendCommand:@"recording_cooloff_duration&value=120" baseCommand:@"recording_cooloff_duration" retryOnFailure:3];
        [self analyticWithCMDResponse:response command:@"recording_cooloff_duration"];
        [SetupFailIndicator errorRecordingCollOffRespone:response];
        result = [result stringByAppendingFormat:@", %@", response];
        DLog(@"%s recording_cooloff_duration respnse:%@", __FUNCTION__, result);
        
        //response = [[HttpCom instance].comWithDevice sendCommandAndBlock:@"recording_active_duration&value=90"];
        response = [self sendCommand:@"recording_active_duration&value=90" baseCommand:@"recording_active_duration" retryOnFailure:3];
        [self analyticWithCMDResponse:response command:@"recording_active_duration"];
        [SetupFailIndicator errorRecordingActiveRespone:response];
        result = [result stringByAppendingFormat:@", %@", response];
        DLog(@"%s recording_active_duration respnse:%@", __FUNCTION__, result);
    }
}

- (NSString *)sendCommand:(NSString*)command baseCommand:(NSString*)baseCmd retryOnFailure:(NSInteger)retryTime{
    if( retryTime < 0 ) {
        DLog(@"%s, command: %@ all retries failed", __FUNCTION__, command);
        return nil;
    }
    else{
        NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:command];
        if ([SetupFailIndicator isDefaultSuccessPattern:response command:baseCmd]) {
            return response;
        }
        else {
            return [self sendCommand:command baseCommand:baseCmd retryOnFailure:retryTime - 1];
        }
    }
}

#pragma mark - Methods

- (void)analyticWithCMDResponse:(NSString*)response command:(NSString*)cmd{
}

- (void)moveToNextStep
{
    Step_06_ViewController *step06ViewController = [[Step_06_ViewController alloc] initWithNibName:@"Step_06_ViewController" bundle:nil];
    NSRange noQoute = NSMakeRange(1, _selectedWifiEntry.ssidWithQuotes.length - 2);
    NSString *wifiName = [_selectedWifiEntry.ssidWithQuotes substringWithRange:noQoute];
    
    [[NSUserDefaults standardUserDefaults] setObject:wifiName forKey:HOST_SSID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    step06ViewController.isOtherNetwork = [wifiName isEqualToString:@"Other Network"];
    step06ViewController.ssid = wifiName;
    step06ViewController.security = _selectedWifiEntry.authMode;
    
    if ( _camProfile ) {
        step06ViewController.isSetupViaLan = YES;
    }
    else {
        step06ViewController.isSetupViaLan = NO;
    }
    
    [self.navigationController pushViewController:step06ViewController animated:NO];
}

- (void)moveToFinalStep
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    Step_10_ViewController *step10ViewController = [[Step_10_ViewController alloc] initWithNibName:@"Step_10_ViewController" bundle:nil];

    [self.navigationController pushViewController:step10ViewController animated:NO];
}

- (void)showDialogToConfirm:(NSString *)homeWifi selectedWifi:(NSString *)selectedWifi
{
    NSString *msg = LocStr(@"You have selected the Wi-Fi network '%@' which is not the same as your Home Wi-Fi network '%@'. If you continue, additional steps will be required to set up your camera. Proceed?");
    msg = [NSString stringWithFormat:msg, selectedWifi, homeWifi];
    
    UIAlertView *alertViewNotice = [[UIAlertView alloc] initWithTitle:LocStr(@"Notice")
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:LocStr(@"Cancel")
                                              otherButtonTitles:LocStr(@"Continue"), nil];
    alertViewNotice.tag = ALERT_CONFIRM_TAG;
    [alertViewNotice show];
}

- (void)queryWifiList
{
    DLog(@"Step_05_VC - queryWifiList. Waiting...");
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *fwVersion = [userDefaults stringForKey:FW_VERSION]; // 01.12.58

    BOOL newCmdFlag = YES;
    NSData *routerListData;
    
//    if ( [fwVersion compare:FW_MILESTONE] >= NSOrderedSame || _camProfile ) {
        // fw >= FW_MILESTONE
        routerListData = [[HttpCom instance].comWithDevice sendCommandAndBlock_raw:GET_ROUTER_LIST2
                                                                       withTimeout:2*DEFAULT_TIME_OUT];
    
        DLog(@"%s - router_list_raw: %@", __FUNCTION__, [[NSString alloc] initWithData:routerListData encoding:NSUTF8StringEncoding]);
//    }
//    else {
//        newCmdFlag = NO;
//        routerListData = [[HttpCom instance].comWithDevice sendCommandAndBlock_raw:GET_ROUTER_LIST
//                                                                       withTimeout:2*DEFAULT_TIME_OUT];
//    }
    [SetupFailIndicator errorGetCameraRouterRespone:[[NSString alloc] initWithData:routerListData encoding:NSUTF8StringEncoding]];
    
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    
    if ( routerListData ) {
        WifiListParser *routerListParser = [[WifiListParser alloc] initWithNewCmdFlag:newCmdFlag];
        
        [routerListParser parseData:routerListData completion:^(NSArray *wifiList) {
            [self setWifiResult:wifiList];
        }];
    }
    else {
        DLog(@"Have a nil wifi list from camera");
        //NGHI: 1001 : no cameamra
        if ( _retryWifiCount < MAX_RETRY_WIFI_COUNT ) {
            [self askForRetry];
            self.retryWifiCount++;
        }
        else {
            [self cameraCannotRetry];
            self.retryWifiCount = 0;
        }
    }
}

- (void)askForRetry
{
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:nil
                                                      message:LocStr(@"Failed to communicate with camera. Retry?")
                                                     delegate:self
                                            cancelButtonTitle:LocStr(@"Cancel")
                                            otherButtonTitles:LocStr(@"Retry"), nil];
    myAlert.tag = ALERT_RETRY_WIFI_TAG;
    [myAlert show];
}

- (void)cameraCannotRetry
{
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:nil
                                                      message:LocStr(@"Failed to communicate with camera. You will need to re-pair the camera.")
                                                     delegate:self
                                            cancelButtonTitle:LocStr(@"Ok")
                                            otherButtonTitles:nil, nil];
    myAlert.tag = ALERT_CANNOT_RETRY_WIFI_TAG;
    [myAlert show];
    
    NSString *fw_version = [[HttpCom instance].comWithDevice sendCommandAndBlock:GET_VERSION];
    [SetupFailIndicator errorFirmwareVersionRespone:fw_version];
    NSRange colonRange = [fw_version rangeOfString:@": "];
    
    if ( colonRange.location != NSNotFound ) {
        fw_version = [[fw_version componentsSeparatedByString:@": "] objectAtIndex:1];
    }
    
    _setupErrorLabel.text = [NSString stringWithFormat:@"%@. %@: %@ - FW: %@", LocStr(@"Failed to communicate with camera"), LocStr(@"Error code"), [SetupFailIndicator sharedInstance].errorCode, fw_version] ;
    _setupErrorLabel.hidden = NO;
}

#pragma mark - Alert view delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( alertView.tag == ALERT_RETRY_WIFI_TAG ) {
        switch(buttonIndex) {
            case 0:
            {
                // TODO: Go back to camera detection screen
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
            case 1:
            {
                // retry...
                [self.view addSubview:_viewProgress];
                [self.view bringSubviewToFront:_viewProgress];
                [_indicatorView startAnimating];
                
                DLog(@"OK button pressed");
                [self performSelectorInBackground:@selector(queryWifiList) withObject:nil];
                break;
            }
                
            default:
                break;
        }
    }
    else if ( alertView.tag == ALERT_CANNOT_RETRY_WIFI_TAG ) {
        switch(buttonIndex) {
            case 0:
            {
                // TODO: Go back to camera detection screen
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
            }
            default:
                break;
        }
    }
    else if ( alertView.tag == ALERT_CONFIRM_TAG ) {
        if ( buttonIndex == 1 ) {
            // Continue
            [self moveToNextStep];
        }
    }
    else if(alertView.tag == ALERT_SSID_NAME_WARNING){
        if (buttonIndex != alertView.cancelButtonIndex) {
            NSRange noQoute = NSMakeRange(1, _selectedWifiEntry.ssidWithQuotes.length - 2);
            NSString *wifiName = [_selectedWifiEntry.ssidWithQuotes substringWithRange:noQoute];
            NSString *homeWifi = [PrivateDataUtils homeSSID];
            [self continueSetupWithWifiName:wifiName andHomeWifiName:homeWifi];
        }
    }
    else {
        DLog(@"Step_05_VC - alertDismiss: %ld", (long)alertView.tag);
    }
}

#pragma mark - WifiListParse delegate

- (void)setWifiResult:(NSArray *)wifiList
{
    DLog(@"GOT WIFI RESULT: numentries: %lu", (unsigned long)wifiList.count);

    dispatch_async(dispatch_get_main_queue(), ^{
        // hide progressView
        [_viewProgress removeFromSuperview];
        
        WifiEntry *entry;
        for ( int i = 0; i < wifiList.count; i++ ) {
            entry = wifiList[i];
            DLog(@"entry: %d, ssid_w_quote: %@, bssid: %@, auth_mode: %@, quality: %@", i, entry.ssidWithQuotes, entry.bssid, entry.authMode, entry.quality);
        }
        
        self.listOfWifi = [NSMutableArray arrayWithArray:wifiList];
        [_listOfWifi addObject:_otherWiFi];
        
        self.listOfWifi = [self filteredCameraList:_listOfWifi];
        [_tableView reloadData];
    });
}

- (NSMutableArray *)filteredCameraList:(NSArray *)listOfWifi
{
    NSMutableArray *wifiList = [@[] mutableCopy];
    for ( int i = 0; i < listOfWifi.count; i++ ) {
        WifiEntry *wifi = listOfWifi[i];
        NSString *ssid = wifi.ssidWithQuotes;
        
        if (![ssid hasPrefix:[NSString stringWithFormat:@"\"%@", DEFAULT_SSID_PREFIX]] &&
            ![ssid hasPrefix:[NSString stringWithFormat:@"\"%@", DEFAULT_SSID_HD_PREFIX]] &&
            ![ssid isEqualToString:@"\"\""] )
        {
            [wifiList addObject:wifi];
        }
    }
    
    return wifiList;
}

#pragma mark - Table view delegates & datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( section == 1 ) {
        return 1;
    }
    return _listOfWifi.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 ) {
        if ( indexPath.row < _listOfWifi.count - 1 ) {
            static NSString *CellIdentifier = @"Step05Cell";
            Step05Cell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"Step05Cell" owner:nil options:nil];
            for ( id curObj in objects ) {
                if ( [curObj isKindOfClass:[Step05Cell class]] ) {
                    cell = (Step05Cell *)curObj;
                    break;
                }
            }
            
            WifiEntry *entry = _listOfWifi[indexPath.row];
            [cell displayWifiInfo:entry];
            
            return cell;
        }
        else {
            return _cellOtherNetwork;
        }
    }
    else {
        return _cellRefresh;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 ) {
        _continueButton.enabled = YES;
        self.selectedWifiEntry = (WifiEntry *)_listOfWifi[indexPath.row];
    }
    else {
        [self.view addSubview:_viewProgress];
        [self.view bringSubviewToFront:_viewProgress];
        [_indicatorView startAnimating];
        [self performSelectorInBackground:@selector(queryWifiList) withObject:nil];
    }
}

@end
