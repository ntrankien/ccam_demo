//
//  Step05Cell.m
//  Cinatic
//
//  Created by Developer on 2/28/14.
//  Copyright (c) 2014 Cinatic Connected Ltd. All rights reserved.
//

#import "Step05Cell.h"
#import "WifiEntry.h"

@implementation Step05Cell

- (void)drawRect:(CGRect)rect
{
    UIImageView *imageViewBottomLine = (UIImageView *)[self viewWithTag:508];
    imageViewBottomLine.frame = CGRectMake(0, rect.size.height - 0.5, rect.size.width, 0.5);
}

- (void)displayWifiInfo:(WifiEntry*)wifiEntry{
    self.lblName.text = [wifiEntry.ssidWithQuotes substringWithRange:NSMakeRange(1, wifiEntry.ssidWithQuotes.length - 2)]; // Remove " & "
    if (!wifiEntry.quality.length || wifiEntry.quality.integerValue <= 0 || wifiEntry.quality.integerValue > 100) {
        self.imgWifiStrength.image = nil;
    }
    else if (wifiEntry.quality.integerValue <= 25) {
        self.imgWifiStrength.image = [UIImage imageNamed:@"setup_wifi_25"];
    }
    else if (wifiEntry.quality.integerValue <= 50) {
        self.imgWifiStrength.image = [UIImage imageNamed:@"setup_wifi_50"];
    }
    else if (wifiEntry.quality.integerValue <= 75) {
        self.imgWifiStrength.image = [UIImage imageNamed:@"setup_wifi_75"];
    }else{
        self.imgWifiStrength.image = [UIImage imageNamed:@"setup_wifi_green"];
    }
}

@end
