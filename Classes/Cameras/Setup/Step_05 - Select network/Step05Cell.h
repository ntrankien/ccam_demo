//
//  Step05Cell.h
//  Cinatic
//
//  Created by Developer on 2/28/14.
//  Copyright (c) 2014 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WifiEntry;

@interface Step05Cell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UIImageView *imgWifiStrength;

- (void)displayWifiInfo:(WifiEntry*)wifiEntry;

@end
