//
//  Step_05_ViewController.h
//  Cinatic
//
//  Created by Cinatic on 7/25/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WifiEntry.h"
#import "Step_06_ViewController.h"

@interface Step_05_ViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *listOfWifi;
@property (nonatomic, strong) CamProfile *camProfile;

@end
