//
//  Step_07_ViewController.m
//  Cinatic
//
//  Created by Cinatic on 8/6/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import "Step_07_ViewController.h"
#import "Step_06_ViewController.h"

@interface Step_07_ViewController ()

@property (nonatomic, weak) IBOutlet UITableViewCell *cellView;
@property (nonatomic, strong) NSArray *securityTypes;
@property (nonatomic, assign) NSInteger selectedIndex;

@end

@implementation Step_07_ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = LocStr(@"Security");
    
    self.securityTypes = @[@"open", @"wep" , @"wpa/wpa2 psk"];
    self.selectedIndex = -1;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return   ((interfaceOrientation == UIInterfaceOrientationPortrait) ||
              (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
              (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma  mark - Table View delegate & datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = (NSString *)_securityTypes[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger tag = tableView.tag;
    if ( tag == 12 ) {
        return _securityTypes.count;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger tag = tableView.tag;
    if ( tag == 12 ) {
        return 1;
    }
    return 0; 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
    if ( _selectedIndex == indexPath.row ) {
        return;
    }
    
    NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:_selectedIndex inSection:0];
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (newCell.accessoryType == UITableViewCellAccessoryNone) {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.selectedIndex = indexPath.row;
    }
    
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldIndexPath];
    if (oldCell.accessoryType == UITableViewCellAccessoryCheckmark) {
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    if ([_lastStep isKindOfClass:[Step_06_ViewController class]]) {
        Step_06_ViewController *backStep = (Step_06_ViewController *)_lastStep;
        backStep.security = _securityTypes[_selectedIndex];
    }
}

@end
