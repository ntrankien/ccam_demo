//
//  Step_07_ViewController.h
//  Cinatic
//
//  Created by Cinatic on 8/6/12.
//  Copyright (c) 2012 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Step_07_ViewController : UIViewController

@property (nonatomic, weak) UIViewController *lastStep;

@end
