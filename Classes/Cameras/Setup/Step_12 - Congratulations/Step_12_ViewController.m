//
//  Step_12_ViewController.m
//  Cinatic
//
//  Created by Cinatic on 2/26/13.
//  Copyright (c) 2013 Cinatic Connected Ltd. All rights reserved.
//

#import "Step_12_ViewController.h"
#import "SetupViewController.h"
#import <HUBComm/HUBCommCore.h>
#import "MBProgressHUD.h"

@interface Step_12_ViewController()

@property (nonatomic, weak) IBOutlet UILabel *congratsLabel;
@property (nonatomic, weak) IBOutlet UILabel *cameraName;
@property (nonatomic, weak) IBOutlet UILabel *cameraFoundLabel;
@property (nonatomic, weak) IBOutlet UIButton *viewLiveCameraButton;

@end

@implementation Step_12_ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = LocStr(@"Set up successful");

    _congratsLabel.text = LocStr(@"Congratulations");
    _cameraFoundLabel.text = LocStr(@"Camera has been connected");
    [_viewLiveCameraButton setTitle:LocStr(@"View live camera") forState:UIControlStateNormal];
    
    [_viewLiveCameraButton setBackgroundImage:[UIImage imageNamed:@"green_btn"] forState:UIControlStateNormal];
    [_viewLiveCameraButton setBackgroundImage:[UIImage imageNamed:@"green_btn_pressed"] forState:UIControlEventTouchDown];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _cameraName.text = (NSString *)[userDefaults objectForKey:CAMERA_NAME];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES];
    //    self.navigationItem.leftBarButtonItem.enabled = NO;
}

#pragma mark - Custom action methods

- (IBAction)viewLiveCameraButtonAction:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *registrationID = [userDefaults objectForKey:CAMERA_UDID];

    [userDefaults setBool:YES forKey:PUSH_TO_LIVE_VIEW_IMMEDIATELY];
    [userDefaults setObject:registrationID forKey:REG_ID];
    [userDefaults synchronize];
    
    DLog(@"STEP12 START MONITOR -reg: %@", registrationID);
    
    // Disable Keep screen on
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    //NSString *ssid = [userDefaults stringForKey:CAMERA_SSID];
    NSString *camModelId = [[NSUserDefaults standardUserDefaults] objectForKey:CAMERA_MODEL_ID];
    
    SetupViewController *camSetupVC = [SetupViewController findInNavigationController:self.navigationController];
    if (camSetupVC.setupCompletion) {
        camSetupVC.setupCompletion(YES, camModelId, nil);
    }
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end
