//
//  Step_12_ViewController.h
//  Cinatic
//
//  Created by Cinatic on 2/26/13.
//  Copyright (c) 2013 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CamProfile;

@interface Step_12_ViewController : UIViewController

@property (strong, nonatomic) CamProfile* camProfile;

@end
