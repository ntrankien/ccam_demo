//
//  CameraSetupGuideViewController.m
//  CCam
//
//  Created by Tran Kien Nghi on 3/23/17.
//  Copyright © 2017 Cinatic Technology. All rights reserved.
//

#import "SetupGuideViewController.h"
#import "SetupViewController.h"
#import "SetupGuideCollectionViewCell.h"
#import "SetupGuideTableViewCell.h"

//Old import
#import "HttpCom.h"
#import "MBProgressHUD.h"
#import "Step_04_ViewController.h"
#import <MessageUI/MessageUI.h>

@interface SetupGuideViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *guideClv;
@property (weak, nonatomic) IBOutlet UITableView *guideTbv;
@property (strong, nonatomic) NSArray* guideTbvSelectedIndex;

//Old variable
@property (nonatomic, copy) NSString *cameraMac;
@property (nonatomic, copy) NSString *cameraName;
@property (nonatomic, copy) NSString *homeWifiSSID;
@property (nonatomic, strong) NSString *cameraModel;

@property (nonatomic, strong) NSDate *timeoutDate;
@property (nonatomic) BOOL taskCancelled;
@property (nonatomic, strong) NSTimer *timerCheckConnectionToCamera;
@property (nonatomic, strong) MBProgressHUD *hub;

// For debug only
@property (nonatomic, strong) NSData *camLogData;
@property (nonatomic, copy) NSString *camLogName;

@end

@implementation SetupGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.guideTbvSelectedIndex = @[@(0), @(1)];
}

- (void)viewWillStyle{
    [self setDefaultBackButton];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleEnteredBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(becomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    self.timeoutDate = nil; // Will restart processing if coming back this view again.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //[self showGuideView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ( [self.navigationController.viewControllers indexOfObject:self] == NSNotFound ) {
        // View is disappearing because it was popped from the stack
        DLog(@"View controller was popped --- We are closing down..task_cancelled = YES");
        self.taskCancelled = YES;
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [self stopCheckingCameraTimer];
}

#pragma mark - UI Actions

- (IBAction)cancelButtonTapped:(id)sender {
    [self stopCheckingCameraTimer];
    self.timeoutDate = nil;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    SetupViewController* setupVC = [SetupViewController findInNavigationController:self.navigationController];
    
    if (setupVC.setupCompletion) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString* modelID = [userDefaults stringForKey:CAMERA_MODEL_ID];
        setupVC.setupCompletion(NO, modelID, nil);
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Private methods

- (void)handleEnteredBackground
{
    //Ensur process hided
    [self hideProgess];
}

- (void)becomeActive
{
    self.taskCancelled = NO;
    
    //if ( _viewState != ViewStateShowDialogToUpgradeFw && _viewState != ViewStateUpgradingFw && _viewState != ViewStateUploadingFw && !_checkScanningLan && !_timeoutDate ) {
    if (!_timeoutDate ) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setInteger:WIFI_SETUP forKey:SET_UP_CAMERA];
        [userDefaults synchronize];
        self.timeoutDate = [NSDate dateWithTimeIntervalSinceNow:TIME_OUT_DETECT_CAMERA_VIA_WIFI];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        self.hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hub.labelText = LocStr(@"Detecting camera...");
        
        [self checkConnectionToCamera:nil];
        [self performSelector:@selector(checkCameraSSIDifNedded) withObject:nil afterDelay:2.0];
    }
}

- (void)checkCameraSSIDifNedded{
    NSString *currentSSID = [CameraPassword fetchSSIDInfo];
    
    if (!( [currentSSID hasPrefix:DEFAULT_SSID_PREFIX]
          || [currentSSID hasPrefix:DEFAULT_SSID_HD_PREFIX])) {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:LocStr(@"Warning")
                                    message:LocStr(@"Ensure app is joining camera's WiFi to start pairing")
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* goSettings = [UIAlertAction
                             actionWithTitle:LocStr(@"Settings")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=WIFI"]];
                                 self.timeoutDate = nil;
                                 [self stopCheckingCameraTimer];
                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=WIFI"]];
                             }];
        [alert addAction:goSettings];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:LocStr(@"Continue")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 
                             }];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)showProgress:(NSTimer *)exp
{
//    DLog(@"show progress ");
//
//    if ( _progressView ) {
//        DLog(@"show progress 01 ");
//        _progressView.hidden = NO;
//        _progressView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        [self.view addSubview:_progressView];
//        [self.view bringSubviewToFront:_progressView];
//    }
}

- (void)hideProgess
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
- (void)checkConnectionToCamera:(NSTimer *)expired
{
    NSDate *now = [NSDate date];
    
    if ( [now compare:_timeoutDate] == NSOrderedDescending ) {
        
        [self hideProgess];
        
        NSString* errorMessageString;
        NSString* fwVersion = [[NSUserDefaults standardUserDefaults] stringForKey:GET_VERSION];
        
        //TODO: Using fake Error code
        if (fwVersion.length) {
            errorMessageString = [NSString stringWithFormat:@"%@ %@: %@. FW: %@", LocStr(@"Could not detect camera. Press and hold the Pair/Reset button on the bottom of your camera for 3 seconds, then try again."), LocStr(@"Error code"), @"1000", fwVersion];
        }
        else {
            errorMessageString = [NSString stringWithFormat:@"%@ %@: %@", LocStr(@"Could not detect camera. Press and hold the Pair/Reset button on the bottom of your camera for 3 seconds, then try again."), LocStr(@"Error code"), @"1000"];
        }
        
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:LocStr(@"Warning")
                                    message:errorMessageString
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* goSettings = [UIAlertAction
                                     actionWithTitle:LocStr(@"Settings")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                         //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=WIFI"]];
                                         self.timeoutDate = nil;
                                         [self stopCheckingCameraTimer];
                                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=WIFI"]];
                                     }];
        [alert addAction:goSettings];
        
        UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:LocStr(@"Cancel")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action) {
                                 [self cancelButtonTapped:nil];
                             }];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];

        //
        
//        @try {
//            [NSException raise:NSInvalidArgumentException
//                        format:@"Camera Wifi setup - Checking connection to camera timeout"];
//        } @catch (NSException* exception) {
//            [Crittercism logHandledException:exception];
//            [GoogleAnalyticsUtil trackExceptionWithDescription:@"Camera Wifi setup - Checking connection to camera timeout"];
//        }
        
        return;
    }
    
    DLog(@"checkConnectionTo-Camera");
    
    NSString *bc1 = @"";
    NSString *own1 = @"";
    [AppDelegate getBroadcastAddress:&bc1 AndOwnIp:&own1];
    
    DLog(@"@s-Camera 01");
    
    // Check for ip available before check for SSID to avoid crashing.
    if ( [own1 isEqualToString:@""] ) {
        DLog(@"IP is not available.. comeback later..");
        
        // check back later..
        if ( _taskCancelled ) {
            // was popped by its parent
            return;
        }
        
        self.timerCheckConnectionToCamera = [NSTimer scheduledTimerWithTimeInterval:3
                                                                             target:self
                                                                           selector:@selector(checkConnectionToCamera:)
                                                                           userInfo:nil
                                                                            repeats:NO];
        return;
    }
    
    DLog(@"checkConnectionTo-Camera 01");
    
    NSString *currentSSID = [CameraPassword fetchSSIDInfo];
    DLog(@"checkConnectionTo-Camera 03: %@, system-version:%@", currentSSID, [UIDevice currentDevice].systemVersion);
    
    if ( [currentSSID hasPrefix:DEFAULT_SSID_PREFIX] ||
        [currentSSID hasPrefix:DEFAULT_SSID_HD_PREFIX]
        ) {
        // Yeah we're connected ... check for ip??
        NSString * bc = @"";
        NSString * own = @"";
        [AppDelegate getBroadcastAddress:&bc AndOwnIp:&own];
        
        DLog(@"Check mac address is %@ and Ip address is %@", bc, own);
        if ( [own hasPrefix:DEFAULT_IP_PREFIX]  ||
            [own hasPrefix:DEFAULT_IP_PREFIX_CAMERA_C89]
            || [own hasPrefix:@"192.168.222"]
            
            ) {
            // Set default ip first, will use later for all
            if ( [own hasPrefix:DEFAULT_IP_PREFIX_CAMERA_C89] ) {
                DLog(@"Set default ip for camera c89 %@", own);
                [[HttpCom instance].comWithDevice setDevice_ip:DEFAULT_BM_IP_CAMERA_C89];
            }
            else if ( [own hasPrefix:DEFAULT_IP_PREFIX] ) {
                DLog(@"Set default ip for camera %@", own);
                [[HttpCom instance].comWithDevice setDevice_ip:DEFAULT_BM_IP];
            }
            
            else if ([own hasPrefix:@"192.168.222"]){
                DLog(@"Set default ip for mebo camera %@", own);
                [[HttpCom instance].comWithDevice setDevice_ip:@"192.168.222.1"];
            }
            
            else {
                DLog(@"Set default ip for camera %@", own);
                [[HttpCom instance].comWithDevice setDevice_ip:DEFAULT_BM_IP];
            }
            
            // Device Port
            [HttpCom instance].comWithDevice.device_port = DEFAULT_BM_PORT;
            
            DLog(@"camera mac: %@ ip:%@", _cameraMac, own );
            
            // Remember the mac address .. very important
            self.cameraMac = [CameraPassword fetchBSSIDInfo];
            self.cameraName = currentSSID;
            
            // Don't reschedule another wake up
            [self hideProgess];
            
            // Save and check firmware version to upgrade
            if ( ![self saveCameraInfoAndCheckUpgradeFirmware] ) {
                //have not valid firmware version
                [self moveToNextStep];
            }
            
            return;
        }
    }
    
    if ( !_taskCancelled ) {
        // Check back later..
        self.timerCheckConnectionToCamera = [NSTimer scheduledTimerWithTimeInterval:3
                                                                             target:self
                                                                           selector:@selector(checkConnectionToCamera:)
                                                                           userInfo:nil
                                                                            repeats:NO];
    }
}

- (void)retrieveCameraLog
{
    self.hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [_hub setDetailsLabelText:LocStr(@"Retrieving Camera Log...")];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString *camLog = [NSString stringWithFormat:@"http://%@:8080/cgi-bin/logdownload.cgi", [HttpCom instance].comWithDevice.device_ip];
        
        NSURLResponse *response = nil;
        NSError *error = nil;
        
        self.camLogData = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:camLog]] returningResponse:&response
                                                            error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *responseCmd = [[HttpCom instance].comWithDevice sendCommandAndBlock:RESTART_HTTP_CMD];
            
            DLog(@"%s response:%@", __FUNCTION__, responseCmd);
            
            if ( error && !_camLogData ) {
                NSString *msg = error?error.description:@"Retrieve Cam Log failed!";
                DLog(@"%s msg:%@", __FUNCTION__, msg);
                [_hub setDetailsLabelText:msg];
                [_hub hide:YES afterDelay:3];
            }
            else {
                DLog(@"%s Check Wifi connecting.", __FUNCTION__);
                self.camLogName = response.suggestedFilename;
                [_hub setDetailsLabelText:LocStr(@"Checking Wifi Network...")];
                [self step3CheckConnectionToHomeWifi:nil];
            }
            
            [self.navigationItem.leftBarButtonItem setEnabled:YES];
        });
    });
}

- (NSString *)modelIdFromUDIDString:(NSString *)aUDID
{
    if (aUDID) {
        if (aUDID.length == 26 || aUDID.length == 24) {
            return [aUDID substringWithRange:NSMakeRange(3, 3)];
        }
    }
    
    return CP_MODEL_NA;
}

- (BOOL)saveCameraInfoAndCheckUpgradeFirmware
{
    DLog(@"saveCameraInfoAndCheckUpgradeFirmware");
    NSString *fw_version = [[HttpCom instance].comWithDevice sendCommandAndBlock:GET_VERSION];
    NSRange colonRange = [fw_version rangeOfString:@": "];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( colonRange.location != NSNotFound ) {
        fw_version = [[fw_version componentsSeparatedByString:@": "] objectAtIndex:1];
        [userDefaults setObject:fw_version forKey:FW_VERSION];
        [userDefaults setObject:_cameraName forKey:CAMERA_SSID];
    }
    
    NSString *udid = [[HttpCom instance].comWithDevice sendCommandAndBlock:GET_UDID];
    
    if ( udid && [udid hasPrefix:GET_UDID] && udid.length > GET_UDID.length + 2 ) {
        udid = [udid substringFromIndex:GET_UDID.length + 2];
        [userDefaults setObject:udid forKey:CAMERA_UDID];
    }
    
    NSString *modelId = [self modelIdFromUDIDString:udid];
    
    if ( modelId ) {
        [userDefaults setObject:modelId forKey:CAMERA_MODEL_ID];
    }
    
    [userDefaults synchronize];
    return NO;
}

- (void)step3CheckConnectionToHomeWifi:(NSTimer *) expired
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * homeSsid = [PrivateDataUtils homeSSID]; //home wifi
    NSString * currentSSID = [CameraPassword fetchSSIDInfo]; //current wifi
    NSString *wifiCameraSetup = [userDefaults stringForKey:CAMERA_SSID]; //current wifi of camera setup
    
    if ( !currentSSID || [currentSSID isEqualToString:wifiCameraSetup] ) {
        DLog(@"Step-03 Now, still connected to wifiOf Camera, continue check | currentSSID = nil");
        
        NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:RESTART_HTTP_CMD];
        
        DLog(@"%s response:%@", __FUNCTION__, response);
        
        [NSTimer scheduledTimerWithTimeInterval:3.0
                                         target:self
                                       selector:@selector(step3CheckConnectionToHomeWifi:)
                                       userInfo:nil
                                        repeats:NO];
    }
    else {
        DLog(@"Yeah, already connected to another wifi: %@ ",currentSSID);
        
        if ( [currentSSID isEqualToString:homeSsid] ) {
            DLog(@"It is wifi home");
        }
        else {
            DLog(@"It is NOT wifi home");
        }
        
        // What if this wifi does not have internet connect OR
        // the wifi selected for camera does not have internet connection ????
        
        NSString *bc = @"";
        NSString *own = @"";
        [AppDelegate getBroadcastAddress:&bc AndOwnIp:&own];
        
        if ( ![own isEqualToString:@""] ) {
            [_hub hide:YES];
            [self sendsCameraLogData:_camLogData name:_camLogName info:nil];
        }
        else {
            DLog(@"Dont get IP from wifi home");
            [NSTimer scheduledTimerWithTimeInterval:3.0
                                             target:self
                                           selector:@selector(step3CheckConnectionToHomeWifi:)
                                           userInfo:nil
                                            repeats:NO];
        }
    }
}

- (void)moveToNextStep
{
    //TODO: THIS IS JUST TEMP SOLUTION, will move to new solution when new UI is ready
    
    DLog(@"Step_03 - moveToNextStep");
    
    [self hideProgess];
    //Make sure the failure screen is not displayed after moving to step 4
    [self stopCheckingCameraTimer];
    
    Step_04_ViewController *step04ViewController = [[Step_04_ViewController alloc] initWithNibName:@"Step_04_ViewController" bundle:nil];
    step04ViewController.cameraMac = _cameraMac;
    step04ViewController.cameraName = _cameraName;
    [self.navigationController pushViewController:step04ViewController animated:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)sendsCameraLogData:(NSData *)aData name:(NSString *)logName info:(NSString*)info
{
    if ( ![MFMailComposeViewController canSendMail] ) {
        NSString *msg = LocStr(@"Cannot send email from this device.\nPlease set up or verify your email account settings.");
        DLog(@"%s Can not send Email from this device", __FUNCTION__);
        [[[UIAlertView alloc] initWithTitle:LocStr(@"Send Email")
                                        message:msg
                                       delegate:nil
                              cancelButtonTitle:nil
                              otherButtonTitles:LocStr(@"Ok"), nil] show];
    }
    else {
        MFMailComposeViewController *mc = [AppDelegate composeEmailWithCamLog:aData name:logName additionalInfo:info callback:self];
        [self.navigationController setNavigationBarHidden:YES];
        // Show email view
        [self presentViewController:mc animated:YES completion:NULL];
    }
}

- (void)stopCheckingCameraTimer{
    [_timerCheckConnectionToCamera invalidate];
    self.timerCheckConnectionToCamera = nil;
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SetupGuideCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SetupGuideCollectionViewCell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0: {
            cell.guideImv.image = [UIImage imageNamed:@"wifi_setup_guide_1-2"];
        }
            break;
        case 1: {
            cell.guideImv.image = [UIImage imageNamed:@"wifi_setup_guide_3-4"];
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellWith = SCREEN_WIDTH;
    CGFloat cellHeight = cellWith * 1.575; //504 * cellWith / 320
    
    return CGSizeMake(cellWith, cellHeight);
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SetupGuideTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"SetupGuideTableViewCell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0: {
            cell.guideLbl.text = LocStr(@"1) Go to your device's home screen");
        }
            break;
        case 1: {
            cell.guideLbl.text = LocStr(@"2) Open settings");
        }
            break;
        case 2: {
            cell.guideLbl.text = LocStr(@"3) Select \"WiFi\"");
        }
            break;
        case 3: {
            cell.guideLbl.text = LocStr(@"4) Select the camera network from the list of available WiFi networks");
        }
            break;
        default:
            break;
    }
    
    BOOL cellSelected = NO;
    for (NSNumber* selectedIndex in _guideTbvSelectedIndex) {
        if (indexPath.row == selectedIndex.integerValue) {
            cellSelected = YES;
            break;
        }
    }
    
    if (cellSelected){
        [cell.guideLbl setFont:[UIFont boldSystemFontOfSize:cell.guideLbl.font.pointSize]];
    }
    else {
        [cell.guideLbl setFont:[UIFont systemFontOfSize:cell.guideLbl.font.pointSize]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == _guideClv) {
        NSArray* visibleCells = [_guideClv visibleCells];
        if (visibleCells.count >= 1){
            NSIndexPath* indexPath = [_guideClv indexPathForCell:visibleCells[0]];
            if (indexPath.row == 0) {
                self.guideTbvSelectedIndex = @[@(0), @(1)];
            }
            else{
                self.guideTbvSelectedIndex = @[@(2), @(3)];
            }
        }
        
        [_guideTbv reloadData];
    }
}

@end
