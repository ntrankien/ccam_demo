//
//  SetupViewController.h
//  CCam
//
//  Created by Tran Kien Nghi on 3/16/17.
//  Copyright © 2017 Cinatic Technology. All rights reserved.
//

#import "BaseViewController.h"

typedef void (^CameraSetupCompletion) (BOOL success, NSString* cameraModel, NSError* error);

@interface SetupViewController : BaseViewController

@property (nonatomic, copy) CameraSetupCompletion setupCompletion;
@property (nonatomic) BOOL skipToWifiSetup;

+ (SetupViewController*)findInNavigationController:(UINavigationController *)navigationController;

@end
