//
//  SetupViewController.m
//  CCam
//
//  Created by Tran Kien Nghi on 3/16/17.
//  Copyright © 2017 Cinatic Technology. All rights reserved.
//

#import "SetupViewController.h"

@interface SetupViewController ()

@property (weak, nonatomic) IBOutlet UILabel *welcomeLbl;
@property (weak, nonatomic) IBOutlet UILabel *startParingPromtLblb;
@property (weak, nonatomic) IBOutlet UILabel *firstGuideLbl;

@property (weak, nonatomic) IBOutlet UIButton *addCameraBtn;

@end

@implementation SetupViewController

+ (SetupViewController*)findInNavigationController:(UINavigationController *)navigationController
{
    SetupViewController *controller = nil;
    for ( UIViewController *aController in navigationController.viewControllers ) {
        if ( [aController isKindOfClass:[SetupViewController class]] ) {
            controller = (SetupViewController *)aController;
            break;
        }
    }
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    //self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

- (void)viewWillStyle{
    _welcomeLbl.text = LocStr(@"Welcome!");
    _startParingPromtLblb.text = LocStr(@"Start pairing your device now");
    _firstGuideLbl.text = LocStr(@"First, turn on the pairing button on your device");
    
    [_addCameraBtn setTitle:LocStr(@"Add Camera") forState:UIControlStateNormal];
    _addCameraBtn.layer.cornerRadius = 5.0;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - UI Actions
- (IBAction)cancelButtonTapped:(id)sender {
//    [self.navigationController dismissViewControllerAnimated:YES completion:^{
//        if (_setupCompletion) {
//            _setupCompletion(NO, nil, nil);
//        }
//    }];
    
    _setupCompletion(NO, nil, nil);
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
