//
//  CamerasViewController.h
//  Cinatic
//
//  Created by Developer on 12/16/13.
//  Copyright (c) 2013 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CamerasViewController : UIViewController

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *camChannels;


@end
