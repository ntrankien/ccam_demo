//
//  CamerasCollectionViewCell.h
//  Cinatic
//
//  Created by Nicolas Morin on 2015-08-14.
//  Copyright (c) 2015 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CamChannel;

@protocol CamerasCollectionViewCellDelegate <NSObject>

-(void)didChooseSettingsForCamChannel:(CamChannel *)camChannel;

@end

@interface CamerasCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<CamerasCollectionViewCellDelegate>delegate;

-(void)configureCellWithCameraChannel:(CamChannel *)camChannel;

- (void)hideSettingButton:(BOOL)hidden;

@end
