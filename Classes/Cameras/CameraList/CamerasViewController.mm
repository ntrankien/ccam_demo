//
//  CamerasViewController.m
//  Cinatic
//
//  Created by Developer on 12/16/13.
//  Copyright (c) 2013 Cinatic Connected Ltd. All rights reserved.
//

#import "MBProgressHUD.h"
#import "CamerasCollectionViewCell.h"
#import "CamerasViewController.h"
#import <HUBComm/HUBComm.h>
#import "HttpCom.h"
#import "RMCHandler_2.h"
#import "CameraPlayerViewController.h"
#import <CameraScanner/CameraScanner.h>
#import "SetupViewController.h"
#import "CameraListManager.h"

@interface CamerasViewController () < CamerasCollectionViewCellDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, HUBCommCoreDataSource, CameraListManagerDelegate>

@property (nonatomic, strong) NSArray *snapshotImages;
@property (nonatomic, copy) NSString *strDocDirPath;
@property (nonatomic) BOOL thumbsLoaded;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) CameraPlayerViewController *cameraPlayerViewController;

//
@property (nonatomic, copy) NSString* currentAPIKey;

@end

@implementation CamerasViewController

#pragma mark - UIViewController methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = LocStr(@"Devices");
    
    // Setup a custom title view so we can show a nice looking logo image

    self.strDocDirPath = PathForDir;
    
    UINib *cellNib = [UINib nibWithNibName:@"CamerasCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"CamerasCollectionViewCell"];

    self.collectionView.alwaysBounceVertical = YES;
    self.refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.tintColor = [UIColor grayColor];
    [_refreshControl addTarget:self action:@selector(refreshCamsInfo:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:_refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
    [CameraListManager sharedInstance].delegate = self;
    
    //Set data source for HUBComm library
    [[HUBCommCore sharedInstance] setDataSource:self];
    
    [self requestAPIKeyWithCompletion:^(BOOL success, NSString *apiKey) {
        [self getCameraList];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_collectionView.collectionViewLayout invalidateLayout];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [_collectionView.collectionViewLayout invalidateLayout];
}

#pragma mark - UI Actions

- (IBAction)addDeviceButtonTapped:(id)sender {
    if ( _camChannels.count >= MAX_CAM_ALLOWED ) {
        NSString *msg = LocStr(@"Please remove one camera from the current list before adding a new one.");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:msg
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:LocStr(@"Ok"), nil];
        [alert show];
    }
    else {
        [[CameraListManager sharedInstance] closeP2PSessions];
        [self continueWithAddCameraAction];
    }
}

#pragma mark - Get Camera list

- (void)refreshCamsInfo:(id)sender {
    [self getCameraList];
}

- (void)getCameraList {
    
    __weak CamerasViewController* weakSelf = self;
    [[CameraListManager sharedInstance] getCameraListWithCompletion:^(NSMutableArray *cameraChannels, NSError *error) {
        weakSelf.camChannels = cameraChannels;
        
        [weakSelf.refreshControl endRefreshing];
        [weakSelf.collectionView reloadData];
    }];
}

#pragma mark - AddCameraVCDelegate protocol methods

- (void)continueWithAddCameraAction
{
    SetupViewController* setupVC = [SetupStoryBoard instantiateViewControllerWithIdentifier:@"SetupViewController"];
    setupVC.setupCompletion = ^(BOOL success, NSString* cameraModel, NSError* error) {
//        if (success) {  
//        }
        
        //Reload cam list no matter what
        [self getCameraList];
    };
    
    [self.navigationController pushViewController:setupVC animated:YES];
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    count = _camChannels.count;
    
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CamerasCollectionViewCell *camCell = (CamerasCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CamerasCollectionViewCell" forIndexPath:indexPath];
    camCell.delegate = self;
    CamChannel *channel = (CamChannel *)[_camChannels objectAtIndex:indexPath.row]; //indexPath.item
    [camCell configureCellWithCameraChannel:channel];
    
    
	return camCell;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
	//CGSize bounds = [UIScreen mainScreen].bounds.size;
    CGSize bounds = self.view.bounds.size;
    
    return CGSizeMake(bounds.width, 40);
    
//    if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
//        return CGSizeMake(bounds.width / 3, bounds.width / 3);
//    } else {
//        return CGSizeMake(bounds.width, 9 * bounds.width / 16);
//    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(0,0,0,0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_camChannels.count) {
        return;
    }
    else { // Camera cell
        CamChannel *ch = (CamChannel *)[_camChannels objectAtIndex:indexPath.row];

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

        if ([ch.profile isNotAvailable])
        {
            if ([ch.profile isSharedCam] || [ch.profile isNotAvailable]) {
                DLog(@"CamerasVC - didSelectRowAtIndexPath - Selected camera is NOT available & is SHARED_CAM");
            }
            else {
                //TODO: Nghi - Go to event web view if needed
            }
        }
        else {
            ch.profile.isSelected = YES;
            
            [UIApplication sharedApplication].idleTimerDisabled = YES;
            
            [userDefaults setObject:ch.profile.registrationID forKey:REG_ID];
            [userDefaults setObject:ch.profile.mac_address forKey:CAM_IN_VEW];
            [userDefaults synchronize];
            
            self.cameraPlayerViewController = [[CameraPlayerViewController alloc] init];
            _cameraPlayerViewController.selectedChannel = ch;
            _cameraPlayerViewController.sStreamMode =@"PL";
            
            RMCHandler_2 * handler = (RMCHandler_2 *)ch.profile.p2pHandler;
            switch (handler.p2pMode )
            {
                case P2PModeLocal:
                    _cameraPlayerViewController.sStreamMode =@"PL";
                    break;
                case P2PModeRemote:
                    _cameraPlayerViewController.sStreamMode =@"PR";
                    break;
                    
                default:
                case P2PModeRelay:
                    _cameraPlayerViewController.sStreamMode =@"PS";
                    break;
                
            }

            [self.navigationController pushViewController:_cameraPlayerViewController animated:YES];
        }
    }
}

#pragma mark - CamerasCollectionViewCellDelegate

- (void)didChooseSettingsForCamChannel:(CamChannel *)camChannel {
//	MenuViewController *menuVC = (MenuViewController *)self.parentVC;
//	CameraMenuViewController *cameraMenuVC = [[CameraMenuViewController alloc] init];
//	cameraMenuVC.camChannel = camChannel;
//	cameraMenuVC.cameraMenuDelegate = menuVC.menuDelegate;
//	[self.navigationController pushViewController:cameraMenuVC animated:YES];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ( UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    }
}

- (BOOL)shouldAutorotate{
    return NO;
}

#pragma mark - HUBCommCoreDataSource

- (NSString *)apiKeyForHUBCommCore{
    return _currentAPIKey;
}

- (NSString *)hostServerForHUBCommCore{
    //NSString *hostServer = @"https://dev-api.cinatic.com/v1";
    NSString *hostServer = @"https://api.cinatic.com/v1";
    return hostServer;
}

- (void)requestAPIKeyWithCompletion:(void (^) (BOOL success, NSString* apiKey))completion{
    HUBCommCore *mgr = [HUBCommCore sharedInstance];
    
    NSString* refreshToken = @"8fd255fe50463cb0425986a7b2354102";
    
    if (refreshToken.length) {
        [mgr getAuthenTokenWithRefreshToken:refreshToken success:^(NSDictionary *responseDict) {
            if (responseDict) {
                NSInteger statusCode = [responseDict[@"status"] intValue];
                
                if (statusCode == 200) {
                    // success
                    NSString *newAPIKey = [responseDict[@"data"] stringForKey:@"access_token"];
                    DLog(@"%s new API key: %@", __func__, newAPIKey);
                    
                    if (newAPIKey.length) {
                        self.currentAPIKey = newAPIKey;
                    }
                    
                    if (completion) {
                        completion(YES, newAPIKey);
                    }
                }
                else{
                    if (completion) {
                        completion(NO, nil);
                    }
                }
            }
        } failure:^(NSError *error, NSInteger statusCode) {
            DLog(@"%s Login error, status: %ld, description: %@", __func__, (long)statusCode, error.description);
            if (completion) {
                completion(NO, nil);
            }
        }];
    }
    else {
        completion(NO, nil);
    }
}

- (NSData*)serverCertificateDataForHUBCommCore{
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"certificate" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    return certData;
}

#pragma mark - CameraListManagerDelegate

- (void)cameraListStatusUpdatedWithChannel:(CamChannel *)channel{
    [_collectionView reloadData];
}

@end
