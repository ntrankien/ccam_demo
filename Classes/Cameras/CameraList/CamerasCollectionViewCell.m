//
//  CamerasCollectionViewCell.m
//  Cinatic
//
//  Created by Nicolas Morin on 2015-08-14.
//  Copyright (c) 2015 Cinatic Connected Ltd. All rights reserved.
//

#import "CamerasCollectionViewCell.h"
#import <CameraScanner/CameraScanner.h>
#import "RMCHandler_2.h"

@interface CamerasCollectionViewCell()// <RMCHandlerDataPrevDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageview;
@property (weak, nonatomic) IBOutlet UILabel *onlineLabel;
@property (weak, nonatomic) IBOutlet UIImageView *onlineImageview;
@property (weak, nonatomic) IBOutlet UIView *onlineBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *cameraLabel;
@property (weak, nonatomic) IBOutlet UILabel *previewStats;
@property (weak, nonatomic) IBOutlet UIButton *onlineButton;

@property (nonatomic, strong) CamChannel *camChannel;
@property (weak, nonatomic) IBOutlet UIView *backgroundTempView;
@property (strong, nonatomic) IBOutlet UIButton *btnCamSettings;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *viewActivityP2P;

@property (nonatomic) NSInteger countSnapshot;


@end

@implementation CamerasCollectionViewCell

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

-(UIImage* ) getImageForCamChannel:(CamChannel *) camChannel
{
    
    UIImage *image = nil;
    
    NSString *fileName= [NSString stringWithFormat:@"%@_preview.jpg", camChannel.profile.registrationID];
    NSString *imgNameJpg = [NSString stringWithFormat:@"%@.jpg", camChannel.profile.registrationID];
    NSFileManager* fm = [NSFileManager defaultManager];
    NSDictionary* attrs = [fm attributesOfItemAtPath:PathForFileName(fileName) error:nil];
    
    if (attrs != nil) {
        NSDate *datePrev = (NSDate*)[attrs objectForKey: NSFileCreationDate];
        attrs = [fm attributesOfItemAtPath:(imgNameJpg) error:nil];
        
        if (attrs != nil && datePrev) {
            NSDate *date = (NSDate*)[attrs objectForKey: NSFileCreationDate];
            
            if ([date compare:datePrev] == NSOrderedDescending) {
                fileName = imgNameJpg;
            }
        }
    }
    else {
        fileName = imgNameJpg;
    }
    
    image = [UIImage imageWithContentsOfFile:PathForFileName(fileName)];
    
    
    return image;
}
- (void)configureCellWithCameraChannel:(CamChannel *)camChannel
{
	self.camChannel = camChannel;
	_onlineBackgroundView.layer.cornerRadius = 2;
	self.backgroundColor = [UIColor clearColor];
	self.backgroundView.backgroundColor = [UIColor clearColor];
	_backgroundTempView.layer.borderWidth = 1.0f;
	_backgroundTempView.layer.borderColor = [UIColor whiteColor].CGColor;
	//_backgroundTempView.layer.cornerRadius = 10;
	_backgroundTempView.layer.masksToBounds = YES;
	_cameraLabel.text = camChannel.profile.name;
	
	//if ([camChannel.profile isNotAvailable]) {
    if (camChannel.profile.minuteSinceLastComm != 1) {
		_onlineImageview.image = [UIImage imageNamed:@"offline"];
		_onlineLabel.text = LocStr(@"Offline");
        _onlineButton.selected = NO;
	} else {
		_onlineImageview.image = [UIImage imageNamed:@"online"];
		_onlineLabel.text = LocStr(@"Online");
        _onlineButton.selected = YES;
	}
  
    
   if (camChannel.profile.isP2PAlready && camChannel.profile.minuteSinceLastComm == 1)
    {
       
#if 0
        //Get the Snapshot of camera
        UIImage *image = [self getImageForCamChannel:camChannel];
        
        if (image) {
            _backgroundImageview.image = image;
        }
        else {
            //_backgroundImageview.image = [UIImage imageNamed:@"camera_image_bg"];
        }
#else
        
        //We support Preview - no need to show Snapshot
#endif
        
        if (camChannel.profile.p2pHandler) {
            RMCHandler_2 *theTmp = (RMCHandler_2 *)camChannel.profile.p2pHandler;

            self.countSnapshot = 0;
        }
        else {
            [_viewActivityP2P stopAnimating];
            [_viewActivityP2P setHidden:YES];
            //_backgroundImageview.image = [UIImage imageNamed:@"camera_image_bg"];
        }
        
        [_viewActivityP2P setHidden:YES];
    }
    else {
        NSLog(@">> P2p is not Ready for cam: %@", camChannel.profile.registrationID );

        [_viewActivityP2P startAnimating];
        [_viewActivityP2P setHidden:NO];
        //[_backgroundImageview setImage:nil]; // nil is fine
        //_backgroundImageview.image = [UIImage imageNamed:@"camera_image_bg"];
    }
}

- (void)hideSettingButton:(BOOL)hidden
{
    _btnCamSettings.hidden = hidden;
}

- (IBAction)settingsButtonClicked:(id)sender {
    if (_delegate && _camChannel) {
        [self.delegate didChooseSettingsForCamChannel:_camChannel];
    }
}

#pragma mark Preview callback

-(void)updateVideoFps:(double)fps
{
    
   // NSLog(@"Preview fps: %f",fps);


    dispatch_async(dispatch_get_main_queue(), ^{
    
        self.previewStats.text =  [NSString stringWithFormat:@"%0.2f",fps];
    });
    
    
}

- (BOOL)allowDisplayingPreviewer{
    return _camChannel.profile.isP2PAlready;
}

@end
