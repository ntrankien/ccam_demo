//
//  CameraListManager.m
//  CCam
//
//  Created by Tran Kien Nghi on 3/9/17.
//  Copyright © 2017 Cinatic Connected Ltd. All rights reserved.
//

#import "CameraListManager.h"
#import <CameraScanner/CameraScanner.h>
#import <HUBComm/HUBComm.h>
#import <MDeviceComm/MDeviceComm.h>
#import "RMCHandler_2.h"
#include <stdlib.h>

@interface CameraListManager()

@property (nonatomic, strong) NSMutableArray* devices;
@property (nonatomic, strong) NSMutableArray* channels;

@property (nonatomic) BOOL isUpdatingCameraList;
@property (nonatomic) int p2pSessionsNumbers;
@property (nonatomic) BOOL wantToStop;

@end

@implementation CameraListManager

+(instancetype)sharedInstance{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        ((CameraListManager*)sharedInstance).devices = @[].mutableCopy;
    });
    return sharedInstance;
}

- (void)addDevices:(NSArray*)devices{
    @synchronized (self) {
        NSMutableArray* allDevices = [[NSMutableArray alloc] initWithArray:_devices];
        for (id device in devices) {
            if ([device isKindOfClass:[CamProfile class]]) {
                [allDevices addObject:device];
            }
        }
        
        self.devices = allDevices;
    }
}

- (void)removeDevices:(NSArray*)devices{
    @synchronized (self) {
        NSMutableArray* allDevices = [[NSMutableArray alloc] initWithArray:_devices];
        for (CamProfile* device in devices) {
            
            CamProfile* foundDevice = nil;
            for (CamProfile* camProfile in allDevices) {
                if ([device.registrationID isEqualToString:camProfile.registrationID]) {
                    foundDevice = camProfile;
                    break;
                }
            }
            
            if (foundDevice){
                [allDevices removeObject:foundDevice];
            }
        }
        
        self.devices = allDevices;
    }
}

- (NSArray*)cameraList{
    NSMutableArray* camList = @[].mutableCopy;
    for (CamProfile* camProfile in _devices) {
        
        //TODO: check to make sure device is camera
        [camList addObject:camProfile];
    }
    
    return camList;
}

- (NSArray*)cameraChannelList{
    return _channels;
}

- (NSArray*)doorBellList{
    NSMutableArray* camList = @[].mutableCopy;
    for (CamProfile* camProfile in _devices) {
        //if([CameraUtil isDoorBellWithUDID:[camProfile getModelFromUDID]]){
            //[camList addObject:camProfile];
        //}
    }
    
    return camList;
}

- (NSArray*)allDevices{
    return _devices;
}

- (void)getCameraListWithCompletion:(void (^)(NSMutableArray* cameraChannels, NSError* error))completion{
    
    //Temp solution: Just remove all camera channels and devices
    self.wantToStop = NO;
    for (CamChannel* channel in _channels) {
        [channel.profile.p2pHandler stop];
    }
    [self.channels removeAllObjects];
    [self.devices removeAllObjects];
    //
    
    HUBCommCore *mgr = [HUBCommCore sharedInstance];
    self.isUpdatingCameraList = YES;
    [mgr getAllDevices:^(NSDictionary *responseDict)
     {
         DLog(@"%s: %@", __FUNCTION__, responseDict);
         
         NSMutableArray *camProfiles = nil;
         NSInteger status = [responseDict[@"status"] intValue];
         NSMutableArray* channels = [NSMutableArray new];
         
         if ( status == 200 ) {
             NSArray *dataArr = responseDict[@"data"];
             
             
             if ( ![dataArr isEqual:[NSNull null]] && dataArr.count > 0 ) {
                 camProfiles = [self parseCameraList:dataArr];
                 [self setDevices:camProfiles];
             }
         }
         
         for (int camProfIndex = 0; camProfIndex < camProfiles.count; camProfIndex++) {
             CamChannel *ch = [[CamChannel alloc] initWithChannelIndex:camProfIndex];
             CamProfile* cp = camProfiles[camProfIndex];
             [ch setCamProfile:cp];
             //[cp setChannel:ch];
             
             [channels addObject:ch];
         }
         self.channels = channels;
         
         for (int i = 0; i < camProfiles.count; i++) {
             CamProfile *aCp = (CamProfile *)camProfiles[i];
             
             [self createP2PSessionForCam:aCp];
             
             aCp.hasUpdateLocalStatus = YES;
         }
         
         
         if (completion) {
             completion(channels, nil);
         }

         self.isUpdatingCameraList = NO;
     } failure:^(NSError *error)
     {
         DLog(@"%s error: %@", __FUNCTION__, error.localizedDescription);
         
         if (completion) {
             completion(nil, error);
         }
         
         self.isUpdatingCameraList = NO;
     }];
}

- (NSMutableArray *)parseCameraList:(NSArray *)dataArr
{
    NSMutableArray *camList = [[NSMutableArray alloc] init];
    
    for (NSDictionary *camEntry in dataArr) {
        CamProfile *cp = [self createCamProfileWithDict:camEntry];
        [camList addObject:cp];
    }
    
    return camList;
}

- (id)createCamProfileWithDict:(NSDictionary*)camEntry {
    NSInteger deviceID       = [camEntry[@"id"] integerValue];
    NSString *camName        = [camEntry stringForKey:@"name"];
    NSString *registrationID = [camEntry stringForKey:@"device_id"];
    NSString *camMac         = [camEntry stringForKey:@"mac"];
    
    if ( camMac.length != 12 ) {
        camMac = @"00:00:00:00:00:00";
    }
    else {
        camMac = [Util add_colon_to_mac:camMac];
    }
    
    NSString *fwTime          = [camEntry stringForKey:@"last_update"];
    NSDictionary *deviceLocation = [camEntry dictionaryForKey:@"device_location"];
    NSString *localIp = nil;
    
    if ( [deviceLocation isEqual:[NSNull null]] ) {
        localIp = nil;
    }
    else {
        localIp = [deviceLocation stringForKey:@"local_ip"];
    }
    
    NSString *isAvailable   = [camEntry stringForKey:@"is_online"];
    
    //For test: SET ON TO TEST **************************************
    //isAvailable = @"1";
    //
    
    NSDictionary *firmware_obj = [camEntry dictionaryForKey:@"firmware"];
    NSString *fwVersion     = [firmware_obj stringForKey:@"version"];
    NSInteger fwStatus = [[firmware_obj stringForKey:@"status"] integerValue];
    
    
    
    NSString *hostSSID = [camEntry stringForKey:@"router_ssid"];
    
    NSInteger storageMode = 0;
    
    /*
     NSDictionary *attr = [camEntry dictionaryForKey:@"atts"];
     if (attr[@"strmd"]) {
     storageMode = [attr[@"strmd"] integerValue];
     }
     */
    
    
    
    NSString *aMode = [camEntry stringForKey:@"status"];
    
    CamProfile *cp = [[CamProfile alloc] initWithMacAddr:camMac];
    cp.camProfileID = deviceID;
    cp.storageMode = storageMode;
    
    
    if ([fwTime isEqual:[NSNull null]]) {
        cp.fwTime = nil;
    }
    else {
        cp.fwTime = fwTime;
    }
    
    cp.name = camName;
    
    if( [isAvailable intValue] == 1 ) {
        cp.minuteSinceLastComm = 1;
    }
    else {
        cp.minuteSinceLastComm = 24*60;
    }
    
    if ( !localIp || [localIp isEqual:[NSNull null]] ) {
        DLog(@"garbage ip");
    }
    else if ( localIp.length == 0 || [localIp isEqualToString:@"null"] ) {
        DLog(@"garbage ip");
    }
    else {
        cp.ip_address = localIp;
    }
    
    cp.fw_version     = fwVersion;
    cp.registrationID = registrationID;
    cp.fwStatus = fwStatus;
    cp.timezone =[NSString stringWithFormat:@"%0.2f",[camEntry doubleForKey:@"time_zone"] ];
    cp.plan_id = [camEntry stringForKey:@"plan_id"];
    
    if ( ![hostSSID isEqual:[NSNull null]] ) {
        cp.hostSSID = hostSSID;
    }
    
    
    if (![aMode isEqual:[NSNull null]]) {
        cp.mode = aMode;
    }
    
    cp.trialIsUsed = [camEntry boolForKey:@"trial_used"];
    cp.freeTrialQuota = [camEntry integerForKey:@"trial_number"];
    
    DLog(@"Log - fwStatus:%ld, Fw:%@, local_ip:%@, reg:%@, Avail:%@, host_ssid:%@, mode:%@, streammode:%@, name:%@, id(%ld) - macAddress(%@) - storage(%ld) - parent(%ld) - freeTrialQuota(%ld)", (long)fwStatus, fwVersion, localIp, registrationID, isAvailable, hostSSID, aMode, cp.streamMode, cp.name, (long)cp.camProfileID, cp.mac_address, (long)cp.storageMode, (long)cp.parentID, (long)cp.freeTrialQuota);
    
    return cp;
}

- (CamChannel*)cameraChannelWithUDID:(NSString*)udid{
    for (CamChannel *ch in _channels) {
        if ([ch.profile.registrationID.uppercaseString isEqualToString:udid.uppercaseString]) {
            return ch;
        }
    }
    return nil;
}

- (void)checkCameraIsAvailable:(void (^)(BOOL success))returnFound withMac:(NSString *)macWithColon
{
    [[HUBCommCore sharedInstance] getAllDevices:^(NSDictionary *responseDict)
     {
         NSMutableArray *camProfiles = nil;
         BOOL found = NO;
         NSInteger status = [responseDict[@"status"] intValue];
         
         if ( status == 200 ) {
             NSArray *dataArr = responseDict[@"data"];
             if ( ![dataArr isEqual:[NSNull null]] && dataArr.count > 0 ) {
                 camProfiles = [self parseCameraList:dataArr];
                 for ( CamProfile *cp in camProfiles ) {
                     DLog(@"CameraProfile mac: %@, macWithcolon: %@, minSinceLastComm: %i", cp.mac_address, macWithColon, cp.minuteSinceLastComm);
                     
                     if ( cp.mac_address &&
                         [cp.mac_address isEqualToString:[macWithColon uppercaseString]] &&
                         cp.minuteSinceLastComm == 1 ) // is_available = 1
                     {
                         found = YES;
                         break;
                     }
                 }
             }
         }
         
         returnFound(found);
     } failure:^(NSError *error) {
         DLog(@"%s error: %@", __FUNCTION__, error.localizedDescription);
         returnFound(NO);
     }];
}

#pragma mark - P2P connections

- (void)closeP2PSessions
{
    self.wantToStop = YES;
    self.p2pSessionsNumbers = 0;
    
    NSArray* camChannels = [self cameraChannelList];
    for (CamChannel *ch in camChannels) {
        ch.profile.hasUpdateLocalStatus = NO;
        RMCHandler_2 *theTmp = (RMCHandler_2*) ch.profile.p2pHandler;
        
        if ([theTmp isKindOfClass:[RMCHandler_2 class]]) {
            //STOP p2p session
            [theTmp stop];
        }
    }
}

- (void)createP2PSessionForCam:(CamProfile *)cp
{
    NSArray* camChannels = [self cameraChannelList];
    for (CamChannel *ch in camChannels) {
        if ([ch.profile.registrationID isEqualToString:cp.registrationID]) {
            if ([cp isNotAvailable]) {
                DLog(@"%s This cam:%@ is NOT available!", __func__, cp.name);
                cp.isP2PAlready = YES; // Stop Activity
                
                //TODO: Reload camera
                //[_camerasVC reloadItemAtCam:cp.camProfileID];
                
            }
            else if (_p2pSessionsNumbers > 4) { // 4 is requirement
                DLog(@"%s 4 slots is reached! This cam is late.", __FUNCTION__);
                //de-comment if do not scan for online
                cp.isP2PAlready = YES; // Stop Activity
                
                //TODO: Reload camera
                //[_camerasVC reloadItemAtCam:cp.camProfileID];
            }
            else {
                _p2pSessionsNumbers++;
                
                if ( cp.isInLocal) {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                        struct timeval pTimeNow;
//                        gettimeofday(&pTimeNow, NULL);
                        
                        
                        ch.profile.timeStartP2P = [[NSDate date] timeIntervalSince1970]; //pTimeNow.tv_sec;
                        
                        ch.profile.p2pHandler = nil;
                        
                        //TODO: start local stream
                        [self startP2PStreamForCam:ch];
                    });
                }
                else
                {
                    [self performSelectorInBackground:@selector(runBg:) withObject:ch];
                }
            }
            
            break;
        }
    }
}

-(void) runBg:(CamChannel*) ch
{
    DLog(@">>BG for %@", ch.profile.mac_address);
//    struct timeval pTimeNow;
//    gettimeofday(&pTimeNow, NULL);

    
    ch.profile.timeStartP2P = [[NSDate date] timeIntervalSince1970];
    
    

        
    {
        [self startP2PStreamRemoteForCam:ch];
    }
    
}


//Create P2P Local Connection
- (void)startP2PStreamForCam:(CamChannel *)ch
{
    
    DLog(@"%s LOCAL LOCAL UDID: %@  >>>>>>>>>>>>>>>>>>>>>>> LOCAL", __FUNCTION__, ch.profile.registrationID);
    
    RMCHandler_2* theTmpRMC = [self createTheRMCHandlerForCam:ch];
    
    
    
    //NSString *mode = P2P_MODE_REMOTE_2;
    __weak CameraListManager *weakSelf = self;
    
    P2PCommResult (^blockCallback2)(PConfigInfo*, NSString*) = ^(PConfigInfo *config, NSString* udid) {
        P2PCommResult t2 = [weakSelf communicateWithCameraConfigInfo:config forChannel:ch mode:P2P_MODE_LOCAL_2];
        
        if (t2 == P2PCommSuccess) {
            //[weakSelf setStreamingStatus:@"Getting P2P Stream..."];
            DLog(@"%s %@ is success", __FUNCTION__, ch.profile.registrationID);
        }
        else {
            //[weakSelf setStreamingStatus:@"Fail to get Response from Cam"];
        }
        
        return t2;
    };
    
    theTmpRMC.outputCallback = blockCallback2;
    theTmpRMC.delegate = self;
    
    
    
    
    NSString *camUdid = ch.profile.registrationID;
    [theTmpRMC connectToCam:camUdid WithMode:P2P_MODE_LOCAL_2];
    
    
    
}

//Create P2P Remote Connection
- (void)startP2PStreamRemoteForCam:(CamChannel *)ch
{
    DLog(@"%s UDID: %@", __FUNCTION__, ch.profile.registrationID);
    
    RMCHandler_2* theTmpRMC = [self createTheRMCHandlerForCam:ch];
    
    //NSString *mode = P2P_MODE_REMOTE_2;
    __weak CameraListManager *weakSelf = self;
    
    P2PCommResult (^blockCallback2)(PConfigInfo*, NSString*) = ^(PConfigInfo *config, NSString* udid) {
        P2PCommResult t2 = [weakSelf communicateWithCameraConfigInfo:config forChannel:ch mode:P2P_MODE_REMOTE_2];
        return t2;
    };
    
    theTmpRMC.outputCallback = blockCallback2;
    theTmpRMC.delegate = self;
    
    
    
    
    
    NSString *camUdid = ch.profile.registrationID;
    [theTmpRMC connectToCam:camUdid WithMode:P2P_MODE_REMOTE_2];
}

- (RMCHandler_2 *)createTheRMCHandlerForCam:(CamChannel *)ch
{
    
    RMCHandler_2 *theTmpRMC = nil;
    
    //Phung: Create ONLY if the current RMCHandler_2 is nil /nul
    if (ch.profile.p2pHandler == nil)
    { //ONly first time
        
        theTmpRMC = [[RMCHandler_2 alloc] init];
        [theTmpRMC setDelegate:self];
        theTmpRMC.fwVersion = ch.profile.fw_version;
        theTmpRMC.camModel = ch.profile.model;
        
        ch.profile.p2pHandler = theTmpRMC;
        ch.profile.p2pSess = nil;
    }
    else {
        
        DLog(@"Cam profile p2pHanlder is available, reuse it >>>>>>>> ");
        theTmpRMC  = ch.profile.p2pHandler;
    }
    
    return theTmpRMC;
}

- (P2PCommResult)communicateWithCameraConfigInfo:(PConfigInfo *)configInfo forChannel:(CamChannel*)ch mode:(NSString *) aMode
{
    /**
     * key=[AES_DECRYPT_KEY]
     * &sip=[RELAY_SERVER_IP]
     * &sp=[RELAY_SERVER_PORT]
     * &rn=[RANDOM_NUMBER]
     **/
    
    NSString * device_udid = ch.profile.registrationID;
    
    NSString *aUUIDString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSString *uuidString = [aUUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    u_int32_t streamNum = arc4random_uniform(9);
    uuidString = [uuidString stringByReplacingCharactersInRange:NSMakeRange(uuidString.length - 2, 2) withString:[NSString stringWithFormat:@"_%d", streamNum]];
    //    NSString *streamName = uuidString;
    
    NSString *cmd = [NSString stringWithFormat:@"get_session_key&mode=%@&port1=%d&ip=%@&streamname=%@", configInfo.commMode, configInfo.port, configInfo.ip, uuidString];
    
    
    
    if (  [aMode isEqualToString:P2P_MODE_LOCAL_2] ==  YES)
    {
        
        HttpCommunication *dev_com = [[HttpCommunication alloc] initWithNewFlag:YES];
        dev_com.device_port = 9090;
        dev_com.device_ip = ch.profile.ip_address  ;
        
        
        
        NSString *reply = [dev_com sendCommandAndBlock:cmd];
        DLog(@"%s %@ in local, reply:%@", __FUNCTION__,device_udid, reply);
        
        if ( reply.length <= @"get_session_key: -1".length ) {
            return P2PCommFailure;
        }
        
        NSString *response = [[[reply substringFromIndex:@"get_session_key: ".length] componentsSeparatedByString:@","] objectAtIndex:1];
        response = [response stringByReplacingOccurrencesOfString:@"&&" withString:@"&"];
        NSMutableArray *token = [response componentsSeparatedByString:@"&"].mutableCopy;
        
        for ( int i = 0; i < token.count; i++ ) {
            NSString *value = token[i];
            NSRange range = [value rangeOfString:@"="];
            
            if ( range.location != NSNotFound ) {
                token[i] = [value substringWithRange:NSMakeRange(range.location + 1, value.length - (range.location + 1))];
            }
        }
        
        if ( !token || token.count < 2 ) {
            return P2PCommFailure;
        }
        
        if ( token.count < 3 ) {
            [token addObject:@"68656C6C6F"];
        }
        
        configInfo.port  = [token[0] intValue];
        configInfo.ip = token[1];
        [configInfo keyFromHexString:token[2]];
        
        
        
    }
    else
    {
        
        
        
        ///REMOTE CASE
        DeviceCommunication* dc = [[DeviceCommunication alloc] init];
        
        __block id sessionKeyData;
        [dc sendInstanceCommand:cmd
                       toDevice:device_udid
                        timeout:6
                     completion:^(BOOL success, id data, NSError *error) {
                         if (success && [data isKindOfClass:[NSDictionary class]]) {
                             sessionKeyData = [data arrayForKey:@"DATA"];
                         }
                         else{
                             DLog(@"%s failed, data: %@", __FUNCTION__, data);
                         }
                     }];
        
        
        
        
        NSDate *startTime = [NSDate date];
        
        while (!sessionKeyData && (ABS(startTime.timeIntervalSinceNow) < 30) && !_wantToStop) {
            [NSThread sleepForTimeInterval:0.5];
        }
        
        
        
        if (_wantToStop || !sessionKeyData || ![sessionKeyData isKindOfClass:[NSArray class]]) {
            DLog(@"%s %@ Cancel:%d OR MQTT fails ", __FUNCTION__, device_udid,_wantToStop);
            
            return P2PCommFailure;
        }
        
        DLog(@"%s %@ MQTT response:%@", __FUNCTION__, device_udid, sessionKeyData);
        
        NSArray *values = (NSArray *)sessionKeyData;
        NSInteger error;
        
        for (NSString *value in values) {
            if ([value hasPrefix:@"error="]) {
                error = [[value stringByReplacingOccurrencesOfString:@"error=" withString:@""] intValue];
            }
            if ([value hasPrefix:@"key="]) {
                NSString *key = [value stringByReplacingOccurrencesOfString:@"key=" withString:@""];
                [configInfo keyFromHexString:key];
            }
            if ([value hasPrefix:@"sip="]) {
                NSString  *sip = [value stringByReplacingOccurrencesOfString:@"sip=" withString:@""];
                configInfo.sip = sip;
            }
            if ([value hasPrefix:@"sp="]) {
                NSString *sp = [value stringByReplacingOccurrencesOfString:@"sp=" withString:@""];
                configInfo.sp = [sp intValue];
            }
            if ([value hasPrefix:@"rn="]) {
                NSString *rn = [value stringByReplacingOccurrencesOfString:@"rn=" withString:@""];
                [configInfo rnFromHexString:rn];
            }
            if ([value hasPrefix:@"port1="]) {
                NSString *port1 = [value stringByReplacingOccurrencesOfString:@"port1=" withString:@""];
                configInfo.port = [port1 intValue];
            }
            if ([value hasPrefix:@"ip="]) {
                NSString *ip = [value stringByReplacingOccurrencesOfString:@"ip=" withString:@""];
                configInfo.ip = ip;
            }
        }
        
        //3id: E076D0148F99&time: 1464606896&get_session_key: error=200,port1=46612&ip=118.68.40.25&key=716d487524654933245e256730277d31
        
        configInfo.streamName = uuidString;
        
        
    }
    return P2PCommSuccess;
}

#pragma mark - RmcHanlderDelegate_2

- (void) rmcHandler:(RMCHandler_2*)rmchandler didFailToConnectCam:(NSString *)aMac mode:(NSString *)aMode
{
    DLog(@"%s MAC: %@, mode: %@ numofSess: %ld ", __FUNCTION__, aMac, aMode, (long)_p2pSessionsNumbers);
    _p2pSessionsNumbers --;
    
    if (_p2pSessionsNumbers <0)
    {
        _p2pSessionsNumbers = 0;
    }
    
    //TODO: STOP clean up
}

- (void) rmcHandler:(RMCHandler_2*)rmchandler didConnectSuccessToDeviceID:(NSString *)udid
{
    DLog(@"%s UDID: %@", __FUNCTION__, udid);
    
    CamChannel* ch = [self cameraChannelWithUDID:udid];
    ch.profile.isP2PAlready = YES; //Stop indicator
    
    if (_delegate && [_delegate respondsToSelector:@selector(cameraListStatusUpdatedWithChannel:)]) {
        [_delegate cameraListStatusUpdatedWithChannel:ch];
    }
}

- (void) rmcHandler:(RMCHandler_2*)rmchandler timeoutWhileReceivingDataWithDeviceID:(NSString *)udid
{
    DLog(@"%s UDID: %@ - numofsession: %d", __FUNCTION__, udid, (int)_p2pSessionsNumbers);
    _p2pSessionsNumbers --;
    if (_p2pSessionsNumbers <0)
    {
        _p2pSessionsNumbers = 0;
    }
}

@end
