//
//  CameraListManager.h
//  CCam
//
//  Created by Tran Kien Nghi on 3/9/17.
//  Copyright © 2017 Cinatic Connected Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CamProfile;
@class CamChannel;

@protocol CameraListManagerDelegate <NSObject>

- (void)cameraListStatusUpdatedWithChannel:(CamChannel*)channel;

@end

@interface CameraListManager : NSObject

@property (nonatomic, weak) id<CameraListManagerDelegate> delegate;

+(instancetype)sharedInstance;

- (void)setDevices:(NSMutableArray*)devices;
- (void)addDevices:(NSArray*)devices;
- (void)removeDevices:(NSArray*)devices;
- (void)closeP2PSessions;

- (NSArray*)cameraList;
- (NSArray*)cameraChannelList;
- (NSArray*)doorBellList;
- (NSArray*)allDevices;

- (void)getCameraListWithCompletion:(void (^)(NSMutableArray* cameraChannels, NSError* error))completion;
- (void)checkCameraIsAvailable:(void (^)(BOOL success))returnFound withMac:(NSString *)macWithColon;

@end
