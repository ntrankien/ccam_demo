//
//  PlayerCallbackHandler.h
//  App
//
//  Created by Developer on 18/9/13.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

@protocol PlayerCallbackHandler <NSObject>

- (void)handleMessage:(int)msg ext1:(int)ext1 ext2:(int)ext2;

@end


