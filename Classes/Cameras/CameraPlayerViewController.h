//
//  CameraPlayerViewController.h
//  App
//
//  Created by Developer on 3/9/13.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <UIKit/UIKit.h>
#import <CameraScanner/CameraScanner.h>

typedef enum{
    PlayerDisplayingModePush,
    PlayerDisplayingModePresent,
}PlayerDisplayingMode;

@class RMCHandler_2;
@class CameraCommand;

@interface CameraPlayerViewController: UIViewController

@property (nonatomic, strong) CamChannel *selectedChannel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (nonatomic) BOOL isStreamInfoEnabled, hasLocalIp;
@property (nonatomic) NSString *sStreamMode;
@property (nonatomic) PlayerDisplayingMode displayMode;

- (void)stopStreamIsGoBack:(BOOL )isGoBack;
- (void)startStreamPlayback;
- (void)prepareGoBackToCameraList;

@end
