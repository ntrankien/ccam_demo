//
//  CameraCommandManager.h
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/14/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>
#import "CameraCommand.h"

@interface CameraCommandManager : NSObject

+ (instancetype)sharedInstance;
- (CameraCommand*)commandWithType:(DemoCommandType)cmdTypel;

@end
