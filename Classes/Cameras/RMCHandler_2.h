//
//  RMCHandler.h
//  LucyDemo
//
//  Created by Kronus on 9/30/16.
//  Copyright © 2016 LucyDemo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReliableChannel/ReliableChannel.h>

#define STUN_SERVER_URL "stun.cinatic.com"

#define STUN_SERVER_PORT 9080


#define P2P_MODE_LOCAL_2   @"local"
#define P2P_MODE_REMOTE_2  @"remote"
#define P2P_MODE_COMBINE_2 @"combine"
#define P2P_MODE_RELAY_2   @"relay"

#define FIRMWARE_VERSION_FOR_PREVIEW_MODE @"02.20.20"

typedef NS_ENUM(NSInteger, P2PMode) {
    P2PModeNone = 0,
    P2PModeLocal,
    P2PModeRemote,
    P2PModeRelay
};

typedef NS_ENUM(NSInteger, P2PStreamSourceType) {
    P2PStreamSourceCameraList = 0,
    P2PStreamSourcePushNotification
};

@class RMCHandler_2;

// Below ↘︎
@protocol RmcHanlderDataDelegate_2 <NSObject>

@required
//- (void)didRecveData:(void *)data withSize:(uint32_t )size type:(int )type timestamp:(uint32_t )ts andID:(NSInteger)aID; // -->
- (void)didRecveVideoData:(void *)data withSize:(uint32_t )size idr:(bool )isIdr timestamp:(uint32_t )ts; // -->
- (void)didRecveAudioData:(void *)data withSize:(uint32_t )size timestamp:(uint32_t )ts; // -->
- (void)didRecveOtherData:(void *)data withSize:(uint32_t )size type:(int )type timestamp:(uint32_t )ts; // -->

@end

@protocol RmcHanlderDelegate_2 <NSObject>

@required

- (void)rmcHandler:(RMCHandler_2*)p2pHanlder didConnectSuccessToDeviceID:(NSString*)udid;                       // -->
- (void)rmcHandler:(RMCHandler_2*)p2pHanlder timeoutWhileReceivingDataWithDeviceID:(NSString*)udid;        // -->
- (void)rmcHandler:(RMCHandler_2*)p2pHanlder didFailToConnectCam:(NSString *)aMac mode:(NSString *)aMode; // -->

@end

//typedef P2PCommResult (^blockCommCallback)(PConfigInfo *config);
typedef P2PCommResult (^p2pCommCallbackBlock)(PConfigInfo *config, NSString* deviceID);

@interface RMCHandler_2 : NSObject

@property (nonatomic)       BOOL        wantToCancel; // Set from outside <--

@property (nonatomic) P2PMode       p2pMode;

@property (nonatomic) P2PStreamSourceType p2pSourceType;

@property (nonatomic, copy) NSString* camModel;
@property (nonatomic, copy) NSString* fwVersion;

@property (nonatomic, copy) p2pCommCallbackBlock outputCallback;

@property (nonatomic, weak) id<RmcHanlderDelegate_2> delegate; // -->
@property (nonatomic, weak) id<RmcHanlderDataDelegate_2> delegate2; // (for updating data only) -->

//+ (instancetype)sharedInstance;

- (void)switchCamToP2pSesMode:(int)mode;
- (void)connectToCam:(NSString *) camUdid WithMode:(NSString *)aMode;
- (void)connectToCam:(NSString *) camUdid notificationP2PInfo:(NSDictionary*)p2pInfo;

- (NSString *)sendCommand:(NSString *)cmd andFlag:(BOOL)isReq;
- (void)sendAudioData:(uint8_t *)outBuf length:(int)outSize ts:(int)ts;

- (void)stop;
- (void)closeSession;

@end
