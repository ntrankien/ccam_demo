//
//  CameraPlayerViewController.m
//  App
//
//  Created by Company on 3/9/13.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraPlayerViewController.h"
#import <H264MediaPlayer/H264MediaPlayer.h>
#import "HttpCom.h"
#import "MBProgressHUD.h"
#import "H264PlayerListener.h"
#import "CameraCommandManager.h"
//#import <HUBComm/HUBComm.h>
#import "RMCHandler_2.h"
#import <MDeviceComm/MDeviceComm.h>
#import "P2PConfigInfo.h"
#import "Reachability.h"
//#import "TalkbackManager.h"
#import "AppDelegate.h"

#define P2P_MODE_LOCAL @"local"
#define P2P_MODE_REMOTE @"remote"
#define P2P_MODE_RELAY @"relay"

typedef enum : NSUInteger {
    MediaPlayerIsNotInit,
    MediaPlayerIsSettingListener,
    MediaPlayerIsSettingDatasource,
    MediaPlayerIsStarted
} MediaPlayerStatus;

#define LogMediaStatus(v) (v==0?@"MediaPlayerIsNotInit":(v==1?@"MediaPlayerIsSettingListener":(v==2?@"MediaPlayerIsSettingDatasource":@"MediaPlayerIsStarted")))

@interface CameraPlayerViewController () <UIScrollViewDelegate, PlayerCallbackHandler, RmcHanlderDelegate_2, RmcHanlderDataDelegate_2, UITextFieldDelegate, DeviceCommunicationDelegate>

@property (weak, nonatomic) IBOutlet UILabel *firmwareVersionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *customIndicator;
@property (nonatomic, weak) IBOutlet UILabel *ib_lbStreamingStatus;
@property (nonatomic, weak) IBOutlet UILabel *ib_lbCameraName;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewVideo;
@property (nonatomic, weak) IBOutlet UIView *viewDebugInfo;
@property (nonatomic, weak) IBOutlet UIView *eventViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblRecordingTime;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeGestureExitStreaming;

//New design
@property (weak, nonatomic) IBOutlet UIButton *videoScreenSizeButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoScreenHeightConstraint;
@property (nonatomic) CGFloat videoScreenMinHeight;

@property (nonatomic) BOOL isLandScapeMode; // cheat to display correctly timeline bottom
@property (nonatomic, copy) NSString *messageStreamingState;
@property (nonatomic, strong) NSTimer *timerRestartMainProcess;
@property (nonatomic, strong) NSTimer *timerSaveSnapshot;
@property (nonatomic) BOOL isMoniterInBackground;
@property (nonatomic) NSInteger currentMediaStatus;
@property (nonatomic, strong) UIImageView *imageViewStreamer;
@property (nonatomic, copy) NSString *strDocDirPath;
@property (nonatomic) MediaPlayerStatus mediaProcessStatus;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@property (nonatomic, strong) NSTimer *timerBufferingTimeout;
@property (nonatomic) BOOL didStopStream;
@property (nonatomic, strong) NSDate *timeStartingStageTwo;
@property (nonatomic, strong) NSDate *timeStartPlayerView;
@property (nonatomic, copy) NSString *current_ssid;
@property (nonatomic) BOOL isFirstLoadStreaming;
@property (nonatomic) BOOL userWantToCancel;
@property (nonatomic) BOOL isExploitHaEnabled;
@property (nonatomic) status_t p2pMediaInitStatus;
@property (nonatomic) BOOL isP2PMode;
@property (nonatomic) BOOL wantToStop;
@property (nonatomic) BOOL isAVSyncEnabled;
@property (nonatomic) BOOL isRtspWithTcpEnabled;

@property (nonatomic, assign) H264PlayerListener *h264StreamerListener;
@property (nonatomic) BOOL h264StreamerIsInStopped;
@property (nonatomic, strong) id sessionKeyData;

@property (nonatomic) NSInteger timeoutWhileStreaming;
@property (nonatomic) BOOL isPlayAllFrame;
@property (nonatomic) BOOL isInAppRecording;

@property (nonatomic) UIBackgroundTaskIdentifier backgroundUpdateTask;

//Statistic start
@property (nonatomic) NSTimeInterval start_timestamp;
@property (nonatomic) double avg_audio_fps;
@property (nonatomic) double avg_video_fps;
@property (nonatomic) int  video_framenum;
@property (nonatomic) int  audio_framenum;
//Statistic end

- (IBAction)videoViewTapped:(id)sender;
- (IBAction)inAppSnapshotTapped:(id)sender;
- (IBAction)videoScreenSizeButtonTapped:(id)sender;

@end

@implementation CameraPlayerViewController

#define USING_FOR_DEMO YES

#define MAXIMUM_ZOOMING_SCALE   5.0
#define MINIMUM_ZOOMING_SCALE   1.0f
#define ZOOM_SCALE              1.5f

#define TAG_ALERT_VIEW_REMOTE_TIME_OUT 559

#define _is_Loggedin @"bool_isLoggedIn"
#define SESSION_KEY @"SESSION_KEY"
#define STREAM_ID   @"STREAM_ID"

#define TF_DEBUG_FRAME_RATE_TAG             5001//
#define TF_DEBUG_RESOLUTION_TAG             5002//
#define TF_DEBUG_BIT_RATE_TAG               5003
#define TF_DEBUG_WIFI_SIGNAL_STRENGTH_TAG   5004
#define TF_DEBUG_WIFI_SIGNAL_STRENGTH_ICON_TAG   6000
#define TF_DEBUG_BATTERY_LEVEL   5005//
#define TF_DEBUG_SPEED_VALUE     5006
#define TF_DEBUG_MODE_STREAM     5007//


#define TIMEOUT_BUFFERING           15// 15 seconds
#define TIMEOUT_REMOTE_STREAMING    5*60 // 5 minutes
#define TIMEOUT_RTMP_202            2 // 2 minutes

// Port from V2
#define TAG_ALERT_FW_OTA_UPGRADE_AVAILABLE  579
#define TAG_ALERT_FW_OTA_UPGRADE_DONE       590
#define TAG_ALERT_FW_OTA_UPGRADE_FAILED     589

#pragma mark - UIViewController methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    MediaPlayer::Instance(); // try to init media asap
    MediaPlayer::Instance()->setTimeoutWhileReadFrame((int)_timeoutWhileStreaming);
    
//    [self initializeUI];
    //Left bar button
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_arrow_left"] style:UIBarButtonItemStylePlain target:self action:@selector(prepareGoBackToCameraList)];
    self.navigationItem.leftBarButtonItem = backButton;

    //Initialize variables for player
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    self.isExploitHaEnabled = [userDefaults boolForKey:kIsEnableExploitHA];
    self.isAVSyncEnabled = [userDefaults boolForKey:kIsAVSyncEnabled];
    self.isRtspWithTcpEnabled = [userDefaults boolForKey:kIsRtspWithTcpEnabled];
    self.timeoutWhileStreaming = [userDefaults integerForKey:kTimeoutWhileStreaming];
    self.isPlayAllFrame = [userDefaults boolForKey:kIsPlayAllFrame];
    
    self.imageViewStreamer = [[UIImageView alloc] initWithFrame:_imageViewVideo.frame];
    self.strDocDirPath = PathForDir;
    self.start_timestamp = [[NSDate date] timeIntervalSince1970];
    [self setupHttpPort];
    self.isStreamInfoEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"streaming_infos"];
    self.isFirstLoadStreaming = NO;//YES;
    
    //Start streaming
    [self setupStream:_isFirstLoadStreaming];
    
    //Setup menu items
    [self setupLiveStreamMenu];
}

- (void)initializeUI {
    //Config navigation bar
    self.navigationItem.title = _selectedChannel.profile.name;
    //    [self.navigationController.navigationBar setTitleTextAttributes:
    //     [NSDictionary dictionaryWithObjectsAndKeys:
    //      [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1.0],
    //      NSForegroundColorAttributeName,
    //      nil]];
    
    
    //Don't automatically turn off display while streaming
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    [_imageViewVideo setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_imageViewStreamer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    CGFloat videoHeight = SCREEN_WIDTH * 9 / 16;
    _videoScreenHeightConstraint.constant = videoHeight;
    self.videoScreenMinHeight = videoHeight;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self startStreamPlayback];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self checkOrientation];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self initializeUI];
    
    NSString *imgName = [NSString stringWithFormat:@"%@_lastest_snapshot.png", _selectedChannel.profile.registrationID];
    UIImage *aImg = [UIImage imageWithContentsOfFile:[_strDocDirPath stringByAppendingPathComponent:imgName]];
    
    if (aImg) {
        [_imageViewStreamer setImage:aImg];
    }
    
    [_viewDebugInfo setHidden:!_isStreamInfoEnabled];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

#pragma mark - Memory Management methods

- (void)dealloc
{
    DLog(@"%s", __FUNCTION__);
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    // Need to undo capture of the swipe to go back gesture.
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
}

#pragma mark - Public methods

- (void)setSelectedChannel:(CamChannel *)selectedChannel
{
    _selectedChannel = selectedChannel;
    CamProfile *cp = _selectedChannel.profile;
    self.title = cp.name;
    
    // Ensure scrollview zoom is reset
    [self centerScrollViewContents];
    [self resetZooming];
}

#pragma mark - Private methods

- (BOOL)isVisible {
    return [self isViewLoaded] && self.view.window;
}

- (void)setupStream:(BOOL)isFirstLoad
{
    // p2p flow
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        self.isP2PMode = YES;
        NSString *camUdid = [self heckCamUdid];
        
        NSLog(@"camUdid:%@", camUdid);
        
        if ([camUdid isKindOfClass:[NSString class]] && camUdid.length) {
            _selectedChannel.profile.registrationID       =  camUdid; // @"010001E076D0148F99ADEQCOBU";
            _selectedChannel.profile.mac_address          =  [self macFromUdid:camUdid]; // @"E076D0148F99";
        }
        
        while (!_selectedChannel.profile.p2pHandler) {
            NSLog(@"CameraPlayerViewController: P2P handler is nil. Wait for MenuViewController to create it");
            [NSThread sleepForTimeInterval:1];
        }
        
        if (_selectedChannel.profile.p2pHandler){
            //Nghi: Switch stream mode to FULL MODE
            [(RMCHandler_2*)_selectedChannel.profile.p2pHandler switchCamToP2pSesMode:3];
            [self rmcHandler:_selectedChannel.profile.p2pHandler didConnectSuccessToDeviceID:camUdid];
        }
    });
}

- (NSString *)heckCamUdid
{
    NSString *str = [kUserDefault stringForKey:kCamUdid];
    if (str.length == 24) {
        char *t = (char *)str.UTF8String;
        char t2[32];
        memset(t2, 0, 32);
        memcpy(t2, t, 24);
        str = [NSString stringWithCString:t2 encoding:NSUTF8StringEncoding];
    }
    
    if (!str.length) {
        str = _selectedChannel.profile.registrationID;
    }
    
    return str;
}

- (NSString *)macFromUdid:(NSString *)udid
{
    if (udid.length == 26 || udid.length == 24) {
        return [udid substringWithRange:NSMakeRange(6, 12)];
    }
    
    return @"";
}

- (void)reachForInternetWithTimeout:(NSTimeInterval)timeout completion:(void (^)(BOOL internetConnected))completion{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        Reachability* internetReachability = [Reachability reachabilityForInternetConnection];
        [internetReachability startNotifier];
        
        for (int i = 0; i < timeout; ++i) {
            NetworkStatus netStatus = [internetReachability currentReachabilityStatus];
            switch (netStatus)
            {
                case NotReachable:{
                    //Continue to reach
                }
                    break;
                case ReachableViaWWAN:
                case ReachableViaWiFi:{
                    [internetReachability stopNotifier];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(YES);
                    });
                    return;
                }
                    break;
                default:
                    break;
            }
            
            [NSThread sleepForTimeInterval:1];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(NO);
        });
        [internetReachability stopNotifier];
    });
}

- (void)setupHttpPort
{
    [HttpCom instance].comWithDevice.device_ip = _selectedChannel.profile.ip_address;
    [HttpCom instance].comWithDevice.device_port = (int)_selectedChannel.profile.port;
    
    // init the ptt port to default
    _selectedChannel.profile.ptt_port = IRABOT_AUDIO_RECORDING_PORT;
}

- (BOOL)canStreamInLocal{
    NSString *deviceSSID = [CameraPassword fetchSSIDInfo];
    if ([deviceSSID hasPrefix:@"Lucy"] ||
        _hasLocalIp == YES ) {
        return YES;
    }
    else{
        return NO;
    }
}

- (void)setStreamingStatus:(NSString*)streamimgStatus{
    
    //Hot fix, need a better solution to do not show streaming status on first load
    if(_isFirstLoadStreaming && _isP2PMode){
        streamimgStatus = nil;
    }
    self.messageStreamingState = streamimgStatus;
    __weak CameraPlayerViewController* weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.ib_lbStreamingStatus.text = streamimgStatus;
        if (streamimgStatus.length) {
            weakSelf.ib_lbStreamingStatus.hidden= NO;
            [weakSelf displayCustomIndicator:YES];
        }
        else{
            
            weakSelf.ib_lbStreamingStatus.hidden= YES;
            [weakSelf displayCustomIndicator:NO];
        }
    });
}

- (void)setStreamingStatus:(NSString*)streamimgStatus afterDelay:(NSTimeInterval)delay skipIfCurrentStatusChange:(BOOL)skip{
    
    //Hot fix, need a better solution to do not show streaming status on first load
    if(_isFirstLoadStreaming && _isP2PMode){
        streamimgStatus = nil;
        _ib_lbStreamingStatus.text = nil;
        skip = NO;
    }
    
    NSString* oldStatus = _ib_lbStreamingStatus.text;
    __weak CameraPlayerViewController* weakSelf = self;
    dispatch_time_t popTimes = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
    dispatch_after(popTimes, dispatch_get_main_queue(), ^{
        if (skip) {
            NSString* currentStatus = weakSelf.messageStreamingState;
            if([oldStatus isEqualToString:@""] || currentStatus == oldStatus || [currentStatus isEqualToString:oldStatus]){
                weakSelf.messageStreamingState = streamimgStatus;
                weakSelf.ib_lbStreamingStatus.text = streamimgStatus;
                if (streamimgStatus.length) {
                    [weakSelf displayCustomIndicator:YES];
                }
                else{
                    [weakSelf displayCustomIndicator:NO];
                }
            }
            else{
                //Do nothing, the status has been changed by another process
            }
        }
        else{
            weakSelf.messageStreamingState = streamimgStatus;
            weakSelf.ib_lbStreamingStatus.text = streamimgStatus;
            if (streamimgStatus.length) {
                [weakSelf displayCustomIndicator:YES];
            }
            else{
                [weakSelf displayCustomIndicator:NO];
            }
        }
    });
}

- (void)updateDebugInfoFrameRate:(NSInteger)fps
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UITextField *tfFrameRate = (UITextField *)[_viewDebugInfo viewWithTag:TF_DEBUG_FRAME_RATE_TAG];
        tfFrameRate.text = [NSString stringWithFormat:@"%ld", (long)fps];
    
        UITextField *tfMS = (UITextField *)[_viewDebugInfo viewWithTag:TF_DEBUG_MODE_STREAM];
        tfMS.text = _sStreamMode;
    });
}

- (void)updateDebugInfoResolutionWidth:(NSInteger)width heigth:(NSInteger)height
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UITextField *tfResolution = (UITextField *)[_viewDebugInfo viewWithTag:TF_DEBUG_RESOLUTION_TAG];
        //tfResolution.text = [NSString stringWithFormat:@"%ldx%ld", (long)width, (long)height];
        
        //Old definition
        /* Types for heights
         3: 640x360
         2: 864x486
         1: 1280x720
         */
        //NSArray* resolutionTypes = @[@720, @486, @360];
        //NSString* resolutionType = [NSString stringWithFormat:@"%u", (u_int)[resolutionTypes indexOfObject:@(height)] + 1];
        //tfResolution.text = resolutionType;
        
        //New definition
        CameraCommand* cmd = [[CameraCommandManager sharedInstance] commandWithType:DemoCommandTypeResolution];
        int resolutionIndex = 0;
        for (; resolutionIndex < cmd.valueList.count; ++resolutionIndex) {
            NSString* value = cmd.valueList[resolutionIndex];
            if ([value hasPrefix:[NSString stringWithFormat:@"%ld", (long)height]]) {
                break;
            }
        }
        if (resolutionIndex < cmd.valueList.count) {
            NSString* resolutionType = [NSString stringWithFormat:@"%u", (u_int)resolutionIndex + 1];
            tfResolution.text = resolutionType;
        }
        else{
            tfResolution.text = nil;
        }
        
        DLog(@"Resolution: %ldx%ld", (long)width, (long)height);
        if ( _isStreamInfoEnabled ) {
            if ( _viewDebugInfo.hidden ) {
                _viewDebugInfo.hidden = NO;
            }
        }
    });
}

- (void)updateDebugInfoResolutionIndex:(int)index
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UITextField *tfResolution = (UITextField *)[_viewDebugInfo viewWithTag:TF_DEBUG_RESOLUTION_TAG];
        tfResolution.text = [NSString stringWithFormat:@"%d", index];
    });
}

- (void)updateDebugInfoBitRate:(NSInteger)bitRate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UITextField *tfBitRate = (UITextField *)[_viewDebugInfo viewWithTag:TF_DEBUG_BIT_RATE_TAG];
        tfBitRate.text = [NSString stringWithFormat:@"%.02f", (bitRate * 8) / (2 * 1000.0f)];
    });
}

- (void)updateDebugInfoBattery:(NSString*)batteryString{
    dispatch_async(dispatch_get_main_queue(), ^{
        UITextField *tfBitRate = (UITextField *)[_viewDebugInfo viewWithTag:TF_DEBUG_BATTERY_LEVEL];
        tfBitRate.text = batteryString;
    });
}

- (void)startStreamPlayback
{
    // Calling remove before adding to be sure as startStreamPlayback calls may be duplicated
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(h264_HandleWillEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(h264_HandleEnteredBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];

    // Alway show custom indicator, when view appears
    //self.isShowCustomIndicator = YES;
}


- (void)h264_HandleEnteredBackground
{
    //Simply close this view to go back to camera list VIEW, for now
    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
//        self.backgroundUpdateTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
//            //TODO:[self closeP2PSessions]; PUT IT HERE TO ABUSE THE IOS Background time !@!
//        }];
        DLog(@"Going Back to Camera list ");
        
        self.isFirstLoadStreaming = NO;//YES;
        self.wantToStop = YES;
        _selectedChannel.stopStreaming = YES;
        
        [self invalidteTimerMainProcess];
        [self invalidateSaveSnapshotTimer];
        
        if ( _isP2PMode ) {
            
            
            _customIndicator.hidden = NO;
            
            if (_timerBufferingTimeout) {
                [_timerBufferingTimeout invalidate];
                self.timerBufferingTimeout = nil;
            }
            
            self.p2pMediaInitStatus = -1;
            MediaPlayer::Instance()->setListener(NULL);
            MediaPlayer::Instance()->stopP2PStream(YES);
            
            if (_h264StreamerListener) {
                delete _h264StreamerListener;
                self.h264StreamerListener = NULL;
            }
            
            self.mediaProcessStatus = MediaPlayerIsNotInit;
            
            
            //Close p2p stream
            [self closeP2PStreamGoBack:@(YES)];
            
            
        }
        
        self.h264StreamerIsInStopped = YES;
        
        self.currentMediaStatus = 0;
        
        
        
//        [[UIApplication sharedApplication] endBackgroundTask: self.backgroundUpdateTask];
//        self.backgroundUpdateTask = UIBackgroundTaskInvalid;
    //});
    
    
    
#if 0
    self.isFirstLoadStreaming = NO;//YES;
    self.wantToStop = YES;
    _selectedChannel.stopStreaming = YES;
    
    [self invalidteTimerMainProcess];
    [self invalidateSaveSnapshotTimer];
    
    if ( _isP2PMode ) {

        
        _customIndicator.hidden = NO;
        
        if (_timerBufferingTimeout) {
            [_timerBufferingTimeout invalidate];
            self.timerBufferingTimeout = nil;
        }
        
        self.p2pMediaInitStatus = -1;
        MediaPlayer::Instance()->setListener(NULL);
        MediaPlayer::Instance()->stopP2PStream(YES);
        
        if (_h264StreamerListener) {
            delete _h264StreamerListener;
            self.h264StreamerListener = NULL;
        }
        
        self.mediaProcessStatus = MediaPlayerIsNotInit;
        
    
        //All P2P sessions will be terminated by Menu VC -
        
    }
    else {
        [self stopMediaProcessGoBack:NO backgroundMode:YES];
    }

    self.h264StreamerIsInStopped = YES;
    
    self.currentMediaStatus = 0;
    
#endif 
    
    DLog(@"%s", __FUNCTION__);
}

- (void)h264_HandleWillEnterForeground
{
    DLog(@"%s Do-nothing ", __FUNCTION__);
   
}

- (void)invalidteTimerMainProcess
{
    if ( _timerRestartMainProcess ) {
        [_timerRestartMainProcess invalidate];
        self.timerRestartMainProcess = nil;
    }
}

- (void)invalidateSaveSnapshotTimer
{
    if (_timerSaveSnapshot) {
        [_timerSaveSnapshot invalidate];
        self.timerSaveSnapshot = nil;
    }
}

- (void)storeSnapshotFromStream:(NSTimer *)timer
{
    NSString *imgName = [NSString stringWithFormat:@"%@_lastest_snapshot.png", _selectedChannel.profile.registrationID];
    NSString *snapshotFile = [_strDocDirPath stringByAppendingPathComponent:imgName];
    UIImage *aImage = _imageViewStreamer.image;
    
    if (aImage) {
        [UIImagePNGRepresentation(aImage) writeToFile:snapshotFile atomically:YES];
    }
}

- (BOOL)stopMediaProcessGoBack:(BOOL)isGoBack backgroundMode:(BOOL)isBgMode
{
    DLog(@"%s isGoBack:%d, isBgMode:%d", __FUNCTION__, isGoBack, isBgMode);
    BOOL stopStreamImmediately = NO;
    
    if ( _mediaProcessStatus == MediaPlayerIsNotInit ) {
        stopStreamImmediately = YES;
    }
    else if ( _mediaProcessStatus == MediaPlayerIsSettingListener ) {
        MediaPlayer::Instance()->sendInterrupt();
        [self stopStreamIsGoBack:isGoBack];
    }
    else if ( _mediaProcessStatus == MediaPlayerIsSettingDatasource ) {
        DLog(@"%s Waiting for response from Media lib.", __FUNCTION__);
        MediaPlayer::Instance()->sendInterrupt();
        
        if ( isBgMode ) {
            self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
                DLog(@"Background handler called. Not running background tasks anymore.");
                [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
                self.backgroundTask = UIBackgroundTaskInvalid;
            }];
            
            DLog(@"%s Waiting for call back from MediaPlayer lib. backgroundTask:%lu", __FUNCTION__, (unsigned long)_backgroundTask);
        }
        
        if ( isGoBack ) {
            MediaPlayer::Instance()->setShouldWait(FALSE);
            isGoBack = NO;
        }

        stopStreamImmediately = YES;
    }
    else { // MEDIAPLAYER_STARTED
        [self stopStreamIsGoBack:isGoBack];
        stopStreamImmediately = YES;
    }
    
    if ( isGoBack ) {
        MediaPlayer::Instance()->setShouldWait(FALSE);
        [self goBack];
    }
    
    return stopStreamImmediately;
}

- (void)stopStreamIsGoBack:(BOOL )isGoBack
{
    BOOL isMainThread = [NSThread isMainThread];
    DLog(@"%s MainThread: %d", __FUNCTION__, isMainThread);
    
    @synchronized(self) {
        if ( _timerBufferingTimeout ) {
            [_timerBufferingTimeout invalidate];
            self.timerBufferingTimeout = nil;
        }
        
        self.mediaProcessStatus = MediaPlayerIsNotInit;
        MediaPlayer::Instance()->setListener(NULL);
        
        if (_h264StreamerListener) {
            delete _h264StreamerListener;
            self.h264StreamerListener = nil;
        }
        
        MediaPlayer::Instance()->suspend(isGoBack);
        
//        if (_isMoniterInBackground && [UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
//            MediaPlayer::Instance()->stop2();
//        }
//        else {
//            MediaPlayer::Instance()->stop();
//        }
        
        MediaPlayer::Instance()->stop();
        
        if ( _backgroundTask != UIBackgroundTaskInvalid ) {
            [[UIApplication sharedApplication] endBackgroundTask:_backgroundTask];
            self.backgroundTask = UIBackgroundTaskInvalid;
        }
        
        self.didStopStream = true;
    }
}

- (void)prepareGoBackToCameraList
{
    DLog(@"CampPlayerVC- %s - self.currentMediaStatus: %ld", __FUNCTION__, (long)self.currentMediaStatus);

    self.userWantToCancel = YES;
    
    // Check to see if Timeline over rode the back button (with a "Delete selected")
    if ([NSThread currentThread].isMainThread) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
    }
    self.view.userInteractionEnabled = NO;
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    _selectedChannel.stopStreaming = YES;
    

    
    [self invalidteTimerMainProcess];
    [self invalidateSaveSnapshotTimer];
    
    if ( _isP2PMode ) {
        [self closeP2PStreamGoBack:@(YES)];
    }
    else {
        [self stopMediaProcessGoBack:YES backgroundMode:NO];
    }
}

- (BOOL)isCurrentConnection3G
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    if ( [reachability currentReachabilityStatus] == ReachableViaWWAN ) {
        return YES;
    }
    
    return NO;
}

- (void)setupLiveStreamMenu{
//    self.liveStreamMenuView = [[LiveStreamMenuView alloc] initWithCamChannel:_selectedChannel];
//    _liveStreamMenuView.delegate = self;
//    
//    //__block NSString* ip_address = self.selectedChannel.profile.ip_address;
//    __weak CameraPlayerViewController* weakSelf = self;
//    _liveStreamMenuView.actionBlock = ^(LiveStreamMenu menuType, CamChannel* camChannel){
//        switch (menuType) {
//            case LiveStreamMenuSettings:{
//                //TODO: Show camera settings
//            }
//                break;
//            case LiveStreamMenuTakeSnapshot:{
//                [weakSelf saveCurrentStreamingFrame];
//            }
//                break;
//            case LiveStreamMenuRecordVideo:{
//                //TODO: Record video
//            }
//                break;
//            case LiveStreamMenuTalkback:{
//                
//            }
//                break;
//            case LiveStreamMenuCloud:
//            case LiveStreamMenuSDCard:{
//                HUBCommCore *mgr = [HUBCommCore sharedInstance];
//                NSString *registrationID = weakSelf.selectedChannel.profile.registrationID;
//                
//                [mgr getWebEventAccessWithRegistrationId:registrationID success:^(NSDictionary *responseDict) {
//                    if ( [responseDict integerForKey:@"status"] == 200 ) {
//                        NSDictionary* dataDict = [responseDict dictionaryForKey:@"data"];
//                        NSString* sdCardURL = [dataDict stringForKey:@"local_access"];
//                        NSString* googleDriveURL = [dataDict stringForKey:@"gdrive_access"];
//                        
//                        if (menuType == LiveStreamMenuSDCard) {
//                            EventWebViewController *webViewController;
//                            if (weakSelf.selectedChannel.profile.isInLocal) {
//                                DLog(@"Load SD Card event url: %@", sdCardURL);
//                                webViewController = [EventWebViewController webViewURlString:sdCardURL];
//                                webViewController.navigationItem.title = LocStr(@"Events");
//                                [weakSelf.navigationController pushViewController:webViewController animated:YES];
//                                
//                                //pause streaming
//                                [(RMCHandler_2*)weakSelf.selectedChannel.profile.p2pHandler setDelegate2:nil];
//                            }
//                            else{
//                                UIAlertController * alert=   [UIAlertController
//                                                              alertControllerWithTitle:nil
//                                                              message:LocStr(@"App and camera must be in the same network to view events on SD Card")
//                                                              preferredStyle:UIAlertControllerStyleAlert];
//                                UIAlertAction* ok = [UIAlertAction
//                                                     actionWithTitle:@"OK"
//                                                     style:UIAlertActionStyleDefault
//                                                     handler:^(UIAlertAction * action) {
//                                                         
//                                                     }];
//                                [alert addAction:ok];
//                                [weakSelf presentViewController:alert animated:YES completion:nil];
//                            }
//                        }
//                        else if(menuType == LiveStreamMenuCloud){
//                            NSString *apiKey = [[HUBCommCore sharedInstance].dataSource apiKeyForHUBCommCore];;
//                            NSString *username = [PrivateDataUtils username];
//                            EventWebViewController *webViewController = [EventWebViewController webViewURlString:[NSString stringWithFormat:@"%@/#/login/%@/%@/%@", googleDriveURL, username, apiKey, weakSelf.selectedChannel.profile.name]];
//                            webViewController.navigationItem.title = LocStr(@"Google drive Events");
//                            [weakSelf.navigationController pushViewController:webViewController animated:YES];
//                            
//                            webViewController.delegate = weakSelf;
//                            
//                            //pause streaming
//                            [(RMCHandler_2*)weakSelf.selectedChannel.profile.p2pHandler setDelegate2:nil];
//                        }
//                    }
//                    else {
//                        DLog(@"Get Web Event Access failed: %@", [responseDict stringForKey:@"status"]);
//                    }
//                } failure:^(NSError *error) {
//                    DLog(@"Get Web Event Access failed with error: %@", error.description);
//                }];
//            }
//                break;
//            default:
//                break;
//        }
//    };
//    [_liveStreamMenuView reload];
}

#pragma mark - Zoom in & out

- (void)centerScrollViewContents
{
    CGSize boundsSize = self.scrollView.bounds.size;
    CGRect contentsFrame = _imageViewStreamer.frame;
    
    if ( contentsFrame.size.width < boundsSize.width ) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    }
    else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if ( contentsFrame.size.height < boundsSize.height ) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    }
    else {
        contentsFrame.origin.y = 0.0f;
    }
    
    _imageViewStreamer.frame = contentsFrame;
}

- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer
{
    DLog(@"double tap scrollViewDoubleTapped");
    
    // Get the location within the image view where we tapped
    CGPoint pointInView = [recognizer locationInView:_imageViewStreamer];
    
    // Get a zoom scale that's zoomed in slightly, capped at the maximum zoom scale specified by the scroll view
    CGFloat newZoomScale = _scrollView.zoomScale * ZOOM_SCALE;
    newZoomScale = MIN(newZoomScale, _scrollView.maximumZoomScale);
    
    // Figure out the rect we want to zoom to, then zoom to it
    CGSize scrollViewSize = self.scrollView.bounds.size;
    
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    
    [self.scrollView zoomToRect:rectToZoomTo animated:YES];
}

- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer
{
    DLog(@"Two finger tap scrollViewTwoFingerTapped");
    
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = _scrollView.zoomScale / ZOOM_SCALE;
    newZoomScale = MAX(newZoomScale, _scrollView.minimumZoomScale);
    [_scrollView setZoomScale:newZoomScale animated:YES];
}

- (void)resetZooming
{
    CGFloat newZoomScale = MINIMUM_ZOOMING_SCALE;
    [_scrollView setZoomScale:newZoomScale animated:YES];
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _imageViewStreamer; // Return the view that we want to zoom
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    [self centerScrollViewContents]; // The scroll view has zoomed, so we need to re-center the contents
}

- (IBAction)zoomValueChanged:(id)sender {
//    float newValue = self.zoomSlider.value;
//    CGFloat newZoomScale = (MAXIMUM_ZOOMING_SCALE - MINIMUM_ZOOMING_SCALE) * newValue;
//    newZoomScale = MIN(newZoomScale, _scrollView.maximumZoomScale);
//    [_scrollView setZoomScale:newZoomScale animated:YES];
}

- (void)addGesturesPichInAndOut
{
    //Add contraint to _imageViewStreamer
    [_imageViewVideo setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_imageViewStreamer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_scrollView insertSubview:_imageViewStreamer aboveSubview:_imageViewVideo];
    
    [self.scrollView addConstraint:[NSLayoutConstraint
                              constraintWithItem:self.imageViewStreamer
                              attribute:NSLayoutAttributeWidth
                              relatedBy:NSLayoutRelationEqual
                              toItem:self.imageViewVideo
                              attribute:NSLayoutAttributeWidth
                              multiplier:1
                              constant:0.0]];
    
    [self.scrollView addConstraint:[NSLayoutConstraint
                              constraintWithItem:self.imageViewStreamer
                              attribute:NSLayoutAttributeHeight
                              relatedBy:NSLayoutRelationEqual
                              toItem:self.imageViewVideo
                              attribute:NSLayoutAttributeHeight
                              multiplier:1
                              constant:0.0]];
    
    [self.scrollView addConstraint:[NSLayoutConstraint
                              constraintWithItem:self.imageViewStreamer
                              attribute:NSLayoutAttributeCenterX
                              relatedBy:NSLayoutRelationEqual
                              toItem:self.imageViewVideo
                              attribute:NSLayoutAttributeCenterX
                              multiplier:1.0
                              constant:0.0]];
    
    [self.scrollView addConstraint:[NSLayoutConstraint
                              constraintWithItem:self.imageViewStreamer
                              attribute:NSLayoutAttributeCenterY
                              relatedBy:NSLayoutRelationEqual
                              toItem:self.imageViewVideo
                              attribute:NSLayoutAttributeCenterY
                              multiplier:1.0
                              constant:0.0]];

    [_scrollView setUserInteractionEnabled:YES];
    [_scrollView setScrollEnabled:YES];
    
    // set background for scrollView
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    
    // processing for pinch gestures
    _scrollView.delegate = self;
    _scrollView.maximumZoomScale = MAXIMUM_ZOOMING_SCALE;
    _scrollView.minimumZoomScale = MINIMUM_ZOOMING_SCALE;
    [self centerScrollViewContents];
    [self resetZooming];
    
    // Add action for touch
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [_imageViewStreamer addGestureRecognizer:doubleTapRecognizer];
    
//    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
//    twoFingerTapRecognizer.numberOfTapsRequired = 1;
//    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
//    [_imageViewStreamer addGestureRecognizer:twoFingerTapRecognizer];
}

#pragma mark - Setup camera

- (void)setupButtonTapped:(id)sender{
    //Stop streaming before setup
    [self h264_HandleEnteredBackground];
}

#pragma mark - Initialize camera streaming

- (void)setupLocalStream
{
    self.sStreamMode = @"uAP";
    
    [self setupHttpPort]; // Update if needed.
    self.mediaProcessStatus = MediaPlayerIsNotInit;

    if ( _selectedChannel.profile.isInLocal ) {
    
        _selectedChannel.stream_url = [NSString stringWithFormat:@"rtsp://stream:video@%@:6667/streamhd", _selectedChannel.profile.ip_address];
        
        DLog(@"%s Start stage 2>> >> WITH HW DECODER? %d ", __FUNCTION__, _isExploitHaEnabled);
        
        self.timeStartingStageTwo = [NSDate date];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self startStream];
        });
    }
    else {
        DLog(@"Device does not connect to the demo camera Wifi!");
    }
  
}

- (void)startStream
{
    self.didStopStream = NO;
    self.currentMediaStatus = 0;
    
    if (_mediaProcessStatus == MediaPlayerIsSettingListener) {
        DLog(@"%s The previous Media process is onging. Return!", __FUNCTION__);
        return;
    }
    
    self.mediaProcessStatus = MediaPlayerIsSettingListener;
    
    self.h264StreamerListener = new H264PlayerListener(self);
    MediaPlayer::Instance()->setListener(_h264StreamerListener);
    MediaPlayer::Instance()->setPlaybackAndSharedCam(false, NO);
    
    bool tcpEnabled = _isRtspWithTcpEnabled;
    
    //if (_viewCommandHeader.isP2PMode == NO && _hasLocalIp == YES)
    if (_hasLocalIp == YES)
    {
        DLog(@"LAN IP use UDP");
        tcpEnabled = NO;
    }
    self.start_timestamp = [[NSDate date] timeIntervalSince1970];
    
    MediaPlayer::Instance()->setStreamOptions(_isExploitHaEnabled, YES, _isAVSyncEnabled, tcpEnabled, _isPlayAllFrame);
    
    if ( _isP2PMode ) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            self.p2pMediaInitStatus = MediaPlayer::Instance()->startP2PStream(_imageViewStreamer, _selectedChannel.profile.isInLocal);
            
            if ( self.p2pMediaInitStatus == NO_ERROR)
            {
                DLog(@"HOOK CameraPlayerViewController to p2pHandler.delegate2 ");
                
                //Phung: move here from didConnectSuccessToDeviceID:
                [(RMCHandler_2*)_selectedChannel.profile.p2pHandler setDelegate2:self];
            }
            else{
                DLog(@"self.p2pMediaInitStatus = MediaPlayer::Instance()->startP2PStream failed with error: %d", self.p2pMediaInitStatus);
            }
        });
    }
    else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self startStream_bg];
        });
    }
}

- (void)startStream_bg
{
    status_t status = !NO_ERROR;
    
    // Store current SSID - to check later
    NSDictionary *t = [P2PConfigInfo fetchSSIDInfo];
    NSString *t2 = t?[t objectForKey:@"SSID"]:nil;
    
    self.current_ssid = t2 ;// [CameraPassword fetchSSIDInfo];
    
    DLog(@"Current SSID is: %@", _current_ssid);
    
//    if ( _current_ssid ) {
//        [PrivateDataUtils saveStreamingSSID:_current_ssid];
//    }

    NSString *url = _selectedChannel.stream_url;
    DLog(@"%s url: %@, mediaProcessStatus:%@", __FUNCTION__, url, LogMediaStatus(_mediaProcessStatus));
    self.mediaProcessStatus = MediaPlayerIsSettingDatasource;
    
    do {
        if ( !url || [url isEqualToString:@""] ) {
            break;
        }
        
        status = MediaPlayer::Instance()->setDataSource([url UTF8String]);
        
        if ( status != NO_ERROR ) { // NOT OK
            DLog(@"%s  error: %d", __FUNCTION__, status);
            break;
        }
        
        MediaPlayer::Instance()->setVideoSurface(_imageViewStreamer);
        status = MediaPlayer::Instance()->prepare();
        
        if ( status != NO_ERROR ) { // NOT OK
            break;
        }
        
        // Play anyhow
        status = MediaPlayer::Instance()->start();
        
        if ( status != NO_ERROR ) { // NOT OK
            break;
        }
    }
    while (false);
    
    DLog(@"%s mediaProcessStatus: %@", __FUNCTION__, LogMediaStatus(_mediaProcessStatus));
    self.mediaProcessStatus = MediaPlayerIsStarted;
    
    double delayInSeconds = 0.5;
    
    if (status != NO_ERROR) {
        delayInSeconds = 1.0;
         [self setStreamingStatus:@"Fail to access stream. Retry..."];
    }
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if ( status == NO_ERROR ) {
            [self handleMessage:MEDIA_PLAYER_STARTED ext1:0 ext2:0];
        }
        else {
            // Consider it's down and perform necessary action.
            [self handleMessage:MEDIA_ERROR_SERVER_DIED ext1:0 ext2:0];
        }
    });
}

#pragma mark - Delegate Stream callback

- (void)handleMessage:(int)msg ext1:(int)ext1 ext2:(int)ext2
{
    NSArray *args = @[@(msg),
                      @(ext1),
                      @(ext2)];
    
    if (msg == MEDIA_INFO_ERR_SET_DATA_SOURCE) {
        NSString *tagEvent = @"Streaming timeout";
        
        if (ext1 == AVERROR_STREAM_NOT_FOUND) {
            tagEvent = @"Stream ID not found";
        }
    }
    
    [self performSelectorOnMainThread:@selector(handleMessageOnMainThread:) withObject:args waitUntilDone:NO];
}

- (void)forceRestartStream:(NSTimer *)timer
{
    [self handleMessage:MEDIA_ERROR_SERVER_DIED ext1:-99 ext2:-1];
}

- (void)handleMessageOnMainThread:(NSArray *)args
{
    NSNumber *numberMsg = (NSNumber *)args[0];
    NSInteger msg = [numberMsg integerValue];
    
    NSInteger ext1 = -1, ext2 = -1;
    
    if ( args.count >= 3) {
        ext1 = [args[1] integerValue];
        ext2 = [args[2] integerValue];
    }
    
    switch (msg) {
        case MEDIA_INFO_GET_AUDIO_PACKET:
        {
            [_timerBufferingTimeout invalidate];
            self.timerBufferingTimeout = [NSTimer scheduledTimerWithTimeInterval:TIMEOUT_BUFFERING
                                                                          target:self
                                                                        selector:@selector(forceRestartStream:)
                                                                        userInfo:nil
                                                                         repeats:NO];
            break;
        }
        case MEDIA_INFO_START_BUFFERING:
        {
            DLog(@"%s MEDIA_INFO_START_BUFFERING", __FUNCTION__);
            [_timerBufferingTimeout invalidate];
            self.timerBufferingTimeout = [NSTimer scheduledTimerWithTimeInterval:TIMEOUT_BUFFERING
                                                                          target:self
                                                                        selector:@selector(forceRestartStream:)
                                                                        userInfo:nil
                                                                         repeats:NO];
            break;
        }
        case MEDIA_INFO_STOP_BUFFERING:
        {
            DLog(@"%s MEDIA_INFO_STOP_BUFFERING", __FUNCTION__);
            [_timerBufferingTimeout invalidate];
            break;
        }
        case MEDIA_INFO_FRAMERATE_VIDEO:
        {
            if ( _isStreamInfoEnabled ) {
                
                //don't update realtime video frame rate for demo
                //[self updateDebugInfoFrameRate:ext1];
                //
            }
            
            break;
        }
        case MEDIA_INFO_VIDEO_SIZE:
        {
            DLog(@"video size: %ld x %ld", (long)ext1, (long)ext2);
            [self updateDebugInfoResolutionWidth:ext1 heigth:ext2];
            
            float top = 0 , left = 0;
            float destWidth;
            float destHeight;
            
            // Maintain Aspect Ratio
            if (ext1 == 0 || ext2 == 0) {
                break;
            }
            
            float ratio = (float) ext1/ (float)ext2;
            float fw = _imageViewVideo.frame.size.height * ratio;
            float fh = _imageViewVideo.frame.size.width  / ratio;
            
            DLog(@"video adjusted size:r= %f    fw=%f  fh=%f", ratio, fw, fh);
            
            if ( fw > _imageViewVideo.frame.size.width) {
                // Use the current width with new-height
                destWidth = _imageViewVideo.frame.size.width ;
                destHeight = fh;
                
                // so need to adjust the origin
                left = _imageViewVideo.frame.origin.x;
            }
            else {
                // Use the new-width with current height
                destWidth = fw;
                destHeight = _imageViewVideo.frame.size.height;
                
                // so need to adjust the origin
                if (_imageViewVideo.frame.size.width > fw) {
                    left = (_imageViewVideo.frame.size.width - fw)/2;
                }
                else {
                    left = _imageViewVideo.frame.origin.x;
                }
            }
            
            DLog(@"video adjusted size: %f x %f", destWidth, destHeight);
            _imageViewStreamer.frame = CGRectMake(left, top, destWidth, destHeight);
            
            break;
        }
        case MEDIA_INFO_BITRATE_BPS:
        {
            if ( _isStreamInfoEnabled ) {
                [self updateDebugInfoBitRate:ext1];
            }
            
            break;
        }
        case MEDIA_INFO_HAS_FIRST_IMAGE:
        {
            [self setStreamingStatus:nil];

            self.timeStartingStageTwo = 0;
            DLog(@"[MEDIA_PLAYER_HAS_FIRST_IMAGE]");
            
            self.currentMediaStatus = msg;
            
            if ( _h264StreamerIsInStopped ) {
                _selectedChannel.stopStreaming = YES;
                break;
            }
            
            
            
            _imageViewStreamer.userInteractionEnabled = YES;
            [self invalidateSaveSnapshotTimer];
            self.timerSaveSnapshot = [NSTimer scheduledTimerWithTimeInterval:2.f target:self
                                                                    selector:@selector(storeSnapshotFromStream:)
                                                                    userInfo:nil repeats:NO];
            break;
        }
        case MEDIA_PLAYER_STARTED:
        {
            self.currentMediaStatus = msg;
            self.timeStartPlayerView = [NSDate date];

            self.activity.hidden = YES;
            [self.activity stopAnimating];
        }
            break;
        case MEDIA_ERROR_SERVER_DIED:
        case MEDIA_ERROR_TIMEOUT_WHILE_STREAMING:
        {
            
            // Double check to make sure this view is still loaded
            if ( !self.isViewLoaded ) {
                return;
            }

            if (!_h264StreamerListener) {
                DLog(@"%s Even after setting listener to nill", __FUNCTION__);
                return;
            }
            
            self.currentMediaStatus = msg;
            // set custom indication is YES when server die
            //_isShowCustomIndicator = YES;
            
            [_timerBufferingTimeout invalidate];
            self.timerBufferingTimeout = nil;
            
            DLog(@"%s Timeout While streaming  OR server DIED - error: %ld", __FUNCTION__, (long)msg);
            
            [self stopStreamIsGoBack:_userWantToCancel];
            if ( _userWantToCancel ) {
                DLog(@"*[MEDIA_ERROR_TIMEOUT_WHILE_STREAMING] *** USER want to cancel **.. cancel after .1 sec...");
                _selectedChannel.stopStreaming = YES;
                [self goBack];
                return;
            }
            
            _selectedChannel.stopStreaming = YES;
            _selectedChannel.stream_url = nil;
            
            if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground && (!_isMoniterInBackground)){
                NSLog(@"\n%s return", __FUNCTION__);
                
                return;
            }
            
            if (_wantToStop) {
                NSLog(@"Stop processing");
                return;
            }
            
            NSString* failureReason;
            if ( msg == MEDIA_ERROR_SERVER_DIED ) {
                failureReason = @"Media Error Server Died";
            }
            else if ( msg == MEDIA_ERROR_TIMEOUT_WHILE_STREAMING ) {
                failureReason = @"Media Timeout While Streaming";
            }
            
            [self setupStream:NO];
            
            break;
        }
        case MEDIA_INFO_RECEIVED_VIDEO_FRAME:
        {
            [self displayCustomIndicator:NO];
            break;
        }
        case MEDIA_INFO_CORRUPT_FRAME_TIMEOUT:
        {
            [self displayCustomIndicator:YES];
            break;
        }
        case MEDIA_INFO_RECORDING_TIME:
        {
            double ticks   = ext1;
            double seconds = fmod(ticks, 60.0);
            double minutes = fmod(trunc(ticks / 60.0), 60.0);
            double hours   = trunc(ticks / 3600.0);
            NSString *timeToDisplay = [NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f", hours, minutes, seconds];
            
            if (ticks == 0) {
                NSLog(@"%s timeToDisplay:%@", __FUNCTION__, timeToDisplay);
            }
            
            _lblRecordingTime.text = timeToDisplay;
        }
            break;
        case MEDIA_INFO_ERR_SET_DATA_SOURCE:
        {
            if (_currentMediaStatus != msg) { // May get this msg every 100msec, so just update UI if needed.
                self.currentMediaStatus = msg;
            }
        }
            break;
            
        default:
            break;
    }
}


#pragma mark - Custom Indicator

- (void)startCustomIndicatorAnimation
{
    _customIndicator.hidden = NO;
    [self.view addSubview:_customIndicator];
    [self.view bringSubviewToFront:_customIndicator];
    
    _customIndicator.animationDuration = 1.5;
    _customIndicator.animationRepeatCount = 0;
    [_customIndicator startAnimating];
}

- (void)displayCustomIndicator:(BOOL)shouldShowIndicator
{
    if ( shouldShowIndicator ) {
        [self startCustomIndicatorAnimation];
    }
    else {
        [_customIndicator stopAnimating];
        _customIndicator.hidden = YES;
    }
}


#pragma mark - Rotation screen

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self adjustViewsForOrientation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
}

- (void)checkOrientation
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    [self adjustViewsForOrientation:orientation];
}

- (void)adjustViewsForOrientation:(UIInterfaceOrientation)orientation
{
    DLog(@"Camera View - %s", __FUNCTION__);
    
    [self resetZooming];
    
    // Remove all subviews (including _melodyViewController.view) before reloading the xib
    NSArray *viewsToRemove = [self.view subviews];
    
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
//    CGRect screenSize = [UIScreen mainScreen].bounds;
//    float w = screenSize.size.width;
//    float h = screenSize.size.height;
//    
//    if ( UIInterfaceOrientationIsLandscape(orientation) && w < h ) {
//        float temp = w;
//        w = h;
//        h = temp;
//    }
//    else if ( UIInterfaceOrientationIsPortrait(orientation) && h < w ) {
//        float temp = w;
//        w = h;
//        h = temp;
//    }
    
    if ( UIInterfaceOrientationIsLandscape(orientation)) {
        self.isLandScapeMode = YES;
        
        // Check orientation again here as we added the progressView logic wrinkle
        [[NSBundle mainBundle] loadNibNamed:@"CameraPlayerViewController_land" owner:self options:nil];
        
       
        
        _viewDebugInfo.hidden = !_isStreamInfoEnabled;
        
        BOOL hideNavBar = UIInterfaceOrientationIsLandscape(orientation);
        [self.navigationController setNavigationBarHidden:hideNavBar animated:YES];
        [[UIApplication sharedApplication] setStatusBarHidden:hideNavBar withAnimation:UIStatusBarAnimationSlide];
//        
//        // Ensure view width and height are setup correctly.
//        CGRect rect = self.view.frame;
//        rect.size.width = w;
//        rect.size.height = h;
//        self.view.frame = rect;
//        
//        // Scale the video port to fit the landscape width and letter box top and bottom
//        int imageViewHeight = (w * (9/16.0f)) + 1; // +1 gets rid of white line at bottom
//        CGRect newRect = CGRectMake(0, (h - imageViewHeight) / 2, w, imageViewHeight);
//        
//        self.imageViewVideo.frame = CGRectMake(0, -1, w, imageViewHeight);
//        self.scrollView.frame = newRect;
    }
    else {
        self.isLandScapeMode = NO;
        
        [[NSBundle mainBundle] loadNibNamed:@"CameraPlayerViewController" owner:self options:nil];
        

        _viewDebugInfo.hidden = !_isStreamInfoEnabled;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController setNavigationBarHidden:NO];
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
        });
        
//        // Ensure view width and height are setup correctly.
//        CGRect rect = self.view.frame;
//        rect.size.width = w;
//        rect.size.height = h;
//        self.view.frame = rect;
//        
//        int y = 64;
//        int imageViewHeight = w * (9/16.0f);
//        CGRect newRect = CGRectMake(0, y, w, imageViewHeight);
//        self.scrollView.frame = newRect;
//        self.imageViewVideo.frame = CGRectMake(0, 0, w, imageViewHeight);
        
//        _liveStreamMenuView.frame = _eventViewContainer.bounds;
//        [_eventViewContainer addSubview:_liveStreamMenuView];
    }

    _imageViewStreamer.frame = _imageViewVideo.frame;
    [self addGesturesPichInAndOut];
    
    _customIndicator.image = [UIImage imageNamed:@"loader_a"];
    _customIndicator.animationImages = @[[UIImage imageNamed:@"loader_b"],
                                         [UIImage imageNamed:@"loader_c"],
                                         [UIImage imageNamed:@"loader_d"],
                                         [UIImage imageNamed:@"loader_e"],
                                         [UIImage imageNamed:@"loader_f"]];
    _customIndicator.hidden = YES;
    
    [self setStreamingStatus:_messageStreamingState];
    
    // trigger re-cal of videosize
    if ( MediaPlayer::Instance()->isPlaying() ) {
        //_isShowCustomIndicator = NO;
    }
    
    if ( _currentMediaStatus != 0 ) {
        MediaPlayer::Instance()->videoSizeChanged();
    }

    [_viewDebugInfo setHidden:!_isStreamInfoEnabled];
    _viewDebugInfo.userInteractionEnabled = NO;
    
    _ib_lbCameraName.text = _selectedChannel.profile.fw_version;
    
    UITextField *tfMS = (UITextField *)[_viewDebugInfo viewWithTag:TF_DEBUG_MODE_STREAM];
    tfMS.text = _sStreamMode;
}

#pragma mark - UI Actions

- (IBAction)inAppSnapshotTapped:(id)sender {
    [self saveSnapShot:_imageViewStreamer.image];
}

- (IBAction)videoScreenSizeButtonTapped:(id)sender {
    
    //TODO: Supporting landscape only
    
    if (_videoScreenSizeButton.selected) {
        //Video is full screen, minimize video screen now
        
        [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.imageViewStreamer setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.imageViewVideo setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        
        //CGRect navFrame = CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 64.0f);
        [UIView animateWithDuration:0.2 animations:^{
            self.videoScreenHeightConstraint.constant = self.videoScreenMinHeight;
            
            //self.navigationController.navigationBar.frame = navFrame;
            [self.navigationController setNavigationBarHidden:NO];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            self.videoScreenHeightConstraint.constant = self.videoScreenMinHeight;
            
            [self.view layoutIfNeeded];
            
            NSLog(@"***************************************************************self.imageViewStreamer.bounds.size: (%.0f, %.0f", self.imageViewStreamer.bounds.size.width, self.imageViewStreamer.bounds.size.height);
            
            [self.scrollView setContentSize:self.imageViewStreamer.bounds.size];

            //self.navigationController.navigationBar.frame = navFrame;
            [self.navigationController setNavigationBarHidden:NO];
        }];
    }
    else {
        //Video is minimal, set video full screen new
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        
        //CGRect navFrame = CGRectMake(0.0f, -64.0f, SCREEN_WIDTH, 64.0f);
        [UIView animateWithDuration:0.2 animations:^{
            self.videoScreenHeightConstraint.constant = SCREEN_HEIGHT;
            
            //self.navigationController.navigationBar.frame = navFrame;
            [self.navigationController setNavigationBarHidden:YES];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
            [self.imageViewStreamer setTranslatesAutoresizingMaskIntoConstraints:NO];
            [self.imageViewVideo setTranslatesAutoresizingMaskIntoConstraints:NO];
            
            self.videoScreenHeightConstraint.constant = SCREEN_HEIGHT;
            
            [self.view layoutIfNeeded];
            
            NSLog(@"***************************************************************self.imageViewStreamer.bounds.size: (%.0f, %.0f", self.imageViewStreamer.bounds.size.width, self.imageViewStreamer.bounds.size.height);

            [self.imageViewVideo setTranslatesAutoresizingMaskIntoConstraints:YES];
            [self.imageViewStreamer setTranslatesAutoresizingMaskIntoConstraints:YES];
            [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
            
            [self.scrollView setContentSize:CGSizeMake(self.imageViewStreamer.bounds.size.width + 1000, self.imageViewStreamer.bounds.size.height)];
            
            CGRect streamImgFrame = self.imageViewStreamer.frame;
            streamImgFrame.origin.x = 0;
            streamImgFrame.origin.y = 0;
            self.imageViewStreamer.frame = streamImgFrame;
            self.imageViewVideo.frame = streamImgFrame;
            
            //[self.scrollView scrollRectToVisible:CGRectMake((streamImgFrame.size.width-SCREEN_WIDTH)/2, 0, SCREEN_WIDTH, SCREEN_HEIGHT) animated:NO];
            self.scrollView.contentOffset = CGPointMake((streamImgFrame.size.width-SCREEN_WIDTH)/2, 0);
            
            //self.navigationController.navigationBar.frame = navFrame;
            [self.navigationController setNavigationBarHidden:YES];
        }];
    }
    
    _videoScreenSizeButton.selected = !_videoScreenSizeButton.selected;
}

- (IBAction)exitGestureSwiped:(id)sender {
    [self prepareGoBackToCameraList];
}

- (IBAction)videoViewTapped:(id)sender {
}

- (void)sendCommand:(CameraCommand*)cmd withGet:(BOOL)isGet completion:(void(^) (CameraCommand* command, BOOL success))completion{
    if (isGet) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            if (_wantToStop) {
                return;
            }
            
            __block NSString *bodyKey = nil;
            
            if (![self canStreamInLocal] && _isP2PMode) {
                //App is connecting to router and is streaming via P2P mode
                BOOL isReq = YES;
#if kIsFakedToTest
                isReq = NO;
#endif
                bodyKey = [(RMCHandler_2*)_selectedChannel.profile.p2pHandler sendCommand:cmd.getCommand andFlag:isReq];
            }
            else if([self canStreamInLocal]){
                bodyKey = [[HttpCom instance].comWithDevice sendCommandAndBlock:cmd.getCommand];
            }
            else{
                //Do not send command if app is not in local or is streaming
                if (completion) {
                    completion(cmd, NO);
                }
                return;
            }
            
            DLog(@"Command %@ response string: %@  p2p: %d ", cmd.getCommand, bodyKey, _isP2PMode);
            cmd.isLoading = NO;
            
            if (!bodyKey.length) { //Wait a moment when send command failed
                [NSThread sleepForTimeInterval:0.3];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (_wantToStop) {
                    return ;
                }
                
                if (bodyKey.length) {
                    
                    NSArray *tokens = [bodyKey componentsSeparatedByString:@": "];
                    
                    if ([tokens count] >=2 ) {
                        NSString *bodyPrefix = tokens[0];
                        
                        if ([bodyPrefix hasPrefix:cmd.getCommand]) { // ok
                            cmd.currentValue = [tokens[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            if (cmd.commandType == DemoCommandTypeVideoConfig) {
                                NSString *cmdValue = cmd.currentValue;
                                
                                if ([cmdValue isEqualToString:@"0"]) {
                                    cmdValue = @"low";
                                }
                                else if ([cmdValue isEqualToString:@"1"]) {
                                    cmdValue = @"medium";
                                }
                                else if ([cmdValue isEqualToString:@"2"]) {
                                    cmdValue = @"high";
                                }
                                else if ([cmdValue isEqualToString:@"-1"]) {
                                    cmdValue = @"err: -1";
                                }
                                
                                cmd.currentValue = cmdValue;
                            }
                            
                            if (completion) {
                                completion(cmd, YES);
                            }
                            return;
                        }
                        else {
                            //Failed
                        }
                    }
                    else {
                        //Failed
                    }
                }
                else {
                    //Failed
                }
                
                if (completion) {
                    completion(cmd, NO);
                }
            });
        });
    }
    else{
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            if (_wantToStop) {
                return;
            }
            
            __block NSString *bodyKey = nil;
            
            NSString* setCommand = @"";
            
            if (cmd.commandType == DemoCommandTypeVideoConfig) {
                NSString *cmdValue = cmd.currentValue;
                
                if ([cmdValue isEqualToString:@"low"]) {
                    cmdValue = @"0";
                }
                else if ([cmdValue isEqualToString:@"medium"]) {
                    cmdValue = @"1";
                }
                else if ([cmdValue isEqualToString:@"high"]) {
                    cmdValue = @"2";
                }
                
                setCommand = [cmd.setCommand stringByAppendingFormat:@"&index=%@", cmdValue];
            }
            else {
                setCommand = [cmd.setCommand stringByAppendingFormat:@"&value=%@", cmd.currentValue];
            }
            
            if (cmd.commandType == DemoCommandTypeBitRateStationary) {
                setCommand = [setCommand stringByAppendingString:@"&speed=1"];
            }
            
            if (cmd.commandType == DemoCommandTypeBitRateMoving) {
                setCommand = [setCommand stringByAppendingString:@"&speed=2"];
            }
            
            if (![self canStreamInLocal] && _isP2PMode) {
                //App is connecting to router and is streaming via P2P mode
                BOOL isReq = YES;
#if kIsFakedToTest
                isReq = NO;
#endif
                bodyKey = [(RMCHandler_2*)_selectedChannel.profile.p2pHandler sendCommand:setCommand andFlag:isReq];
            }
            else if([self canStreamInLocal]) {
                bodyKey = [[HttpCom instance].comWithDevice sendCommandAndBlock:setCommand];
            }
            else{
                //Do not send command if app is not in local or is streaming
                if (completion) {
                    completion(cmd, NO);
                }
                return;
            }
            
            DLog(@"Send command %@, response string: %@ isp2p? %d ", setCommand, bodyKey, _isP2PMode);
            cmd.isLoading = NO;
                        
            dispatch_async(dispatch_get_main_queue(), ^{
                if (_wantToStop) {
                    return;
                }
                
                if (bodyKey.length) {
                    NSArray *tokens = [bodyKey componentsSeparatedByString:@": "];
                    
                    if ([tokens count] >=2 ) {
                        NSString *bodyPrefix = tokens[0];
                        
                        if ([bodyPrefix hasPrefix:cmd.setCommand]) { // ok
                            if (completion) {
                                completion(cmd, YES);
                            }
                            
               //             [hud setLabelText:LocStr(@"Success")];
             //               [hud hide:YES afterDelay:1.0];
                            
                            return;
                        }
                        else {
                            //Failed
                        }
                    }
                    else {
                        //Failed
                    }
                }
                else {
                    //Failed
                }

                [cmd restoreCurrentValue];
                if (completion) {
                    completion(cmd, NO);
                }
                
             //   [hud setLabelText:LocStr(@"Failed")];
           //     [hud hide:YES afterDelay:1.0];
            });
        });
    }
}

#pragma mark - RmcHanlderDelegate_2

- (void) rmcHandler:(RMCHandler_2*)rmchandler didFailToConnectCam:(NSString *)aMac mode:(NSString *)aMode
{
    if (([UIApplication sharedApplication].applicationState == UIApplicationStateBackground &&
         !(_isMoniterInBackground && _selectedChannel.profile.isInLocal))) {
        DLog(@"%s Cancelling flow", __FUNCTION__);
        return;
    }
    
    DLog(@"%s aMode:%@", __FUNCTION__, aMode);
    
    if ([aMode isEqualToString:P2P_MODE_LOCAL]) {
        
    }
    else if ([aMode isEqualToString:P2P_MODE_REMOTE] || [aMode isEqualToString:P2P_MODE_RELAY]) {
        
        double delayInSecs = 7;
        dispatch_time_t popTimes = dispatch_time(DISPATCH_TIME_NOW, delayInSecs * NSEC_PER_SEC);
        dispatch_after(popTimes, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            // NSString *camMac = _selectedChannel.profile.mac_address;
            
            NSLog(@"%s p#iferform p2p re...", __FUNCTION__);
            
            NSString *mode = P2P_MODE_REMOTE;
            
            if ([aMode isEqualToString:P2P_MODE_REMOTE]) {
                self.sStreamMode = @"PS";
                mode = P2P_MODE_RELAY;
                [self setStreamingStatus:@"Connecting p2p relay..."];
            }
            else {
                
                [self setStreamingStatus:@"Connecting p2p..."];
            }
            
        });
    }
    else {
        NSLog(@"\nOut of control");
    }
}

- (void) rmcHandler:(RMCHandler_2*)rmchandler didConnectSuccessToDeviceID:(NSString *)udid
{
    //To ensure th udid is belonged to current camera
    if (![_selectedChannel.profile.registrationID.lowercaseString isEqualToString:udid.lowercaseString]) {
        return;
    }
    
    if (([UIApplication sharedApplication].applicationState == UIApplicationStateBackground &&
         !(_isMoniterInBackground && _selectedChannel.profile.isInLocal))) {
        DLog(@"%s Cancelling flow", __FUNCTION__);
        return;
    }
    
    if (!_wantToStop) {
        [self setStreamingStatus:@"Starting P2P stream..."];
    }
    
    self.isP2PMode = YES;
    [self startStream];
}

- (void) rmcHandler:(RMCHandler_2*)rmchandler timeoutWhileReceivingDataWithDeviceID:(NSString *)udid
{
    //To ensure th udid is belonged to current camera
    if (![_selectedChannel.profile.registrationID.lowercaseString isEqualToString:udid.lowercaseString]) {
        return;
    }
    
    if (([UIApplication sharedApplication].applicationState == UIApplicationStateBackground &&
         !(_isMoniterInBackground && _selectedChannel.profile.isInLocal))) {
        DLog(@"%s Cancelling flow", __FUNCTION__);
        return;
    }
    
    self.isP2PMode = NO;
    [self setStreamingStatus:@"P2P timeout (no data in 15s)"];
    
    dispatch_async(dispatch_get_main_queue(), ^{ // UI
        _customIndicator.hidden = NO;
    });
    
    DLog(@"%s", __FUNCTION__);
    
    
    self.p2pMediaInitStatus = -1;
    MediaPlayer::Instance()->setListener(NULL);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        MediaPlayer::Instance()->stopP2PStream(NO);
        [NSThread sleepForTimeInterval:1.5];
        [self setupP2PStream];
    });
    
    self.mediaProcessStatus = MediaPlayerIsNotInit;
}

#pragma mark - P2P Handler

- (void)setupP2PStream
{
    if (self.isFirstLoadStreaming && _isP2PMode) {
        return;
    }

    RMCHandler_2* t = [[RMCHandler_2 alloc] init];
    
    self.sStreamMode = @"PR";
    
    __weak CameraPlayerViewController *weakSelf = self;
    P2PCommResult (^blockCallback2)(PConfigInfo*, NSString*) = ^(PConfigInfo *config, NSString* deviceID) {
        P2PCommResult t2 = [weakSelf communicateWithCameraConfigInfo:config];
        
        if (t2 == P2PCommSuccess) {
            [weakSelf setStreamingStatus:@"Getting P2P Stream..."];
        }
        else {
            [weakSelf setStreamingStatus:@"Fail to get Response from Cam"];
        }
        
        return t2;
    };
    
    t.outputCallback = blockCallback2;
    t.delegate = self;
    
    NSString *camUdid = _selectedChannel.profile.registrationID;
    NSLog(@"%s %@", __FUNCTION__, camUdid);
    [t connectToCam:camUdid WithMode:P2P_MODE_REMOTE_2];
    
    //self.rmcHandler2 = t;
    _selectedChannel.profile.p2pHandler = t;
}

- (void)closeP2PStreamGoBack:(NSNumber *)isGoBack
{
    //action=command&command=close_p2p_rtsp_stun&streamname=[MAC_ADDRESS]
    DLog(@"%s isGoBack:%d", __FUNCTION__, [isGoBack boolValue]);
    self.isP2PMode = NO;
    
    [[DeviceCommunication sharedInstance] setSessionKeyDelegate:nil];
    
    self.p2pMediaInitStatus = -1;
    
    delete _h264StreamerListener;
    MediaPlayer::Instance()->setListener(NULL);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        MediaPlayer::Instance()->stopP2PStream(NO);
    });
    
    //keep connection, just don't handle responsed data
    [self.selectedChannel.profile.p2pHandler setDelegate2:nil];
    self.selectedChannel.profile.isP2PAlready = YES;
    
    //Set stream mode to PREVIEW MODE
    if ([FIRMWARE_VERSION_FOR_PREVIEW_MODE compare:_selectedChannel.profile.fw_version] <= NSOrderedSame) {
        [(RMCHandler_2*)_selectedChannel.profile.p2pHandler switchCamToP2pSesMode:1];
    }
    
    if ( isGoBack.boolValue ) {
        [self goBack];
    }
    else {
        self.mediaProcessStatus = MediaPlayerIsNotInit;
    }
}

- (void)goBack
{
    DLog(@"self.currentMediaStatus: %ld", (long)_currentMediaStatus);
    
    //TODO: Don't stop stream right now, will stop after 5 minutes
    //_selectedChannel.stopStreaming = YES;
    
    MediaPlayer::Instance()->setVideoSurface(NULL);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults removeObjectForKey:CAM_IN_VEW];
    [userDefaults synchronize];
    
    _selectedChannel.profile.isSelected = NO;
    
    [self popViewOnMainThread];
}

- (void)popViewOnMainThread
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    if (_displayMode == PlayerDisplayingModePush) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self dismissViewControllerAnimated:NO completion:^{
//            AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//            MBP_iosViewController *rootVC = delegate.viewController;
//            if(!rootVC.menuVC){
//                [rootVC sendStatus:DISMISS_PLAYER_FROM_PUSH_NOTIFICATION];
//            }
        }];
    }
}


/* 
  added some kind of statistic data 
  - Audio fps, audio accumulated frame rcvd
  - Video Fps, vidoe accumulated frame rcvd
 
 */

-(void ) updateStatistic:(int)audioOrVideo
{
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval timediff;
    //double audio_fps, video_fps;
    
    timediff = now - self.start_timestamp;//in SECOND
    
    //Calculate the averaga audio fps and video fps
    
    if ( audioOrVideo == 1)
    {
        self.video_framenum ++;
        self.avg_video_fps = (double)self.video_framenum/(double)timediff;
    }
    else
    {
        self.audio_framenum ++;
         self.avg_audio_fps = (double)self.audio_framenum/(double)timediff;
    }
    
    
    if (_isStreamInfoEnabled == YES)
    {
        
        //UPdate on the label
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString * statistic_str = [NSString stringWithFormat:@"%@: Afps: %0.2f total: %d  Vfps: %0.2f total: %d ",self.sStreamMode,
                                        self.avg_audio_fps, self.audio_framenum, self.avg_video_fps, self.video_framenum];
            self.firmwareVersionLabel.text = statistic_str;
            self.firmwareVersionLabel.hidden = NO;
            
        });
        
    }
    
    
   
}

//#define kDebugLog 0

// [Ω]delegete 22

- (void)didRecveVideoData:(void *)data withSize:(uint32_t )size idr:(bool )isIdr timestamp:(uint32_t )ts
{
    [self updateStatistic:1];
    
    if (_p2pMediaInitStatus == NO_ERROR) {
        MediaPlayer::Instance()->receiveVideoData(data, size, isIdr, ts);
    }
}

- (void)didRecveAudioData:(void *)data withSize:(uint32_t )size timestamp:(uint32_t )ts
{
#if !kIsFakedToTest
        uint8_t t[16];
        memset(t, 0, 16);
        
        // 00 00 00 01
        uint8_t *d = (uint8_t *)data;
        if (!memcmp(t, d, 16) ||
            (d[0]==0x00 && d[1]==0x00 && d[2]==0x00 && d[3]==0x01)) {
#if DEBUG
            NSMutableString *log = [NSMutableString string];
            
            if ( d ) {
                for (int i = 0; i < size; ++i) {
                    [log appendFormat:@"%02x ", d[i]];
                }
            }
            
            NSLog(@"log:%d, %d, %@", ts, size, log);
            NSLog(@"Return!"); // Log is overflow if talkback!
#endif
            return;
        }
#if 0
        
        if ([kUserDefault boolForKey:@"test_audio_fake"]) {
            for (int i = 0; i < size; i++) {
                if (d[i] != 0x00) {
                    NSMutableString *log = [NSMutableString string];
                    
                    if ( d ) {
                        for (int i = 0; i < size; ++i) {
                            [log appendFormat:@"%02x ", d[i]];
                        }
                    }
                    
                    NSLog(@"log222:%d, %d, %@", ts, size, log);
                    return;
                }
            }
        }
#endif
#endif
    
    [self updateStatistic:0];
    
    
    if (_p2pMediaInitStatus == NO_ERROR) {
        MediaPlayer::Instance()->receiveAudioData(data, size, ts);
    }
}

- (void)didRecveOtherData:(void *)data withSize:(uint32_t )size type:(int)type timestamp:(uint32_t )ts
{
    if (type == eMediaSubTypeVideoJPEG) {
        //Get P2P UDP feedback. Don't handle streaming for this data
//        NSData* feedbackData = [NSData dataWithBytes:(const void *)data length:size];
//        [[CameraFeedbackManager sharedInstance] handleFeedbackData:feedbackData];
        return;
    }
    
    if (type == eMediaSubTypeUserDefine) {
        uint8_t *d = (uint8_t *)data;
        NSLog(@"log eMediaSubTypeUserDefine:%02x %02x %02x", d[0], d[1], d[2]);
    }
}

- (void)didRecveData:(void *)data withSize:(uint32_t )size type:(int )type timestamp:(uint32_t )ts andID:(NSInteger)aID
{
    if (type == eMediaSubTypeVideoJPEG) {
        //Get P2P UDP feedback. Don't handle streaming for this data
//        NSData* feedbackData = [NSData dataWithBytes:(const void *)data length:size];
//        [[CameraFeedbackManager sharedInstance] handleFeedbackData:feedbackData];
        return;
    }

    if (type == eMediaSubTypeUserDefine) {
        uint8_t *d = (uint8_t *)data;
        NSLog(@"log eMediaSubTypeUserDefine:%02x %02x %02x", d[0], d[1], d[2]);
    }

#if !kIsFakedToTest
    if (type / 10 == 2) {
        uint8_t t[16];
        memset(t, 0, 16);
        
        // 00 00 00 01
        uint8_t *d = (uint8_t *)data;
        if (!memcmp(t, d, 16) ||
            (d[0]==0x00 && d[1]==0x00 && d[2]==0x00 && d[3]==0x01)) {
#if DEBUG
                NSMutableString *log = [NSMutableString string];
                
                if ( d ) {
                    for (int i = 0; i < size; ++i) {
                        [log appendFormat:@"%02x ", d[i]];
                    }
                }
                
                NSLog(@"log:%d, %d, %@", ts, size, log);
                NSLog(@"Return!"); // Log is overflow if talkback!
#endif
            return;
        }
#if 0
        
        if ([kUserDefault boolForKey:@"test_audio_fake"]) {
            for (int i = 0; i < size; i++) {
                if (d[i] != 0x00) {
                    NSMutableString *log = [NSMutableString string];
                    
                    if ( d ) {
                        for (int i = 0; i < size; ++i) {
                            [log appendFormat:@"%02x ", d[i]];
                        }
                    }
                    
                    NSLog(@"log222:%d, %d, %@", ts, size, log);
                    return;
                }
            }
        }
#endif
    }
#endif
    
    if (_p2pMediaInitStatus == NO_ERROR) {
#if ENABLE_P2P_VTD
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground &&
            _isExploitHaEnabled && type/10 == 1) {
            //            free(data);
        }
        else {
#endif
            MediaPlayer::Instance()->writeFile(data, size, type, ts);
#if ENABLE_P2P_VTD
        }
#endif
    }

}

- (P2PCommResult)communicateWithCameraConfigInfo:(PConfigInfo *)configInfo
{
    /**
     * key=[AES_DECRYPT_KEY]
     * &sip=[RELAY_SERVER_IP]
     * &sp=[RELAY_SERVER_PORT]
     * &rn=[RANDOM_NUMBER]
     **/
    
    self.sessionKeyData = nil;
    NSString *aUUIDString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *uuidString = [aUUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    u_int32_t streamNum = arc4random_uniform(9);
    uuidString = [uuidString stringByReplacingCharactersInRange:NSMakeRange(uuidString.length - 2, 2) withString:[NSString stringWithFormat:@"_%d", streamNum]];
//    NSString *streamName = uuidString;
    
    NSString *cmd = [NSString stringWithFormat:@"get_session_key&mode=%@&port1=%d&ip=%@&streamname=%@", configInfo.commMode, configInfo.port, configInfo.ip, uuidString];
    
    if ( [configInfo.commMode isEqualToString:P2P_MODE_LOCAL] ) {
        HttpCommunication *dev_com = [[HttpCommunication alloc] initWithNewFlag:YES];
        dev_com.device_port = 9090;

        dev_com.device_ip = _selectedChannel.profile.ip_address  = @"192.168.3.65";
        
        NSString *reply = [dev_com sendCommandAndBlock:cmd];
        DLog(@"%s local, reply:%@", __FUNCTION__, reply);
        
        if ( reply.length <= @"get_session_key: -1".length ) {
            return P2PCommFailure;
        }
        
        NSString *response = [[[reply substringFromIndex:@"get_session_key: ".length] componentsSeparatedByString:@","] objectAtIndex:1];
        response = [response stringByReplacingOccurrencesOfString:@"&&" withString:@"&"];
        NSMutableArray *token = (NSMutableArray *)[response componentsSeparatedByString:@"&"];
        
        for ( int i = 0; i < token.count; i++ ) {
            NSString *value = token[i];
            NSRange range = [value rangeOfString:@"="];
            
            if ( range.location != NSNotFound ) {
                token[i] = [value substringWithRange:NSMakeRange(range.location + 1, value.length - (range.location + 1))];
            }
        }
        
        if ( !token || token.count < 2 ) {
            return P2PCommFailure;
        }
        
        if ( token.count < 3 ) {
            [token addObject:@"68656C6C6F"];
        }
        
        configInfo.port  = [token[0] intValue];
        configInfo.ip = token[1];
        [configInfo keyFromHexString:token[2]];
        
        return P2PCommSuccess;

    }
    
    [[DeviceCommunication sharedInstance] setSessionKeyDelegate:self];
    [[DeviceCommunication sharedInstance] requestCommandForDevice:cmd onChannel:_selectedChannel.profile.registrationID];
    
    NSDate *startTime = [NSDate date];
    
    while (!_sessionKeyData && (ABS(startTime.timeIntervalSinceNow) < 30) && !_wantToStop) {
        [NSThread sleepForTimeInterval:0.5];
    }
    
    DLog(@"%s Cancel:%d response:%@", __FUNCTION__, _wantToStop, _sessionKeyData);

    [[DeviceCommunication sharedInstance] setSessionKeyDelegate:nil];
    
    if (_wantToStop || !_sessionKeyData || ![_sessionKeyData isKindOfClass:[NSArray class]]) {
        return P2PCommFailure;
    }
    
    NSArray *values = (NSArray *)_sessionKeyData;
    NSInteger error;
    
    for (NSString *value in values) {
        if ([value hasPrefix:@"error="]) {
            error = [[value stringByReplacingOccurrencesOfString:@"error=" withString:@""] intValue];
        }
        if ([value hasPrefix:@"key="]) {
            NSString *key = [value stringByReplacingOccurrencesOfString:@"key=" withString:@""];
            [configInfo keyFromHexString:key];
        }
        if ([value hasPrefix:@"sip="]) {
            NSString  *sip = [value stringByReplacingOccurrencesOfString:@"sip=" withString:@""];
            configInfo.sip = sip;
        }
        if ([value hasPrefix:@"sp="]) {
            NSString *sp = [value stringByReplacingOccurrencesOfString:@"sp=" withString:@""];
            configInfo.sp = [sp intValue];
        }
        if ([value hasPrefix:@"rn="]) {
            NSString *rn = [value stringByReplacingOccurrencesOfString:@"rn=" withString:@""];
            [configInfo rnFromHexString:rn];
        }
        if ([value hasPrefix:@"port1="]) {
            NSString *port1 = [value stringByReplacingOccurrencesOfString:@"port1=" withString:@""];
            configInfo.port = [port1 intValue];
        }
        if ([value hasPrefix:@"ip="]) {
            NSString *ip = [value stringByReplacingOccurrencesOfString:@"ip=" withString:@""];
            configInfo.ip = ip;
        }
    }
    
    //3id: E076D0148F99&time: 1464606896&get_session_key: error=200,port1=46612&ip=118.68.40.25&key=716d487524654933245e256730277d31
    
    configInfo.streamName = uuidString;
    
    return P2PCommSuccess;
}

- (void)getSessionKey:(NSDictionary *)data
{
    self.sessionKeyData = [data arrayForKey:@"DATA"];
}

- (void)commandTimeout:(NSString *)command
{
    NSLog(@"\n\n\n%s", __FUNCTION__);
    self.sessionKeyData = @"";
//    [self tryP2PFaileAtCam:@"" modd:@""];
}

- (void)commandTimeout:(NSString *)command withDevice:(NSDictionary *)deviceInfo
{
    NSLog(@"\n\n\n%s", __FUNCTION__);
    self.sessionKeyData = @"";
    //    [self tryP2PFaileAtCam:@"" modd:@""];
}

#pragma mark - SnapShot

- (void)saveSnapShot:(UIImage *)image
{
    // save to photo album
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
}

- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *message;
    NSString *title;
    if ( !error ) {
        title = LocStr(@"Snapshot");
        message = LocStr(@"Saved to Photo Album");
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setLabelText:title];
        [hud setDetailsLabelText:message];
        [hud hide:YES afterDelay:1.0];
    }
    else {
        title = LocStr(@"Error");
        
        NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
        NSString *appName = info[@"CFBundleDisplayName"];
        message = [NSString stringWithFormat:LocStr(@"Allow permission to save media in gallery. Settings > Privacy > Photos > %@ :- turn switch on."), appName];
        
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [hud setLabelText:title];
        [hud setDetailsLabelText:message];
        [hud hide:YES afterDelay:5.0];
    }
}

@end
