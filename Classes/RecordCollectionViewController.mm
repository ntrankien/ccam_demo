//
//  RecordCollectionViewController.m
//  Dev
//
//  Created by Dev on 12/5/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "RecordCollectionViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MessageUI/MessageUI.h>
#import "PlaylistInfo.h"
#import "PublicDefine.h"
#import "Recording.h"
#import "MBProgressHUD.h"
#import "RecordColletionCell.h"
#import "PlaybackViewController.h"

#define TAG_ALERT_DELETE_THESE_VIDEOS 2

@interface RecordCollectionViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate, PlaybackDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *listRecords;
@property (nonatomic, strong) NSMutableArray *listSelectedRecords;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, weak) UIBarButtonItem *selectAllButton;
@property (nonatomic) BOOL isEditing;

@property (nonatomic) CGPoint scrollPositionBeforeRotation;
@property (nonatomic) BOOL shouldUpdate;
@property (nonatomic) BOOL viewDidAppearCompleted;

@property (nonatomic, strong) NSString *preplayVideoTitle;

@end

@implementation RecordCollectionViewController

#pragma mark - UIViewController methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kDidLoadRecordingList];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.navigationItem.title = LocStr(@"Videos");

    self.refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(doRefresh) forControlEvents:UIControlEventValueChanged];
    [_collectionView addSubview:_refreshControl];
    
    self.view.backgroundColor = [UIColor colorWithRed:202.f/255.f green:202.f/255.f blue:202.f/255.f alpha:255.f/255.f];
    _collectionView.backgroundColor = [UIColor colorWithRed:202.f/255.f green:202.f/255.f blue:202.f/255.f alpha:255.f/255.f];
    
    UINib *cell = [UINib nibWithNibName:@"RecordColletionCell" bundle:nil];
    [_collectionView registerNib:cell forCellWithReuseIdentifier:@"RecordColletionCell"];
    _collectionView.alwaysBounceVertical = YES;
    
    self.listRecords = [NSMutableArray array];
    self.listSelectedRecords = [NSMutableArray array];
    
    self.shouldUpdate = YES;
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.viewDidAppearCompleted = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (!self.presentedViewController) {
        self.shouldUpdate = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (_shouldUpdate) {
        _collectionView.collectionViewLayout = [self getLayoutForCollectionView];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        [hud setLabelText:LocStr(@"Loading...")];
        [self requestRecordList];
        [hud hide:YES];
    }
    
    if (self.preplayVideoTitle) {
        [self playVideoWithPreplayVideoTitle];
    }
    
    self.viewDidAppearCompleted = YES;
}


#if 1
- (void)willRotateToInferfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    _collectionView.alpha = 0;
    self.scrollPositionBeforeRotation = CGPointMake(_collectionView.contentOffset.x / _collectionView.contentSize.width,
                                                    _collectionView.contentOffset.y / _collectionView.contentSize.height);
    
    _collectionView.collectionViewLayout = [self getLayoutForCollectionView];
    [_collectionView reloadData];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
        CGPoint newContentOffset = CGPointMake(_scrollPositionBeforeRotation.x * _collectionView.contentSize.width,
                                               _scrollPositionBeforeRotation.y * _collectionView.contentSize.height);
        if (!isnan(newContentOffset.x) && !isnan(newContentOffset.y)) {
            [_collectionView setContentOffset:newContentOffset animated:NO];
        }
    }
    _collectionView.alpha = 1;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    _collectionView.alpha = 0;
    
    if (_collectionView.contentSize.height == 0) {
        self.scrollPositionBeforeRotation = CGPointMake(_collectionView.contentOffset.x / _collectionView.contentSize.width, 0);
    }
    else {
        self.scrollPositionBeforeRotation = CGPointMake(_collectionView.contentOffset.x / _collectionView.contentSize.width,
                                                        _collectionView.contentOffset.y / _collectionView.contentSize.height);
    }
    
    _collectionView.collectionViewLayout = [self getLayoutForCollectionView];
    [_collectionView reloadData];
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
        [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
            
        } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
            CGPoint newContentOffset = CGPointMake(_scrollPositionBeforeRotation.x * _collectionView.contentSize.width,
                                                   _scrollPositionBeforeRotation.y * _collectionView.contentSize.height);
            if (!isnan(newContentOffset.x) && !isnan(newContentOffset.y)) {
                [_collectionView setContentOffset:newContentOffset animated:NO];
            }
            _collectionView.alpha = 1;
        }];
    }
    else {
        _collectionView.alpha = 1;
    }
}
#endif

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - actions

-(void)doRefresh {
    if (!_isEditing) {
        [_refreshControl beginRefreshing];
        [self requestRecordList];
    }
    else {
        [_refreshControl endRefreshing];
    }
}

- (void)editButtonAction {
    self.isEditing = !self.isEditing;
    
    if (_isEditing) {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                   target:self
                                                                   action:@selector(editButtonAction)];
        UIBarButtonItem *selectAllButton = [[UIBarButtonItem alloc] initWithTitle:LocStr(@"Select all")
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:self
                                                                           action:@selector(selectAllAction)];
        selectAllButton.tag = 0;
        self.selectAllButton = selectAllButton;
        
        UIBarButtonItem *spaceButton = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:nil
                                                                       action:nil];
        
        [self.listSelectedRecords removeAllObjects];
        
        if ([UIScreen mainScreen].bounds.size.width >= 480) {
            self.navigationItem.rightBarButtonItems = @[editButton, spaceButton, selectAllButton];
        }
        else {
            self.navigationItem.rightBarButtonItems = @[editButton, selectAllButton];
        }
    }
    else {
        UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                    target:self
                                                                                    action:@selector(editButtonAction)];
        self.navigationItem.rightBarButtonItems = (_listRecords.count > 0 ? @[editButton] : nil);
        [self.navigationItem setLeftBarButtonItem:nil animated:YES];
        
        for (Recording *record in _listRecords) {
            record.isSelected = NO;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_collectionView reloadData];
    });
}

- (void)selectAllAction {
    if (_selectAllButton.tag == 0) {
        _selectAllButton.tag = 1;
        
        [_listSelectedRecords removeAllObjects];
        [_listSelectedRecords addObjectsFromArray:_listRecords];
        
        for (Recording *record in _listSelectedRecords) {
            record.isSelected = YES;
        }
        
        [_selectAllButton setTitle:LocStr(@"Deselect all")];
        
        UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithTitle:LocStr(@"Delete selected")
                                                                         style:UIBarButtonItemStyleDone
                                                                        target:self
                                                                        action:@selector(deleteSelectedButtonAction)];
        deleteButton.tintColor = [UIColor redColor];
        [self.navigationItem setLeftBarButtonItem:deleteButton animated:YES];
    }
    else {
        _selectAllButton.tag = 0;
        
        for (Recording *record in _listSelectedRecords) {
            record.isSelected = NO;
        }
        
        [_listSelectedRecords removeAllObjects];
        [_selectAllButton setTitle:LocStr(@"Select all")];
        [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    }
    
    [_collectionView reloadData];
}

- (void)deleteSelectedButtonAction {
    UIAlertView *alertDeleteAllEvents = [[UIAlertView alloc] initWithTitle:LocStr(@"Do you want to delete the selected videos?")
                                                                   message:nil
                                                                  delegate:self
                                                         cancelButtonTitle:LocStr(@"No")
                                                         otherButtonTitles:LocStr(@"Yes"), nil];
    alertDeleteAllEvents.tag = TAG_ALERT_DELETE_THESE_VIDEOS;
    [alertDeleteAllEvents show];
}


#pragma mark - public methods

- (void)preplayVideoWithTitle:(NSString *)videoTitle {
    self.preplayVideoTitle = [videoTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (_viewDidAppearCompleted && self.preplayVideoTitle) {
        [self playVideoWithPreplayVideoTitle];
    }
}

#pragma mark - handle data

-(void)requestRecordList {
    [_listRecords removeAllObjects];
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
//                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = PathForDir;
    NSFileManager *manager = [NSFileManager defaultManager];
    NSArray *fileList = [manager contentsOfDirectoryAtPath:documentsDirectory
                                                     error:nil];
    for (NSString *fileName in fileList) {
        BOOL isFLV = [[fileName pathExtension] caseInsensitiveCompare:@"flv"] == NSOrderedSame;
        
        if ( isFLV ) {
            Recording *record = [[Recording alloc] init];
            record.recordName = fileName;
            record.isSelected = NO;
            [record initAttributes];
            [_listRecords addObject:record];
        }
    }
    
    [_listRecords sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *d1 = [[[NSFileManager defaultManager] attributesOfItemAtPath:[documentsDirectory stringByAppendingPathComponent:[(Recording*)obj1 recordName]] error:nil] fileCreationDate];
        NSDate *d2 = [[[NSFileManager defaultManager] attributesOfItemAtPath:[documentsDirectory stringByAppendingPathComponent:[(Recording*)obj2 recordName]] error:nil] fileCreationDate];
        return [d1 compare:d2] == NSOrderedAscending ? YES : NO;
    }];
    
    self.isEditing = YES;
    [self editButtonAction];
    [_refreshControl endRefreshing];
}

- (void)playVideoWithPreplayVideoTitle {
    if (_listRecords.count == 0) {
        [self requestRecordList];
        [_collectionView reloadData];
    }
    
    Recording *record = nil;
    NSInteger index = 0;
    
    for (NSInteger i=0; i<_listRecords.count; i++) {
        Recording *recording = [_listRecords objectAtIndex:i];
        NSString *fileName = [recording recordName];
        if ([fileName isEqualToString:self.preplayVideoTitle]) {
            record = recording;
            index = i;
            break;
        }
    }
    if (record) {
        PlaylistInfo *clipInfo = [[PlaylistInfo alloc] init];
        clipInfo.urlFile = record.fullPath;
        
        PlaybackViewController *playbackViewController = [[PlaybackViewController alloc] init];
        playbackViewController.clipInfo = clipInfo;
        playbackViewController.playbackVCDelegate = self;
        playbackViewController.recordSelectedIndex = index;
        
        
        [self presentViewController:playbackViewController animated:YES completion:nil];
    }
    self.preplayVideoTitle = nil;
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
//    NSLog(@"%s _listRecords.count:%lu", __FUNCTION__, (unsigned long)_listRecords.count);
    
    return _listRecords.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RecordColletionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RecordColletionCell"
                                                                          forIndexPath:indexPath];
    
    Recording *record = (Recording*)[_listRecords objectAtIndex:indexPath.row];
    [cell showRecording:record];
    
//    NSLog(@"\n\nn\n\n\n\n\n\nn\n\n\nja ajjajlaklakj");
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    RecordColletionCell *cell = (RecordColletionCell*)[collectionView cellForItemAtIndexPath:indexPath];
    Recording *record = (Recording*)[_listRecords objectAtIndex:indexPath.row];
    
    if (_isEditing) {
        record.isSelected = !record.isSelected;
        [cell showRecording:record];
        if (record.isSelected) {
            [_listSelectedRecords addObject:record];
        }
        else {
            [_listSelectedRecords removeObject:record];
        }
        
        if (_listSelectedRecords.count) {
            UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithTitle:LocStr(@"Delete selected")
                                                                             style:UIBarButtonItemStyleDone
                                                                            target:self
                                                                            action:@selector(deleteSelectedButtonAction)];
            deleteButton.tintColor = [UIColor redColor];
            [self.navigationItem setLeftBarButtonItem:deleteButton animated:YES];
        }
        else {
            [self.navigationItem setLeftBarButtonItem:nil animated:YES];
        }
        
        return;
    }
    
    self.shouldUpdate = NO;
    
    PlaylistInfo *clipInfo = [[PlaylistInfo alloc] init];
    clipInfo.urlFile = record.fullPath;
    clipInfo.imgSnapshot = cell.snapshotImage.image;
    
    PlaybackViewController *playbackViewController = [[PlaybackViewController alloc] init];
    playbackViewController.clipInfo = clipInfo;
    playbackViewController.playbackVCDelegate = self;
    playbackViewController.recordSelectedIndex = [indexPath item];
    
    [self presentViewController:playbackViewController animated:YES completion:nil];
}

- (UICollectionViewLayout*)getLayoutForCollectionView {
    CGSize size = [UIScreen mainScreen].bounds.size;
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
        if (size.width < size.height) {
            CGFloat h = size.width;
            size.width = size.height;
            size.height = h;
        }
    }
    else if (size.width > size.height) {
        CGFloat h = size.width;
        size.width = size.height;
        size.height = h;
    }
    
    CGFloat width = 140;
    CGFloat minimumLineSpacing = 5;
    CGFloat minimumInteritemSpacing = 5;
    if (size.width >= 768) {
        minimumLineSpacing = 10;
        minimumInteritemSpacing = 10;
    }
    
    NSInteger columnPerRow = 2;
    while (columnPerRow < 6) {
        width = (size.width - (columnPerRow + 1) * minimumInteritemSpacing) / columnPerRow;
        if (width >= 140 && width <= 250) {
            break;
        }
        columnPerRow++;
    }
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    layout.minimumLineSpacing = minimumLineSpacing;
    layout.minimumInteritemSpacing = minimumInteritemSpacing;
    _collectionView.contentInset = UIEdgeInsetsMake(minimumInteritemSpacing, minimumInteritemSpacing, minimumInteritemSpacing, minimumInteritemSpacing);
    
    width = (size.width - (columnPerRow + 1) * minimumInteritemSpacing) / columnPerRow;
    layout.itemSize = CGSizeMake(width, width*9.0/16 + 45);
    
    return layout;
}

#pragma mark - AlertView Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    
    switch (alertView.tag) {
        case TAG_ALERT_DELETE_THESE_VIDEOS:
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            [hud setLabelText:LocStr(@"Deleting events...")];
            
            NSFileManager *manager = [NSFileManager defaultManager];
            
            for (Recording *record in _listSelectedRecords) {
                NSString *fullName = record.fullPath;
                [manager removeItemAtPath:fullName error:nil];
                [manager removeItemAtPath:[fullName stringByReplacingOccurrencesOfString:@"flv" withString:@"png" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [fullName length])] error:nil];
            }
            
            [_listRecords removeObjectsInArray:_listSelectedRecords];
            [_listSelectedRecords removeAllObjects];
            
            if (_listRecords.count == 0) {
                self.isEditing = YES;
                [self editButtonAction];
            }
            else {
                _selectAllButton.tag = 0;
                [_selectAllButton setTitle:LocStr(@"Select all")];
                [self.navigationItem setLeftBarButtonItem:nil animated:YES];
                [_collectionView reloadData];
            }
            
            [hud hide:YES];
        }

        default:
            break;
    }
}

#pragma mark - PlaybackDelegate

- (void)deleteVideoAtIndex:(NSInteger)index {
    if (index >= 0 && index < _listRecords.count) {
        NSFileManager *manager = [NSFileManager defaultManager];
        Recording *record = _listRecords[index];
        NSString *fullName = record.fullPath;
        [manager removeItemAtPath:fullName error:nil];
        [manager removeItemAtPath:[fullName stringByReplacingOccurrencesOfString:@"flv" withString:@"png" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [fullName length])] error:nil];
        
        [_listRecords removeObjectAtIndex:index];
        [_collectionView reloadData];
    }
}

@end
