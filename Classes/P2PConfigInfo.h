//
//  P2PConfigInfo.h
//  App
//
//  Created by Developer on 1/21/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

#if !TARGET_IPHONE_SIMULATOR
#import <ReliableChannel/ReliableChannel.h>
#endif

@interface P2PConfigInfo : NSObject

// Shared
@property (nonatomic, copy) NSString *strEncKey;

// Local | reponse from camera.
@property (nonatomic) uint32_t portComm;
@property (nonatomic, copy) NSString *strIpAddress;

// Remote
@property (nonatomic) int socketFd; //socket_fd
@property (nonatomic) int upnp; // upnp response from camera fw version 01_10

@property (nonatomic, copy) NSString *strPublicIP;
@property (nonatomic) uint16_t publicPort;

// Relay
@property (nonatomic, copy) NSString *strRanNo;         // RANDOM_NUMBER
@property (nonatomic, copy) NSString *strRelayIP;
@property (nonatomic) uint16_t sp;

@property (nonatomic, copy) NSString *streamName;

- (void)strRanNoFromHexString:(NSString *)hexStr;
- (void)strEncKeyFromHexString:(NSString *)hexStr;
- (NSString *)getIPAddress;

+ (NSDictionary *)fetchSSIDInfo;

@end
