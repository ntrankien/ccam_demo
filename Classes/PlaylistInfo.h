//
//  PlaylistInfo.h
//  Dev
//
//  Created by Dev on 6/9/13.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

#define PLAYBACK_DURATION_MAX 60*60

@interface PlaylistInfo : NSObject

@property (nonatomic, strong) UIImage *imgSnapshot;
@property (nonatomic, copy) NSString *macAddr;
@property (nonatomic, copy) NSString *urlImage;
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *urlFile;
@property (nonatomic, copy) NSString *alertType;
@property (nonatomic, copy) NSString *alertVal;
@property (nonatomic, copy) NSString *registrationID;
@property (nonatomic, copy) NSString *cameraName;
@property (nonatomic, strong) NSMutableArray *nextClips;
@property (nonatomic) NSInteger storageMode;
@property (nonatomic, copy) NSString *ipAddress;
@property (nonatomic, copy) NSString *fileName;

- (NSString *)getAlertVal;
- (NSString *)getAlertType;
- (NSDate *)getTimeCode;
- (BOOL)isLastClip;
- (BOOL)containsClip:(NSString *)aString;

- (int)getDurationIfLastClip;

@end
