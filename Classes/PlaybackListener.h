//
//  PlaybackListener.h
//
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology


#include <H264MediaPlayer/mediaplayer.h>
#import "PlayerCallbackHandler.h"

class PlaybackListener: public MediaPlayerListener
{
public:
    PlaybackListener(id<PlayerCallbackHandler> handler);
    ~PlaybackListener();
    void notify(int msg, int ext1, int ext2);
    int getNextClip(char**);
    
    
    void updateClips(NSMutableArray *  newClips);
    void updateFinalClipCount(int clip_count); 
    
private:
    
    id<PlayerCallbackHandler> mHandler; 
    
    NSMutableArray *mClips;
    int current_clip_index;
    int final_number_of_clips;

};

