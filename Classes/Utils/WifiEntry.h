//
//  WifiEntry.h
//  App
//
//  Created by Developer on 6/29/12.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#define VERSION_1 @"1.0"
#define VERSION_1_1 @"1.1"

@interface WifiEntry : NSObject

@property (nonatomic, copy) NSString *ssidWithQuotes;
@property (nonatomic, copy) NSString *bssid;
@property (nonatomic, copy) NSString *authMode;
@property (nonatomic, copy) NSString *encryptType; //version 1.1
@property (nonatomic, copy) NSString *quality;

@property (nonatomic) int signalLevel;
@property (nonatomic) int noiseLevel;
@property (nonatomic) int channel;

- (id)initWithSSID:(NSString *)ssid;
- (NSString*)getSSIDWithoutQuotes;

@end
