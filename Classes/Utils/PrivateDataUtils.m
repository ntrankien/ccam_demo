//
//  PrivateDataUtils.m
//  App
//
//  Created by DienNT on 9/15/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "PrivateDataUtils.h"
#import "NSString+AES.h"
#import "PublicDefine.h"

@implementation PrivateDataUtils

+ (void)saveUsername:(NSString *)username
{
    [[NSUserDefaults standardUserDefaults] setObject:[username stringAfterEncrypt] forKey:@"PortalUsername"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)savePassword:(NSString *)password
{
    [[NSUserDefaults standardUserDefaults] setObject:[password stringAfterEncrypt] forKey:@"PortalPassword"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)saveApiKey:(NSString *)apiKey
{
    [[NSUserDefaults standardUserDefaults] setObject:[apiKey stringAfterEncrypt] forKey:@"PortalApiKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)saveEmail:(NSString *)email
{
    [[NSUserDefaults standardUserDefaults] setObject:[email stringAfterEncrypt] forKey:@"PortalUseremail"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)saveStreamingSSID:(NSString *)streamingSSID
{
    [[NSUserDefaults standardUserDefaults] setObject:[streamingSSID stringAfterEncrypt] forKey:@"string_Streaming_SSID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)saveHomeSSID:(NSString *)homeSSID
{
    [[NSUserDefaults standardUserDefaults] setObject:[homeSSID stringAfterEncrypt] forKey:@"home_ssid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)saveServerName:(NSString *)serverName
{
    [[NSUserDefaults standardUserDefaults] setObject:[serverName stringAfterEncrypt] forKey:@"name_server"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)savePolicyAccepted:(NSString *)policyVersion
{
    [[NSUserDefaults standardUserDefaults] setObject:[policyVersion stringAfterEncrypt] forKey:@"accepted_policy"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)username
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"PortalUsername"] stringAfterDecrypt];
}

+ (NSString *)password
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"PortalPassword"] stringAfterDecrypt];
}

+ (NSString *)apiKey
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"PortalApiKey"] stringAfterDecrypt];
}

+ (NSString *)email
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"PortalUseremail"] stringAfterDecrypt];
}

+ (NSString *)streamingSSID
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"string_Streaming_SSID"] stringAfterDecrypt];
}

+ (NSString *)homeSSID
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"home_ssid"] stringAfterDecrypt];
}

+ (NSString *)serverName
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"name_server"] stringAfterDecrypt];
}

+ (NSString *)policyAccepted
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"accepted_policy"] stringAfterDecrypt];
}

+ (NSString *)passwordAfterLogout {
    NSString *pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"PortalPasswordLogout"];
    if (pass) {
        pass = [pass stringAfterDecrypt];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PortalPasswordLogout"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return pass;
}

+ (void)removePassword {
    NSString *pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"PortalPassword"];
    if (pass) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PortalPassword"];
        [[NSUserDefaults standardUserDefaults] setObject:pass forKey:@"PortalPasswordLogout"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (NSString*)getCurrentUserSubscriptionPlan {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"UserSubscriptionPlan_"];
}

+ (void)setCurrentUserSubscriptionPlan:(NSString*)plan {
    if (!plan || [plan isKindOfClass:[NSNull class]]) {
        plan = @"";
    }
    [[NSUserDefaults standardUserDefaults] setObject:plan forKey:@"UserSubscriptionPlan_"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)removeCurrentUserSubscriptionPlan {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserSubscriptionPlan_"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)changeUpdatedDateForApplyPlan {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]]
                                              forKey:@"ApplyUserPlanDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (double)getUpdatedDateForApplyPlan {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"ApplyUserPlanDate"];
}

#pragma mark - for open from another app

+ (NSString *)decryptStringForKey:(NSString*)key {
    return [key stringAfterDecrypt];
}

+ (NSString *)apiKeyWithEncrypt {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"PortalApiKey"];
}

+ (NSString *)usernameWithEncrypt {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"PortalUsername"];
}

@end
