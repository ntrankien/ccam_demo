//
//  NSString+AES.h
//  App
//
//  Created by DienNT on 9/15/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

@interface NSString (AES)

// Return a string after encrypted
- (NSString *)stringAfterEncrypt;

// Return a root string before encrypted
- (NSString *)stringAfterDecrypt;

@end
