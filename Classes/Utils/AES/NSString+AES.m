//
//  NSString+AES.m
//  App
//
//  Created by DienNT on 9/15/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "NSString+AES.h"
#import "NSData+AESCrypt.h"

static char keys[] = {0x53, 0x75, 0x70, 0x65, 0x72, 0x2D, 0x4C, 0x6F, 0x76, 0x65, 0x6C, 0x79, 0x44, 0x75, 0x63, 0x6B};

@implementation NSString (AES)


+ (NSString *) privateKey
{
    NSMutableString *newString = [NSMutableString string];
    
    for (int i = 0; i < sizeof(keys); i++) {
        [newString appendFormat:@"%c", keys[i]];
    }
    return newString;
}


// Return a string after encrypted
- (NSString *)stringAfterEncrypt
{
    NSData *rootData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptedData = [rootData EncryptAES:[NSString privateKey]];
    return [encryptedData base64Encoding];
}

// Return a root string before encrypted
- (NSString *)stringAfterDecrypt
{
    NSData *encryptedData = [NSData base64DataFromString:self];
    NSData *decryptedData = [encryptedData DecryptAES:[NSString privateKey]];
    return [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
}

@end
