//
//  NSData+AESCrypt.h
//
//  AES128Encryption + Base64Encoding
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

@interface NSData (AESCrypt)

+ (NSData *)gzipData:(NSData*)pUncompressedData;

+ (NSData *)base64DataFromString:(NSString *)string;

- (NSData *)AES128EncryptWithKey:(NSString *)key;

- (NSData *)EncryptAES:(NSString *)key;

- (NSData *)DecryptAES:(NSString *)key;

- (NSString *)base64Encoding;

@end
