//
//  WifiListParser.m
//  App
//
//  Created by Developer on 6/29/12.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "WifiListParser.h"

@interface WifiListParser ()

@property (nonatomic, copy) void (^completionBlock)(NSArray *wifiList);
@property (nonatomic, copy) void (^failureBlock)(NSError *parseError);

@property (nonatomic, strong) NSMutableArray *wifiLists;
@property (nonatomic, assign) BOOL isErrorParser;

@property (nonatomic, strong) NSXMLParser *xmlParser;
@property (nonatomic, strong) NSMutableString *currentStringValue;
@property (nonatomic, strong) WifiEntry *currentEntry;
@property (nonatomic, copy) NSString *listVersion;

@end

#define WIFI_CMD @"%@"

NSString *WIFI_LIST_VERSION  = @"wifi_list";
NSString *WIFI_LIST_VERSION_ATT = @"version";

NSString *NUM_ENTRY = @"num_entries";
NSString *WIFI_ENTRY = @"wifi";
NSString *WIFI_ENTRY_SSID = @"ssid";
NSString *WIFI_ENTRY_BSSID = @"bssid";
NSString *WIFI_ENTRY_AUTH_MODE = @"auth_mode";
NSString *WIFI_ENTRY_QUALITY = @"quality";
NSString *WIFI_ENTRY_SIGNAL_LEVEL = @"signal_level";
NSString *WIFI_ENTRY_NOISE_LEVEL = @"noise_level";
NSString *WIFI_ENTRY_CHANNEL = @"channel";

@implementation WifiListParser

- (id)initWithNewCmdFlag:(BOOL)flag
{
    self = [super init];
    
    if (self) {
        if (flag) {
            WIFI_LIST_VERSION  = @"wl";
            WIFI_LIST_VERSION_ATT = @"v";
            
            NUM_ENTRY = @"n";
            WIFI_ENTRY = @"w";
            WIFI_ENTRY_SSID = @"s";
            WIFI_ENTRY_BSSID = @"b";
            WIFI_ENTRY_AUTH_MODE = @"a";
            WIFI_ENTRY_QUALITY = @"q";
            WIFI_ENTRY_SIGNAL_LEVEL = @"si";
            WIFI_ENTRY_NOISE_LEVEL = @"nl";
            WIFI_ENTRY_CHANNEL = @"ch";
        }
        else {
            WIFI_LIST_VERSION  = @"wifi_list";
            WIFI_LIST_VERSION_ATT  = @"version";
            
            NUM_ENTRY  = @"num_entries";
            WIFI_ENTRY = @"wifi";
            WIFI_ENTRY_SSID = @"ssid";
            WIFI_ENTRY_BSSID = @"bssid";
            WIFI_ENTRY_AUTH_MODE = @"auth_mode";
            WIFI_ENTRY_QUALITY = @"quality";
           WIFI_ENTRY_SIGNAL_LEVEL = @"signal_level";
            WIFI_ENTRY_NOISE_LEVEL = @"noise_level";
            WIFI_ENTRY_CHANNEL = @"channel";
        }
    }
    
    return self;
}

- (void)parseData:(NSData *)xmlWifiList completion:(void (^)(NSArray *wifiList))completionBlock;
{
    self.completionBlock = completionBlock;
	self.xmlParser = [[NSXMLParser alloc] initWithData:xmlWifiList];
    
	[_xmlParser setDelegate:self];
    [_xmlParser setShouldProcessNamespaces:NO];
    [_xmlParser setShouldReportNamespacePrefixes:NO];
    [_xmlParser setShouldResolveExternalEntities:NO];
    [_xmlParser parse];
}

- (void)parseData:(NSData *)xmlWifiList completion:(void (^)(NSArray *wifiList))completionBlock failure:(void (^)(NSError *parseError))failureBlock;
{
    self.completionBlock = completionBlock;
    self.failureBlock = failureBlock;
    
	self.xmlParser = [[NSXMLParser alloc] initWithData:xmlWifiList];
    
	[_xmlParser setDelegate:self];
    [_xmlParser setShouldProcessNamespaces:NO];
    [_xmlParser setShouldReportNamespacePrefixes:NO];
    [_xmlParser setShouldResolveExternalEntities:NO];
    [_xmlParser parse];
}

#pragma mark - NSXMLParserDelegate methods

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    DLog(@"Document started");
    self.isErrorParser = YES;
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DLog(@"Error: %@", [parseError localizedDescription]);
//    _failureBlock(parseError);
    self.isErrorParser = NO;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:WIFI_LIST_VERSION]) {
        self.listVersion = [attributeDict objectForKey:WIFI_LIST_VERSION_ATT];
    }
    
    if ([elementName isEqualToString:NUM_ENTRY]) {
        self.currentStringValue = nil;
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY]) {
        self.currentEntry = [[WifiEntry alloc] init];
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY_SSID]) {
        self.currentStringValue = nil;
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY_BSSID]) {
        self.currentStringValue = nil;
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY_AUTH_MODE]) {
        self.currentStringValue = nil;
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY_QUALITY]) {
        self.currentStringValue = nil;
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	
    if ([elementName isEqualToString:NUM_ENTRY]) {
        if ( _currentStringValue ) {
            int numEntry = [_currentStringValue intValue];
            self.wifiLists = [[NSMutableArray alloc] initWithCapacity:numEntry];
        }
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY]) {
        if ( _wifiLists && _currentEntry ) {
            [_wifiLists addObject:_currentEntry];
        }
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY_SSID]) {
        if ( _currentEntry ) {
            _currentEntry.ssidWithQuotes = _currentStringValue;
        }
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY_BSSID]) {
        if ( _currentEntry ) {
            _currentEntry.bssid = _currentStringValue;
        }
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY_AUTH_MODE])
    {
        if ( _currentEntry ) {
            if ( !_currentStringValue || [_currentStringValue isEqualToString:@"OPEN"]) {
                _currentEntry.authMode = @"open";
            }
            else if ([_currentStringValue hasPrefix:@"WPA"]) {
                _currentEntry.authMode = @"wpa";
            }
            else if ([_currentStringValue hasPrefix:@"WEP"]) {
                _currentEntry.authMode = @"wep";
            }
            else if ([_currentStringValue isEqualToString:@"SHARED"]) {
                _currentEntry.authMode = @"shared";
            }
        }
    }
    
    if ([elementName isEqualToString:WIFI_ENTRY_QUALITY]) {
        if ( _currentEntry ) {
            if ( _currentStringValue ) {
                _currentEntry.quality = _currentStringValue;
            }
        }
    }

    /*
    if ([elementName isEqualToString:WIFI_ENTRY_SIGNAL_LEVEL])
    {
        //Do nothing for now
    }
    if ([elementName isEqualToString:WIFI_ENTRY_NOISE_LEVEL])
    {
        //Do nothing for now
    }
    if ([elementName isEqualToString:WIFI_ENTRY_CHANNEL])
    {
        //Do nothing for now
    }
    */

    //reset for the next element
    self.currentStringValue = nil;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (!_currentStringValue) {
        // currentStringValue is an NSMutableString instance variable
        self.currentStringValue = [[NSMutableString alloc] initWithCapacity:50];
    }
    [_currentStringValue appendString:string];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    _completionBlock(_wifiLists);
}

- (BOOL)checkStatusParserXML
{
    return self.isErrorParser;
}

@end
