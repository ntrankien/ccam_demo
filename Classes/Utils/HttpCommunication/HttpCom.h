//
//  HttpCom.h
//  App
//
//  Created by Developer on 9/1/14.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>
#import <CameraScanner/HttpCommunication.h>

@interface HttpCom : NSObject {
    HttpCommunication *_comWithDevice;
}
@property (nonatomic, strong) HttpCommunication *comWithDevice;

- (id)init;
+ (HttpCom *) instance;
@end
