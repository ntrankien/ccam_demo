//
//  HttpCom.m
//  App
//
//  Created by Developer on 9/1/14.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "HttpCom.h"

@implementation HttpCom
@synthesize comWithDevice = _comWithDevice;

+ (HttpCom *) instance
{
    static HttpCom *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[HttpCom alloc] init];
    });
    return _sharedInstance;
}

-(id) init

{
    self = [super init];
    if (self) {
        _comWithDevice = [[HttpCommunication alloc] initWithNewFlag:YES];
    }
    return self;
}
@end
