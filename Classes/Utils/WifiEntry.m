//
//  WifiEntry.m
//  App
//
//  Created by Developer on 6/29/12.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "WifiEntry.h"

@implementation WifiEntry

- (id)initWithSSID:(NSString *)ssid
{
	self = [super init];
	self.ssidWithQuotes = ssid;
	return self;
}

- (NSString*)getSSIDWithoutQuotes {
    NSRange noQoute = NSMakeRange(1, _ssidWithQuotes.length - 2);
    NSString *wifiName = [_ssidWithQuotes substringWithRange:noQoute];
    return wifiName;
}

@end
