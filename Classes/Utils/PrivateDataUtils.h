//
//  PrivateDataUtils.h
//  App
//
//  Created by DienNT on 9/15/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

@interface PrivateDataUtils : NSObject

+ (void)saveUsername:(NSString *)username;
+ (void)savePassword:(NSString *)password;
+ (void)saveApiKey:(NSString *)apiKey;
+ (void)saveEmail:(NSString *)email;
+ (void)saveStreamingSSID:(NSString *)streamingSSID;
+ (void)saveHomeSSID:(NSString *)homeSSID;
+ (void)saveServerName:(NSString *)serverName;
+ (void)savePolicyAccepted:(NSString *)policyVersion;

+ (NSString *)username;
+ (NSString *)password;
+ (NSString *)apiKey;
+ (NSString *)email;
+ (NSString *)streamingSSID;
+ (NSString *)homeSSID;
+ (NSString *)serverName;
+ (NSString *)passwordAfterLogout;
+ (NSString *)policyAccepted;

+ (void)removePassword;

+ (NSString*)getCurrentUserSubscriptionPlan;
+ (void)setCurrentUserSubscriptionPlan:(NSString*)plan;
+ (void)removeCurrentUserSubscriptionPlan;
+ (void)changeUpdatedDateForApplyPlan;
+ (double)getUpdatedDateForApplyPlan;

+ (NSString *)decryptStringForKey:(NSString*)key;
+ (NSString *)apiKeyWithEncrypt;
+ (NSString *)usernameWithEncrypt;

@end
