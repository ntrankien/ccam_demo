//
//  WifiListParser.h
//  App
//
//  Created by Developer on 6/29/12.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "WifiEntry.h"

@interface WifiListParser : NSObject <NSXMLParserDelegate>

- (id)initWithNewCmdFlag:(BOOL)flag;
- (void)parseData:(NSData *)xmlWifiList completion:(void (^)(NSArray *wifiList))completionBlock;
- (void)parseData:(NSData *)xmlWifiList completion:(void (^)(NSArray *wifiList))completionBlock failure:(void (^)(NSError *parseError))failureBlock;
- (BOOL)checkStatusParserXML;

@end
