//
//  P2PConfigInfo.m
//  App
//
//  Created by Developer on 1/21/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "P2PConfigInfo.h"

#if !TARGET_IPHONE_SIMULATOR
#include <ifaddrs.h>
#include <arpa/inet.h>

#import <SystemConfiguration/CaptiveNetwork.h>
#endif

@implementation P2PConfigInfo

- (void)strRanNoFromHexString:(NSString *)hexStr
{
    self.strRanNo = [self stringFromHexString:hexStr];
    DLog(@"%s -> %@", __FUNCTION__, _strRanNo);
}

- (void)strEncKeyFromHexString:(NSString *)hexStr
{
    self.strEncKey = [self stringFromHexString:hexStr];
    DLog(@"%s -> %@", __FUNCTION__, _strEncKey);
}

- (NSString *)stringFromHexString:(NSString *)hexString {
    NSMutableString *newString = [[NSMutableString alloc] init];
    int i = 0;
    
    while ( i <= (hexString.length - 2) ) { // Make sure "Range {0, 2} out of bounds; string length 1" is not happened!
        NSString *hexChar = [hexString substringWithRange: NSMakeRange(i, 2)];
        int value = 0;
        sscanf([hexChar cStringUsingEncoding:NSASCIIStringEncoding], "%x", &value);
        [newString appendFormat:@"%c", (char)value];
        i += 2;
    }
    
    return newString;
}

#if !TARGET_IPHONE_SIMULATOR

- (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

+ (NSDictionary *)fetchSSIDInfo
{
    NSArray *interfaceNames = CFBridgingRelease(CNCopySupportedInterfaces());
    NSLog(@"%s: Supported interfaces: %@", __func__, interfaceNames);
    
    NSDictionary *SSIDInfo;
    for (NSString *interfaceName in interfaceNames) {
        SSIDInfo = CFBridgingRelease(
                                     CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName));
        NSLog(@"%s: %@ => %@", __func__, interfaceName, SSIDInfo);
        
        BOOL isNotEmpty = (SSIDInfo.count > 0);
        if (isNotEmpty) {
            break;
        }
    }
    return SSIDInfo;
}

#endif

@end
