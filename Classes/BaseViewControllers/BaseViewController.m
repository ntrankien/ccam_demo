//
//  BaseViewController.m
//  Cinatic
//
//  Created by Sang Nguyen on 10/08/2016.
//  Copyright © 2016 Cinatic Connected Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "PublicDefine.h"

@interface BaseViewController ()
@property (nonatomic, strong) NSString *navTitleString;
@property (nonatomic, strong) UIViewController *moreNavVC;
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self viewWillStyle];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Utility methods

- (void)viewWillStyle{ }

- (void)setDefaultBackButton {
    UIBarButtonItem* backBarBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_arrow_left"]
                                                               style:UIBarButtonItemStylePlain
                                                              target:self action:@selector(backButtonTapped:)];
    self.navigationItem.leftBarButtonItem = backBarBtn;
}

- (void)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
