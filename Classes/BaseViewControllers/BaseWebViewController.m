//
//  BaseWebViewController.m
//  Cinatic
//
//  Created by Tran Kien Nghi on 11/7/15.
//  Copyright © 2015 Cinatic Connected Ltd. All rights reserved.
//

#import "BaseWebViewController.h"
#import "MBProgressHUD.h"

@interface BaseWebViewController ()<UIWebViewDelegate>

@property (nonatomic, strong) NSString* urlString;

@end

@implementation BaseWebViewController

+ (instancetype)webViewURlString:(NSString*)urlString{
    id baseViewVC = [[[self class] alloc] init];
    
    ((BaseWebViewController*)baseViewVC).urlString = urlString;
    
    return baseViewVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _webView.delegate = self;
    
    if (_urlString.length) {
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]];
        [_webView loadRequest:request];
    }
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    MBProgressHUD *hub = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hub setDetailsLabelText:LocStr(@"Loading ...")];
    hub.userInteractionEnabled = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
