//
//  BaseWebViewController.h
//  Cinatic
//
//  Created by Tran Kien Nghi on 11/7/15.
//  Copyright © 2015 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseWebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

+ (instancetype)webViewURlString:(NSString*)urlString;

@end
