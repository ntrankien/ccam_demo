//
//  BaseTableViewController.m
//  Cinatic
//
//  Created by Sang Nguyen on 10/08/2016.
//  Copyright © 2016 Cinatic Connected Ltd. All rights reserved.
//

#import "BaseTableViewController.h"
#import "PublicDefine.h"

@interface BaseTableViewController ()
@property (nonatomic, strong) NSString *navTitleString;
@property (nonatomic, strong) UIViewController *moreNavVC;
@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self viewWillStyle];
}

- (void)viewWillStyle{ }

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

@end
