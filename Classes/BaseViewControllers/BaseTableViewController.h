//
//  BaseTableViewController.h
//  Cinatic
//
//  Created by Sang Nguyen on 10/08/2016.
//  Copyright © 2016 Cinatic Connected Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UITableViewController

- (void)viewWillStyle;

@end
