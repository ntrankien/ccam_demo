//
//  TalkbackManager.m
//  LucyDemo
//
//  Created by Tran Kien Nghi on 5/23/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "TalkbackManager.h"
#import <H264MediaPlayer/H264MediaPlayer.h>
#import "HttpCom.h"
#import "AudioOutStreamer.h"

#define kIsP2PEnabled 1

#if kIsP2PEnabled
#import "AudioOutStreamerP2P.h"
#endif

@interface TalkbackManager()<AudioOutStreamerDelegate, AudioOutStreamerP2PDelegate>

@property (nonatomic, strong) AudioOutStreamer *audioOut;


@property (nonatomic, strong) AudioOutStreamerP2P *audioOutStreamerP2P;

@end;

@implementation TalkbackManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t p = 0;
    
    __strong static id _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

- (void)enableTalkbackWithCamCh:(CamChannel *)ch mode:(int)p2p completion:(void (^)(BOOL success))completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        BOOL isSuccess = NO;
        
        if (!p2p) {
            NSInteger t = 3;
            
            while (t-- > 0) {
                NSString *command = @"audio_out1";
                NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:command];
                DLog(@"%s Send to camera Cmd: %@, response:%@", __FUNCTION__, command, response);
                
                if (response && [response isEqualToString:@"audio_out1: 0"]) {
                    isSuccess = YES;
                    break;
                }
            }
        }
        else {
            isSuccess = YES;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.walkieTalkieEnabled = isSuccess;
            
            if (isSuccess) {
                [self enableLocalPTT:_walkieTalkieEnabled camChannel:ch mode:p2p completion:^(BOOL success) {
                    if (completion) {
                        completion(success);
                    }
                }];
            }
            else{
                if (completion) {
                    completion(NO);
                }
            }
        });
    });
}

- (void)disableTalkbackWithCamCh:(CamChannel *)ch mode:(int)p2p completion:(void (^)(BOOL success))completion
{
    
    BOOL isSuccess = NO;
    
    if (!p2p) {
        NSInteger t = 1;
        while (t-- > 0) {
            NSString *command = @"audio_out0";
            NSString *response = [[HttpCom instance].comWithDevice sendCommandAndBlock:command];
            DLog(@"%s Send to camera Cmd: %@, response:%@", __FUNCTION__, command, response);
            
            if (response && [response isEqualToString:@"audio_out0: 0"]) {
                isSuccess = YES;
                break;
            }
        }
    }
    else {
        isSuccess = YES;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.walkieTalkieEnabled = !isSuccess;
        
        if (isSuccess) {
            [self enableLocalPTT:_walkieTalkieEnabled camChannel:ch mode:p2p completion:^(BOOL success) {
                if (completion) {
                    completion(success);
                }
            }];
        }
        else{
            if (completion) {
                completion(NO);
            }
        }
    });
}

- (void)enableLocalPTT:(BOOL)walkieTalkieEnable completion:(void (^)(BOOL success))completion{
    DLog(@"%s walkieTalkieEnable: %d", __FUNCTION__, walkieTalkieEnable);
    if ( walkieTalkieEnable ) {
        // 1. Starting

        // Mute audio to MediaPlayer lib
        MediaPlayer::Instance()->setPlayOption(MEDIA_STREAM_AUDIO_MUTE);
        
#if kIsP2PEnabled
        if (_isP2PMode) {
//            // audioOutStreamerP2P
//            if (self.audioOutStreamerP2P) {
//                //Make sure all local P2P streams stopped before start a new one
//                [self.audioOutStreamerP2P stopP2PTalkback];
//                self.audioOutStreamerP2P.audioOutStreamerP2PDelegate = nil;
//                self.audioOutStreamerP2P = nil;
//            }
//            
////            self.audioOutStreamerP2P = [[AudioOutStreamerP2P alloc] initWithP2PSession:_selectedChannel.profile.p2pSess];
//            self.audioOutStreamerP2P = [[AudioOutStreamerP2P alloc] initWithCamCh:_ch];
//            self.audioOutStreamerP2P.audioOutStreamerP2PDelegate = self;
//            [_audioOutStreamerP2P startRecordingSound];
//            
//            if(completion) {
//                completion(YES);
//            }
        }
        else {
#endif
            if (self.audioOut) {
                [self.audioOut disconnectFromAudioSocket];
                self.audioOut.audioOutStreamerDelegate = nil;
                self.audioOut = nil;
            }
            
            // Init connectivity to Camera via socket & prevent loss of audio data
            self.audioOut = [[AudioOutStreamer alloc] initWithDeviceIp:[HttpCom instance].comWithDevice.device_ip
                                                            andPTTport:IRABOT_AUDIO_RECORDING_PORT];
            [_audioOut startRecordingSound];
            
            if ( _audioOut ) {
                DLog(@"Connect to Audio Soccket in setEnablePtt function");
                [_audioOut connectToAudioSocket];
                _audioOut.audioOutStreamerDelegate = self;
                if(completion) {
                    completion(YES);
                }
            }
            else {
                DLog(@"NEED to enable audioOut now BUT audioOut = nil!!!");
                if(completion) {
                    completion(NO);
                }
            }
#if kIsP2PEnabled
        }
#endif
    }
    else {
        // 2. Stopping
        MediaPlayer::Instance()->setPlayOption(MEDIA_STREAM_AUDIO_NOT_MUTE);

        if (_isP2PMode) { // maybe no need
            if ( _audioOutStreamerP2P ) {
                [_audioOutStreamerP2P stopP2PTalkback];
                self.audioOutStreamerP2P = nil;
            }
        }
        else {
            if ( _audioOut ) {
                [_audioOut disconnectFromAudioSocket];
                self.audioOut = nil;
            }
        }
        
        if(completion) {
            completion(YES);
        }
    }
}

- (void)enableLocalPTT:(BOOL)walkieTalkieEnable camChannel:(CamChannel *)ch mode:(int)p2p completion:(void (^)(BOOL success))completion{
    DLog(@"%s walkieTalkieEnable: %d", __FUNCTION__, walkieTalkieEnable);
    if ( walkieTalkieEnable ) {
        // 1. Starting
        
        // Mute audio to MediaPlayer lib
        MediaPlayer::Instance()->setPlayOption(MEDIA_STREAM_AUDIO_MUTE);
        
#if kIsP2PEnabled
        if (p2p) {
            // audioOutStreamerP2P
            if (self.audioOutStreamerP2P) {
                //Make sure all local P2P streams stopped before start a new one
                [self.audioOutStreamerP2P stopP2PTalkback];
                self.audioOutStreamerP2P.audioOutStreamerP2PDelegate = nil;
                self.audioOutStreamerP2P = nil;
            }
            
            //            self.audioOutStreamerP2P = [[AudioOutStreamerP2P alloc] initWithP2PSession:_selectedChannel.profile.p2pSess];
            self.audioOutStreamerP2P = [[AudioOutStreamerP2P alloc] initWithCamCh:ch];
            self.audioOutStreamerP2P.audioOutStreamerP2PDelegate = self;
            [_audioOutStreamerP2P startRecordingSound];
            
            if(completion) {
                completion(YES);
            }
        }
        else {
#endif
            if (self.audioOut) {
                [self.audioOut disconnectFromAudioSocket];
                self.audioOut.audioOutStreamerDelegate = nil;
                self.audioOut = nil;
            }
            
            // Init connectivity to Camera via socket & prevent loss of audio data
            self.audioOut = [[AudioOutStreamer alloc] initWithDeviceIp:[HttpCom instance].comWithDevice.device_ip
                                                            andPTTport:IRABOT_AUDIO_RECORDING_PORT];
            [_audioOut startRecordingSound];
            
            if ( _audioOut ) {
                DLog(@"Connect to Audio Soccket in setEnablePtt function");
                [_audioOut connectToAudioSocket];
                _audioOut.audioOutStreamerDelegate = self;
                if(completion) {
                    completion(YES);
                }
            }
            else {
                DLog(@"NEED to enable audioOut now BUT audioOut = nil!!!");
                if(completion) {
                    completion(NO);
                }
            }
#if kIsP2PEnabled
        }
#endif
    }
    else {
        // 2. Stopping
        MediaPlayer::Instance()->setPlayOption(MEDIA_STREAM_AUDIO_NOT_MUTE);
        
        if (p2p) { // maybe no need
            if ( _audioOutStreamerP2P ) {
                [_audioOutStreamerP2P stopP2PTalkback];
                self.audioOutStreamerP2P = nil;
            }
        }
        else {
            if ( _audioOut ) {
                [_audioOut disconnectFromAudioSocket];
                self.audioOut = nil;
            }
        }
        
        if(completion) {
            completion(YES);
        }
    }
}

#pragma mark -  PTT

- (void)cleanupWithCamCh:(CamChannel *)ch mode:(int)p2p
{
    [self disableTalkbackWithCamCh:nil mode:p2p completion:^(BOOL success) {
    }];
}

- (void)showTalkbackErrorDialog:(NSString *)errString
{
    if (_delegate && [_delegate respondsToSelector:@selector(talkbackFailedWithError:)]) {
        
        self.walkieTalkieEnabled = NO;
        
        NSDictionary *userInfo = @{
                                   NSLocalizedDescriptionKey: @"Operation was unsuccessful.",
                                   NSLocalizedFailureReasonErrorKey: errString,
                                   NSLocalizedRecoverySuggestionErrorKey:@"Have you tried turning it off and on again?"
                                   };
        NSError *error = [NSError errorWithDomain:@"Talkback failed"
                                             code:-1
                                         userInfo:userInfo];
        [_delegate talkbackFailedWithError:error];
    }
}

@end
