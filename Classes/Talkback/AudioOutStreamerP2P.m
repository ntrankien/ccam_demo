//
//  AudioOutStreamerP2P.m
//  App
//
//  Created by Developer on 2/3/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "AudioOutStreamerP2P.h"
#import <H264MediaPlayer/PCMPlayer.h>
#import <H264MediaPlayer/g722_enc.h>
#import <ReliableChannel/ReliableChannel.h>

@interface AudioOutStreamerP2P ()

@property (nonatomic, strong) PCMPlayer *pcmPlayer;
@property (nonatomic, strong) NSMutableData *pcmData;
@property (nonatomic, strong) NSTimer *timerSendAudioData;
@property (nonatomic) void *p2pSession;
@property (nonatomic) BOOL hasStartRecordingSound;
@property (nonatomic) NSInteger bufferLength;
@property (nonatomic) g722_enc_t g722Enc;


@end

@implementation AudioOutStreamerP2P

static uint8_t outBuf[4 * 1024];

-(void)dealloc{
    DLog(@"AudioOutStreamerP2P - %s", __FUNCTION__);
    if ( _timerSendAudioData ) {
        [_timerSendAudioData invalidate];
        self.timerSendAudioData = nil;
    }
}

- (id)initWithP2PSession:(void *)session
{
    if ( self = [super init] ) {
        self.p2pSession = session;
    }
    
    return self;
}

- (id)initWithCamCh:(CamChannel *)ach
{
    if ( self = [super init] ) {
    }
    
    return self;

}

- (void )startRecordingSound
{
    if ( !_pcmPlayer ) {
        DLog(@"Start recording!!!.******");
        
        // Start the player to playback & record
        self.pcmPlayer = [[PCMPlayer alloc] init];
        self.pcmData = [[NSMutableData alloc] init];
        
        [_pcmPlayer playWithRecordEnabled:YES]; //initialize
        DLog(@"Check _pcmPlayer is %@", _pcmPlayer);
        
        [_pcmPlayer processPlayerPlayNow:NO]; //disable playback
        
//        DLog(@"check _pcmPlayer.recorder %@", _pcmPlayer.recorder);
        [_pcmPlayer startRecording];
        
        g722_enc_init( &_g722Enc );
        
        self.hasStartRecordingSound = YES;
    }
    
    if ( _timerSendAudioData ) {
        [_timerSendAudioData invalidate];
        self.timerSendAudioData = nil;
    }
    
    self.timerSendAudioData = [NSTimer scheduledTimerWithTimeInterval: 0.125 // Sending 8 times per second audio-data packet to camera
                                                        target:self
                                                      selector:@selector(sendAudioPacket:)
                                                      userInfo:nil
                                                       repeats:YES];
}

- (void) sendAudioPacket:(NSTimer *)timerExp
{
    if ( !_timerSendAudioData ) {
        DLog(@"%s Invalide timer.", __FUNCTION__);
        [timerExp invalidate];
        timerExp = nil;
        
        return;
    }
 
    // Read 2kb everytime
//    self.bufferLength = [_pcmPlayer.recorder.inMemoryAudioFile readBytesPCM:_pcmData
//                                                                     withLength:2*1024]; //2*1024
    self.bufferLength = [_pcmPlayer recoderReadBytesPCM:_pcmData withLength:2*1024]; //2*1024
    
    [self sendTalkbackData:(uint8_t *)_pcmData.bytes length:(int)_bufferLength];
}

- (void )sendTalkbackData:(uint8_t *)talkbackData length:(int)length
{
    int outSize;
    g722_enc_encode(&_g722Enc, (short *)talkbackData, length/2, outBuf, &outSize);
    
    static uint32_t ts = 0;
#if 1
    [[ReliableChannel sharedInstance] sendAudioData:outBuf length:outSize ts:ts++];
#elif 1
    RMCP2PHandler *aTmp = _ch.profile.p2pHandler;
    [aTmp sendAudioData:outBuf length:outSize ts:ts++];
#else
    if ( RMCRxSideAudioSend(_p2pSession, outBuf, outSize, ts) < 0 ) {
        DLog(@"%s Failed", __FUNCTION__);
    }
    else {
        DLog(@"%s Success Size:%d", __FUNCTION__, outSize);
    }
#endif
}

- (void)stopP2PTalkback
{
    if ( _pcmPlayer ) {
        DLog(@"pcmPlayer stop & release ");
        [_pcmPlayer processPlayerPlayNow:NO];
        [_pcmPlayer stopRecording];
        [_pcmPlayer stopPlayer];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSTimer scheduledTimerWithTimeInterval:0.25f
                                         target:self
                                       selector:@selector(checkTheBufferLengthToStop:)
                                       userInfo:nil
                                        repeats:YES];
    });
}

- (void)checkTheBufferLengthToStop:(NSTimer *)timer
{
    DLog(@"%s, bufLen: %ld", __FUNCTION__, (long)self.bufferLength);
    
    if ( self.bufferLength == 0 ) {
        g722_enc_deinit(&_g722Enc);
        
        if ( _pcmPlayer ) {
            [_pcmPlayer recoderFlushMemoryAudioFile];
             self.pcmPlayer = nil;
        }
        
        if ( _timerSendAudioData ) {
            [_timerSendAudioData invalidate];
            self.timerSendAudioData = nil;
        }
        
        if ( _pcmData ) {
            self.pcmData = nil;
        }
        
        [timer invalidate];
        
        // Notify main view
        if ( _audioOutStreamerP2PDelegate && [_audioOutStreamerP2PDelegate respondsToSelector:@selector(cleanupWithCamCh:mode:)]) {
            [_audioOutStreamerP2PDelegate cleanupWithCamCh:nil mode:1];
            self.audioOutStreamerP2PDelegate = nil;
        }
    }
}

@end
