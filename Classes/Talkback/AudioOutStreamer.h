//
//  AudioOutStreamer.h
//  App
//
//  Created by Developer on 5/10/12.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <H264MediaPlayer/PCMPlayer.h>

@protocol AudioOutStreamerDelegate <NSObject>

@optional
- (void)cleanupWithCamCh:(CamChannel *)ch mode:(int)p2p;
- (void)showTalkbackErrorDialog:(NSString *)errString;

@end

@interface AudioOutStreamer : NSObject

@property (nonatomic, strong) NSMutableData *pcmData;
@property (nonatomic, strong) PCMPlayer *pcmPlayer;
@property (nonatomic, weak) id<AudioOutStreamerDelegate> audioOutStreamerDelegate;
@property (nonatomic) NSInteger bufferLength;

- (id)initWithDeviceIp:(NSString *)ip andPTTport:(int)port;
- (void)connectToAudioSocket;
- (void)disconnectFromAudioSocket;
- (void)sendAudioPacket:(NSTimer *)timerExp;
- (void)startRecordingSound;

@end
