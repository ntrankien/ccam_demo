//
//  AudioOutStreamerP2P.h
//  App
//
//  Created by Developer on 2/3/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>
#import <CameraScanner/CameraScanner.h>

@protocol AudioOutStreamerP2PDelegate <NSObject>

- (void)cleanupWithCamCh:(CamChannel *)ch mode:(int)p2p;

@end

@interface AudioOutStreamerP2P : NSObject

@property (nonatomic, weak) id<AudioOutStreamerP2PDelegate> audioOutStreamerP2PDelegate;

//- (id)initWithP2PSession:(void *)session;
- (id)initWithCamCh:(CamChannel *)ch;
- (void)startRecordingSound;
- (void)stopP2PTalkback;

@end
