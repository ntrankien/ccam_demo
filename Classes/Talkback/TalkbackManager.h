//
//  TalkbackManager.h
//  LucyDemo
//
//  Created by Tran Kien Nghi on 5/23/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>
#import <CameraScanner/CameraScanner.h>

@class CameraCommand;

@protocol TalkbackManagerDelegate <NSObject>

@optional
- (void)talkbackFailedWithError:(NSError*)error;

@end

@interface TalkbackManager : NSObject

@property (nonatomic, strong) CamChannel *selectedChannel;
@property (nonatomic) BOOL walkieTalkieEnabled;

@property (nonatomic, weak) id<TalkbackManagerDelegate> delegate;

@property (nonatomic) BOOL isP2PMode;

+ (instancetype)sharedInstance;

- (void)enableTalkbackWithCamCh:(CamChannel *)ch mode:(int)p2p completion:(void (^)(BOOL success))completion;
- (void)disableTalkbackWithCamCh:(CamChannel *)ch mode:(int)p2p completion:(void (^)(BOOL success))completion;

@end
