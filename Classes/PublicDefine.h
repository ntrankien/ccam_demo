/*
 *  PublicDefines.h
 *
 *  Copyright (C) 2016 Cinatic Technology
 *
 *  This unpublished material is proprietary to Cinatic Technology.
 *  All rights reserved. The methods and
 *  techniques described herein are considered trade secrets
 *  and/or confidential. Reproduction or distribution, in whole
 *  or in part, is forbidden except by express written permission
 *  of Cinatic Technology
 */

#ifndef PUBLICDEFINE_H_
#define PUBLICDEFINE_H_

typedef enum MotionStorage{
    INIT    = -1,
    CLOUD   = 0,
    SDCARD  = 1,
    NO_USE
} STORAGE;

#define kUserDefault [NSUserDefaults standardUserDefaults]
#define sharedApplicationDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define MainStoryBoard [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]
#define SetupStoryBoard [UIStoryboard storyboardWithName:@"Setup" bundle:[NSBundle mainBundle]]

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

#define CAMERA_UDID @"udid"
#define HOST_SSID   @"host_ssid"
#define HOST_ROUTER @"host_router"
#define CAMERA_NAME @"CameraName"
#define CAMERA_FW_IN_FILE @"downloaded_fw_save_in_file"

#define SET_UP_CAMERA @"SET_UP_CAMERA"
#define REG_ID      @"RegistrationID"
#define CAM_IN_VEW      @"string_Camera_Mac_Being_Viewed"

#define CAMERA_SSID @"CAMERA_SSID"
#define CAMERA_MODEL_ID @"CAMERA_MODEL_ID"
#define PUSH_TO_LIVE_VIEW_IMMEDIATELY @"PUSH_TO_LIVE_VIEW_IMMEDIATELY"
#define FW_VERSION @"FW_VERSION"

#define SET_UP_CAMERA @"SET_UP_CAMERA"
#define BLUETOOTH_SETUP  1
#define WIFI_SETUP      2

// Timeout
#define MAX_LENGTH_CAMERA_NAME  30
#define MIN_LENGTH_CAMERA_NAME  5
#define TIME_OUT_RECONNECT_BLE  30.0
#define SHORT_TIME_OUT_SEND_COMMAND     7.0
#define LONG_TIME_OUT_SEND_COMMAND     30.0
#define TIME_OUT_DETECT_CAMERA_VIA_WIFI 60.0
#define TIME_OUT_DETECT_CAMERA_VIA_BLE  30
#define TIME_OUT_DETECT_CAMERA_VIA_LAN  20

#define DEFAULT_SSID_PREFIX @"Camera-"
#define DEFAULT_SSID_HD_PREFIX @"CameraHD-"

//define default IP
#define DEFAULT_IP_PREFIX @"192.168.2."
#define DEFAULT_IP_PREFIX_CAMERA_C89    @"192.168.193."

#define DEFAULT_BM_IP @"192.168.2.1"
#define DEFAULT_BM_IP_CAMERA_C89    @"192.168.193.1"
#define DEFAULT_BM_PORT 80

#define DEFAULT_IP_PETCAM @"192.168.43.1"
#define DEFAULT_PORT_PETCAM 8080

#define FW_VERSION_NOT_SET_RECORD_DURATION    @"01.16.45"

#define DEV_STATUS_UNKOWN                   0
#define DEV_STATUS_NOT_IN_MASTER            1
#define DEV_STATUS_NOT_REGISTERED           2
#define DEV_STATUS_REGISTERED_LOGGED_USER   3
#define DEV_STATUS_REGISTERED_OTHER_USER    4
#define DEV_STATUS_DELETED                  5

#define WIFI_PASSWORD_MAX_LENGTH 63
#define WIFI_PASSWORD_MIN_LENGTH 8


#define USE_ENCRYPT_DATA_KEY @"USE_ENCRYPT_DATA_KEY"

#define API_KEY @"API_KEY"
#define CAMERA_STATE_UNKNOWN       -1
#define CAMERA_STATE_FW_UPGRADING   0
#define CAMERA_STATE_IS_AVAILABLE   1
#define CAMERA_STATE_REGISTED_LOGGED_USER 2

#define MAX_CAM_ALLOWED 100

#define THEME_BAR_TINT_COLOR [UIColor whiteColor]
#define THEME_PRIMARY_TINT_COLOR [UIColor colorWithRed:4/255.0f green:225/255.0f blue:233/255.0f alpha:1.0]
#define THEME_TEXT_TINT_COLOR [UIColor colorWithRed:98/255.0f green:98/255.0f blue:98/255.0f alpha:1.0]
#define THEME_TEXT_EDIT_COLOR [UIColor colorWithRed:248/255.0f green:73/255.0f blue:18/255.0f alpha:1.0]


#define kIsEnableExploitHA      @"exploit_h_a"
#define PathForDir [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
#define PathForFileName(v) ([PathForDir stringByAppendingPathComponent:v])
#define IRABOT_AUDIO_RECORDING_PORT 51108

#define CMD_SEND_FREQUENCY [[[NSUserDefaults standardUserDefaults] stringForKey:@"command_interval"] floatValue]
#define CMD_DURATION [[[NSUserDefaults standardUserDefaults] stringForKey:@"command_duration"] floatValue]
#define CMD_VOLUME_DURATION 1 //1s

#define ALREADY_LAUNCH_KEY @"ALREADY_LAUNCH_KEY"
#define ALLOW_P2P_REMINDER_POPUP_KEY @"ALLOW_P2P_REMINDER_POPUP_KEY"

#define kWaitForCommand @"wait_for_cmd_response"
#define kWaitForP2PCommand @"wait_for_P2P_cmd_response"

#define kMoveDuration @"command_duration"
#define kP2PMoveDuration @"p2p_command_duration"

#define kHandDuration @"hand_duration"
#define kP2PHandDuration @"p2p_hand_duration"
#define kHandRequency @"hand_interval"

//Move speed
#define MIN_OF_MOVE_SPEED 5
#define MAX_OF_MOVE_SPEED 255
#define NUM_OF_LEVEL 10
#define DEFAULT_MOVE_SPEED_VALUE MIN_OF_MOVE_SPEED
#define MOVE_SPEED_LEVEL_DURATION ((MAX_OF_MOVE_SPEED - MIN_OF_MOVE_SPEED) / NUM_OF_LEVEL)

#define RETRY_STOP_CMD_INETRVAL 0.2 //200ms
#define RETRY_STOP_CMD_TIMES 3 //3 times
#define MAX_NUMBER_OF_SETUP 3 //3 cameras

//User default key
#define MOVE_SPEED_VALUE @"MOVE_SPEED_VALUE"
#define IS_SETUP @"IS_SETUP"
#define IS_NEW_SETUP @"IS_NEW_SETUP"
#define IS_USING_MOVE_HAND_ABIT @"IS_USING_MOVE_HAND_ABIT"

#define kDisplayStatusLocked @"DisplayStatusLocked" // For remote P2P (need not to know now)


#define kCamUdid @"cam_udid"

#define kIsAVSyncEnabled @"av_sync_enabled"
#define kIsRtspWithTcpEnabled    @"rtsp_tcp_enabled"
#define kTimeoutWhileStreaming   @"timeout_stream"

#define kIsPlayAllFrame          @"play_all_frame"

#define kDidLoadRecordingList @"Recording_view_did_load"


#ifdef DEBUG // incase we forget reseting this flag when releasing
#define kIsLocalTest   0
#define kIsFakedToTest 0
#endif

#endif /* PUBLICDEFINE_H_ */
