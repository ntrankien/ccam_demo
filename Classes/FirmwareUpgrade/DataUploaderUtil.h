//
//  DataUploaderUtil.h
//
//  Created by Tran Kien Nghi on 3/14/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

@class DataUploaderUtil;

typedef void (^UploaderCompletion)(DataUploaderUtil* downloader, NSInteger statusCode,  NSError* error);
typedef void (^UploaderProcessing)(NSUInteger __unused bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite);

@interface DataUploaderUtil : NSObject

//Created to support Firmware upgarde function
- (void)uploadFormData:(NSData*)data fileName:(NSString*)fileName url:(NSString*)urlString processing:(UploaderProcessing)processing completion:(UploaderCompletion)completion;

@end
