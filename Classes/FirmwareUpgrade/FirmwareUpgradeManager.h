//
//  FirmwareUpgradeManager.h
//
//  Created by Tran Kien Nghi on 3/14/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

#define FIRMWARE_DEFAULT_LINK @"http://"

@class CamProfile;

#define FIRMWARE_UPGRADE_DOMAIN @"Firmware upgrade error"

typedef enum {
    FirmwareUpgradeErrorFirmwareUpToDate = 1,
    FirmwareUpgradeErrorFirmwareNotExisted,
    FirmwareUpgradeErrorDownloadSecurityFileFailed,
    FirmwareUpgradeErrorDownloadFirmwarePackgeFailed,
    FirmwareUpgradeErrorChecksumFailed,
    FirmwareUpgradeErrorUploadSecurityFileFailed,
    FirmwareUpgradeErrorUploadFirmwarePackgeFailed,
    FirmwareUpgradeErrorUpgradeProcessFailed,
    FirmwareUpgradeErrorUpgradeProcessTimedOut,
    FirmwareUpgradeErrorUserCanceled,
    FirmwareUpgradeErrorUnknown,
}FirmwareUpgradeError;

typedef void (^DownloaderStartBlock)();
typedef void (^DownloaderProcessingBlock)(long long downloadedDataLength, long long totalLength);
typedef void (^DownloaderSuccessBlock)();
typedef void (^DownloaderFailureBlock)(NSError* error);

typedef void (^UploaderStartBlock)();
typedef void (^UploaderProcessingBlock)(long long uploadedDataLength, long long totalLength);
typedef void (^UploaderSuccessBlock)();
typedef void (^UploaderFailureBlock)(NSError* error);

typedef void (^UpgraderStartBlock)();
typedef void (^UpgraderProcessingBlock)(long completedPercent);
typedef void (^UpgraderCompletion)();
//typedef void (^UpgraderFailureBlock)(NSError* error);

typedef void (^UpgraderProcessFailureBlock)(NSError* error);

@interface FirmwareUpgradeManager : NSObject

@property (nonatomic, strong) NSString* latestFirmwareVersion;
@property (nonatomic, strong) CamProfile *camModel;

@property (copy, nonatomic) DownloaderStartBlock downloaderStart;
@property (copy, nonatomic) DownloaderSuccessBlock downloaderCompletion;
@property (copy, nonatomic) DownloaderProcessingBlock downloaderProcessing;
@property (copy, nonatomic) DownloaderFailureBlock downloaderFailure;

@property (copy, nonatomic) UploaderStartBlock uploaderStart;
@property (copy, nonatomic) UploaderSuccessBlock uploaderCompletion;
@property (copy, nonatomic) UploaderProcessingBlock uploaderProcessing;
@property (copy, nonatomic) UploaderFailureBlock uploaderFailure;

@property (copy, nonatomic) UpgraderStartBlock upgraderStart;
@property (copy, nonatomic) UpgraderProcessingBlock upgraderProcessing;
@property (copy, nonatomic) UpgraderCompletion upgraderCompletion;
//@property (copy, nonatomic) UpgraderSuccessBlock upgradeSuccess;
//@property (copy, nonatomic) UpgraderFailureBlock upgradeFailure;

@property (copy, nonatomic) UpgraderProcessFailureBlock upgraderProcessFailure;

@property (nonatomic) BOOL isFirmwareDataDownloaded;

- (void)downloadFirmwarePakage;
- (void)startUpgardeProcess;

/**
 Reset all data
 */
- (void)reset;

- (void)setFirmwareURL:(NSString*)urlString;

//for test
- (void)setFirmwareData:(NSData*)firmwareData version:(NSString*)fwVersionString;

@end
