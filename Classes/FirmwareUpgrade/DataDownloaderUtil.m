//
//  DataDownloaderUtil.m
//
//  Created by Tran Kien Nghi on 2/29/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "DataDownloaderUtil.h"

@interface DataDownloaderUtil()

@property (nonatomic, strong) NSURLRequest *request;
@property (nonatomic) long long totalSize;
@property (nonatomic) long long downloadedSize;
@property (nonatomic) NSInteger fwUpgradeStatus;
@property (nonatomic, strong) NSMutableData *downloadedData;

@property (nonatomic, copy) DownloaderStart start;
@property (nonatomic, copy) DownloaderCompletion completion;
@property (nonatomic, copy) DownloaderProcessing processing;

@end

@implementation DataDownloaderUtil

- (void)downloadDataWithUrlString:(NSString*)urlString
                       processing:(void (^)(NSData* receivedData, long long downloadedSize, long long totalSize))processing
                       completion:(void (^)(DataDownloaderUtil* downloader, NSData* downloadedData, NSError* error))completion{
    
    self.totalSize = 1;
    self.downloadedSize = 0;
    self.downloadedData = nil;
    
    self.completion = completion;
    self.processing = processing;
    NSURL *url = [NSURL URLWithString:urlString];
    self.request = [[NSURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    [NSURLConnection connectionWithRequest:_request delegate:self];
}

- (void)downloadDataWithUrlString:(NSString*)urlString
                            start:(DownloaderStart)startBlock
                       processing:(void (^)(NSData* receivedData, long long downloadedSize, long long totalSize))processing
                       completion:(void (^)(DataDownloaderUtil* downloader, NSData* downloadedData, NSError* error))completion{
    
    self.totalSize = 1;
    self.downloadedSize = 0;
    self.downloadedData = nil;
    
    self.start = startBlock;
    self.completion = completion;
    self.processing = processing;
    NSURL *url = [NSURL URLWithString:urlString];
    self.request = [[NSURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    [NSURLConnection connectionWithRequest:_request delegate:self];
}

#pragma mark - Connection delegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    _totalSize = 1;
    _downloadedSize = 0;
    _downloadedData = nil;
    
    if (_completion) {
        _completion(self, _downloadedData, error);
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_downloadedData appendData:data];
    _downloadedSize += [data length];

    if (_processing) {
        _processing(data, _downloadedSize, _totalSize);
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _totalSize = [response expectedContentLength];
    _downloadedSize = 0;
    _downloadedData = nil;
    _downloadedData = [[NSMutableData alloc] init];
    
    if (_start) {
        _start(self, _totalSize);
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if ( !_downloadedData || _downloadedData.length == 0 ) {
        if (_completion) {
            _completion(self, nil, nil);
        }
    }
    else {
        if (_completion) {
            _completion(self, _downloadedData, nil);
        }
    }
}


@end
