//  DataDownloaderUtil.h
//
//  Created by Tran Kien Nghi on 2/29/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

@class DataDownloaderUtil;

typedef void (^DownloaderStart)(DataDownloaderUtil* downloader, long long totalSize);
typedef void (^DownloaderCompletion)(DataDownloaderUtil* downloader, NSData* downloadedData, NSError* error);
typedef void (^DownloaderProcessing)(NSData* receivedData, long long downloadedSize, long long totalSize);
typedef void (^DownloaderFailure)(NSError* error);

@interface DataDownloaderUtil : NSObject

- (void)downloadDataWithUrlString:(NSString*)urlString
                       processing:(DownloaderProcessing)processing
                       completion:(DownloaderCompletion)completion;
- (void)downloadDataWithUrlString:(NSString*)urlString
                            start:(DownloaderStart)startBlock
                       processing:(DownloaderProcessing)processing
                       completion:(DownloaderCompletion)completion;

@end
