//
//  FirmwareUpgradeManager.m
//
//  Created by Tran Kien Nghi on 3/14/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "FirmwareUpgradeManager.h"
#import "AFHTTPRequestOperationManager.h"
#import <CameraScanner/CameraScanner.h>
#import "CameraCommandManager.h"
#import "HttpCom.h"
#import "DataDownloaderUtil.h"
#import "DataUploaderUtil.h"

#define FIRMWARE_UPGRADE_IN_PROGRESS      0

#define TIME_FW_UPGRADE 300 // 5*60 = 5 minutes
#define MAX_NUMBER_GET_BURNING_PROGRESS 100

@interface FirmwareUpgradeManager()

@property (nonatomic, strong) NSMutableData *downloadedData;
@property (nonatomic) long long totalSize;
@property (nonatomic) long long downloadedSize;
@property (nonatomic) NSInteger fwUpgradeStatus;
@property (nonatomic) NSInteger fwUpgradedProgress;
@property (nonatomic) BOOL isUpgradingFirmware;//Control to continue downloading/uploading data to camera if the app entered background
@property (nonatomic) BOOL isFirmwareUploadAlmostDone;
@property (nonatomic, strong) NSTimer* checkUpgradeStatusTimer;
@property (nonatomic, copy) NSString* firmwareURL;

@end

@implementation FirmwareUpgradeManager

//for test
- (void)setFirmwareData:(NSData*)firmwareData version:(NSString*)fwVersionString{
    self.downloadedData = firmwareData.mutableCopy;
}
//

- (void)startUpgardeProcess{
    self.isUpgradingFirmware = YES;
    if (_isFirmwareDataDownloaded) {
        [self doUploadFirmwareData];
    }
    else{
        [self doDownloadFirmwareData];
    }
}

- (void)reset{
    self.downloadedData = nil;
    self.isFirmwareDataDownloaded = NO;
    self.totalSize = 0;
    self.downloadedSize = 0;
}

#pragma mark - Private methods
-(void)doDownloadFirmwareData
{
    NSLog(@"%s %@", __FUNCTION__, _camModel.fw_version);
    
    self.isFirmwareUploadAlmostDone = NO;
    self.isUpgradingFirmware = YES;
    
    [self downloadFirmwarePakage];
}

- (void)downloadFirmwarePakage{
    NSLog(@"%s", __FUNCTION__);

    dispatch_async(dispatch_get_main_queue(), ^{
        DataDownloaderUtil* dataDownloader = [DataDownloaderUtil new];
        __weak FirmwareUpgradeManager* weakSelf = self;
        [dataDownloader downloadDataWithUrlString:_firmwareURL
                                            start:^(DataDownloaderUtil *downloader, long long totalSize) {
                                                weakSelf.totalSize = totalSize;
                                                
                                                if(_downloaderStart){
                                                    _downloaderStart();
                                                }
                                            }
                                       processing:^(NSData *receivedData, long long downloadedSize, long long totalSize) {
                                           _downloadedSize += [receivedData length];
                                           if(_downloaderProcessing){
                                               _downloaderProcessing(downloadedSize, totalSize);
                                           }
                                       }
                                       completion:^(DataDownloaderUtil *downloader, NSData *downloadedData, NSError *error) {
                                           if (error || !downloadedData || downloadedData.length <= 0) {
                                               NSLog(@"Download firmware failed: %@", error.localizedDescription);
                                               weakSelf.isUpgradingFirmware = NO;
                                               _totalSize = 0;
                                               _downloadedSize = 0;
                                               _downloadedData = nil;
                                               
                                               NSMutableDictionary* details = [NSMutableDictionary dictionary];
                                               [details setValue:@"Download firmware package failed" forKey:NSLocalizedDescriptionKey];
                                               if (error) {
                                                   [details setValue:error.localizedDescription forKey:NSLocalizedFailureReasonErrorKey];
                                               }
                                               NSError* err = [NSError errorWithDomain:FIRMWARE_UPGRADE_DOMAIN code:FirmwareUpgradeErrorDownloadFirmwarePackgeFailed userInfo:details];
                                               
                                               if (_downloaderFailure) {
                                                   _downloaderFailure(err);
                                               }
                                               if (_upgraderProcessFailure) {
                                                   _upgraderProcessFailure(err);
                                               }
                                           }
                                           else{
                                               self.downloadedData = downloadedData.mutableCopy;
                                               
                                               if ( !_downloadedData || _downloadedData.length == 0) {
                                                   
                                                   NSMutableDictionary* details = [NSMutableDictionary dictionary];
                                                   [details setValue:@"Checksum does not match or firmware data is emtpy" forKey:NSLocalizedDescriptionKey];
                                                   NSError* err = [NSError errorWithDomain:FIRMWARE_UPGRADE_DOMAIN code:FirmwareUpgradeErrorDownloadFirmwarePackgeFailed userInfo:details];
                                                   
                                                   if (_downloaderFailure) {
                                                       _downloaderFailure(err);
                                                   }
                                                   if (_upgraderProcessFailure) {
                                                       _upgraderProcessFailure(err);
                                                   }
                                               }
                                               else{
                                                   
                                                   if(_downloaderCompletion){
                                                       self.isFirmwareDataDownloaded = YES;
                                                       _downloaderCompletion();
                                                   }
                                               }
                                           }
                                       }];
    });
}

#pragma mark - Upload firmware pakage

- (void)doUploadFirmwareData
{
    if(_uploaderStart){
        _uploaderStart();
    }
    
    self.isFirmwareUploadAlmostDone = NO;
    self.isUpgradingFirmware = YES;//continue to upload firmware
    
    //Pre-process: free memory for camera, just send a http request
    [self sendPreFWUploadToCameraIP:_camModel.ip_address withTimeout:15 completion:^(NSString *response, NSError *error) {
        [self uploadFirmwarePakage];
    }];
}

- (void)uploadFirmwarePakage{
    NSString *fileName = [NSString stringWithFormat:@"%@.tar", _latestFirmwareVersion.length ? _latestFirmwareVersion : @"firmware_upgrade"];
    
    NSLog(@"uploadFirmwarePakage file name: %@", fileName);
    
    NSString *uploadFWUrl = [NSString stringWithFormat:@"http://%@:8080/cgi-bin/haserlupgrade.cgi", _camModel.ip_address];
    
    NSLog(@"uploadFirmwarePakage to: %@", uploadFWUrl);
    
    DataUploaderUtil* uploader = [[DataUploaderUtil alloc] init];
    
    __weak FirmwareUpgradeManager* weakSelf = self;
    [uploader uploadFormData:_downloadedData
                    fileName:fileName
                         url:uploadFWUrl
                  processing:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                      
                      if (weakSelf.isFirmwareUploadAlmostDone) {
                          //Don't process more if firmware almost uploaed (>= 99.5%)
                          return;
                      }
                      
                      NSLog(@"Uploaded: %ld / %lld", (unsigned long)totalBytesWritten, totalBytesExpectedToWrite);
                      
                      float uploadProcess = (float)totalBytesWritten / (float)totalBytesExpectedToWrite;
                      long uploadProcessPercent = lroundf(uploadProcess * 100); //>=99.5
                      
                      if (uploadProcessPercent == 100 ) {
                          weakSelf.isFirmwareUploadAlmostDone = YES;
                          weakSelf.fwUpgradedProgress = 0;
                          weakSelf.fwUpgradeStatus = FIRMWARE_UPGRADE_IN_PROGRESS;
                          weakSelf.downloadedData = nil;
                          [weakSelf reset];
                          if(weakSelf.uploaderCompletion){
                              weakSelf.uploaderCompletion();
                          }
                          
                          [weakSelf upgradeFwProgressInBackground];
                          
                          weakSelf.isUpgradingFirmware = NO;//No need to enter background while camera is upgrading
                      }
                      else{
                          if(weakSelf.uploaderProcessing){
                              weakSelf.uploaderProcessing(totalBytesWritten, totalBytesExpectedToWrite);
                          }
                      }
    }
                  completion:^(DataUploaderUtil *downloader, NSInteger statusCode, NSError *error) {
        if (!error && 0 < statusCode && statusCode < 400) {
            //Success
            //Don't handle anymore because all are handled when downloading done 99.5%
        }
        else{
            if (weakSelf.isFirmwareUploadAlmostDone) {
                //Don't process failure if firmware almost uploaded (>= 99.5%)
                return ;
            }
            
            NSLog(@"Upload firmware file failed");
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:@"Upload firmware file failed" forKey:NSLocalizedDescriptionKey];
            if (error) {
                [details setValue:error.localizedDescription forKey:NSLocalizedFailureReasonErrorKey];
            }
            NSError* err = [NSError errorWithDomain:FIRMWARE_UPGRADE_DOMAIN code:FirmwareUpgradeErrorUploadFirmwarePackgeFailed userInfo:details];
            
            if (weakSelf.uploaderFailure) {
                weakSelf.uploaderFailure(err);
            }
            if (weakSelf.upgraderProcessFailure) {
                weakSelf.upgraderProcessFailure(err);
            }
        }
    }];
}

#pragma mark - Upload firmware pakage

- (void)upgradeFwProgressInBackground{
    if(_upgraderStart){
        _upgraderStart();
    }
    
    self.fwUpgradedProgress = 0;
    
    float sleepPeriod = TIME_FW_UPGRADE / 100.f; // 100 cycles
    self.checkUpgradeStatusTimer = [NSTimer scheduledTimerWithTimeInterval:sleepPeriod target:self selector:@selector(checkFwUpgradeStatusFromCamera) userInfo:nil repeats:YES];
}

- (void)checkFwUpgradeStatusFromCamera
{
    if(_fwUpgradedProgress >= MAX_NUMBER_GET_BURNING_PROGRESS){
        [_checkUpgradeStatusTimer invalidate];
        self.checkUpgradeStatusTimer = nil;
        
        if (_upgraderCompletion) {
            _upgraderCompletion();
        }
        return;
    }
    
    if (_upgraderProcessing) {
        _upgraderProcessing(_fwUpgradedProgress);
    }
    
    ++self.fwUpgradedProgress;
}

- (void)sendPreFWUploadToCameraIP:(NSString*)cameraIPAddress withTimeout:(float)timeout completion:(void (^)( NSString* response, NSError* error))completion{
    NSString *burningProcessUrl = [NSString stringWithFormat:@"http://%@:8080/cgi-bin/prefwupload", cameraIPAddress];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:burningProcessUrl]
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:timeout];
    
    NSError *error = nil;
    NSURLResponse *urlResponse = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest
                                         returningResponse:&urlResponse
                                                     error:&error];
    if (!error) {
        NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]; //preupload: 0 -> success
        
        /*TODO: Should check the response and retry if it's failure
         */
        if (completion) {
            completion(response, error);
        }
    }
    else{
        NSLog(@"%s error:%@", __FUNCTION__, error.description);
    }
}

@end
