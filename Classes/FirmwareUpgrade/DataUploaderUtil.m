//
//  DataUploaderUtil.m
//
//  Created by Tran Kien Nghi on 3/14/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "DataUploaderUtil.h"
#import "AFNetworking.h"

@implementation DataUploaderUtil

- (void)uploadFormData:(NSData*)data fileName:(NSString*)fileName url:(NSString*)urlString processing:(UploaderProcessing)processing completion:(UploaderCompletion)completion{
    
    //for test
    NSLog(@"Upload data length: %lud", (unsigned long)data.length);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    request.HTTPMethod = @"POST";
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[data length]] forHTTPHeaderField:@"Content-Length"];
    
    NSString *myboundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",myboundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *postData = [NSMutableData data];
    [postData appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", myboundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uploadfile\"; filename=\"%@\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[NSData dataWithData:data]];
    [postData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", myboundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // set request body
    [request setHTTPBody:postData];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
    [contentTypes addObject:@"text/html"];
    operation.responseSerializer.acceptableContentTypes = contentTypes;
    
    operation.shouldUseCredentialStorage = manager.shouldUseCredentialStorage;
    operation.credential = manager.credential;
    operation.securityPolicy = manager.securityPolicy;
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        
        //for test
        NSLog(@"Upload operation.response.statusCode: %ld", operation.response.statusCode);
        
        if (operation.responseData != nil) {
            if (completion){
                completion(self, operation.response.statusCode, nil);
            }
        }
        else{
            if (completion){
                completion(self, -1, nil);
            }
        }
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        
        //for test
        NSLog(@"Upload failed - operation.response.statusCode: %ld, error:%@", operation.response.statusCode, error.localizedDescription);
        
        if (completion){
            completion(self, operation.response.statusCode, error);
        }
    }];
    
    // Set the progress block of the operation.
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        if (processing) {
            processing(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
        }
    }];
    
    operation.completionQueue = manager.completionQueue;
    operation.completionGroup = manager.completionGroup;
    
    [operation start];
}

@end
