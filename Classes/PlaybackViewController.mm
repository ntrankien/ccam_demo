//
//  PlaybackViewController.m
//  Dev
//
//  Created on 6/9/13.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "MBProgressHUD.h"
#import <objc/message.h>

#import "PlaybackViewController.h"
#import "PlaybackListener.h"
#import "HttpCom.h"
#import "PrivateDataUtils.h"
#import "NSDictionary+SBJSONHelper.h"

#import "AppDelegate.h"

#define START 0
#define END 100.0
#define HEIGHT_BG_CONTROL 45
#define HEIGHT_SLIDER_DEFAULT 33
#define DELETE_RECORD_DIALOG_TAG           222
#define EXIT_DIALOG_TAG                    223
#define RETRY_PLAYBACK  3

@interface PlaybackViewController()

@property (nonatomic, weak) IBOutlet UIImageView *imageVideo;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) IBOutlet UIImageView *ib_bg_top_player;

@property (nonatomic, weak) IBOutlet UIView *ib_viewControlPlayer;
@property (nonatomic, weak) IBOutlet UIButton *ib_closePlayBack;
@property (nonatomic, weak) IBOutlet UIButton *ib_playPlayBack;
@property (nonatomic, weak) IBOutlet UISlider *ib_sliderPlayBack;
@property (nonatomic, weak) IBOutlet UILabel *ib_timerPlayBack;
@property (nonatomic, weak) IBOutlet UIButton *ib_zoomingPlayBack;

@property (nonatomic, weak) IBOutlet UIView *ib_myOverlay;
@property (nonatomic, weak) IBOutlet UIView *ib_viewOverlayVideo;
@property (nonatomic, weak) IBOutlet UIButton *ib_delete;
@property (nonatomic, weak) IBOutlet UIButton *ib_download;
@property (nonatomic, weak) IBOutlet UIButton *ib_share;
@property (nonatomic, weak) IBOutlet UILabel *deleteLabel;
@property (nonatomic, weak) IBOutlet UILabel *downloadLabel;
@property (nonatomic, weak) IBOutlet UILabel *shareLabel;

@property (nonatomic, assign) BOOL isUserCancelDownloading;

@property (nonatomic, copy) NSString *urlVideoStr;

@property (nonatomic, assign) MediaPlayer *playbackStreamer;
@property (nonatomic, assign) PlaybackListener *listener;

@property (nonatomic) BOOL isSwitchingWhenPress;
@property (nonatomic) BOOL isClickedOnZooming;

@property (nonatomic) BOOL isPause;
@property (nonatomic) BOOL fromStart;
@property (nonatomic) double duration;
@property (nonatomic) int64_t startPositionMovieFile;
@property (nonatomic) double timeStarting;
@property (nonatomic) BOOL shouldRestartProcess;
@property (nonatomic) NSInteger mediaCurrentState;
@property (nonatomic) double currentTimeOfPreviousClips;
@property (nonatomic) double currentTime;

@property (nonatomic) BOOL isRemoteRecordingOnBG;
@property (nonatomic) BOOL isDownloading;
@property (nonatomic) BOOL isSharing;
@property (nonatomic, copy) NSString *remoteFilePath;
@property (nonatomic) int retryPlayback;

@property (nonatomic, strong) MBProgressHUD *hud;

@property (nonatomic, strong) UIImageView *firstImageOfVideoView;

@end

@implementation PlaybackViewController

#pragma mark - UIViewController methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.firstImageOfVideoView = [[UIImageView alloc] initWithFrame:self.view.frame];

    [self applyFont];
    [self.view addSubview:_ib_myOverlay];
    self.view.userInteractionEnabled = NO;
    
    _ib_myOverlay.hidden = YES;
    _ib_viewOverlayVideo.hidden = YES;
    [_ib_playPlayBack setImage:[UIImage imageNamed:@"video_play"] forState:UIControlStateSelected];
    [_ib_playPlayBack setImage:[UIImage imageNamed:@"video_pause"] forState:UIControlStateNormal];

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:singleTap];
    
    self.startPositionMovieFile = 0;
    self.timeStarting = 0;
    self.shouldRestartProcess = FALSE;
    self.mediaCurrentState = 0;
    _deleteLabel.text = LocStr(@"Delete");
    
    _isRemoteRecordingOnBG = NO;
    _isDownloading = NO;
    _isSharing = NO;
    _remoteFilePath = @"";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    // Show indicator
    _activityIndicator.hidden = NO;
    [_activityIndicator startAnimating];

    // Do any additional setup after loading the view.
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playbackEnteredBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    self.ib_viewControlPlayer.hidden = NO;
    
    // Hide download capability unless cameraName is set on the clipInfo
    if ( !_clipInfo.cameraName ) {
        _ib_download.hidden = YES;
        _downloadLabel.hidden = YES;
        
        // Also hide delete and move share to the delete (centre) frame area
        _ib_delete.hidden = YES;
        _deleteLabel.hidden = YES;
        _ib_share.frame = _ib_delete.frame;
        _shareLabel.frame = _deleteLabel.frame;
        _shareLabel.textAlignment = NSTextAlignmentCenter;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self checkOrientation];
    [self becomeActive];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)handleResetPasswordNotification {
    [self closePlayBack:nil];
}

#pragma mark - NSNotificationCenter

/**
  Only pause the player
 */
- (void)playbackEnteredBackground
{   
    if (MediaPlayer::Instance()->isPlaying()) {
        self.isPause = YES;
        MediaPlayer::Instance()->pause();
        self.ib_playPlayBack.selected = YES;
    }   
}

#pragma mark - PLAY VIDEO

- (void)becomeActive
{
    self.currentTime = 0;
    self.shouldRestartProcess = NO;
    self.mediaCurrentState = -1;
    self.clips = [[NSMutableArray alloc] init];
    self.retryPlayback = 0;
    // Decide whether or not to start the background polling
    
    if ( _clipInfo ) {
        self.listener = new PlaybackListener(self);
        
        if (_clipInfo.nextClips) {
            if ( _clipInfo.nextClips.count == 0 ) {
                [_clips addObject:_clipInfo.urlFile];
                _listener->updateClips(_clips);
                _listener->updateFinalClipCount(1);
            }
            else {
                [_clips addObject:_clipInfo.urlFile];
                [self performSelectorInBackground:@selector(getCameraPlaylistEventOnSDCard) withObject:nil];
            }
        }
        else {
            self.duration = [self.clipInfo getDurationIfLastClip];
            
            DLog(@"this is the only clip do not poll");
            [_clips addObject:_clipInfo.urlFile];
            _listener->updateClips(_clips);
            _listener->updateFinalClipCount(1);
            [self detectDurationFromFileName:_clipInfo.urlFile];
            
            //DLog(@" url streaming: %@", _clipInfo.urlFile);
            self.urlVideoStr = _clipInfo.urlFile;
        }
		
        if ( [UIApplication sharedApplication].applicationState != UIApplicationStateActive ) {
            DLog(@"%s. Inactive mode", __FUNCTION__);
        }
        else {
            [self performSelector:@selector(setupStream) withObject:nil afterDelay:0.1];
        }
    }
}

- (void)detectDurationFromFileName:(NSString *)fileName {
    NSString *clipname = [fileName lastPathComponent];
    if ( [clipname rangeOfString:@"_last.flv"].location != NSNotFound ) {
        NSArray *infos = [clipname componentsSeparatedByString:@"_"];
        if (infos.count > 2) {
            int duration = [infos[infos.count - 2] intValue];
            [self setMultiClipsDuration:duration];
            DLog(@"Duration of clip: %d", duration);
        }
    }
}

- (void)setupStream
{
    NSInteger iCount = 0;
    
    while ( !_userWantToBack && MediaPlayer::Instance()->isShouldWait() && iCount++ < 6 ) {
        [NSThread sleepForTimeInterval:0.5];
    }
    
    if ( _userWantToBack ) {
        DLog(@"%s Cancel Playpack process. Return!", __FUNCTION__);
        return;
    }
    
    self.shouldRestartProcess = YES;
    
    MediaPlayer::Instance()->setListener(_listener);
    
    if ( [_urlVideoStr hasPrefix:@"rtmp"] || [_urlVideoStr hasPrefix:@"rtmps"] ) {
        MediaPlayer::Instance()->setPlaybackAndSharedCam(false, false);
        self.ib_playPlayBack.enabled = NO;
    }
    else {
        MediaPlayer::Instance()->setPlaybackAndSharedCam(true, false);
        self.ib_playPlayBack.enabled = YES;
    }

    status_t status = !NO_ERROR;
    NSString *urlStr = _urlVideoStr;

    self.mediaCurrentState = 0;
//    DLog(@"URL URL URL %s", [urlStr UTF8String]);
    status = MediaPlayer::Instance()->setDataSource([urlStr UTF8String]);

    DLog(@"%s status: %d, retry(%d)", __FUNCTION__, status, _retryPlayback);

    if ( status != NO_ERROR ) {
        // NOT OK
        DLog(@"setDataSource error: %d\n", status);
        if ( ([_urlVideoStr hasPrefix:@"rtmp"] || [_urlVideoStr hasPrefix:@"rtmps"])) {
            if (_retryPlayback < RETRY_PLAYBACK) {
                self.retryPlayback += 1;
                [self performSelector:@selector(setupStream) withObject:nil afterDelay:3.0];
            }
            else {
                [self handleMessage:MEDIA_ERROR_SERVER_DIED ext1:0 ext2:0];
            }
        }
        else {
            [self handleMessage:MEDIA_ERROR_SERVER_DIED ext1:0 ext2:0];
        }

        return;
    }

    MediaPlayer::Instance()->setVideoSurface(self.imageVideo);

    DLog(@"Prepare the player");
    status =  MediaPlayer::Instance()->prepare();

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageVideo setAlpha:1];
        [self.activityIndicator setHidden:YES];
        self.view.userInteractionEnabled = YES;
        self.ib_myOverlay.hidden = NO;
        [self.ib_sliderPlayBack setMinimumTrackTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"video_progress_green"]]];
        [_ib_sliderPlayBack setThumbImage:[[UIImage alloc] init] forState:UIControlStateNormal];
        [self watcher];
    });

    DLog(@"prepare return: %d\n", status);

    if ( status != NO_ERROR ) {
        // NOT OK
        DLog(@"prepare() error: %d\n", status);
        [self finishAndPopViewController];
    }
    else {
        [self playStream];
    }
}

- (void)saveFirstImage {
    if (_imageVideo.image) {
        if (!_firstImageOfVideoView.image) {
            _firstImageOfVideoView.image = _imageVideo.image;
        }
    }
    else {
        [self performSelector:@selector(saveFirstImage) withObject:nil afterDelay:1.0];
    }
}

- (void)handleMessage:(int)msg ext1:(int)ext1 ext2:(int)ext2
{
    switch (msg) {
        case MEDIA_PLAYER_PREPARED:
            break;

        case MEDIA_PLAYER_STARTED:
        {
            DLog(@"%s msg: MEDIA_PLAYER_STARTED", __FUNCTION__);
            self.mediaCurrentState = msg;
            [self performSelector:@selector(recordRemoteStream) withObject:nil afterDelay:0.2];
            [self performSelector:@selector(saveFirstImage) withObject:nil afterDelay:1.0];
            break;
        }
            
        case MEDIA_ERROR_SERVER_DIED:
        case MEDIA_PLAYBACK_COMPLETE:
        {
            //DONE Playback
            //clean up
            NSString *t = @"call finishAndPopViewController";
            
            if ( _userWantToBack == FALSE && [UIApplication sharedApplication].applicationState == UIApplicationStateActive ) {
//                DLog(@"%s call finishAndPopViewController", __FUNCTION__);
                
                //Fixed: https://app.crittercism.com/developers/crash-details/53d6a3761787842dba000001/ca79e84eebf788d3a30ea6243afbb34cfe47ffdf761fb6702667b2e8#backtrace
                [self closePlayBack:nil];
            }
            else {
                t = @"NOT call finishAndPopViewController";
            }
            
            DLog(@"Got playback complete>>>>  OUT, %@", t);
        }
            break;
        case MEDIA_SEEK_COMPLETE:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.activityIndicator.hidden = YES;
                self.ib_playPlayBack.selected = NO;
                self.ib_myOverlay.userInteractionEnabled = YES;
                [self performSelector:@selector(watcher) withObject:nil afterDelay:2];
            });
            break;
        }
            
        default:
            break;
    }
}

- (void)recordRemoteStream {

    if ( [_urlVideoStr hasPrefix:@"rtmp"] || [_urlVideoStr hasPrefix:@"rtmps"]) {
        
        NSString *fileName = [NSString stringWithFormat:@"%@.%@", _clipInfo.cameraName, @"flv"];
        fileName = [NSString stringWithFormat:@"%@_%@", [PlaybackViewController CreateUUID], fileName];
        NSString *fullPath = @"";

        if (_clipInfo.fileName.length > 0) {
            fullPath = [self getRecordingPathWithFileName:_clipInfo.fileName];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if ([fileManager fileExistsAtPath:fullPath]) {
                return;
            }
        }
        else {
            fullPath = [self getRecordingPathWithFileName:fileName];
        }
        
        _isRemoteRecordingOnBG = YES;
        _remoteFilePath = fullPath;
        MediaPlayer::Instance()->startRecord([fullPath UTF8String]);
    }
}

- (void)playStream
{
    status_t status = !NO_ERROR;
    status =  MediaPlayer::Instance()->start();
    DLog(@"start() return: %d\n", status);

    if ( status != NO_ERROR ) {
        // NOT OK
        DLog(@"start() error: %d\n", status);
        [self handleMessage:MEDIA_ERROR_SERVER_DIED ext1:0 ext2:0];
        return;
    }

    if ( status == NO_ERROR ) {
        [self handleMessage:MEDIA_PLAYER_STARTED ext1:0 ext2:0];
    }
    self.isPause = NO;
}

- (void)stopStream
{
    @synchronized(self) {
        if ( MediaPlayer::Instance()->isPlaying() ) {
            DLog(@"Stop stream start ");
            
            MediaPlayer::Instance()->suspend(YES);
            MediaPlayer::Instance()->stop();
            MediaPlayer::Instance()->setListener(nil);
            
            DLog(@"Stop stream end");
        }
    }
}

- (void)pauseStream
{
    if ( MediaPlayer::Instance()->isPlaying() ) {
        self.isPause = YES;
        MediaPlayer::Instance()->pause();
    }
}

- (void)resumeStream
{
    self.isPause = NO;
    [self watcher];
    MediaPlayer::Instance()->resume();
}

- (void)seekToStream:(double)seconds
{
    MediaPlayer::Instance()->seekTo(seconds);
    self.isPause = NO;
    [self watcher];
}

#pragma mark - FONT

- (void)applyFont
{
    [_ib_timerPlayBack setFont:[UIFont systemFontOfSize:13]];
//    _ib_timerPlayBack.textColor = [UIColor textTimerPlayBackColor];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)finishAndPopViewController
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    _ib_playPlayBack.enabled = NO;
    
    [self stopStream];
    
    if ( _list_refresher ) {
        [_list_refresher invalidate];
    }
    
    DLog(@"%s After calling stop stream.", __FUNCTION__); // Debug log, 'cause sometime app hang here, high chance, relate to Crittercism crash
    
    if ( _playbackVCDelegate && [_playbackVCDelegate respondsToSelector:@selector(playbackStopped)] ) {
        [_playbackVCDelegate playbackStopped];
    }
    
//    DLog(@"%s After calling playback stopped.", __FUNCTION__); // Debug log, 'cause sometime app hang here, high chance, relate to Crittercism crash

    dispatch_async(dispatch_get_main_queue(), ^{
        if ( self.navigationController ) {
            if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft ||
                [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight ||
                [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft ||
                [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
            {
                if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
                    objc_msgSend([UIDevice currentDevice], @selector(setOrientation:), UIDeviceOrientationPortrait);
                }
            }
            
            [self.navigationController setNavigationBarHidden:NO animated:YES];
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
            [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
    });
}

#pragma mark - Rotation screen

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ( !_userWantToBack ) {
        [self adjustViewsForOrientation:toInterfaceOrientation];
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self checkOrientation];
}

- (void)checkOrientation
{
	[self adjustViewsForOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

- (void)adjustViewsForOrientation:(UIInterfaceOrientation)orientation
{
    float w = self.view.bounds.size.width;
    float h = self.view.bounds.size.height;
    if ( UIInterfaceOrientationIsLandscape(orientation) && w < h ) {
        w = self.view.bounds.size.height;
        h = self.view.bounds.size.width;
    }
    else if ( UIInterfaceOrientationIsPortrait(orientation) && h < w ) {
        w = self.view.bounds.size.height;
        h = self.view.bounds.size.width;
    }

	if ( UIInterfaceOrientationIsLandscape(orientation) ) {
        [_imageVideo setFrame:CGRectMake(0, 0, w, h)];
        
        [_ib_closePlayBack setImage:[UIImage imageNamed:@"vertcal_video_close"] forState:UIControlStateNormal];
        [_ib_closePlayBack setImage:[UIImage imageNamed:@"video_fullscreen_close_pressed"] forState:UIControlEventTouchDown];
        _ib_bg_top_player.hidden = NO;
	}
	else if ( UIInterfaceOrientationIsPortrait(orientation) ) {
        int viewHeight = w * (9/16.0f);
        int y = h/2 - viewHeight/2;
        
        CGRect newRect = CGRectMake(0, y, w, viewHeight);
        [self.imageVideo setFrame:newRect];
        
        [self.ib_closePlayBack setImage:[UIImage imageNamed:@"vertcal_video_close"] forState:UIControlStateNormal];
        [self.ib_closePlayBack setImage:[UIImage imageNamed:@"vertcal_video_close_pressed"] forState:UIControlEventTouchDown];
        self.ib_bg_top_player.hidden = NO;
	}
}

#pragma mark - Hide&Show Control

- (void)singleTapGestureCaptured:(id)sender
{
    if ( _ib_myOverlay.hidden ) {
        [self showControlMenu];
    }
    else {
        [self hideControlMenu];
    }
}

- (void)hideControlMenu
{
    [self.ib_myOverlay setHidden:YES];
    [self.ib_viewOverlayVideo setHidden:YES];
    self.view.userInteractionEnabled = YES;
}

- (void)showControlMenu
{
    [self.ib_myOverlay setHidden:NO];
    [self.view bringSubviewToFront:self.ib_myOverlay];
    
    if (_isDownloading) {
        _ib_viewOverlayVideo.hidden = YES;
    }
    else {
        _ib_viewOverlayVideo.hidden = NO;
    }
    
    [self.view bringSubviewToFront:self.ib_viewOverlayVideo];
    
    if ( _timerHideMenu ) {
        [self.timerHideMenu invalidate];
        self.timerHideMenu = nil;
    }
    
    self.timerHideMenu = [NSTimer scheduledTimerWithTimeInterval:10
                                                          target:self
                                                        selector:@selector(hideControlMenu)
                                                        userInfo:nil
                                                         repeats:NO];
}

#pragma mark - Actions

- (IBAction)deleteVideo:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"Confirm")
                                                    message:LocStr(@"Are you sure?")
                                                   delegate:self
                                          cancelButtonTitle:LocStr(@"Cancel")
                                          otherButtonTitles:LocStr(@"Ok"), nil];
    alert.tag = DELETE_RECORD_DIALOG_TAG;
    [alert show];
}

// Show a progress overlay, download the video and write it to
// the Videos folder and give user a confirmation of success or failure.
// Create and show a progress HUD.
- (IBAction)downloadVideo:(id)sender
{
    if (_isDownloading) {
        return;
    }
    
    _isDownloading = YES;
    _ib_viewOverlayVideo.hidden = YES;

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = LocStr(@"Downloading...");
    
    if (_isRemoteRecordingOnBG) {
        if ( hud.mode != MBProgressHUDModeAnnularDeterminate ) {
            // Show determinate in the HUD
            [hud willChangeValueForKey:@"mode"];
            hud.mode = MBProgressHUDModeAnnularDeterminate;
            [hud didChangeValueForKey:@"mode"];
        }
        hud.progress = 0;

        return;
    }
    else {
        // Pause if needed
        if ( MediaPlayer::Instance()->isPlaying() ) {
            [self playVideo:nil]; // Yes this pauses... I know it looks nasty!!!
        }
    }
}

// Show a progress overlay, download the video and then share allow to Email or Facebook.
- (IBAction)shareVideo:(id)sender
{
    if (!_isRemoteRecordingOnBG) {
        // Pause if needed
        if ( MediaPlayer::Instance()->isPlaying() ) {
            [self playVideo:nil]; // Yes this pauses... I know it looks nasty!!!
        }
        
        if ( [_urlVideoStr hasPrefix:@"/"] ) {
            [self shareLocalVideo:[NSURL fileURLWithPath:_urlVideoStr]];
            return;
        }
    }
    
    if (_isDownloading) {
        return;
    }
    
    _isDownloading = YES;
    _isSharing = YES;
    _ib_viewOverlayVideo.hidden = YES;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = LocStr(@"Downloading...");
    
    if (_isRemoteRecordingOnBG) {
        if ( hud.mode != MBProgressHUDModeAnnularDeterminate ) {
            // Show determinate in the HUD
            [hud willChangeValueForKey:@"mode"];
            hud.mode = MBProgressHUDModeAnnularDeterminate;
            [hud didChangeValueForKey:@"mode"];
        }
        hud.progress = 0;
        
        return;
    }
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
    double seekTarget = sender.value * _duration;
    NSInteger currentTime = floor(seekTarget);
    NSInteger totalTime = floor(self.duration);

    [self updateTimerWithCurrentTime:currentTime andTotalTime:totalTime];
}

- (IBAction)sliderProgressTouchDownAction:(UISlider *)sender
{
    DLog(@"%s", __FUNCTION__);
    [self pauseStream];
}

- (IBAction)sliderProgressTouchUpInsideAction:(UISlider *)sender
{
    double seekTarget = sender.value * _duration;
    DLog(@"%s value: %f, target: %f", __FUNCTION__, sender.value, seekTarget);

    [self seekToStream:seekTarget];
}

- (IBAction)closePlayBack:(id)sender
{
    if ( _userWantToBack ) {
        return;
    }
    
    self.view.userInteractionEnabled = NO;
    
    DLog(@"\n\n%s", __FUNCTION__);
    if (sender && _isRemoteRecordingOnBG && _isDownloading) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LocStr(@"WARNING")
                                                            message:LocStr(@"Exiting this screen will cancel downloading. Do you want to proceed?")
                                                           delegate:self
                                                  cancelButtonTitle:LocStr(@"Cancel")
                                                  otherButtonTitles:LocStr(@"Ok"), nil];
            alert.tag = EXIT_DIALOG_TAG;
            [alert show];
        });
        return;
    }
    
    if (_isRemoteRecordingOnBG) {
        MediaPlayer::Instance()->stopRecord();
        _isRemoteRecordingOnBG = NO;
        
        if (!_isDownloading && ![_remoteFilePath isEqualToString: @""]) {
            [[NSFileManager defaultManager] removeItemAtPath: _remoteFilePath error: nil];
        }
        
        if (_isSharing) {
            [self shareLocalVideo:[NSURL fileURLWithPath:_remoteFilePath]];
            return;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if ( ([_urlVideoStr hasPrefix:@"rtmp"] || [_urlVideoStr hasPrefix:@"rtmps"])) {
            self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [_hud setLabelText:LocStr(@"Close...")];
            [_hud show:YES];
        }
        
        self.ib_viewControlPlayer.hidden = YES;
    });
    
    self.ib_viewControlPlayer.hidden = YES;
    // Handle removing all callback notifications here.
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(watcher)
                                               object:nil];
    if ( _isPause ) {
        MediaPlayer::Instance()->resume();
    }

    self.isUserCancelDownloading = YES;
    self.userWantToBack = YES;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self finishAndPopViewController];
    });
}

- (IBAction)playVideo:(id)sender
{
    DLog(@"%s wants to pause: %d", __FUNCTION__, _isPause);
    if ( _fromStart ) {
        self.fromStart = NO;
        [self playStream];
        [self watcher];
        self.ib_playPlayBack.selected = NO;
    }
    else if ( _isPause ) {
        [self resumeStream];
        [self watcher];
        self.ib_playPlayBack.selected = NO;
    }
    else if ( MediaPlayer::Instance()->isPlaying() ) {
        [self pauseStream];
        self.ib_playPlayBack.selected = YES;
    }
}

#pragma mark - Display Time
- (void)setMultiClipsDuration:(int)duration
{
    self.duration = duration;
}

- (void)watcher
{
    if ( MediaPlayer::Instance() == NULL || _isPause || _userWantToBack ) {
        return;
    }
    
    double totalTimeTemp = _duration;
    
    if (totalTimeTemp <= 0) { // has not known the last sub clip yet
        totalTimeTemp = MediaPlayer::Instance()->getDuration();
    }

    self.timeStarting = MediaPlayer::Instance()->getTimeStarting();
    double timeCurrent = MediaPlayer::Instance()->getCurrentTime() - _timeStarting;
    
    if (timeCurrent < 0) {
        timeCurrent = _currentTime;
    }
    
    self.ib_sliderPlayBack.value = (timeCurrent + 0.5) / (totalTimeTemp == 0 ? 1: totalTimeTemp);

    if (_isRemoteRecordingOnBG && _isDownloading) {
        MBProgressHUD *hud = [MBProgressHUD HUDForView: self.view];
        if (hud) {
            hud.progress = self.ib_sliderPlayBack.value;
        }
    }
    
    self.currentTime = floor(timeCurrent + 0.5);
    NSInteger totalTime = floor(totalTimeTemp);

    [self updateTimerWithCurrentTime:_currentTime andTotalTime:totalTime];

    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(watcher)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)updateTimerWithCurrentTime:(NSInteger)currentTime andTotalTime:(NSInteger)totalTime
{
    if (totalTime == PLAYBACK_DURATION_MAX) { // Duration is not ready yet.
        self.ib_timerPlayBack.text = [NSString stringWithFormat:@"%02ld:%02ld / --:--", currentTime / 60, currentTime % 60];
    }
    else {
        if (currentTime > totalTime) {
            currentTime = totalTime;
        }
        
        self.ib_timerPlayBack.text = [NSString stringWithFormat:@"%02d:%02d / %02ld:%02ld", currentTime / 60, currentTime % 60, totalTime / 60, totalTime % 60];
    }
    
    if ( ([_urlVideoStr hasPrefix:@"rtmp"] || [_urlVideoStr hasPrefix:@"rtmps"]) && currentTime >= totalTime && totalTime > 0) {
        DLog(@"\n\n%s closePlayBack", __FUNCTION__);
        [self closePlayBack:nil];
    }
}

#pragma mark - Poll camera events

- (void)getCameraPlaylistEventOnSDCard
{
}

- (void)getCameraPlaylistForEvent:(NSTimer *)clipTimer
{
}

- (void)getPlaylistSuccessWithResponse:(NSDictionary *)responseDict
{
}

- (void)getPlaylistFailedWithResponse:(NSDictionary *)responseDict
{
    DLog(@"getPlaylistFailedWithResponse");
    self.list_refresher = [NSTimer scheduledTimerWithTimeInterval:10.0
                                                           target:self
                                                         selector:@selector(getCameraPlaylistForEvent:)
                                                         userInfo:_clipInfo repeats:NO];
}

- (void)getPlaylistUnreachableSetver
{
    DLog(@"getPlaylistUnreachableSetver");
    self.list_refresher = [NSTimer scheduledTimerWithTimeInterval:10.0
                                                           target:self
                                                         selector:@selector(getCameraPlaylistForEvent:)
                                                         userInfo:_clipInfo repeats:NO];
}

#pragma mark - Private methods

+ (NSString *)CreateUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge_transfer NSString *)string;
}

- (NSString *)getRecordingPathWithFileName:(NSString *)fileName
{
    NSString *fullPath = PathForFileName(fileName);
    fullPath = [fullPath stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    return fullPath;
}

- (void)shareLocalVideo:(NSURL *)videoURL
{
}

#pragma mark - UIAlertView delegate

- (void )alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex != alertView.cancelButtonIndex ) {
        if ( alertView.tag == DELETE_RECORD_DIALOG_TAG ) {
            if ( [_playbackVCDelegate respondsToSelector:@selector(deleteVideoAtIndex:)] && _recordSelectedIndex >= 0) {
                [_playbackVCDelegate deleteVideoAtIndex:_recordSelectedIndex];
                [self closePlayBack:nil];
            }
            else if (_intEventId > 0) {
            }
        }
        else if ( alertView.tag == EXIT_DIALOG_TAG ) {
            _isDownloading = NO;
            _isSharing = NO;
            [self closePlayBack: nil];
        }
    }
}

#pragma mark - Memory management

- (void)dealloc
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    _playbackVCDelegate = nil;
    _playbackStreamer = nil;
    _listener = nil;
}

#pragma mark - Create thumbnail image for download video

- (void)createThumbImage:(void(^)(UIImage *image))complete {
    if (_clipInfo.imgSnapshot) {
        if (complete) {
            complete(_clipInfo.imgSnapshot);
        }
    }
    else if (_clipInfo.urlImage.length) {
    }
    else {
        if (complete) {
            complete(_firstImageOfVideoView.image);
        }
    }
}

@end
