//
//  RecordCollectionViewController.h
//  Dev
//
//  Created by Dev on 12/5/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <UIKit/UIKit.h>

@interface RecordCollectionViewController : UIViewController

- (void)preplayVideoWithTitle:(NSString *)videoTitle;

@end
