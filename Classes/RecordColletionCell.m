//
//  RecordColletionCell.m
//  Dev
//
//  Created by Dev on 12/5/15.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "RecordColletionCell.h"
#import "PublicDefine.h"

@implementation RecordColletionCell

- (void)awakeFromNib {
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _selectedButton.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _selectedButton.layer.borderWidth = 1.0;
}

- (void)showRecording:(Recording*)record{
    self.record = record;
    
    [_nameLabel setText:record.cameraName];
    [_dateLabel setText:record.createdDate]; 
    
    NSString *fileName = record.snapshotImagePath;
    
    NSData *data = [NSData dataWithContentsOfFile:fileName];
    
    if (data) {
        [_snapshotImage setImage:[UIImage imageWithData:data]];
    }
    else {
        [_snapshotImage setImage:nil];
    }
    
    [_selectedButton setSelected:record.isSelected];
    
    if (record.isSelected) {
        _selectedButton.layer.cornerRadius = _selectedButton.frame.size.width/2;
        _selectedButton.hidden = NO;
        self.layer.borderWidth = 2.0f;
        self.layer.borderColor = [[UIColor colorWithRed:19.0/255
                                                  green:154.0/255
                                                   blue:244.0/255
                                                  alpha:1.0] CGColor];
    }
    else {
        _selectedButton.hidden = YES;
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
}

@end
