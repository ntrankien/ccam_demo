//
//  Recording.h
//  Dev
//
//  Created by Dev on 12/17/14.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

@interface Recording : NSObject

@property (nonatomic, copy) NSString *recordName;

@property (nonatomic, strong, getter=fullPath) NSString *fullPath;
@property (nonatomic, strong, getter=snapshotImagePath) NSString *snapshotImagePath;
@property (nonatomic, strong, getter=cameraName) NSString *cameraName;
@property (nonatomic, strong, getter=createdDate) NSString *createdDate;

@property BOOL isSelected;

- (void)initAttributes;

@end
