//
//  Recording.m
//  Dev
//
//  Created by Dev on 12/17/14.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "Recording.h"
#import "PublicDefine.h"

@implementation Recording

- (void)initAttributes {
    if (_recordName) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docPath = [paths objectAtIndex:0];
        NSString *fullName = _recordName;
        NSString *fullPath = [docPath stringByAppendingPathComponent:fullName];
        
        NSInteger underLoc = [fullName rangeOfString:@"_"].location;
        
        NSString *cameraName = nil;
        if (underLoc != NSNotFound) {
            cameraName = [fullName substringFromIndex:underLoc + 1];
        }
        else {
            cameraName = fullName;
        }
        
        cameraName = [cameraName stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@".%@", @"flv"] withString:@""];
        
        NSString *fileName = [fullName stringByReplacingOccurrencesOfString:@"flv" withString:@"png"
                                                                    options:NSCaseInsensitiveSearch
                                                                      range:NSMakeRange(0, [fullName length])];
        fileName = [docPath stringByAppendingPathComponent:fileName];
        
        self.cameraName = cameraName;
        self.createdDate = [Recording getCreatedDateFile:fullPath];
        self.snapshotImagePath = fileName;
    }
}

- (NSString*)cameraName {
    if (!_cameraName) {
        NSInteger underLoc = [_recordName rangeOfString:@"_"].location;
        
        NSString *cameraName = nil;
        if (underLoc != NSNotFound) {
            cameraName = [_recordName substringFromIndex:underLoc + 1];
        }
        else {
            cameraName = _recordName;
        }
        
        cameraName = [cameraName stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@".%@", @"flv"] withString:@""];
        
        self.cameraName = cameraName;
    }
    
    return _cameraName;
}

- (NSString*)fullPath {
    if (!_fullPath) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docPath = [paths objectAtIndex:0];
        NSString *fullPath = [docPath stringByAppendingPathComponent:_recordName];
        self.fullPath = fullPath;
    }
    
    return _fullPath;
}

- (NSString*)snapshotImagePath {
    if (!_snapshotImagePath) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docPath = [paths objectAtIndex:0];
        
        NSString *fileName = [_recordName stringByReplacingOccurrencesOfString:@"flv" withString:@"png"
                                                                       options:NSCaseInsensitiveSearch
                                                                         range:NSMakeRange(0, [_recordName length])];
        fileName = [docPath stringByAppendingPathComponent:fileName];
        self.snapshotImagePath = fileName;
    }
    
    return _snapshotImagePath;
}

- (NSString*)createdDate {
    if (!_createdDate) {
        self.createdDate = [Recording getCreatedDateFile:[self fullPath]];
    }
    
    return _createdDate;
}

+ (NSString *)getLocaleIdentifier
{
    if ([[NSLocale preferredLanguages] firstObject]) {
        return [[NSLocale preferredLanguages] firstObject];
    }
    return @"en_US";
}

+ (NSString *)getCreatedDateFile:(NSString *)path {
    NSString *createdDateStr = nil;
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSDictionary *attrs = [fm attributesOfItemAtPath:path error:nil];
    
    if ( attrs ) {
        NSDate *date = (NSDate *)[attrs fileCreationDate];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:[[self class] getLocaleIdentifier]]];
        
//        if ( [UserUtil isTimeFormat24H] ) {
            [dateFormat setDateFormat:@"MMM dd yyyy HH:mm:ss"];
//        }
//        else {
//            [dateFormat setDateFormat:@"MMM dd yyyy hh:mm:ss a"];
//        }
        
        createdDateStr = [dateFormat stringFromDate:date];
    }
    
    return createdDateStr;
}

@end
