//
//  AppDelegate.m
//  App
//
//  Created by Developer on 9/26/11.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "AppDelegate.h"
#import "CameraPlayerViewController.h"
#import <CameraScanner/CameraScanner.h>
#import "HttpCom.h"
#import "RMCHandler_2.h"
#import <MDeviceComm/MDeviceComm.h>

#import "NSData+AESCrypt.h"
#import "NSString+AES.h"
#import <MessageUI/MessageUI.h>

@interface AppDelegate () <RmcHanlderDelegate_2>

@property (nonatomic, strong) CamChannel* channel;

@end

@implementation AppDelegate

#pragma mark - Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:ALREADY_LAUNCH_KEY]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ALREADY_LAUNCH_KEY];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ALLOW_P2P_REMINDER_POPUP_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self registerDefaultsFromSettingsBundle];
    

    // Redirect NSLog output to file for DEBUG and ADHOC builds so that users
    // can retrieve using iTunes desktop application.
#if TARGET_IPHONE_SIMULATOR == 0
    NSString *logPath = PathForFileName(@"console.log");
    freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding],"a+", stderr);
#endif
    
#if 0
    
    //Reset IS_NEW_SETUP to NO
    NSUserDefaults *userDefaults = kUserDefault;
    [userDefaults setBool:NO forKey:IS_NEW_SETUP];
    [userDefaults setBool:NO forKey:kDidLoadRecordingList];
    [userDefaults synchronize];

    CameraPlayerViewController* videoCtrl = [[CameraPlayerViewController alloc] init];
    CamChannel* ch = [[CamChannel alloc] init];
    ch.profile = [[CamProfile alloc] init];
    ch.profile.name = @"Demo";
    ch.profile.ip_address = @"192.168.222.1";
    ch.profile.registrationID = @"011081000AE230B8AEWAOAGQ";
    ch.profile.hasUpdateLocalStatus = YES;
    ch.profile.minuteSinceLastComm  =   1;
    ch.profile.port                 =  80;
    ch.profile.ptt_port             = IRABOT_AUDIO_RECORDING_PORT;
    ch.profile.isInLocal            = NO;
    
    [HttpCom instance].comWithDevice.device_ip = ch.profile.ip_address;
    [HttpCom instance].comWithDevice.device_port = ch.profile.port;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self startP2PStreamRemoteForCam:ch];
    });
    
    videoCtrl.selectedChannel = ch;
    self.channel = ch;
    
    _window.rootViewController = [[UINavigationController alloc] initWithRootViewController:videoCtrl];
    videoCtrl.navigationController.navigationBar.translucent = NO;
    
#else
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    UIViewController* camVC = [MainStoryBoard instantiateViewControllerWithIdentifier:@"CamerasViewController"];
    _window.rootViewController = [[UINavigationController alloc] initWithRootViewController:camVC];
    camVC.navigationController.navigationBar.translucent = NO;
    
#endif
    
    
    [_window makeKeyAndVisible];
    
    return YES;
}

- (RMCHandler_2 *)createTheRMCHandlerForCam:(CamChannel *)ch
{
    
    RMCHandler_2 *theTmpRMC = nil;
    
    //Phung: Create ONLY if the current RMCHandler_2 is nil /nul
    if (ch.profile.p2pHandler == nil)
    { //ONly first time
        
        theTmpRMC = [[RMCHandler_2 alloc] init];
        [theTmpRMC setDelegate:self];
        theTmpRMC.fwVersion = ch.profile.fw_version;
        theTmpRMC.camModel = ch.profile.model;
        
        ch.profile.p2pHandler = theTmpRMC;
        ch.profile.p2pSess = nil;
    }
    else {
        
        DLog(@"Cam profile p2pHanlder is available, reuse it >>>>>>>> ");
        theTmpRMC  = ch.profile.p2pHandler;
    }
    
    return theTmpRMC;
}

//Create P2P Remote Connection
- (void)startP2PStreamRemoteForCam:(CamChannel *)ch
{
    DLog(@"%s UDID: %@", __FUNCTION__, ch.profile.registrationID);
    
    RMCHandler_2* theTmpRMC = [self createTheRMCHandlerForCam:ch];
    
    //NSString *mode = P2P_MODE_REMOTE_2;
    __weak AppDelegate *weakSelf = self;
    
    P2PCommResult (^blockCallback2)(PConfigInfo*, NSString*) = ^(PConfigInfo *config, NSString* udid) {
        P2PCommResult t2 = [weakSelf communicateWithCameraConfigInfo:config forChannel:ch mode:P2P_MODE_REMOTE_2];
        return t2;
    };
    
    theTmpRMC.outputCallback = blockCallback2;
    theTmpRMC.delegate = self;
    
    NSString *camUdid = ch.profile.registrationID;
    [theTmpRMC connectToCam:camUdid WithMode:P2P_MODE_REMOTE_2];
}

- (P2PCommResult)communicateWithCameraConfigInfo:(PConfigInfo *)configInfo forChannel:(CamChannel*)ch mode:(NSString *) aMode
{
    /**
     * key=[AES_DECRYPT_KEY]
     * &sip=[RELAY_SERVER_IP]
     * &sp=[RELAY_SERVER_PORT]
     * &rn=[RANDOM_NUMBER]
     **/
    
    NSString * device_udid = ch.profile.registrationID;
    
    NSString *aUUIDString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSString *uuidString = [aUUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    u_int32_t streamNum = arc4random_uniform(9);
    uuidString = [uuidString stringByReplacingCharactersInRange:NSMakeRange(uuidString.length - 2, 2) withString:[NSString stringWithFormat:@"_%d", streamNum]];
    //    NSString *streamName = uuidString;
    
    NSString *cmd = [NSString stringWithFormat:@"get_session_key&mode=%@&port1=%d&ip=%@&streamname=%@", configInfo.commMode, configInfo.port, configInfo.ip, uuidString];
    
    
    
    if (  [aMode isEqualToString:P2P_MODE_LOCAL_2] ==  YES)
    {
        
        HttpCommunication *dev_com = [[HttpCommunication alloc] initWithNewFlag:YES];
        dev_com.device_port = 9090;
        dev_com.device_ip = ch.profile.ip_address  ;
        
        
        
        NSString *reply = [dev_com sendCommandAndBlock:cmd];
        DLog(@"%s %@ in local, reply:%@", __FUNCTION__,device_udid, reply);
        
        if ( reply.length <= @"get_session_key: -1".length ) {
            return P2PCommFailure;
        }
        
        NSString *response = [[[reply substringFromIndex:@"get_session_key: ".length] componentsSeparatedByString:@","] objectAtIndex:1];
        response = [response stringByReplacingOccurrencesOfString:@"&&" withString:@"&"];
        NSMutableArray *token = [response componentsSeparatedByString:@"&"].mutableCopy;
        
        for ( int i = 0; i < token.count; i++ ) {
            NSString *value = token[i];
            NSRange range = [value rangeOfString:@"="];
            
            if ( range.location != NSNotFound ) {
                token[i] = [value substringWithRange:NSMakeRange(range.location + 1, value.length - (range.location + 1))];
            }
        }
        
        if ( !token || token.count < 2 ) {
            return P2PCommFailure;
        }
        
        if ( token.count < 3 ) {
            [token addObject:@"68656C6C6F"];
        }
        
        configInfo.port  = [token[0] intValue];
        configInfo.ip = token[1];
        [configInfo keyFromHexString:token[2]];
        
        
        
    }
    else
    {
        
        
        
        ///REMOTE CASE
        DeviceCommunication* dc = [[DeviceCommunication alloc] init];
        
        __block id sessionKeyData;
        [dc sendInstanceCommand_old:cmd
                       toDevice:device_udid
                        timeout:6
                     completion:^(BOOL success, id data, NSError *error) {
                         if (success && [data isKindOfClass:[NSDictionary class]]) {
                             sessionKeyData = [data arrayForKey:@"DATA"];
                         }
                         else{
                             DLog(@"%s failed, data: %@", __FUNCTION__, data);
                         }
                     }];
        
        
        
        
        NSDate *startTime = [NSDate date];
        
        while (!sessionKeyData && (ABS(startTime.timeIntervalSinceNow) < 30)) {
            [NSThread sleepForTimeInterval:0.5];
        }
        
        
        
        if (!sessionKeyData || ![sessionKeyData isKindOfClass:[NSArray class]]) {
            DLog(@"%s %@ OR MQTT fails ", __FUNCTION__, device_udid);
            
            return P2PCommFailure;
        }
        
        DLog(@"%s %@ MQTT response:%@", __FUNCTION__, device_udid, sessionKeyData);
        
        NSArray *values = (NSArray *)sessionKeyData;
        NSInteger error;
        
        for (NSString *value in values) {
            if ([value hasPrefix:@"error="]) {
                error = [[value stringByReplacingOccurrencesOfString:@"error=" withString:@""] intValue];
            }
            if ([value hasPrefix:@"key="]) {
                NSString *key = [value stringByReplacingOccurrencesOfString:@"key=" withString:@""];
                [configInfo keyFromHexString:key];
            }
            if ([value hasPrefix:@"sip="]) {
                NSString  *sip = [value stringByReplacingOccurrencesOfString:@"sip=" withString:@""];
                configInfo.sip = sip;
            }
            if ([value hasPrefix:@"sp="]) {
                NSString *sp = [value stringByReplacingOccurrencesOfString:@"sp=" withString:@""];
                configInfo.sp = [sp intValue];
            }
            if ([value hasPrefix:@"rn="]) {
                NSString *rn = [value stringByReplacingOccurrencesOfString:@"rn=" withString:@""];
                [configInfo rnFromHexString:rn];
            }
            if ([value hasPrefix:@"port1="]) {
                NSString *port1 = [value stringByReplacingOccurrencesOfString:@"port1=" withString:@""];
                configInfo.port = [port1 intValue];
            }
            if ([value hasPrefix:@"ip="]) {
                NSString *ip = [value stringByReplacingOccurrencesOfString:@"ip=" withString:@""];
                configInfo.ip = ip;
            }
        }
        
        //3id: E076D0148F99&time: 1464606896&get_session_key: error=200,port1=46612&ip=118.68.40.25&key=716d487524654933245e256730277d31
        
        configInfo.streamName = uuidString;
        
        
    }
    return P2PCommSuccess;
}

#pragma mark - RmcHanlderDelegate_2

- (void) rmcHandler:(RMCHandler_2*)rmchandler didFailToConnectCam:(NSString *)aMac mode:(NSString *)aMode
{
    NSLog(@"%s MAC: %@, mode: %@", __FUNCTION__, aMac, aMode);
    //TODO: STOP & clean up
}

- (void) rmcHandler:(RMCHandler_2*)rmchandler didConnectSuccessToDeviceID:(NSString *)udid
{
    NSLog(@"%s UDID: %@", __FUNCTION__, udid);
    
    if ([self.channel.profile.registrationID isEqualToString:udid] ) {
        _channel.profile.isP2PAlready = YES; //Stop indicator
    }
}

- (void) rmcHandler:(RMCHandler_2*)rmchandler timeoutWhileReceivingDataWithDeviceID:(NSString *)udid
{
    NSLog(@"%s UDID: %@", __FUNCTION__, udid);
}

#pragma mark -

/*
 * A bit mask of the UIInterfaceOrientation constants that indicate the orientations
 * to use for the view controllers.
 */
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return  UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSString *logPath = PathForFileName(@"console.log");
    [self createANewAppLog:logPath];
}

- (void)registerDefaultsFromSettingsBundle
{
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    
    if ( !settingsBundle ) {
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    
    for ( NSDictionary *prefSpecification in preferences ) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        
        if ( key ) {
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)createANewAppLog: (NSString *)appLogPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = FALSE;
    NSError *error;
    
    if ( [[fileManager attributesOfItemAtPath:appLogPath error:&error] fileSize] > 5000000 ) { // 5MB
        NSString *appLog0 = PathForFileName(@"console0.log");
        
        if ( [fileManager fileExistsAtPath:appLog0] ) {
            success = [fileManager removeItemAtPath:appLog0 error:&error];
            
            if ( success ) {
                DLog(@"Remove app log 0 success");
            }
            else {
                DLog(@"Remove app log 0 error: %@", [error description]);
            }
        }
        
        success = [fileManager copyItemAtPath:appLogPath toPath:appLog0 error:&error];
        
        if ( success ) {
            DLog(@"Copy success");
            success = [fileManager removeItemAtPath:appLogPath error:&error];
            
            if ( success ) {
                DLog(@"Remove app log success");
                freopen([appLogPath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
                
                [self writeSystemInfoLog];
            }
            else {
                NSLog(@"Remove app log err: %@", [error description]);
            }
        }
        else {
            NSLog(@"Copy error: %@", [error description]);
        }
    }
}

- (void)writeSystemInfoLog {
    DLog(@"System Info Log_________________________________________________________________________________________________________");
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appName = infoDictionary[@"CFBundleDisplayName"];
    NSString *appVersion = infoDictionary[@"CFBundleShortVersionString"];
    NSString *appBuild = infoDictionary[(NSString *)kCFBundleVersionKey];
    
    DLog(@"%@ %@ (%@)", appName, appVersion, appBuild);
    
    UIDevice *device = [UIDevice currentDevice];
    
    DLog(@"Name: %@ - Model: %@ - System Name: %@ - Version: %@", device.name, device.model, device.systemName, device.systemVersion);
    DLog(@"________________________________________________________________________________________________________________________");
}

#pragma mark - Util

+ (void)getBroadcastAddress:(NSString **)bcast AndOwnIp:(NSString**)ownip
{
    //Free & re-init Addresses
    FreeAddresses();
    
    GetIPAddresses();
    GetHWAddresses();
    NSString *deviceBroadcastIP = nil;
    NSString *deviceIP = nil ;
    NSString *log = @"";
    int i;
    
    for ( i = 0; i < MAXADDRS; ++i ) {
        static unsigned long localHost = 0x7F000001;		// 127.0.0.1
        unsigned long theAddr;
        
        theAddr = ip_addrs[i];
        
        if (theAddr == INVALID_IP) {
            break;
        }
        
        if (theAddr == localHost) {
            continue;
        }
        
        if (strncmp(if_names[i], "en", strlen("en")) == 0) {
            deviceBroadcastIP =  [NSString stringWithFormat:@"%s", broadcast_addrs[i]];
            deviceIP = [NSString stringWithFormat:@"%s", ip_names[i]];
        }
        
        DLog(@"%d %s %s %s %s\n", i, if_names[i], hw_addrs[i], ip_names[i],
             broadcast_addrs[i]);
        
        log = [log stringByAppendingFormat:@"%d %s %s %s %s\n", i, if_names[i], hw_addrs[i], ip_names[i],
               broadcast_addrs[i]];
    }
    
    if ( deviceIP ) {
        *ownip = [NSString stringWithString:deviceIP];
    }
    
    if ( deviceBroadcastIP ) {
        *bcast = [NSString stringWithString:deviceBroadcastIP];
    }
}

+ (void)getBroadcastAddress:(NSString **)bcast AndOwnIp:(NSString**)ownip ipasLong:(long *)_ownip
{
    //Free & re-init Addresses
    FreeAddresses();
    
    GetIPAddresses();
    GetHWAddresses();
    NSString *deviceBroadcastIP = nil;
    NSString *deviceIP = nil ;
    NSString *log = @"";
    int i;
    
    for ( i = 0; i < MAXADDRS; ++i ) {
        static unsigned long localHost = 0x7F000001;		// 127.0.0.1
        unsigned long theAddr;
        theAddr = ip_addrs[i];
        
        if (theAddr == INVALID_IP) {
            break;
        }
        
        if (theAddr == localHost) {
            continue;
        }
        
        if (strncmp(if_names[i], "en", strlen("en")) == 0) {
            deviceBroadcastIP =  [NSString stringWithFormat:@"%s", broadcast_addrs[i]];
            deviceIP = [NSString stringWithFormat:@"%s", ip_names[i]];
            *_ownip = ip_addrs[i];
        }
        
        log = [log stringByAppendingFormat:@"%d %s %s %s %s\n",
               i, if_names[i], hw_addrs[i], ip_names[i], broadcast_addrs[i]];
    }
    
    if ( deviceIP ) {
        *ownip = [NSString stringWithString:deviceIP];
    }
    
    if ( deviceBroadcastIP ) {
        *bcast = [NSString stringWithString:deviceBroadcastIP];
    }
}

+ (MFMailComposeViewController *)composeEmailWithCamLog:(NSData *)aData name:(NSString *)logName additionalInfo:(NSString*)info callback:(id)sender
{
    // Set the subject of email
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appName = infoDictionary[@"CFBundleDisplayName"];
    NSString *appVersion = infoDictionary[@"CFBundleShortVersionString"];
    NSString *appBuild = infoDictionary[(NSString *)kCFBundleVersionKey];
    
    appVersion = [NSString stringWithFormat:@"%@ (%@)", appVersion, appBuild];
#ifdef DEBUG
    DLog(@"%s app version:%@, DEBUG:%d", __FUNCTION__, appVersion, 1); // Need log before reading app log data.
#elif ADHOC
    DLog(@"%s app version:%@, ADHOC:%d", __FUNCTION__, appVersion, 1); // Need log before reading app log data.
#else
    DLog(@"%s app version:%@, RELEASE:%d", __FUNCTION__, appVersion, 1); // Need log before reading app log data.
#endif
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = sender;
    
    if ( aData ) {
        // Cam log
        [mc addAttachmentData:aData mimeType:@"application/x-gzip" fileName:[NSString stringWithFormat:@"%@.log", logName]];
    }
    
    // App log
    NSMutableData *dataZip = [[NSMutableData alloc] init];
    NSString *logPath0 = PathForFileName(@"console0.log");
    
    if ( [[NSFileManager defaultManager] fileExistsAtPath:logPath0] ) {
        NSData *dataLog0 = [NSData dataWithContentsOfFile:logPath0];
        
        if (dataLog0) {
            [dataZip appendData:dataLog0];
        }
    }
    
    NSString *logAppPath = PathForFileName(@"console.log");
    
    NSData *dataLog = [NSData dataWithContentsOfFile:logAppPath];
    [dataZip appendData:dataLog];
    
    DLog(@"%s dataLog.length:%lu", __FUNCTION__, (unsigned long)dataLog.length);
    NSData *dataZip1 = [NSData gzipData:dataZip];
    [mc addAttachmentData:[dataZip1 AES128EncryptWithKey:@"Super-LovelyDuck"] mimeType:@"text/plain" fileName:@"application.log"];
    
    NSString *email = [PrivateDataUtils email];
    
    if (!email) {
        email = @"Unknown";
    }
    
    [mc setSubject:[NSString stringWithFormat:@"%@, V%@ - <%@>", appName, appVersion, email]];
    NSString* messsageBody = LocStr(@"Resolving your issue is important to us. Please explain your issue in the field below. If possible, please provide your case number so we can more quickly resolve the issue.");
    if (info.length) {
        messsageBody = [[messsageBody stringByAppendingString:@"\n\n"] stringByAppendingString:info];
    }
    
    [mc setMessageBody:messsageBody isHTML:NO];
    [mc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    NSArray *toRecipents = [NSArray arrayWithObject:@"tknghi@gmail.com"];
    [mc setToRecipients:toRecipents];
    
    return mc;
}

+ (NSDictionary*)encryptDeviceConfiguration:(NSDictionary*)dict {
    NSMutableDictionary *dictEncrypt = [NSMutableDictionary dictionary];
    
    for (NSString *key in dict) {
        NSString *value = dict[key];
        NSString *valueEncrypt = [value stringAfterEncrypt];
        if (valueEncrypt) {
            [dictEncrypt setValue:valueEncrypt forKey:key];
        }
        else if (value) {
            [dictEncrypt setValue:value forKey:key];
        }
    }
    return dictEncrypt;
}

+ (NSDictionary*)decryptDeviceConfiguration:(NSDictionary*)dict {
    NSMutableDictionary *dictDecrypt = [NSMutableDictionary dictionary];
    
    for (NSString *key in dict) {
        NSString *value = dict[key];
        NSString *valueDecrypt = [value stringAfterDecrypt];
        if (valueDecrypt) {
            [dictDecrypt setValue:valueDecrypt forKey:key];
        }
        else if (value) {
            [dictDecrypt setValue:value forKey:key];
        }
    }
    return dictDecrypt.count ? dictDecrypt : nil;
}

@end
