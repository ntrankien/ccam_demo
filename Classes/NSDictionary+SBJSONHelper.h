//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

@interface NSDictionary (SBJSONHelper)
- (NSString*)uuidForKey:(id)key;
- (NSString*)stringForKey:(id)key;
- (NSArray*)arrayForKey:(id)key;
- (NSDate*)dateForKey:(id)key;
- (NSNumber*)numberForKey:(id)key;
- (NSDictionary*)dictionaryForKey:(id)key;

- (NSInteger)integerForKey:(id)key;
- (float)floatForKey:(id)key;
- (double)doubleForKey:(id)key;
- (long)longForKey:(id)key;
- (int)intForKey:(id)key;
- (int)intWithDefaultMinusOneForKey:(id)key;
- (BOOL)boolForKey:(id)key;
- (NSString*)stringWithDefaultNilForKey:(id)key;

- (void)setFloat:(float)value forKey:(id)key;
- (void)setInteger:(NSInteger)value forkey:(id)key;
- (void)setBool:(BOOL)value forKey:(id)key;
- (void)setInt:(int)value forKey:(id)key;
- (void)setLong:(long)value forKey:(id)key;
- (void)setDouble:(double)value forKey:(id)key;
- (void)setCGFloat:(CGFloat)value forKey:(id)key;

- (BOOL)canConvertToNumberForKey:(id)key;
- (BOOL)isNilOrNullForKey:(id)key;

- (BOOL)isStatusOk200;

@end
