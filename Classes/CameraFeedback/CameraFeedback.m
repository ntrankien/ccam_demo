//
//  CameraFeedback.m
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/21/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraFeedback.h"

@implementation CameraFeedback

+ (instancetype)feedbackWithTitle:(NSString*)title value:(id)value{
    CameraFeedback* camFB = [CameraFeedback new];
    camFB.title = title;
    camFB.value = value;
    return camFB;
}

- (NSString *)stringValue{
    if ([_value isKindOfClass:[NSString class]]) {
        return _value;
    }
    else if([_value isKindOfClass:[NSNumber class]]) {
        return [NSString stringWithFormat:@"%d", [((NSNumber*)_value) intValue]];
    }
    else if([_value isKindOfClass:[NSData class]]) {
        return @"<data-array>";
    }
    return nil;
}

@end
