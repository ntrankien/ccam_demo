//
//  CameraFeedbackManager.h
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/8/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <Foundation/Foundation.h>

typedef struct tMeboyInternalStat
{
    // Video Sensor /
    char  sensor_flicker;
    char  sensor_flipup;
    char  sensor_resolution;
    char  sensor_videoframerate;
    short sensor_videobitrate;
    char  sensor_videogop;
    char  sensor_videoqp;
    
    // Volume /
    short spk_volume;
    
    /* Wifi strength*/
    short wifi_rssi;
    
    // Meboy up time /
    unsigned int meboy_up_time;
}tMeboyInternalStat;

typedef struct tSOCHandStat
{
    uint8_t left_speed;
    uint8_t right_speed;
    uint8_t reserve;
    
    uint16_t s_cur_pos;
    uint16_t s_up_boundary;
    uint16_t s_down_boundary;
    
    uint16_t h_cur_pos;
    uint16_t h_up_boundary;
    uint16_t h_down_boundary;
    
    uint16_t w_cur_pos;
    uint16_t w_left_boundary;
    uint16_t w_right_boundary;
    
    uint16_t c_cur_pos;
    uint16_t c_open_boundary;
    uint16_t c_close_boundary;
    
}tSOCHandStat;

typedef struct tSOCWheelStat
{
    uint8_t reserve[20];
}tSOCWheelStat;

typedef struct tSOCStatus
{
    uint32_t      soc_vesion;
    tSOCWheelStat wheel;
    tSOCHandStat  hand;
    uint8_t       error_code;
    uint8_t       battery_value;
}tSOCStatus;

typedef struct
{
    // Internal status /
    tMeboyInternalStat internal_stat;
    
    // SOC status /
    tSOCStatus soc_status;
}CemeraFeedbackStruct;

@protocol CameraFeedbackManagerDelegate <NSObject>

- (void)cameraFeedbackManagerReceivedData:(CemeraFeedbackStruct)camFeedback;

@end

@interface CameraFeedbackManager : NSObject

@property (nonatomic) CemeraFeedbackStruct currentFeedback;

+ (instancetype)sharedInstance;

- (BOOL)startListeningCameraFeedback;
- (void)endListeningCameraFeedback;
- (void)addListener:(id<CameraFeedbackManagerDelegate>)listener;
- (void)removeListener:(id<CameraFeedbackManagerDelegate>)listener;
- (void)handleFeedbackData:(NSData *)data;

@end
