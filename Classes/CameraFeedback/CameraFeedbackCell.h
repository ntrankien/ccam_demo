//
//  CameraFeedbackCell.h
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/21/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <UIKit/UIKit.h>

@class CameraFeedback;

@interface CameraFeedbackCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel* lbTitle;
@property (nonatomic, weak) IBOutlet UILabel* lbValue;

- (void)displayCameraFeedback:(CameraFeedback*)cameraFeedback;

@end
