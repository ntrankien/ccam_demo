//
//  CameraFeedbackManager.m
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/8/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraFeedbackManager.h"
#import "GCDAsyncUdpSocket.h"

@interface CameraFeedbackManager()<GCDAsyncUdpSocketDelegate>

@property (nonatomic, strong) NSMutableArray* listeners;
@property (nonatomic, strong) GCDAsyncUdpSocket *udpSock;

uint8_t get_checksum(uint8_t *data, int len);

@end

@implementation CameraFeedbackManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t p = 0;
    
    __strong static id _sharedObject = nil;
    
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    return _sharedObject;
}

- (BOOL)startListeningCameraFeedback{
    if (!self.udpSock) {
        // Receiving socket
        self.udpSock = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        
        NSError *error = nil;
        if (![_udpSock bindToPort:51110 error:&error]) {
            DLog(@"Error binding: %@", error);
            self.udpSock = nil;
            return NO;
        }
        if (![_udpSock beginReceiving:&error]) {
            DLog(@"Error receiving: %@", error);
            self.udpSock = nil;
            return NO;
        }
    }
    return YES;
}

- (void)endListeningCameraFeedback{
    [self.udpSock close];
    self.udpSock.delegate = nil;
    self.udpSock = nil;
}

- (void)addListener:(id<CameraFeedbackManagerDelegate>)listener{
    if (!_listeners) {
        _listeners = [NSMutableArray new];
    }
    
    BOOL listenerExisted = NO;
    for (id ls in _listeners) {
        if (ls == listener) {
            listenerExisted = YES;
            break;
        }
    }
    
    if (!listenerExisted) {
        [_listeners addObject:listener];
    }
}


- (void)removeListener:(id<CameraFeedbackManagerDelegate>)listener{
    [_listeners removeObject:listener];
}

- (void)handleFeedbackData:(NSData *)data{
    if (data.length == sizeof(CemeraFeedbackStruct)) {
        
        //Bind data to struct
        CemeraFeedbackStruct meboyFeedback;
        [data getBytes:&meboyFeedback length:sizeof(CemeraFeedbackStruct)];
        
        self.currentFeedback = meboyFeedback;
        
        for (id<CameraFeedbackManagerDelegate> listener in _listeners) {
            if ([listener respondsToSelector:@selector(cameraFeedbackManagerReceivedData:)]) {
                [listener cameraFeedbackManagerReceivedData:meboyFeedback];
            }
        }
    }
    else {
        //DLog(@"%s UDP socket data error", __FUNCTION__);
    }
}

#pragma mark - GCDAsyncUdpSocketDelegate

/**
 * Called when the socket has received the requested datagram.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext {
    [self handleFeedbackData:data];
}

#pragma mark - Checksum

static uint8_t updateChecksum(uint8_t checksum, uint8_t byte)
{
    if ((uint16_t)checksum + (uint16_t)byte > 0xff)
        ++checksum;
    
    checksum += byte;
    
    return checksum;
}

uint8_t get_checksum(uint8_t *data, int len){
    uint8_t checksum = 0x00;
    if (data)
    {
        for (uint32_t i = 0; i < len; ++i)
        {
            uint8_t byte = data[i];
            checksum = updateChecksum(checksum, byte);
        }
    }
    
    //data[0] = checksum;
    return checksum; 	// data[0] = checksum;
}


@end
