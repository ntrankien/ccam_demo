//
//  CameraFeedbackView.m
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/20/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraFeedbackView.h"
#import "CameraFeedbackCell.h"
#import "CameraFeedback.h"

@interface CameraFeedbackView()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tbvCameraFeedback;

@property (nonatomic, strong) NSMutableArray* feedbacks;
@property (nonatomic, assign) CemeraFeedbackStruct feedbackStruct;

//Optimization: don't reload table view if it is scrolling
@property (nonatomic, assign) BOOL tableViewIsScrolling;

@property (nonatomic, strong) NSTimer* reloadUITimer;

@end

@implementation CameraFeedbackView

- (void)dealloc{
    [_reloadUITimer invalidate];
    self.reloadUITimer = nil;
}

- (CameraFeedbackView*)initWithNibNamed:(NSString*)nibName {
    self = [super init];
    if (self) {
        NSArray* arr = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        self = [arr firstObject];
        
        [self.tbvCameraFeedback registerNib:[UINib nibWithNibName:@"CameraFeedbackCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CameraFeedbackCell"];
        
        self.reloadUITimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(reloadData) userInfo:nil repeats:YES];
    }
    return self;
}

- (void)setFeedback:(CemeraFeedbackStruct)feedbacks{
    self.feedbackStruct = feedbacks;
}

- (void)reloadData{
    if (!_tableViewIsScrolling) {
        self.feedbacks = nil;
        
        //Parse SOC version
        uint32_t socVersion = _feedbackStruct.soc_status.soc_vesion;
        char byte1 = socVersion >> 24;
        char byte2 = (socVersion << 8) >> 24;
        short byte34 = (socVersion << 16) >> 16;
        NSString* socVersionString = [NSString stringWithFormat:@"%d.%d.%d", byte1, byte2, byte34];
        
        self.feedbacks = @[// Internal status /
                           [CameraFeedback feedbackWithTitle:@"Flicker" value:@(_feedbackStruct.internal_stat.sensor_flicker)],
                           [CameraFeedback feedbackWithTitle:@"Flipup" value:@(_feedbackStruct.internal_stat.sensor_flipup)],
                           [CameraFeedback feedbackWithTitle:@"Resolution" value:@(_feedbackStruct.internal_stat.sensor_resolution)],
                           [CameraFeedback feedbackWithTitle:@"Video Framerate" value:@(_feedbackStruct.internal_stat.sensor_videoframerate)],
                           [CameraFeedback feedbackWithTitle:@"Video Bitrate" value:@(_feedbackStruct.internal_stat.sensor_videobitrate)],
                           [CameraFeedback feedbackWithTitle:@"Video GOP" value:@(_feedbackStruct.internal_stat.sensor_videogop)],
                           [CameraFeedback feedbackWithTitle:@"Video QP" value:@(_feedbackStruct.internal_stat.sensor_videoqp)],
                           [CameraFeedback feedbackWithTitle:@"SPK Volume" value:@(_feedbackStruct.internal_stat.spk_volume)],
                           [CameraFeedback feedbackWithTitle:@"WiFi RSSI" value:@(_feedbackStruct.internal_stat.wifi_rssi)],
                           [CameraFeedback feedbackWithTitle:@"Meboy Up Time" value:@(_feedbackStruct.internal_stat.meboy_up_time)],
                           
                           // SOC status /
                           [CameraFeedback feedbackWithTitle:@"SOC Version" value:socVersionString],
                           
                           // wheel
                           //[CameraFeedback feedbackWithTitle:@"SOC Version" value:[NSData dataWithBytes:_feedbackStruct.soc_status.wheel.reserve length:20]],
                           
                           // hand
                           [CameraFeedback feedbackWithTitle:@"Left Speed" value:@(_feedbackStruct.soc_status.hand.left_speed)],
                           [CameraFeedback feedbackWithTitle:@"Right Speed" value:@(_feedbackStruct.soc_status.hand.right_speed)],
                           //[CameraFeedback feedbackWithTitle:@"Reserve" value:@(_feedbackStruct.soc_status.hand.reserve)],
                           [CameraFeedback feedbackWithTitle:@"S Cur Pos" value:@(_feedbackStruct.soc_status.hand.s_cur_pos)],
                           [CameraFeedback feedbackWithTitle:@"S Up Boundary" value:@(_feedbackStruct.soc_status.hand.s_up_boundary)],
                           [CameraFeedback feedbackWithTitle:@"S Down Boundary" value:@(_feedbackStruct.soc_status.hand.s_down_boundary)],
                           [CameraFeedback feedbackWithTitle:@"H Cur Pos" value:@(_feedbackStruct.soc_status.hand.h_cur_pos)],
                           [CameraFeedback feedbackWithTitle:@"H Up Boundary" value:@(_feedbackStruct.soc_status.hand.h_up_boundary)],
                           [CameraFeedback feedbackWithTitle:@"H Down Boundary" value:@(_feedbackStruct.soc_status.hand.h_down_boundary)],
                           [CameraFeedback feedbackWithTitle:@"W Cur Pos" value:@(_feedbackStruct.soc_status.hand.w_cur_pos)],
                           [CameraFeedback feedbackWithTitle:@"W Left Boundary" value:@(_feedbackStruct.soc_status.hand.w_left_boundary)],
                           [CameraFeedback feedbackWithTitle:@"W Right Boundary" value:@(_feedbackStruct.soc_status.hand.w_right_boundary)],
                           [CameraFeedback feedbackWithTitle:@"C Cur Pos" value:@(_feedbackStruct.soc_status.hand.c_cur_pos)],
                           [CameraFeedback feedbackWithTitle:@"C Open Boundary" value:@(_feedbackStruct.soc_status.hand.c_open_boundary)],
                           [CameraFeedback feedbackWithTitle:@"C Close Boundary" value:@(_feedbackStruct.soc_status.hand.c_close_boundary)],
                           
                           [CameraFeedback feedbackWithTitle:@"Error Code" value:@(_feedbackStruct.soc_status.error_code)],
                           [CameraFeedback feedbackWithTitle:@"Battery Value" value:@(_feedbackStruct.soc_status.battery_value)],
                           ].mutableCopy;
        
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [_tbvCameraFeedback reloadData];
    });
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _feedbacks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CameraFeedback* feedback = _feedbacks[indexPath.row];
    CameraFeedbackCell* cell = (CameraFeedbackCell*)[tableView dequeueReusableCellWithIdentifier:@"CameraFeedbackCell"];
    
    [cell displayCameraFeedback:feedback];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return 44;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.tableViewIsScrolling = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    self.tableViewIsScrolling = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.tableViewIsScrolling = YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.tableViewIsScrolling = YES;
}

@end
