//
//  CameraFeedbackView.h
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/20/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <UIKit/UIKit.h>
#import "CameraFeedbackManager.h"

@interface CameraFeedbackView : UIView

- (CameraFeedbackView*)initWithNibNamed:(NSString*)nibName;
- (void)setFeedback:(CemeraFeedbackStruct)feedback;

- (void)reloadData;

@end
