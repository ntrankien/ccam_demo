//
//  CameraFeedbackCell.m
//  LucyDemo
//
//  Created by Tran Kien Nghi on 6/21/16.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import "CameraFeedbackCell.h"
#import "CameraFeedback.h"

@interface CameraFeedbackCell()

@property (nonatomic, strong) CameraFeedback* cameraFeedback;

@end

@implementation CameraFeedbackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)displayCameraFeedback:(CameraFeedback*)cameraFeedback{
    self.cameraFeedback = cameraFeedback;
    _lbTitle.text = _cameraFeedback.title;
    _lbValue.text = _cameraFeedback.stringValue;
}

@end
