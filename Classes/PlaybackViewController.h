//
//  PlaybackViewController
//  Dev
//
//  Created by Dev on 6/9/13.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology
//

#import <UIKit/UIKit.h>
#import <H264MediaPlayer/H264MediaPlayer.h>
#import <CameraScanner/CameraScanner.h>

#import "PlaylistInfo.h"
#import "PlaybackListener.h"
#import "PlayerCallbackHandler.h"

@protocol PlaybackDelegate <NSObject>

@optional

- (void)motionEventDeleted;
- (void)playbackStopped;
- (void)deleteVideoAtIndex:(NSInteger)index;
@end

@interface PlaybackViewController : UIViewController <PlayerCallbackHandler>

@property (nonatomic, weak) id<PlaybackDelegate> playbackVCDelegate;
@property (nonatomic, strong) NSMutableArray *clips;

@property (nonatomic, strong) NSTimer *list_refresher;
@property (nonatomic, strong) PlaylistInfo *clipInfo;
@property (nonatomic, strong) NSMutableArray *clipsInEvent;
@property (nonatomic, strong) NSTimer *timerHideMenu;
@property (nonatomic, copy) NSString *camera_mac;
@property (nonatomic) BOOL userWantToBack;
@property (nonatomic) NSInteger intEventId;
@property (nonatomic) NSInteger recordSelectedIndex;
@property (nonatomic, strong) CamChannel *camChannel;

- (void)stopStream;
- (void)setMultiClipsDuration:(int)duration;

@end
