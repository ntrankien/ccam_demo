//
//  AppDelegate.h
//  App
//
//  Created by Developer on 9/26/11.
//  Copyright (C) 2016 Cinatic Technology
//
//  This unpublished material is proprietary to Cinatic Technology.
//  All rights reserved. The methods and
//  techniques described herein are considered trade secrets
//  and/or confidential. Reproduction or distribution, in whole
//  or in part, is forbidden except by express written permission
//  of Cinatic Technology

#import <UIKit/UIKit.h>

@class MFMailComposeViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate>

@property (nonatomic, strong) IBOutlet UIWindow *window;

+ (void)getBroadcastAddress:(NSString **)bcast AndOwnIp:(NSString**)ownip;
+ (void)getBroadcastAddress:(NSString **)bcast AndOwnIp:(NSString**)ownip ipasLong:(long *)_ownip;
+ (MFMailComposeViewController *)composeEmailWithCamLog:(NSData *)aData name:(NSString *)logName additionalInfo:(NSString*)info callback:(id)sender;
+ (NSDictionary*)encryptDeviceConfiguration:(NSDictionary*)dict;
+ (NSDictionary*)decryptDeviceConfiguration:(NSDictionary*)dict;

@end

